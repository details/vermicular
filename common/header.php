<div class="logo"><a href="http://www.vermicular.jp/"><h1><img src="<?= ST_WWW ?>/img/logo.gif" alt="メイド・イン・ジャパンの鋳物・ホーロー鍋、ココットのブランド、VERMICULAR（バーミキュラ）" /></h1></a></div>
<ul id="gl_nav" class="clearfix">

<li id="home">
<a href="http://www.vermicular.jp/" class="btn"><span>ホーム</span></a>
</li>

<li id="product">
	<a href="/products/" class="btn"><span>製品紹介</span></a>
    <div class="drop">
    <ul>
        <li><a href="/products/">オーブンポットラウンド</a></li>
        <li><a href="/products/ricepot/">ライスポット</a></li>
        <li><a href="/products/kitchenitems/">キッチンアイテム</a></li>
        <li><a href="/products/food/">食べ物</a></li>
        <li class="last"><a href="//shop.vermicular.jp/jp/group.php?id=12">レシピ本</a></li>
    </ul>
    </div>
</li>

<li id="about">
    <a class="btn"><span>バーミキュラについて</span></a>
    
    <div class="drop">
    <ul>
    <li><a href="/about/whatis/">暮らしを変える鍋</a></li>
    <li><a href="/about/teshigoto/">手仕事とテクノロジー</a></li>
    <li><a href="/support/">一生サポート</a></li>
    <li class="last"><a href="/faq/">よくある質問</a></li>
    </ul>
    </div>
    
</li>

<li id="taste">
	<a href="https://owners.vermicular.jp/" class="btn"><span>バーミキュラを楽しむ</span></a>

</li>

<li id="recruit">
    <a class="btn"><span>私たちについて</span></a>
    
    <div class="drop">
    <ul>
    <li><a href="/company/">私たちについて</a></li>
    <li><a href="/recruit/">採用情報</a></li>
    <li class="last"><a href="/pr/">取材に関するお問い合わせ</a></li>
    </ul>
    </div>
   
</li>

<li id="shop">
	<a href="http://shop.vermicular.jp" target="_blank" class="btn"><span>Online Shop</span></a>
</li>

</ul>

<div id="icon_cart"><a href="http://shop.vermicular.jp" target="_blank"><img src="<?= ST_WWW ?>/img/icon_cart.gif" /></a></div>
<div id="icon_for_e"><a href="http://<?= BRAND_DOMAIN_EN ?>/?lang=en"><img src="<?= ST_WWW ?>/img/btn_for_e_site.png" /></a></div>