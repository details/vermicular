﻿<div class="footer_links clearfix">

    <ul class="links">
        <li><a href="/company">会社概要</a></li>
        <li><a href="/privacy">プライバシーポリシー</a></li>
        <li><a href="/sitemap">サイトマップ</a></li>
        <li><a href="/faq">よくある質問</a></li>
        <li><a href="/howto">ご使用方法</a></li>
        <li><a href="/voices">お客様の声</a></li>
        <li><a href="http://shop.vermicular.jp/mail/" target="_blank">メールマガジン</a></li>
        <li><a href="http://shop.vermicular.jp/" target="_blank"><strong>　オンラインショップ　</strong></a></li>
        <li><a href="/shoplist" target="_blank">取り扱い店舗</a></li>
        
    </ul>

    <div id="copyright">© Aichi Dobby. LTD. All Rights Reserved</div>
    
</div><!-- / .footer_links -->

<div id="social">
<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.vermicular.jp&amp;send=false&amp;layout=button_count&amp;width=120&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font=arial&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:125px; height:21px;" allowTransparency="true"></iframe>
</div>