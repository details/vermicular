$(function() {
    if(navigator.userAgent.indexOf("MSIE") != -1) {
        $('.pngfix, .png_bg').each(function() {
                $(this).css({
                    'filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src="' +
                    $(this).attr('src') +
                    '", sizingMethod="scale");'
                });
        });
    }
});