// require jQuery JavaScript Library v1.10.1

/* 即時関数でローカル化 */
$(function(){

/*===================================================================================
	[Plugin]
===================================================================================*/


/* ============================================================
 * Execute JavaScript when the Window Object is fully loaded.
 * ============================================================ */

	$win.on('load', function() {
		if($('a.clbox_youtube')[0]){
			$('a.clbox_youtube').colorbox({
				iframe:true,
				rel:'clbox_youtube',
				innerWidth:640,
				innerHeight:360
			});
		}

		$(".slider").bxSlider({

		});

	});//End

	$win.on('scroll', function(winW,winH) {

	});//End


	$win.on('resize', function(winW,winH) {
		winW = $win.width(),
		winH = $win.height();
	});

});//End