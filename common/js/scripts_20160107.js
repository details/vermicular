// require jQuery JavaScript Library v1.10.1

/* 即時関数でローカル化 */
$(function(){

/*===================================================================================
	[Plugin]
===================================================================================*/


/* --------------------------------------------------------
  [easing ]
 --------------------------------------------------------*/
	/* ------------------------------------------------------------
	 * [ jQuery Easing v1.3 (plugin) ]
	 * ------------------------------------------------------------
	 * jQuery Easing v1.3 - http://gsgd.co.uk/sandbox/jquery/easing/
	 * Uses the built in easing capabilities added In jQuery 1.1
	 * to offer multiple easing options
	 * TERMS OF USE - jQuery Easing
	 * Open source under the BSD License.
	 * Copyright  2008 George McGinley Smith
	 * All rights reserved.
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 * Redistributions of source code must retain the above copyright notice, this list of
	 * conditions and the following disclaimer.
	 * Redistributions in binary form must reproduce the above copyright notice, this list
	 * of conditions and the following disclaimer in the documentation and/or other materials
	 * provided with the distribution.
	 * Neither the name of the author nor the names of contributors may be used to endorse
	 * or promote products derived from this software without specific prior written permission.
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
	 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
	 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
	 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
	 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
	 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
	 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 * ------------------------------------------------------------ */
	// t: current time, b: begInnIng value, c: change In value, d: duration
	jQuery.easing['jswing']=jQuery.easing['swing'];jQuery.extend(jQuery.easing,{def:'easeOutQuad',swing:function(x,t,b,c,d){return jQuery.easing[jQuery.easing.def](x,t,b,c,d)},easeInQuad:function(x,t,b,c,d){return c*(t/=d)*t+b},easeOutQuad:function(x,t,b,c,d){return-c*(t/=d)*(t-2)+b},easeInOutQuad:function(x,t,b,c,d){if ((t/=d/2)<1)return c/2*t*t+b;return-c/2*((--t)*(t-2)-1)+b},easeInCubic:function(x,t,b,c,d){return c*(t/=d)*t*t+b},easeOutCubic:function(x,t,b,c,d){return c*((t=t/d-1)*t*t+1)+b},easeInOutCubic:function(x,t,b,c,d){if ((t/=d/2)<1)return c/2*t*t*t+b;return c/2*((t-=2)*t*t+2)+b},easeInQuart:function(x,t,b,c,d){return c*(t/=d)*t*t*t+b},easeOutQuart:function(x,t,b,c,d){return-c*((t=t/d-1)*t*t*t-1)+b},easeInOutQuart:function(x,t,b,c,d){if ((t/=d/2)<1)return c/2*t*t*t*t+b;return-c/2*((t-=2)*t*t*t-2)+b},easeInQuint:function(x,t,b,c,d){return c*(t/=d)*t*t*t*t+b},easeOutQuint:function(x,t,b,c,d){return c*((t=t/d-1)*t*t*t*t+1)+b},easeInOutQuint:function(x,t,b,c,d){if ((t/=d/2)<1)return c/2*t*t*t*t*t+b;return c/2*((t-=2)*t*t*t*t+2)+b},easeInSine:function(x,t,b,c,d){return-c*Math.cos(t/d*(Math.PI/2))+c+b},easeOutSine:function(x,t,b,c,d){return c*Math.sin(t/d*(Math.PI/2))+b},easeInOutSine:function(x,t,b,c,d){return-c/2*(Math.cos(Math.PI*t/d)-1)+b},easeInExpo:function(x,t,b,c,d){return(t==0)?b:c*Math.pow(2,10*(t/d-1))+b},easeOutExpo:function(x,t,b,c,d){return(t==d)?b+c:c*(-Math.pow(2,-10*t/d)+1)+b},easeInOutExpo:function(x,t,b,c,d){if (t==0)return b;if (t==d)return b+c;if ((t/=d/2)<1)return c/2*Math.pow(2,10*(t-1))+b;return c/2*(-Math.pow(2,-10*--t)+2)+b},easeInCirc:function(x,t,b,c,d){return-c*(Math.sqrt(1-(t/=d)*t)-1)+b},easeOutCirc:function(x,t,b,c,d){return c*Math.sqrt(1-(t=t/d-1)*t)+b},easeInOutCirc:function(x,t,b,c,d){if ((t/=d/2)<1)return-c/2*(Math.sqrt(1-t*t)-1)+b;return c/2*(Math.sqrt(1-(t-=2)*t)+1)+b},easeInElastic:function(x,t,b,c,d){var s=1.70158;var p=0;var a=c;if (t==0)return b;if ((t/=d)==1)return b+c;if (!p)p=d*.3;if (a<Math.abs(c)){a=c;var s=p/4}else var s=p/(2*Math.PI)*Math.asin(c/a);return-(a*Math.pow(2,10*(t-=1))*Math.sin((t*d-s)*(2*Math.PI)/p))+b},easeOutElastic:function(x,t,b,c,d){var s=1.70158;var p=0;var a=c;if (t==0)return b;if ((t/=d)==1)return b+c;if (!p)p=d*.3;if (a<Math.abs(c)){a=c;var s=p/4}else var s=p/(2*Math.PI)*Math.asin(c/a);return a*Math.pow(2,-10*t)*Math.sin((t*d-s)*(2*Math.PI)/p)+c+b},easeInOutElastic:function(x,t,b,c,d){var s=1.70158;var p=0;var a=c;if (t==0)return b;if ((t/=d/2)==2)return b+c;if (!p)p=d*(.3*1.5);if (a<Math.abs(c)){a=c;var s=p/4}else var s=p/(2*Math.PI)*Math.asin(c/a);if (t<1)return-.5*(a*Math.pow(2,10*(t-=1))*Math.sin((t*d-s)*(2*Math.PI)/p))+b;return a*Math.pow(2,-10*(t-=1))*Math.sin((t*d-s)*(2*Math.PI)/p)*.5+c+b},easeInBack:function(x,t,b,c,d,s){if (s==undefined)s=1.70158;return c*(t/=d)*t*((s+1)*t-s)+b},easeOutBack:function(x,t,b,c,d,s){if (s==undefined)s=1.70158;return c*((t=t/d-1)*t*((s+1)*t+s)+1)+b},easeInOutBack:function(x,t,b,c,d,s){if (s==undefined)s=1.70158;if ((t/=d/2)<1)return c/2*(t*t*(((s*=(1.525))+1)*t-s))+b;return c/2*((t-=2)*t*(((s*=(1.525))+1)*t+s)+2)+b},easeInBounce:function(x,t,b,c,d){return c-jQuery.easing.easeOutBounce(x,d-t,0,c,d)+b},easeOutBounce:function(x,t,b,c,d){if ((t/=d)<(1/2.75)){return c*(7.5625*t*t)+b}else if (t<(2/2.75)){return c*(7.5625*(t-=(1.5/2.75))*t+.75)+b}else if (t<(2.5/2.75)){return c*(7.5625*(t-=(2.25/2.75))*t+.9375)+b}else{return c*(7.5625*(t-=(2.625/2.75))*t+.984375)+b}},easeInOutBounce:function(x,t,b,c,d){if (t<d/2)return jQuery.easing.easeInBounce(x,t*2,0,c,d)*.5+b;return jQuery.easing.easeOutBounce(x,t*2-d,0,c,d)*.5+c*.5+b}});

/* --------------------------------------------------------
  [Auto Height ]
 --------------------------------------------------------*/

/*
 * jquery-auto-height.js
 *
 * Copyright (c) 2010 Tomohiro Okuwaki (http://www.tinybeans.net/blog/)
 * Licensed under MIT Lisence:
 * http://www.opensource.org/licenses/mit-license.php
 * http://sourceforge.jp/projects/opensource/wiki/licenses%2FMIT_license
 *
 * Since:   2010-04-19
 * Update:  2013-08-16
 * version: 0.04
 * Comment:
 *
 * jQuery 1.2 <-> 1.10.2
 *
 */

 (function($){
    $.fn.autoHeight = function(options){
        var op = $.extend({

            column  : 0,
            clear   : 0,
            height  : 'height',
            reset   : '',
            descend : function descend (a,b){ return b-a; }

        },options || {}); // optionsに値があれば上書きする

        var self = $(this);
        var n = 0,
            hMax,
            hList = new Array(),
            hListLine = new Array();
            hListLine[n] = 0;

        // 要素の高さを取得
        self.each(function(i){
            if (op.reset == 'reset') {
                $(this).removeAttr('style');
            }
            var h = $(this).height();
            hList[i] = h;
            if (op.column > 1) {
                // op.columnごとの最大値を格納していく
                if (h > hListLine[n]) {
                    hListLine[n] = h;
                }
                if ( (i > 0) && (((i+1) % op.column) == 0) ) {
                    n++;
                    hListLine[n] = 0;
                };
            }
        });

        // 取得した高さの数値を降順に並べ替え
        hList = hList.sort(op.descend);
        hMax = hList[0];

        // 高さの最大値を要素に適用
        var ie6 = typeof window.addEventListener == "undefined" && typeof document.documentElement.style.maxHeight == "undefined";
        if (op.column > 1) {
            for (var j=0; j<hListLine.length; j++) {
                for (var k=0; k<op.column; k++) {
                    if (ie6) {
                        self.eq(j*op.column+k).height(hListLine[j]);
                    } else {
                        self.eq(j*op.column+k).css(op.height,hListLine[j]);
                    }
                    if (k == 0 && op.clear != 0) {
                        self.eq(j*op.column+k).css('clear','both');
                    }
                }
            }
        } else {
            if (ie6) {
                self.height(hMax);
            } else {
                self.css(op.height,hMax);
            }
        }
    };
})(jQuery);



/*===================================================================================
	[Image Button Hover]
===================================================================================*/

function hoverMotion(){
	var easing = "easeInOutQuart",
		speed = 300;
	$(".hover,#footer .list>dl>dt a").not(".hover_no").hover(
		function(){
			$(this).stop().animate({opacity:0.5},speed,easing);
		},
		function(){
			$(this).stop().animate({opacity:1},speed,easing);
		}
	);
}

	/* ------------------------------------------------------------
	 * [ smoothScroll ]
	 * ------------------------------------------------------------ */
	function smoothScroll(btn, target) {
		var animationTime = 500, // アニメーション時間ミリ秒
		    easing        = 'easeOutExpo';
		// $('a[href^=#]').on('click', function() {
		$(btn).on('click', function() {
			if ( !$(this).hasClass('no_scroll') ) {
				var hash = this.hash;
				if(target) {
					var target = $(target);
				} else {
					var target = $(hash);
				}
				var position = target.offset().top, // 行き先までの画面上の高さの数値
						pageTopID = '#header';
				$('html, body').animate({scrollTop:position}, animationTime, easing, function() {
					if (hash != pageTopID) location.hash = hash;
					scInc = $win.scrollTop();
				});
			}
			return false;
		});
//		$(window).mousewheel(function() { $('html, body').stop(); });
		$(document).on('touchmove', function() { $('html, body').stop(); });
	}

	/* ------------------------------------------------------------
	 * [ Page Top ]
	 * ------------------------------------------------------------ */

	function pageTop(scrollInc, winW){
		var $target = $("#page_top_btn"),
			footerPosition = $("#footer").offset().top,
			speed = 150,
			easing = "easeOutQuad",
			winW = $win.width();

		if(scrollInc>700 && winW>1150){
			$target.stop().animate({bottom:30,opacity:"show"},speed,easing);
		}else{
			$target.stop().animate({bottom:0,opacity:"hide"},speed,easing);
		}
	}

	/* ------------------------------------------------------------
	 * [ SmartPhone Tablet Tel  ]
	 * ------------------------------------------------------------ */

	function telCallLink(targetClassImg, targetClassText){
		if(smpUa.iPhone || smpUa.iPad || smpUa.iPod || smpUa.android || smpUa.windows){
			/*  Image Tel */
			$(targetClassImg).each(function(i){
				var maxLoop = $(targetClassImg).length,
					getThisNumber = $(this).find("img").attr("alt"),
					getThisSrc = $(this).find("img").attr("src");

				if(maxLoop > i){
					$(this).html('<a href="tel:'+ getThisNumber +'"><img src="'+ getThisSrc +'" alt="'+ getThisNumber +'" /></a>');
				}else{
					return false;
				}
			});

			/*  Text Tel */
			$(targetClassText).each(function(i){
				var maxLoop = $(targetClassText).length,
					getThisNumber = $(this).text();
				if(maxLoop > i){
					$(this).html('<a href="tel:'+ getThisNumber +'">'+ getThisNumber +'</a>');
				}else{
					return false;
				}
			});

		}// if END

	}

	/* ------------------------------------------------------------
	 * [ allLink ]
	 * ------------------------------------------------------------ */
	function allLink(target) {
		var $linkArea = $(target);
		$linkArea.css('cursor','pointer');
		$linkArea.click(function(){
			var $href = $(this).find('a').attr('href');
			if ($(this).find('a').attr('target') == '_blank') {
				window.open($href, '_blank');
			} else {
				window.location = $href;
			}
			return false;
		});
	};

	/* ------------------------------------------------------------
	 * [ global navi  ]
	 * ------------------------------------------------------------ */

	var $globalNaviWrap = $("#gnavi,#rpr_gnavi"),
		$headerInner = $("#header_inner");


	/* [ Drop Down ]
	---------------------------------------------------------------*/
	function globalNaviDropDown(){
		var $target = $globalNaviWrap.find(".list"),
			activeIndex = 0;
			prevIndex = -1;
			timer = [];

		$target.hover(
			function(){

				if(!$(this).find(".dd_list")[0]) return false;


				var index = $(this).index();

				if(prevIndex==-1) prevIndex = index;
				clearTimeout(timer);

				if(prevIndex != index ){ // [ 表示を切り替え ]
					$target.find(".dd_list.active").stop().fadeOut(100,function(){
						$(this).removeClass("active").css({"height":""}).removeAttr("style").prev().removeClass("active");
						prevIndex = index;

						if($globalNaviWrap.find(".dd_list.active").length!=1){//[ 2個以上あった場合自分以外消す ]
							$globalNaviWrap.find(".dd_list").not(":nth-child("+index+")").stop().fadeOut(100,function(){
								$(this).removeClass("active").removeAttr("style").prev().removeClass("active");
							});
						}


					});



				}else{ // [ 表示を維持 ]
					prevIndex = index;
					clearTimeout(timer);
				}

				$(this).removeClass("active").removeAttr("style").prev().removeClass("active");
				$target.eq(index).children().addClass("active").next(".dd_list").addClass("active").css({"height":"auto"});

			},
			function(){
				if(!$(this).find(".dd_list")[0]) return false;

				var index = $(this).index();
				clearTimeout(timer);
				timer = setTimeout(function(){
					$target.eq(index).find(".dd_list").stop().fadeOut(100,function(){
						$(this).removeClass("active").removeAttr("style").prev().removeClass("active");
						$target.eq(index).find(".dd_list").css({"height":""});
					});
				},350);
			}
		);
	}

	function globalNaviDropDownSMP(){
		var $target = $globalNaviWrap.find(".list").children(".trigger");
		$target.on("click",function(){
			if($("body").hasClass("d_sp")){
				if($(this).hasClass("active")){
					$(this).removeClass("active");
					$(this).next().animate({"height":"hide"},200,"easeInOutQuad");
				}else{
					$(this).addClass("active");
					$(this).next().animate({"height":"show"},200,"easeInOutQuad");
				}
				return false;
			}
		});
	}

	/* ------------------------------------------------------------
	 * [ gnavi fixed ]
	 * ------------------------------------------------------------ */

	 var fixedFlg = false;

	function globalfixed(){

		var winSdr = $win.scrollTop(),
			minSdr = 500; //[ 最低限スクロールさせる量 ]

		if(!$("#gnavi_wrap,#rpr_header")[0] ) return false;

		var $globalNaviWrap = $("#gnavi_wrap,#rpr_header"),
			globalNaviWrapPosi = $globalNaviWrap.offset().top,
			$globalNavi = $("#gnavi,#rpr_header_inner");

		if(winSdr < minSdr) globalNaviWrapPosi = minSdr;

		if(winSdr > globalNaviWrapPosi && fixedFlg == false){
			fixedFlg = true;
			$globalNavi.css({"top":-73});

			$globalNavi.addClass("fixed").delay(200).animate({"top":0},600,"easeOutQuart");
		}
		if(winSdr < globalNaviWrapPosi && fixedFlg == true){
			fixedFlg = false;
			$globalNavi.removeClass("fixed");
		}
	}

	/* ------------------------------------------------------------
	 * [ youtubeのiframeを挿入して再生  ]
	 * ------------------------------------------------------------ */

	function youtubeIframeAdd (){
		$(".youtube_trigger").on("click",function(){
			var targetID = $(this).data("youtube"),
				targetW = $(this).data("width"),
				targetH = $(this).data("height");

			$(this).fadeOut(200);

			$(this).next(".youtube").append('<iframe width="'+targetW+'" height="'+ targetH +'" src="https://www.youtube.com/embed/'+ targetID +'?byline=0&amp;portrait=0&amp;color=ffffff&amp;api=1&amp;autoplay=1&amp;rel=0&amp;wmode=transparent" frameborder="0" allowfullscreen></iframe>');

		});
	}


	/* ------------------------------------------------------------
	 * [ footer  ]
	 * ------------------------------------------------------------ */

	function footerNaviDropDownSMP(){
		var $target = $("#footer").find(".trigger");
		$target.on("click",function(){
			if($("body").hasClass("d_sp")){
				if($(this).hasClass("active")){
					$(this).removeClass("active");
					$(this).parent("dl").find("dd").animate({"height":"hide"},200,"easeInOutQuad");
				}else{
					$(this).addClass("active");
					$(this).parent("dl").find("dd").animate({"height":"show"},200,"easeInOutQuad");
				}
				return false;
			}
		});
	}

	/* ------------------------------------------------------------
	 * [ pageTop ]
	 * ------------------------------------------------------------ */
	var $pageTopBtn = $("#page_top,#online_btn");

	function pageTopfixd(){

		var winScroll = $(window).scrollTop(),
			speed = 150,
			easing = "easeOutQuad";

			if(("#rpr_header")[0]){
				wrapPosition = $("#kv_wrap").height();
			}else{
				wrapPosition = $("#main_wrap").offset().top-250;
			}

		if(winScroll>wrapPosition || divece=="other" ){
			$pageTopBtn.addClass("show");
		}else{
			$pageTopBtn.removeClass("show");
		}
	}

/* ============================================================
 * Device
 * ============================================================ */


	var pcLoadFunction = function(winW,winH){
		$("body").addClass("d_pc");
		$("body").removeClass("d_sp");
		globalNaviDropDown();
		globalfixed();
	}

	var smpLoadFunction = function(winW,winH){
		$("body").removeClass("d_pc");
		$("body").addClass("d_sp");
	}

	var orientationFunction = function(){
		/* [ 向き判定 ] */
		var orientation = window.orientation;
		if(orientation == 0){
			$("body").addClass("portrait");
		}else{
			$("body").removeClass("portrait");
		}
	}


/* ============================================================
 * Execute JavaScript when the Window Object is fully loaded.
 * ============================================================ */

	$win.on('load', function() {
		$("#document").animate({"opacity":1},300,"easeOutSine");
		telCallLink(".tel_img", ".tel_text");
		hoverMotion();
		smoothScroll('#page_top[href^=#]');
		//pageTop();
		allLink(".box_link");
		youtubeIframeAdd();

		pcLoadFunction(winW,winH);

		if(divece!="pc") orientationFunction();

	});//End

	$win.on('scroll', function(winW,winH) {
		globalfixed();
		pageTopfixd();
	});//End


	$win.on('resize', function(winW,winH) {
		winW = $win.width(),
		winH = $win.height();
		pageTopfixd();
	});

	if(divece!="pc"){
		$win.on('orientationchange', function(){
			orientationFunction();
		});
	}


});//End