<div class="footer_links clearfix">

    <ul class="links">
        <li><a href="/company">Company Profile</a></li>
        <li><a href="/privacy">Privacy Policy</a></li>
        <li><a href="/sitemap">Site Map</a></li>
        <li><a href="/faq">FAQ</a></li>
        <li><a href="/howto">How To Use</a></li>
        <li><a href="/voices">Voices</a></li>
        <li><a href="http://shop.vermicular.jp/" target="_blank"><strong>Online Shop</strong></a></li>
    </ul>

    <div id="copyright">© Aichi Dobby. LTD. All Rights Reserved</div>
    
</div><!-- / .footer_links -->


<div id="fb-root"></div>
