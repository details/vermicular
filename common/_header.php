<div class="logo"><a href="http://www.vermicular.jp/"><h1><img src="<?= ST_WWW ?>/img/logo.gif" alt="メイド・イン・ジャパンの鋳物・ホーロー鍋、ココットのブランド、VERMICULAR（バーミキュラ）" /></h1></a></div>
<ul id="gl_nav" class="clearfix">

<li id="home">
<a href="http://www.vermicular.jp/" class="btn"><span>ホーム</span></a>
</li>

<li id="product">
	<a href="/products/" class="btn"><span>製品紹介</span></a>
    <div class="drop">
    <ul>
        <li><a href="/products/22/">Oven Pot Round 22cm</a></li>
        <li><a href="/products/18/">Oven Pot Round 18cm</a></li>
        <li><a href="http://shop.vermicular.jp/jp/free.php?id=13" target="_blank">リペアサービス</a></li>
        <li><a href="/products/name/">ネーミングサービス</a></li>
        <li><a href="/products/food/">Vermicular Curry Roux Set</a></li>
        <li class="last"><a href="/products/kitchenitems/">Organic Linen シリーズ</a></li>
    </ul>
    </div>
</li>

<li id="about">
    <a href="/about" class="btn"><span>バーミキュラについて</span></a>
    
    <div class="drop">
    <ul>
    <li><a href="/about/whatis/">バーミキュラの特長</a></li>
    <li><a href="/about/technology/">バーミキュラのテクノロジー</a></li>
    <li><a href="/about/madeinjapan/">メイド・イン・ジャパン</a></li>
    <li class="last"><a href="/about/us/">私たちについて</a></li>
    </ul>
    </div>
    
</li>

<li id="taste">
	<a href="https://owners.vermicular.jp/" class="btn"><span>バーミキュラを楽しむ</span></a>
    <div class="drop">
    <ul>
    <li><a href="/taste/recipes">シェフ直伝レシピ</a></li>
    <li><a href="/blog">コンシェルジュレシピ</a></li>
    <li><a href="/taste/howto/">バーミキュラの使い方</a></li>
    <li><a href="/taste/restaurant/">バーミキュラのレストラン</a></li>
	<li class="last"><a href="/taste/dietaryeducation/">バーミキュラの食育</a></li>
    </ul>
    </div>
</li>

<li id="recruit">
    <a href="/recruit/" class="btn"><span>リクルート</span></a>
</li>

<li id="shop">
	<a href="http://shop.vermicular.jp" target="_blank" class="btn"><span>Online Shop</span></a>
</li>

</ul>

<div id="icon_cart"><a href="http://shop.vermicular.jp" target="_blank"><img src="<?= ST_WWW ?>/img/icon_cart.gif" /></a></div>