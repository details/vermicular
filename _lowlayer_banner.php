		<section class="banner_section bg_gray">
			<ul class="banner_list clearfix inner">
				<li>
					<dl>
						<dt><a href="<?php echo $page_pass; ?>about/whatis/"><img src="<?php echo $page_pass; ?>img/products/common/banner_img01.jpg" height="130" width="386" alt="暮らしを変える鍋"></a></dt>
						<dd>あなたの暮らしや考え方を知らぬ間に変えていく鍋。</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt><a href="<?php echo $page_pass; ?>about/teshigoto/"><img src="<?php echo $page_pass; ?>img/products/common/banner_img02.jpg" height="130" width="386" alt="手仕事とテクノロジー"></a></dt>
						<dd>繊細な手仕事と、テクノロジーの融合。</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt><a href=""><img src="<?php echo $page_pass; ?>img/products/common/banner_img03.jpg" height="130" width="386" alt="一生サポート"></a></dt>
						<dd>バーミキュラのサポート・リペアサービス。</dd>
					</dl>
				</li>
			</ul>
		</section>