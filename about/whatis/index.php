<?php
$page_id = 'rpr_about_whatis'; //ページ独自にスタイルを指定する場合は入力
$page_type = 'oven_pot_round'; // [ オーブンポッドラウンドのページの場合 「oven_pot_round」 ]
$page_title = '暮らしを変える鍋 | Vermicular（バーミキュラ）OvenPotRound'; //ページ名
$page_description = '「息子がブロッコリーを食べた」「調味料を使わなくなった」「外食が少なくなりました」「誰かといっしょに食べたくなる」。そう、バーミキュラは、その機能性の高さで、あなたの暮らしを変えていく鍋なのです。';
$page_keywords = 'バーミキュラ,Vermicular,ホーロー鍋,愛知ドビー,暮らし';
$page_pass = '../../'; //自分の階層を指定
$page_directry = 'about/whatis/'; //自分のディレクトリ名を指定
$extra_css = '
<link rel="stylesheet" type="text/css" href="'.$page_pass.'css/products.css">
'; //何か追加で読み込みたいCSSがあればlinkタグごと記述
$extra_js = '
<script src="'.$page_pass.'js/products.js"></script>
<script src="'.$page_pass.'common/js/lib.js"></script>
'; //何か追加で読み込みたいJSがあればscriptタグごと記述
include $page_pass.'_header.php'; ?>
		<header id="rpr_page_title_wrap">
			<div class="inner">
				<h1 id="page_title"><img src="../../img/about/whatis/page_title.png" height="38" width="290" alt="暮らしをかえる鍋。"></h1>
				<p class="text">「息子がブロッコリーを食べた」「調味料を使わなくなった」「外食しなくなりました」「誰かといっしょに食べたくなる」<br>
					そう。バーミキュラは、その機能性の高さで、あなたの暮らしを変えていく鍋なのです。</p>
			</div>
		</header><!-- /#kv_wrap -->

		<section id="section01" class="rpr_section_wrap inner_min clearfix">
			<div class="cont_wrap fll">
				<h1 class="rpr_section_title"><img src="../../img/about/whatis/section_title01.png" height="72" width="442" alt="目指したのは、世界一、素材本来の味を引き出す鍋。"></h1>
				<p class="text">バーミキュラ最大の特徴は、「無水調理」を可能にする、非常に高い気密性にあります。野菜の水分やうま味を逃がさないから、調味料をほとんど使わずに、それはそれはおいしくいただけます。あなたは、フタをして火にかけるだけ。じつに簡単です。</p>
				<div class="video_wrap clearfix">
					<p class="thum"><a href="https://www.youtube.com/embed/c-cfVJ1SWoc?autoplay=1&amp;rel=0&amp;wmode=transparent" class="clbox_youtube hover"><img src="../../img/about/whatis/video_thum01.jpg" height="120" width="200" alt="Easy Vermicularの動画サムネイル"></a></p>
					<dl class="text_wrap">
						<dt><img src="../../img/about/whatis/video_title01.png" height="22" width="129" alt="Easy Vermicular"></dt>
						<dd>バーミキュラで簡単料理</dd>
					</dl>
				</div>
			</div>
			<p class="photo_wrap flr"><img src="../../img/about/whatis/section_img01.jpg" height="390" width="580" alt="目指したのは、世界一、素材本来の味を引き出す鍋。イメージ写真"></p>
		</section>

		<section id="section02" class="rpr_section_wrap inner_min clearfix">
			<div class="cont_wrap flr">
				<h1 class="rpr_section_title"><img src="../../img/about/whatis/section_title02.png" height="29" width="443" alt="料理の楽しさを、この鍋が教えてくれた。"></h1>
				<p class="text">バーミキュラひとつで、無水調理、炊く、ロースト、スチームが可能です。<br>無水調理でつくるカレーから、分厚いお肉の絶妙ロースト、お米のうま味が際立つご飯、さらにスイーツやアイスクリームまで。とても簡単にできるのに、ほめられるから、また作りたくなります。</p>
				<div class="video_wrap clearfix">
					<p class="thum"><a href="https://www.youtube.com/embed/GhvUNgcEKLk?autoplay=1&amp;rel=0&amp;wmode=transparent" class="clbox_youtube hover"><img src="../../img/about/whatis/video_thum02.jpg" height="120" width="200" alt="Chef Interviewの動画サムネイル"></a></p>
					<dl class="text_wrap">
						<dt><img src="../../img/about/whatis/video_title02.png" height="22" width="115" alt="Chef Interview"></dt>
						<dd>六本木農園 グランシェフ<br>比嘉康洋</dd>
					</dl>
				</div>
			</div>
			<p class="photo_wrap fll"><img src="../../img/about/whatis/section_img02.jpg" height="390" width="580" alt="料理の楽しさを、この鍋が教えてくれた。イメージ写真"></p>
		</section>

		<section id="section03" class="rpr_section_wrap inner_min clearfix">
			<div class="cont_wrap fll">
				<h1 class="rpr_section_title"><img src="../../img/about/whatis/section_title03.png" height="29" width="444" alt="実証。うま味も、栄養も、逃がさない鍋。"></h1>
				<p class="text">野菜の栄養素は、調理を通して失われていきます。バーミキュラの無水調理は、他の調理方法と比べて栄養素を損ないにくいことがわかっています。</p>
				<div class="video_wrap clearfix">
					<p class="thum"><a href="https://www.youtube.com/embed/IT-hmhRUjYg?autoplay=1&amp;rel=0&amp;wmode=transparent" class="clbox_youtube hover"><img src="../../img/about/whatis/video_thum03.jpg" height="120" width="200" alt="Cooking expert Interviewの動画サムネイル"></a></p>
					<dl class="text_wrap">
						<dt><img src="../../img/about/whatis/video_title03.png" height="22" width="196" alt="Cooking expert Interview"></dt>
						<dd>料理研究家<br>黒田民子</dd>
					</dl>
				</div>
			</div>
			<div class="photo_wrap flr">
				<div class="slider_wrap">
					<ul class="slider">
						<li>
							<h2 class="title"><img src="../../img/about/whatis/slide_graph01.png" height="217" width="482" alt="じゃがいもの「ビタミンC」の変化 ［日本食品分析センター調べ］"></h2>
							<p class="text">バーミキュラの無水調理だと、ビタミンCの損失がその他の加熱調理の損失よりも1/2程度少なく、高い数値が期待されます。ビタミンCは水に溶けだしやすく熱に弱いため、加熱調理では摂取が難しくなります。ただし、じゃがいものビタミンCは比較的熱に強いと言われています。</p>
						</li>
						<li>
							<h2 class="title"><img src="../../img/about/whatis/slide_graph02.png" height="217" width="482" alt="にんじんの「βカロテン」の変化 ［日本食品分析センター調べ］"></h2>
							<p class="text">強い抗酸化作用を持ち、老化防止に効果があるといわれるβカロテンが、バーミキュラの無水調理だと、ほとんど失われないという結果となりました。※βカロテンは体内に吸収後、ビタミンＡに変化します。</p>
						</li>
						<li>
							<h2 class="title"><img src="../../img/about/whatis/slide_graph03.png" height="217" width="482" alt="ブロッコリーの「糖度」の変化 ［日本食品分析センター調べ］"></h2>
							<p class="text">生の状態が一番甘みが多いとの結果。でも、ブロッコリーを生で食べることは少ないですよね。バーミキュラの無水調理だと、他の調理方法に比べて糖度が高いまま調理がされます。また、同じ品種の青果物であっても、産地、収穫時期、土壌、天候、栽培方法などによって、糖度は異なります。</p>
						</li>
					</ul>
				</div>
			</div>
		</section>

		<div class="rpr_back_to_top">
			<div class="center btn w218 clear_black"><a href="<?php echo $page_pass; ?>products/""><span class="btn_inner"><img src="../../img/products/common/back_top_top_btn_text.png" height="10" width="126" alt="Oven Pot Round TOP"></span></a></div>
		</div>

		<?php include $page_pass.'_lowlayer_banner.php'; ?>

<?php include $page_pass.'_footer.php'; ?>