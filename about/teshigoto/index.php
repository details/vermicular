<?php
$page_id = 'rpr_about_teshigoto'; //ページ独自にスタイルを指定する場合は入力
$page_type = 'oven_pot_round'; // [ オーブンポッドラウンドのページの場合 「oven_pot_round」 ]
$page_title = '手仕事とテクノロジー | Vermicular（バーミキュラ）OvenPotRound'; //ページ名
$page_description = 'フタと本体の接合部分を日本の職人が手作業で0.01mmの精度で削り出し、考え抜かれた熱伝達のテクノロジーを重ねることで、究極の無水調理が実現しました。すべては、素材のうま味を引き出すためです。';
$page_keywords = 'バーミキュラ,Vermicular,ホーロー鍋,愛知ドビー,手仕事';
$page_pass = '../../'; //自分の階層を指定
$page_directry = 'about/teshigoto/'; //自分のディレクトリ名を指定
$extra_css = '
<link rel="stylesheet" type="text/css" href="'.$page_pass.'css/products.css">
'; //何か追加で読み込みたいCSSがあればlinkタグごと記述
$extra_js = '
<script src="'.$page_pass.'js/products.js"></script>
<script src="'.$page_pass.'common/js/lib.js"></script>
'; //何か追加で読み込みたいJSがあればscriptタグごと記述
include $page_pass.'_header.php'; ?>
		<header id="rpr_page_title_wrap">
			<div class="inner">
				<h1 id="page_title"><img src="../../img/about/teshigoto/page_title.png" height="38" width="368" alt="手仕事とテクノロジー。"></h1>
				<p class="text">フタと本体の接合部分を日本の職人が手作業で0.01mmの精度で削り出し、<br>
					考え抜かれた熱伝達のテクノロジーを重ねることで、究極の無水調理が実現しました。</p>
			</div>
		</header><!-- /#kv_wrap -->

		<section id="technology_detail_wrap" class="inner_min">

			<h1 class="name"><img src="../../img/about/teshigoto/main_video_title.png" height="50" width="537" alt="TRIPLE THERMO&copy; TECHNOLOGY トリプルサーモテクノロジー"></h1>

			<div class="main_video youtube_wrap">
				<p class="youtube_trigger" data-youtube="u6-CdjwwMwo" data-width="854" data-height="480"><img src="../../img/about/teshigoto/main_video_thum.jpg" height="480" width="854" alt="TRIPLE THERMO&copy; TECHNOLOGY トリプルサーモテクノロジーの動画"></p>
				<div class="youtube"></div>
			</div>


			<h2 class="copy"><img src="../../img/about/teshigoto/main_video_copy.png" height="25" width="381" alt="バーミキュラの熱伝達は、革新的です。"></h2>
			<ul class="point_list clearfix">
				<li>
					<h3 class="title"><img src="../../img/about/teshigoto/main_video_point01.png" height="188" width="340" alt="リブ底"></h3>
					<p class="text">鍋底をリブ状にすることで食材の接地面積を最小限にし、過剰な熱の伝達を抑えます。</p>
				</li>
				<li>
					<h3 class="title"><img src="../../img/about/teshigoto/main_video_point02.png" height="188" width="340" alt="3層ホーローコーティング"></h3>
					<p class="text">３層にコーティングされたホーローが強い遠赤外線を発生し、食材の組織を破壊することなく食材を内側から加熱します。</p>
				</li>
				<li>
					<h3 class="title"><img src="../../img/about/teshigoto/main_video_point03.png" height="188" width="340" alt="テーパーエアタイト&copy;構造"></h3>
					<p class="text">高い密閉性で、蒸気をしっかり閉じ込めます。鍋の中で対流が起き、食材に外側からも熱を入れるので、おいしく仕上がるのです。</p>
				</li>
			</ul>
			<p class="btn center clear_black w218"><a href="http://www.vermicular.jp/about/technology/"><span class="btn_inner"><img src="../../img/about/teshigoto/main_video_btn_text.png" height="13" width="153" alt="テクノロジーの詳細はこちら"></span></a></p>
		</section><!-- /#technology_detail_wrap -->

		<section id="made_in_jp" class="rpr_section_wrap inner_min pb0">
			<div class="clearfix">
				<div class="cont_wrap w_s fll">
					<h1 class="rpr_section_title"><img src="../../img/about/teshigoto/section_title01.png" height="71" width="324" alt="鍋に、0.01mmの精度。それが、メイド・イン・ジャパン。"></h1>
					<p class="text">厚さ3mmの鉄鋳物を0.01mm単位の精度で削り込むことも、800℃で3度のホーロー焼成をしても精度が下がらないようにすることも、何千回という試行錯誤がなければ実現できませんでした。「世界一、素材本来の味を引き出す鍋」を日本の職人の手で作りたいという強い思いが、誰にもまねできないホーロー鍋を生んだのです。</p>
					<p class="btn w218 clear_black"><a href="http://www.vermicular.jp/about/madeinjapan/"><span class="btn_inner"><img src="../../img/products/common/detail_btn_text.png" height="13" width="71" alt="詳細はこちら"></span></a></p>
				</div>

				<div class="photo_wrap w_l flr youtube_wrap">
					<p class="youtube_trigger" data-youtube="TTqnrWaiumg" data-width="640" data-height="360"><img src="../../img/about/teshigoto/section01_video_thum.jpg" height="360" width="640" alt="鍋に、0.01mmの精度。それが、メイド・イン・ジャパン。イメージ写真"></p>
					<div class="youtube"></div>
				</div>

			</div>

			<div class="banner_wrap">
				<p><a href="http://www.vermicular.jp/about/us/story/"><img src="../../img/about/teshigoto/develop_banner.jpg" height="140" width="540" alt="バーミキュラ開発ストーリー"></a></p>
			</div>

		</section>


		<section id="biutiful_wrap">
			<div class="inner_min">
				<div class="text_wrap">
					<h1><img src="../../img/about/teshigoto/section_title02.png" height="28" width="218" alt="使いやすく、美しく。"></h1>
					<p class="text">
						例えば、フタと本体に取り付けられたダブルハンドル。<br>
						日本の女性でも持ちやすいから、重さを感じにくい。<br>
						職人の手で仕上げるから、曲線が滑らかで美しい。
					</p>
				</div>
			</div>
		</section>


		<section id="section03" class="rpr_section_wrap inner_min clearfix">
			<p class="photo_wrap w_l fll"><img src="../../img/about/teshigoto/section03_img.jpg" height="390" width="580" alt="イメージ写真"></p>
			<div class="cont_wrap w_s fll">
				<h1 class="rpr_section_title"><img src="../../img/about/teshigoto/section_title03.png" height="70" width="410" alt="やさしい色には、理由がありました。カドミウムゼロという事実。"></h1>
				<p class="text">カドミウムを使えば、鮮やかな発色が手に入る。しかし毎日からだに入る料理をつくる鍋に、有毒かもしれないカドミウムは使用したくないと、バーミキュラは考えました。淡くやさしいカラーラインナップが多いのは、そんな理由があるからです。</p>
			</div>

		</section>



		<div class="rpr_back_to_top">
			<div class="center btn w218 clear_black"><a href="<?php echo $page_pass; ?>products/"><span class="btn_inner"><img src="../../img/products/common/back_top_top_btn_text.png" height="10" width="126" alt="Oven Pot Round TOP"></span></a></div>
		</div>

		<?php include $page_pass.'_lowlayer_banner.php'; ?>

<?php include $page_pass.'_footer.php'; ?>