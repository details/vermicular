	</main><!-- /#main_wrap -->

	<footer id="footer">
		<div class="inner_min clearfix">
			<p class="title"><img src="<?php echo $page_pass; ?>common/img/tit_footer.png" width="76" height="18" alt="Sitemap"></p>
			<div class="footer_sitemap fll">
				<div class="list">
					<dl>
						<dt>製品ラインナップ</dt>
						<dd>
							<ul>
								<li>
									<dl>
										<dt>鋳物ホーロー鍋</dt>
										<dd>
											<ul>
												<li><a href="<?php echo $page_pass; ?>products/">オーブンポットラウンド</a></li>
											</ul>
										</dd>
									</dl>
								</li>
								<li>
									<dl>
										<dt>キッチン家電</dt>
										<dd>
											<ul>
												<li><a href="<?php echo $page_pass; ?>products/ricepot/">バーミキュラ ライスポット</a></li>
											</ul>
										</dd>
									</dl>
								</li>
								<li>
									<dl>
										<dt>キッチンアイテム</dt>
										<dd>
											<ul>
												<li><a href="<?php echo $page_pass; ?>products/kitchenitems/magnettrivet.html">ナチュラルウッドシリーズ</a></li>
												<li><a href="<?php echo $page_pass; ?>products/kitchenitems/hk_and_ph.html">オーガニックコットンシリーズ</a></li>
												<li><a href="<?php echo $page_pass; ?>products/kitchenitems/apron_and_cloth.html">オーガニックリネンシリーズ</a></li>
											</ul>
										</dd>
									</dl>
								</li>
								<li>
									<dl>
										<dt>食べ物</dt>
										<dd>
											<ul>
												<li><a href="<?php echo $page_pass; ?>products/food/rice.html">有機栽培こしひかり</a></li>
												<li><a href="<?php echo $page_pass; ?>products/food/curry.html">バーミキュラ カレールーセット</a></li>
											</ul>
										</dd>
									</dl>
								</li>
								<li>
									<dl>
										<dt>レシピ本</dt>
										<dd>
											<ul>
												<li><a href="http://shop.vermicular.jp/jp/item.php?id=I00000201" class="ex_link black" target="_blank">Vermicular Recipe Book 00号</a></li>
												<li><a href="http://shop.vermicular.jp/jp/item.php?id=I00000206" class="ex_link black" target="_blank">Vermicular Recipe Book 01号</a></li>
											</ul>
										</dd>
									</dl>
								</li>
							</ul>
						</dd>
					</dl>
				</div><!-- /.list -->
				<div class="list">
					<dl>
						<dt>バーミキュラについて</dt>
						<dd>
							<ul>
								<li><a href="<?php echo $page_pass; ?>about/us/">5つの約束</a></li>
								<li><a href="<?php echo $page_pass; ?>about/us/story/">開発ストーリー</a></li>
								<li><a href="<?php echo $page_pass; ?>voices/">お客様の声</a></li>
								<li><a href="<?php echo $page_pass; ?>taste/howto/">ご使用方法</a></li>
							</ul>
						</dd>
					</dl>
					<dl>
						<dt>レシピ</dt>
						<dd>
							<ul>
								<li><a href="https://owners.vermicular.jp/">オーナーズマイページ</a></li>
							</ul>
						</dd>
					</dl>
					<dl>
						<dt>ご購入</dt>
						<dd>
							<ul>
								<li><a href="http://shop.vermicular.jp/jp/" class="ex_link black" target="_blank">オンラインショップ</a></li>
								<li><a href="<?php echo $page_pass; ?>shoplist/" class="ex_link black" target="_blank">取扱店舗</a></li>
							</ul>
						</dd>
					</dl>
				</div><!-- /.list -->
				<div class="list last">
					<dl>
						<dt>サポート</dt>
						<dd>
							<ul>
								<li><a href="<?php echo $page_pass; ?>support/">オーナーズデスク</a></li>
								<li><a href="<?php echo $page_pass; ?>faq/">よくある質問</a></li>
							</ul>
						</dd>
					</dl>
					<dl>
						<dt><a href="<?php echo $page_pass; ?>company/">私たちについて</a></dt>
						<dd>
							<ul>
								<li><a href="<?php echo $page_pass; ?>company/">私たちについて</a></li>
								<li><a href="<?php echo $page_pass; ?>recruit/">採用情報</a></li>
								<li><a href="<?php echo $page_pass; ?>pr/">取材に関するお問い合わせ先</a></li>
							</ul>
						</dd>
					</dl>
					<dl>
						<dt>その他</dt>
						<dd>
							<ul>
								<li><a href="<?php echo $page_pass; ?>privacy/">プライバシーポリシー</a></li>
								<li><a href="http://www.vermicular.com/?lang=en">English</a></li>
							</ul>
						</dd>
					</dl>
				</div><!-- /.list -->
			</div>
			<div class="footer_other_cnt flr">
				<div class="list">
					<dl>
						<dt>オンラインショップ</dt>
						<dd class="banner">
							<a href="http://shop.vermicular.jp/jp/" target="_blank"><img src="<?php echo $page_pass; ?>common/img/bnr_footer01.jpg" width="280" height="100" alt="Online Shop"></a>
						</dd>
					</dl>
					<dl>
						<dt>バーミキュラの最新情報はこちら</dt>
						<dd>
							<div class="footer_facebook_wrap">
								<div id="fb-root"></div>
								<script>(function(d, s, id) {
								var js, fjs = d.getElementsByTagName(s)[0];
								if (d.getElementById(id)) return;
								js = d.createElement(s); js.id = id;
								js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.5";
								fjs.parentNode.insertBefore(js, fjs);
								}(document, 'script', 'facebook-jssdk'));</script>
								<div class="fb-page" data-href="https://www.facebook.com/vermicular" data-width="280" data-height="205" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/vermicular"><a href="https://www.facebook.com/vermicular">バーミキュラ（Vermicular）</a></blockquote></div></div>
							</div><!-- /.footer_facebook_wrap -->

							<ul class="sns_list">
								<li class="btn clear_gray w133 fll"><a href="https://twitter.com/_VERMICULAR_" target="_blank"><span class="btn_inner"><img src="<?php echo $page_pass; ?>common/img/footer_btn_text01.png" alt=""></span></a></li>
								<li class="btn clear_gray w133 flr"><a href="https://instagram.com/vermicular_japan/" target="_blank"><span class="btn_inner"><img src="<?php echo $page_pass; ?>common/img/footer_btn_text02.png" alt=""></span></a></li>
								<li class="btn clear_gray w278 clear mail"><a href="http://shop.vermicular.jp/mail/" target="_blank"><span class="btn_inner"><img src="<?php echo $page_pass; ?>common/img/footer_btn_text03.png" alt=""></span></a></li>
							</ul>

						</dd>
					</dl>
				</div>
			</div>
		</div>
		<div><!-- /.inner_min -->
		<p id="copy"><img src="<?php echo $page_pass; ?>common/img/txt_copy.png" width="252" height="12" alt="© Aichi Dobby. LTD. All Rights Reserved."></p>
		</div>
	</footer><!-- /#footer -->
</div><!-- /#document -->
<a id="page_top" href="#document"><img src="<?php echo $page_pass; ?>common/img/btn_pagetop.png" width="45" height="45" alt="↑"></a>


<script type="text/javascript" language="javascript">
/* <![CDATA[ */
var yahoo_retargeting_id = 'N2KOEVLHZK';
var yahoo_retargeting_label = '';
/* ]]> */
</script>


<script type="text/javascript" language="javascript" src="//b92.yahoo.co.jp/js/s_retargeting.js"></script>


<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 955077925;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>


<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>


<script type="text/javascript"> 

var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www."); 

document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E")); 

</script> 



<script type="text/javascript"> 

try { 

var pageTracker = _gat._getTracker("UA-12538240-1"); 

pageTracker._trackPageview(); 

} catch(err) {}</script> 







<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/955077925/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript



></body>
</html>