<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ja" class="ie ie6" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 7]>   <html lang="ja" class="ie ie7" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 8]>   <html lang="ja" class="ie ie8" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 9]>   <html lang="ja" class="ie ie9" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="ja" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">

<? include 'configure.php'; ?>

<?php
	if($page_pass==""){
		$layer = "";
	}else{
		$layer = $page_pass;
	}
?>

<?php
	$ua=$_SERVER['HTTP_USER_AGENT'];
	$device = ((strpos($ua,'iPhone')!==false)||(strpos($ua,'iPod')!==false)||(strpos($ua,'Android')!==false)||(strpos($ua,'iPad')!==false));
	if ($device == true){
		$device = 'other';
	}
?>
<?php
	if($device == "other"){
		echo '<meta name="viewport" content="width=1280">';
	}else{
		echo '<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1280">';
	}
?>

<link rel="canonical" href="http://www.vermicular.jp/<?php echo $page_directry; ?>">

<?php if($page_id == "top"): // [ トップページのみ出力 ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ ] ?>
<link rel="alternate" href="http://www.vermicular.jp/" hreflang="ja">
<link rel="alternate" href="http://www.vermicular.com/" hreflang="en">
<?php endif;  // [ トップページのみ出力 ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ ] ?>

<title><?php echo $page_title; ?></title>
<meta name="author" content="愛知ドビー株式会社">

<meta name="description" content="<?php echo $page_description; ?>">
<meta name="keywords" content="<?php echo $page_keywords; ?>">


<meta name="format-detection" content="telephone=no,address=no,email=no">

<link rel="stylesheet" type="text/css" href="<?php echo $page_pass; ?>common/css/normalize.css">
<link rel="stylesheet" type="text/css" href="<?php echo $page_pass; ?>common/css/styles.css">
<?php echo $extra_css; ?>

<link href="<?php echo $page_pass; ?>common/img/apple-touch-icon.png" rel="apple-touch-icon">
<meta name="apple-mobile-web-app-title" content="Vermicular">

<script src="<?php echo $page_pass; ?>common/js/jquery-1.11.0.min.js"></script>
<script src="<?php echo $page_pass; ?>common/js/jquery.transit.js"></script>
<script src="<?php echo $page_pass; ?>common/js/pace.min.js"></script>
<script src="<?php echo $page_pass; ?>common/js/jquery.colorbox-min.js"></script>
<script src="<?php echo $page_pass; ?>common/js/var.js"></script>
<script src="<?php echo $page_pass; ?>common/js/function.js"></script>
<script src="<?php echo $page_pass; ?>common/js/scripts.js"></script>
<script src="<?php echo $page_pass; ?>common/js/jquery.bxslider.min.js"></script>
<?php echo $extra_js; ?>

<!--[if lt IE 9]>
<script src="<?php echo $page_pass; ?>common/js/html5shiv.js"></script>
<script type="text/javascript" src="<?php echo $page_pass; ?>common/js/jquery.belatedPNG.min.js"></script>
<script>
$(function() {
	$(".pngfix").fixPng();
});
</script>
<script src="<?php echo $page_pass; ?>common/js/jquery.backgroundSize.js"></script>
<script>
$(function(){
 $('#header #bg').css('background-size', 'cover');
});
</script>
<![endif]-->
</head>
<body<?php echo ($page_id)? ' id="'.$page_id.'"':''; ?>>
<div id="document"<?php echo ($device == "pc")?' class="device_pc"':''; ?>>
	<?php if($page_id == "top"): // [ トップページのみ出力 ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ ] ?>
	<header id="header">
		<h1><a href="<?php echo $page_pass; ?>"><img src="<?php echo $page_pass; ?>common/img/logo.png" width="152" height="30" alt="VERMICULAR MADE IN JAPAN"></a></h1>
		<h2 class="copy">
			<span class="first_motion"><img src="<?= ST_WWW ?>/img/top/kv/tit_main01.png" height="70" width="67" alt="手"></span>
			<span class="first_motion"><img src="<?= ST_WWW ?>/img/top/kv/tit_main02.png" height="70" width="67" alt="料"></span>
			<span class="first_motion"><img src="<?= ST_WWW ?>/img/top/kv/tit_main03.png" height="70" width="67" alt="理"></span>
			<span class="first_motion"><img src="<?= ST_WWW ?>/img/top/kv/tit_main04.png" height="70" width="44" alt="と"></span>
			<span class="first_motion"><img src="<?= ST_WWW ?>/img/top/kv/tit_main05.png" height="70" width="35" alt="、"></span>
			<span class="second_motion"><img src="<?= ST_WWW ?>/img/top/kv/tit_main06.png" height="70" width="67" alt="生"></span>
			<span class="second_motion"><img src="<?= ST_WWW ?>/img/top/kv/tit_main07.png" height="70" width="45" alt="き"></span>
			<span class="second_motion"><img src="<?= ST_WWW ?>/img/top/kv/tit_main08.png" height="70" width="45" alt="よ"></span>
			<span class="second_motion"><img src="<?= ST_WWW ?>/img/top/kv/tit_main09.png" height="70" width="44" alt="う"></span>
			<span class="second_motion"><img src="<?= ST_WWW ?>/img/top/kv/tit_main10.png" height="70" width="17" alt="。"></span>
		</h2>
		<span id="bg"></span>
		<div id="scroll_down">
			<div class="text"><img src="<?php echo ST_WWW; ?>/img/products/common/scroll_down_text.png" height="11" width="36" alt="Scroll"></div>
			<div class="arrow"><img src="<?php echo ST_WWW; ?>/img/products/common/scroll_down_arrow.png" height="18" width="36" alt=""></div>
            
            
            
		</div>
	</header><!-- /#header -->

	<div id="gnavi_wrap">
		<nav id="gnavi" class="clearfix">
			<img class="image" src="<?= ST_WWW ?>/img/gm/logo_black.png" width="133" height="25" alt="VERMICULAR MADE IN JAPAN">
			<ul class="link_navi clearfix">
				<li class="list">
					<span><img src="<?= ST_WWW ?>/img/gm/btn_nav01.png" width="109" height="17" alt="製品ラインナップ"></span>
					<ul class="dd_list">
						<li class="list01"><a href="<?php echo $page_pass; ?>products/">オープンポットラウンド</a></li>
						<li class="list02"><a href="<?php echo $page_pass; ?>products/ricepot/">ライスポット</a></li>
						<li class="list03"><a href="<?php echo $page_pass; ?>products/kitchenitems/index.html">キッチンアイテム</a></li>
						<li class="list04"><a href="<?php echo $page_pass; ?>products/food/">食べ物</a></li>
						<li class="list05"><a href="http://shop.vermicular.jp/jp/group.php?id=12" class="ex_link white" target="_blank">レシピ本</a></li>
					</ul>
				</li>
				<li class="list">
					<span><img src="<?= ST_WWW ?>/img/gm/btn_nav02.png" width="94" height="17" alt="選ばれる理由"></span>
					<ul class="dd_list">
						<li class="list01"><a href="<?php echo $page_pass; ?>about/whatis/">暮らしを変える鍋</a></li>
						<li class="list02"><a href="<?php echo $page_pass; ?>about/teshigoto/">手仕事とテクノロジー</a></li>
						<li class="list03"><a href="<?php echo $page_pass; ?>support/">一生サポート</a></li>
						<li class="list04"><a href="<?php echo $page_pass; ?>faq/">よくある質問</a></li>
					</ul>
				</li>
				<li class="list"><a href="https://owners.vermicular.jp/"><img src="<?= ST_WWW ?>/img/gm/btn_nav03.png" width="39" height="17" alt="レシピ"></a></li>
				<li class="list">
					<span><img src="<?= ST_WWW ?>/img/gm/btn_nav04_02.png" width="92" height="17" alt="私たちについて"></span>
					<ul class="dd_list">
						<li class="list01"><a href="<?php echo $page_pass; ?>company/">私たちについて</a></li>
						<li class="list02"><a href="<?php echo $page_pass; ?>recruit/">採用情報</a></li>
						<li class="list03"><a href="<?php echo $page_pass; ?>pr/">取材に関するお問い合わせ</a></li>
					</ul>
				</li>
				<li class="list"><a href="http://shop.vermicular.jp/jp/" class="ex_link black" target="_blank"><img src="<?= ST_WWW ?>/img/gm/btn_nav05.png" width="111" height="17" alt="オンラインショップ"></a></li>
				<li class="list"><a href="http://www.vermicular.com/"><img src="<?= ST_WWW ?>/img/gm/btn_nav06.png" width="52" height="17" alt="English"></a></li>
			</ul>
			<ul class="sns_list clearfix">
				<li class="list"><a href="https://www.facebook.com/vermicular" target="_blank"><img src="<?= ST_WWW ?>/img/gm/head_sns_icon_black01.png" height="16" width="17" alt="Facebook"></a></li>
				<li class="list"><a href="https://twitter.com/_VERMICULAR_" target="_blank"><img src="<?= ST_WWW ?>/img/gm/head_sns_icon_black02.png" height="16" width="17" alt="Twitter"></a></li>
				<li class="list"><a href="https://instagram.com/vermicular_japan/" target="_blank"><img src="<?= ST_WWW ?>/img/gm/head_sns_icon_black03.png" height="16" width="17" alt="Instagram"></a></li>
				<li class="list"><a href="http://shop.vermicular.jp/mail/" target="_blank"><img src="<?= ST_WWW ?>/img/gm/head_sns_icon_black04.png" height="16" width="17" alt="Mail"></a></li>
			</ul>
		</nav><!-- /nav -->
	</div>
	<?php endif;  // [ トップページのみ出力 ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ ] ?>


	<?php if($page_type == 'oven_pot_round'): // [ オーブンポットラウンド用 ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ ] ?>

	<header id="rpr_header">
		<div id="rpr_header_inner" class="clearfix">
			<h1 id="rpr_site_logo"><a href="<?php echo $page_pass; ?>"><img src="<?= ST_WWW ?>/img/gm/site_logo.png" height="27" width="143" alt="VERMICULAR（バーミキュラ） MADE IN JAPAN"></a></h1>
			<nav id="rpr_gnavi">
				<ul class="clearfix">
					<li class="list navi01">
                    
            
                    
                    
						<span><img src="<?= ST_WWW ?>/img/gm/gnavi01.png" height="17" width="109" alt="製品ラインナップ"></span>
						<ul class="dd_list">
							<li class="list01"><a href="<?php echo $page_pass; ?>products/">オープンポットラウンド</a></li>
						<li class="list02"><a href="<?php echo $page_pass; ?>products/ricepot/">ライスポット</a></li>
						<li class="list03"><a href="<?php echo $page_pass; ?>products/kitchenitems/index.html">キッチンアイテム</a></li>
						<li class="list04"><a href="<?php echo $page_pass; ?>products/food/">食べ物</a></li>
						<li class="list05"><a href="http://shop.vermicular.jp/jp/group.php?id=12" class="ex_link white" target="_blank">レシピ本</a></li>
						</ul>
					</li>
					<li class="list navi02">
						<span><img src="<?= ST_WWW ?>/img/gm/gnavi02.png" height="17" width="90" alt="選ばれる理由"></span>
						<ul class="dd_list">
							<li class="list01"><a href="<?php echo $page_pass; ?>about/whatis/">暮らしを変える鍋</a></li>
							<li class="list02"><a href="<?php echo $page_pass; ?>about/teshigoto/">手仕事とテクノロジー</a></li>
							<li class="list03"><a href="<?php echo $page_pass; ?>support/">一生サポート</a></li>
							<li class="list04"><a href="<?php echo $page_pass; ?>faq/">よくある質問</a></li>
						</ul>
					</li>
					<li class="list navi03"><a href="https://owners.vermicular.jp/"><img src="<?= ST_WWW ?>/img/gm/gnavi03.png" height="17" width="37" alt="レシピ"></a></li>
					<li class="list navi04">
						<span><img src="<?= ST_WWW ?>/img/gm/gnavi04_02.png" height="17" width="92" alt="私たちについて"></span>
						<ul class="dd_list">
							<li class="list01"><a href="<?php echo $page_pass; ?>company/">私たちについて</a></li>
							<li class="list02"><a href="<?php echo $page_pass; ?>recruit/">採用情報</a></li>
							<li class="list03"><a href="<?php echo $page_pass; ?>pr/">取材に関するお問い合わせ</a></li>
						</ul>
					</li>
					<li class="list navi05"><a href="http://shop.vermicular.jp/jp/" class="ex_link white" target="_blank"><img src="<?= ST_WWW ?>/img/gm/gnavi05.png" height="17" width="110" alt="オンラインショップ"></a></li>
					<li class="list navi06"><a href="http://www.vermicular.com/"><img src="<?= ST_WWW ?>/img/gm/gnavi06.png" height="17" width="50" alt="English"></a></li>
				</ul>
			</nav>
			<ul class="sns_list clearfix">
				<li class="sns01"><a href="https://www.facebook.com/vermicular" target="_blank"><img src="<?= ST_WWW ?>/img/gm/head_sns_icon_white01.png" height="16" width="17" alt="Facebook"></a></li>
				
            
                <li class="sns02"><a href="https://twitter.com/_VERMICULAR_" target="_blank"><img src="<?= ST_WWW ?>/img/gm/head_sns_icon_white02.png" height="16" width="17" alt="Twitter"></a></li>
				<li class="sns03"><a href="https://instagram.com/vermicular_japan/" target="_blank"><img src="<?= ST_WWW ?>/img/gm/head_sns_icon_white03.png" height="16" width="17" alt="Instagram"></a></li>
				<li class="sns04"><a href="http://shop.vermicular.jp/mail/" target="_blank"><img src="<?= ST_WWW ?>/img/gm/head_sns_icon_white04.png" height="16" width="17" alt="Mail"></a></li>
	
            
            
            </ul>
		</div>
	</header><!-- /#header -->

	<?php endif;  // [ オーブンポットラウンド用 ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ ] ?>

	<main id="main_wrap">