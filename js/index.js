// require jQuery JavaScript Library v1.10.1

/* 即時関数でローカル化 */
$(function(){

/* ============================================================
 * Device
 * ============================================================ */


	var pcLoadFunction = function(winW,winH){
	}

	var smpLoadFunction = function(winW,winH){
	}

	/* ------------------------------------------------------------
	 * [ Resize ]
	 * ------------------------------------------------------------ */

    function Resize(winW,winH){
        var $Head = $("#header"),
            $headLogo = $("#header h1"),
            $headText = $("#header h2"),
            $headBg = $("#header #bg"),
            $scrollIcon = $("#header #scroll_down");


        if(winW > minWidth){
            var resizeW = winW;
        }else{
            var resizeW = minWidth;
        }

        if(winH > minHeight){
            var resizeH = winH;
        }else{
            var resizeH = minHeight;
        }

        // if(divece!="pc"){
        // 	var resizeH = $Head.outerHeight();
        // }

        $Head.height(resizeH-56);
        //$headText.css({"margin-top":resizeH/2-68});

    }

	/* ------------------------------------------------------------
	 * [ kvView ]
	 * ------------------------------------------------------------ */

    function kvViewfast(winW,winH){
        var $Head = $("#header");
        $Head.height(winH-56);
    }
    function kvView(winW,winH){
        var $Head = $("#header"),
            $headLogo = $("#header h1"),
            $headText = $("#header h2"),
            $headBg = $("#header #bg"),
            $scrollIcon = $("#header #scroll_down");
        slideArray = [6,1,2,3];

        //$headText.css({"margin-top":winH/2-68});

        var bgFlg = 0;

        $("#header #bg").addClass("slide06");

        setTimeout(function(){
            for( var i=0; $Head.find(".copy span").length > i; i++){
                // if($Head.find(".copy span").length-1 == i ){
                // 	$scrollIcon.delay(1500).animate({opacity:1},400,"easeInOutCirc",function(){
                // 		$headLogo.delay(300).stop().animate({"top":24},600,"easeOutSine");
                //        	scrollMove();
                //        });
                // }
                // $Head.find(".copy span").eq(i).delay(100*i).animate({"opacity":1,"left":0},400,"easeInSine",function(){
                // });

                $Head.find(".copy .first_motion").animate({"opacity":1},1000,"easeInSine",function(){
                    $Head.find(".copy .second_motion").delay(500).animate({"opacity":1},1000,"easeInSine",function(){
                        $scrollIcon.delay(1500).animate({opacity:1},400,"easeInOutCirc",function(){
                            $headLogo.delay(300).stop().animate({"top":24},600,"easeOutSine");
                            scrollMove();
                        });
                    });
                });

            }
        },1000);
        setInterval(function(){
            bgFlg ++;
            slideArray[bgFlg];
            $("#header #bg").animate({"opacity":"hide"},1500,"linear",function(){
                $("#header #bg").removeClass();
                $("#header #bg").addClass("slide0"+slideArray[bgFlg]+"");
            });
            $("#header #bg").animate({"opacity":"show"},1500,"easeInQuad",function(){
                if(bgFlg == 3){
                    bgFlg = -1;
                }
            });
        },6000);
    }

	/* ------------------------------------------------------------
	 * [ scrollMove ]
	 * ------------------------------------------------------------ */
	var $scrollIcon = $("#scroll_down .arrow");

	function scrollMove(){
		$scrollIcon.delay(200).animate({"top":15,"opacity":0},600,"easeOutSine",function(){
			$(this).css({"top":-10});
			$(this).delay(300).animate({"top":0,"opacity":1},300,"easeOutSine",function(){
				scrollMove();
			});
		});
	}

	/* ------------------------------------------------------------
	 * [ onlinebtnShow ]
	 * ------------------------------------------------------------ */
	// var $onlineBtn = $("#online_btn");

	// if(divece=="other") $onlineBtn.not(".show").addClass("always");

	// function onlinebtnShow(){
	// 	var winScroll = $(window).scrollTop(),
	// 		wrapPosition = $("#main_wrap").offset().top-250,
	// 		speed = 150,
	// 		easing = "easeOutQuad";

	// 	if(winScroll>wrapPosition){
	// 		$onlineBtn.not(".show").addClass("show");
	// 	}else{
	// 		$onlineBtn.removeClass("show");
	// 	}
	// }


	/* ------------------------------------------------------------
	 * [ newsSort ]
	 * ------------------------------------------------------------ */

	function newsSort(){
		var $List = $("#news nav ul li"),
		$newsWrap = $(".news_wrap");

		$List.on("click",function(){
			var Index = $(this).index();
			$List.removeClass("active");
			$List.eq(Index).addClass("active");
			$newsWrap.stop().animate({"opacity":0},300,function(){
				$newsWrap.children('div').css({"display":"none"});
				$newsWrap.children('div').eq(Index).css({"display":"block"});
				$newsWrap.children('div').removeClass("active");
				$newsWrap.children('div').eq(Index).addClass("active");
				$newsWrap.animate({"opacity":1},500);
			});
		});
	}

/* ============================================================
 * Execute JavaScript when the Window Object is fully loaded.
 * ============================================================ */
	winW = $win.width(),
	winH = $win.height();

	$doc.on('ready', function() {
		/*if (isIE8){
			$("#header h1 img").attr("src",$("#header h1 img").attr("src").replace(".svg", ".png"));
			$("#header p img").attr("src",$("#header p img").attr("src").replace(".svg", ".png"));
		}*/
		kvViewfast(winW,winH);

		if($("#slider li").length>3) $("#slider").bxSlider({
			slideWidth: 386,
			slideMargin: 20,
			auto: true,
			speed:700,
			//pause:10000,
			minSlides: 1,
			maxSlides: 4,
			moveSlides: 1,
			//easing:'cubic-bezier(0.55, 0.085, 0.68, 0.53)',
			infiniteLoop: true,
			hideControlOnEnd: true
		});
		newsSort();

	});// ready End

	$win.on('load', function() {
		kvView(winW,winH);
		// onlinebtnShow();
		Resize(winW,winH);
	});// load End

	$win.on('resize', function() {
		winW = $win.width(),
		winH = $win.height();
		// onlinebtnShow();
		Resize(winW,winH);
	});// load End


	$win.on('scroll', function(){
		// onlinebtnShow();
	});// scroll End

});//End