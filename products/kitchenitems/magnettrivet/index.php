<?php
$page_id = 'magnettrivet'; //ページ独自にスタイルを指定する場合は入力
$page_type = 'kitchenitem'; // [ オーブンポッドラウンドのページの場合 「oven_pot_round」 ]
$page_title = 'Natural Wood Series MAGNET TRIVET マグネットトリベット | Vermicular（バーミキュラ）'; //ページ名
$page_description = '「無垢の木の温もりと風合い」「日本の手わざ」を活かしたこだわったキッチンアイテムが登場';
$page_keywords = 'バーミキュラ,Vermicular,鋳物,ホーロー鍋,無水調理,レシピ,通販,無水カレー,愛知ドビー,無水,ホーロー,パールホーロー,トリプルサーモーテクノロジー';
$page_pass = '../../../'; //自分の階層を指定
$page_directry = 'products/ricepot'; //自分のディレクトリ名を指定
$extra_css = '
<link rel="stylesheet" type="text/css" href="'.$page_pass.'css/products.css">
<link rel="stylesheet" type="text/css" href="'.$page_pass.'css/index_nr.css">
<link rel="stylesheet" type="text/css" href="../../ricepot/css/kitchenitem.css">
'; //何か追加で読み込みたいCSSがあればlinkタグごと記述
$extra_js = '
<script src="'.$page_pass.'common/js/scripts.js"></script>
<script src="'.$page_pass.'common/js/jquery.cookie.js"></script>
<script src="'.$page_pass.'common/js/lib.js"></script>
<script src="../../ricepot/js/kitchenitems.js"></script>
<script>
$(window).on("load", function() {
    if(divece!="pc"){
        $(".movie").attr("id", "bg");
    }else{
        if(!$("#movie-video")[0]) return false;
        
        var magnetvisitCount;  //訪問回数
        cookieArr = $.cookie();
        if (cookieArr["magnetvisitCount"] == null) {  //初訪問の時
            magnetvisitCount = 1;
            $.cookie("magnetvisitCount", magnetvisitCount,{expires:7});
        } else {  //2回目以降の訪問の時
            magnetvisitCount = $.cookie("magnetvisitCount");
            magnetvisitCount ++;
            $.cookie("magnetvisitCount", magnetvisitCount,{expires:7});
        }
        if(magnetvisitCount % 2 == 0){
            var videoType = "<source src=../assets/img/magnettrivet/kv2.mp4 type=video/mp4>";
        }else{
            var videoType = "<source src=../assets/img/magnettrivet/kv.mp4 type=video/mp4>";
        }
        setTimeout(function(){
            var VideoSource = videoType;
            $("#movie-video").append(VideoSource);
        },500);
        
    }
});
</script>
'; //何か追加で読み込みたいJSがあればscriptタグごと記述
 ?>
<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ja" class="ie ie6" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 7]>   <html lang="ja" class="ie ie7" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 8]>   <html lang="ja" class="ie ie8" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 9]>   <html lang="ja" class="ie ie9" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="ja" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Style-Type" content="text/css">
    <meta http-equiv="Content-Script-Type" content="text/javascript">

    <?php
    if($page_pass==""){
        $layer = "";
    }else{
        $layer = $page_pass;
    }
    ?>

    <?php
    $ua=$_SERVER['HTTP_USER_AGENT'];
    $device = ((strpos($ua,'iPhone')!==false)||(strpos($ua,'iPod')!==false)||(strpos($ua,'Android')!==false)||(strpos($ua,'iPad')!==false));
    if ($device == true){
        $device = 'other';
    }
    ?>
    <?php
    if($device == "other"){
        echo '<meta name="viewport" content="width=1280">';
    }else{
        echo '<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=1280">';
    }
    ?>

    <link rel="canonical" href="http://www.vermicular.jp/<?php echo $page_directry; ?>">

    <title><?php echo $page_title; ?></title>
    <meta name="author" content="愛知ドビー株式会社">

    <meta name="description" content="<?php echo $page_description; ?>">
    <meta name="keywords" content="<?php echo $page_keywords; ?>">


    <meta name="format-detection" content="telephone=no,address=no,email=no">

    <link rel="stylesheet" type="text/css" href="<?php echo $page_pass; ?>common/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $page_pass; ?>common/css/styles.css">
    <?php echo $extra_css; ?>

    <link href="<?php echo $page_pass; ?>common/img/apple-touch-icon.png" rel="apple-touch-icon">
    <meta name="apple-mobile-web-app-title" content="Vermicular">

    <script src="<?php echo $page_pass; ?>common/js/jquery-1.11.0.min.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/jquery.transit.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/pace.min.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/jquery.colorbox-min.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/var.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/function.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/scripts.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/jquery.bxslider.min.js"></script>
    <?php echo $extra_js; ?>

    <!--[if lt IE 9]>
    <script src="<?php echo $page_pass; ?>common/js/html5shiv.js"></script>
    <script type="text/javascript" src="<?php echo $page_pass; ?>common/js/jquery.belatedPNG.min.js"></script>
    <script>
        $(function() {
            $(".pngfix").fixPng();
        });
    </script>
    <script src="<?php echo $page_pass; ?>common/js/jquery.backgroundSize.js"></script>
    <script>
        $(function(){
            $('#header #bg').css('background-size', 'cover');
        });
    </script>
    <![endif]-->
</head>
<body<?php echo ($page_id)? ' id="'.$page_id.'"':''; ?>>
<div id="document"<?php echo ($device == "pc")?' class="device_pc"':''; ?>>
    <header id="rpr_header">
        <div id="rpr_header_inner" class="clearfix">
            <h1 id="rpr_site_logo"><a href="<?php echo $page_pass; ?>"><img src="<?php echo $page_pass; ?>/img/gm/site_logo.png" height="27" width="143" alt="VERMICULAR（バーミキュラ） MADE IN JAPAN"></a></h1>
            <nav id="rpr_gnavi">
                <ul class="clearfix">
                    <li class="list navi01">
                        <span><img src="<?php echo $page_pass; ?>/img/gm/gnavi01.png" height="17" width="109" alt="製品ラインナップ"></span>
                        <ul class="dd_list">
                            <li class="list01"><a href="<?php echo $page_pass; ?>products/">オープンポットラウンド</a></li>
                            <li class="list02"><a href="<?php echo $page_pass; ?>products/ricepot/">ライスポット</a></li>
                            <li class="list03"><a href="<?php echo $page_pass; ?>products/kitchenitems/index.html">キッチンアイテム</a></li>
                            <li class="list04"><a href="<?php echo $page_pass; ?>products/food/">食べ物</a></li>
                            <li class="list05"><a href="http://shop.vermicular.jp/jp/group.php?id=12" class="ex_link white" target="_blank">レシピ本</a></li>
                        </ul>
                    </li>
                    <li class="list navi02">
                        <span><img src="<?php echo $page_pass; ?>/img/gm/gnavi02.png" height="17" width="90" alt="選ばれる理由"></span>
                        <ul class="dd_list">
                            <li class="list01"><a href="<?php echo $page_pass; ?>about/whatis/">暮らしを変える鍋</a></li>
                            <li class="list02"><a href="<?php echo $page_pass; ?>about/teshigoto/">手仕事とテクノロジー</a></li>
                            <li class="list03"><a href="<?php echo $page_pass; ?>support/">一生サポート</a></li>
                            <li class="list04"><a href="<?php echo $page_pass; ?>faq/">よくある質問</a></li>
                        </ul>
                    </li>
                    <li class="list navi03"><a href="https://owners.vermicular.jp/"><img src="<?php echo $page_pass; ?>/img/gm/gnavi03.png" height="17" width="37" alt="レシピ"></a></li>
                    <li class="list navi04">
                        <span><img src="<?php echo $page_pass; ?>/img/products/common/gnavi04.png" height="17" width="54" alt="サポート"></span>
                        <ul class="dd_list">
                            <li class="list01"><a href="<?php echo $page_pass; ?>support/">オーナーズデスク</a></li>
                            <li class="list02"><a href="<?php echo $page_pass; ?>faq/">よくある質問</a></li>
                        </ul>
                    </li>
                    <li class="list navi05"><a href="http://shop.vermicular.jp/jp/" class="ex_link white" target="_blank"><img src="<?php echo $page_pass; ?>/img/gm/gnavi05.png" height="17" width="110" alt="オンラインショップ"></a></li>
                    <li class="list navi06"><a href="http://www.vermicular.com/"><img src="<?php echo $page_pass; ?>/img/gm/gnavi06.png" height="17" width="50" alt="English"></a></li>
                </ul>
            </nav>
            <ul class="sns_list clearfix">
                <li class="sns01"><a href="https://www.facebook.com/vermicular" target="_blank"><img src="<?php echo $page_pass; ?>/img/gm/head_sns_icon_white01.png" height="16" width="17" alt="Facebook"></a></li>


                <li class="sns02"><a href="https://twitter.com/_VERMICULAR_" target="_blank"><img src="<?php echo $page_pass; ?>/img/gm/head_sns_icon_white02.png" height="16" width="17" alt="Twitter"></a></li>
                <li class="sns03"><a href="https://instagram.com/vermicular_japan/" target="_blank"><img src="<?php echo $page_pass; ?>/img/gm/head_sns_icon_white03.png" height="16" width="17" alt="Instagram"></a></li>
                <li class="sns04"><a href="http://shop.vermicular.jp/mail/" target="_blank"><img src="<?php echo $page_pass; ?>/img/gm/head_sns_icon_white04.png" height="16" width="17" alt="Mail"></a></li>



            </ul>
        </div>
    </header><!-- /#header -->
    <header id="page_title_wrap">
        <div id="kv_inner">
            <h1 class="product_name motion"><img src="../assets/img/magnettrivet/kv_text.png?2" height="92" width="286" alt="Natural Wood Series MAGNET TRIVET マグネットトリベット"></h1>
            <p class="text motion">トリベットもバーミキュラの鍋と同様の思想で、木材自体の良さを活かしながら、国内の自社工場で<br>
            0.01ミリの精度で切削・研磨ができる卓越した技術を持った日本の職人の手仕事で作っています。</p>
        </div>
        <div id="scroll_down">
            <div class="text"><img src="../../ricepot/img/products/common/scroll_down_text.png?2" height="11" width="36" alt="Scroll"></div>
            <div class="arrow"><img src="../../ricepot/img/products/common/scroll_down_arrow.png?2" height="18" width="36" alt=""></div>
        </div>
        <div class="movie">
            <video id="movie-video" class="movie__video" preload="metadata" loop autoplay muted></video>
            <div class="movie__backdrop"></div>
        </div>
    </header><!-- /#kv_wrap -->

    <div id="section_wrap01">
        <section class="js__scr-target">
            <div class="text_wrap inner">
                <h1 class="copy">
                    <img src="../assets/img/magnettrivet/tit_section01-1.png?2" height="65" width="353" alt="MAGNET TRIVET マグネット トリベット（鍋敷き）">
                </h1>
                <p class="text">両手で運ぶバーミキュラの鍋には、手を使わずにストレスなく脱<br>
                着できるこの鍋敷きが必需品です。天然無垢材を、職人が鍋底<br>
                のラインに合わせて削り出した美しさも評価していただいています。</p>
                <div class="btn clear_black w198"><a href="http://shop.vermicular.jp/jp/group.php?id=26" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../assets/img/top/onlineshop_text.png?2" height="11" width="99" alt="オンラインショップ"></span></span></a></div>
            </div>
            <span class="bg js__r-fade left top" data-delay="0"><img src="../assets/img/magnettrivet/img_section01-1.jpg?2" height="737" width="804"></span>
        </section>
        <section class="js__scr-target">
            <div class="text_wrap inner">
                <h2 class="copy">
                    <img src="../assets/img/magnettrivet/tit_section01-2.png?2" height="27" width="574" alt="２種類の高級木材と８色の鋳物ホーロープレート">
                </h2>
                <p class="text">質感溢れる北米産高級木材である「ホワイトハードメープル」と「ブラックウォールナット」の２種類をご用意。<br>
                アクセントにあしらわれたロゴプレートはバーミキュラと同じ鋳物ホーロー製で８色から選べます。</p>
            </div>
            <span class="bg">
                <img class="js__fade bottom" data-delay="0" src="../assets/img/magnettrivet/img_section01-2.jpg?2" height="351" width="522">
                <img class="js__fade bottom" data-delay="0.1" src="../assets/img/magnettrivet/img_section01-3.jpg?2" height="351" width="558">
            </span>
        </section>
    </div><!-- /#item_line_up -->

    <section id="section_wrap02">
        <div class="inner clearfix">
            <div class="text_wrap">
                <h1 class="copy"><img src="../assets/img/magnettrivet/tit_section02.png?2" height="73" width="359" alt="手を使わずに着脱可能、これもバーミキュラの発明です。"></h1>
                <p class="text">両手にバーミキュラの鍋を持ったままでも、取り付けたり外したりできるバーミキュラオリジナル設計の画期的な鍋敷きです。鍋敷きの中にマグネットを内蔵しているから、鍋を載せるだけで取り付けができます。取り外すときも鍋を軽く傾けるだけ。鍋をキッチンから直接食卓へ運ぶときも、このマグネットトリベットが鍋にピッタリとついてくるから、アツアツの一番おいしい状態で料理が召し上がれます。</p>
                <div class="feature_wrap">
                    <h2 class="title"><img src="../assets/img/magnettrivet/tit_feature.png?2" height="10" width="56" alt="Feature"></h2>
                    <p class="contents">
                        <img src="../assets/img/magnettrivet/img_feature.png?2" height="88" width="400" alt="マグネット内蔵で着脱自由。一歩進んだ使いやすさ。">
                    </p>
                </div>
            </div>
        </div>
    </section><!-- /#section_wrap02 -->

    <section id="section_wrap03">
        <div class="inner clearfix">
            <div class="text_wrap">
                <h1 class="copy"><img src="../assets/img/magnettrivet/tit_section03.png?2" height="72" width="373" alt="Natural Wood Series 天素材にこだわった天然の無垢材"></h1>
                <p class="text">バーミキュラだから、もちろん鍋敷きの素材にもこだわっています。素材の条件は、調理台や食卓を鍋底の熱から守れて、鍋の保温にも一役買えること。そこで選んだのは「断熱性」と「保温性」に優れ、さらに「耐久性」もある北米産広葉樹。明るい印象の「ホワイトハードメープル」と「ブラックウォールナット」です。天然木のため、１枚ずつ杢目や色などに個性的なバラつきがありますが、それも素材の力を引き出すバーミキュラのものづくりです。</p>
            </div>
            <img class="image" src="../assets/img/magnettrivet/img_section03.jpg?2" height="420" width="540" alt="材質 / ホワイトハードメープル・ブラックウォールナット（ガラス塗装仕上げ）">
        </div>
        <p class="text"><span>材質 / ホワイトハードメープル・ブラックウォールナット（ガラス塗装仕上げ）</span></p>
        <div class="lineup_wrap">
            <ul>
                <li>
                    <img src="../assets/img/magnettrivet/img_lineup01.jpg?2" height="83" width="275" alt="#14">
                    <p class="text">
                        #14<br>
                        ¥3,500（税抜）
                    </p>
                    <div class="btn clear_black w198"><a href="http://shop.vermicular.jp/jp/group.php?id=33" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../assets/img/top/onlineshop_text.png?2" height="11" width="99" alt="オンラインショップ"></span></span></a></div>
                </li>
                <li>
                    <img src="../assets/img/magnettrivet/img_lineup02.jpg?2" height="83" width="275" alt="#18">
                    <p class="text">
                        #18<br>
                        ¥5,000（税抜）
                    </p>
                    <div class="btn clear_black w198"><a href="http://shop.vermicular.jp/jp/group.php?id=23" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../assets/img/top/onlineshop_text.png?2" height="11" width="99" alt="オンラインショップ"></span></span></a></div>
                </li>
                <li>
                    <img src="../assets/img/magnettrivet/img_lineup03.jpg?2" height="83" width="275" alt="#22">
                    <p class="text">
                        #22<br>
                        ¥6,000（税抜）
                    </p>
                    <div class="btn clear_black w198"><a href="http://shop.vermicular.jp/jp/group.php?id=22" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../assets/img/top/onlineshop_text.png?2" height="11" width="99" alt="オンラインショップ"></span></span></a></div>
                </li>
                <li>
                    <img src="../assets/img/magnettrivet/img_lineup04.jpg?2" height="83" width="275" alt="#26">
                    <p class="text">
                        #26<br>
                        ¥7,000（税抜）
                    </p>
                    <div class="btn clear_black w198"><a href="http://shop.vermicular.jp/jp/group.php?id=34" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../assets/img/top/onlineshop_text.png?2" height="11" width="99" alt="オンラインショップ"></span></span></a></div>
                </li>
            </ul>
        </div>
    </section><!-- /#section_wrap03 -->

    <div class="top_items_wrap">
        <div class="inner">
            <div class="btn clear_black w198">
                <a href="../">
                    <span class="btn_inner">
                         <img src="../assets/img/btn_kitchenitems.png?2" height="10" width="113" alt="Kitchen Items TOP">
                    </span>
                </a>
            </div>
        </div>
    </div>

    <a id="online_btn" href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank">
        <img src="../../ricepot/common/img/btn_online.png?2" width="130" height="130" alt="Online Shop">
    </a>

<?php include $page_pass.'_footer.php'; ?>