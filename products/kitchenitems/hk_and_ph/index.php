<?php
$page_id = 'hk_and_ph'; //ページ独自にスタイルを指定する場合は入力
$page_type = 'kitchenitem'; // [ オーブンポッドラウンドのページの場合 「oven_pot_round」 ]
$page_title = 'Organic Cotton Series HEAT KEEPER & POT HOLDER オーガニックコットン ヒートキーパー & ポットホルダー | Vermicular（バーミキュラ）'; //ページ名
$page_description = 'オーガニックコットンとメイドインジャパンにこだわったキッチンアイテムが登場';
$page_keywords = 'バーミキュラ,Vermicular,鋳物,ホーロー鍋,無水調理,レシピ,通販,無水カレー,愛知ドビー,無水,ホーロー,パールホーロー,トリプルサーモーテクノロジー';
$page_pass = '../../../'; //自分の階層を指定
$page_directry = 'products/ricepot'; //自分のディレクトリ名を指定
$extra_css = '
<link rel="stylesheet" type="text/css" href="'.$page_pass.'css/products.css">
<link rel="stylesheet" type="text/css" href="'.$page_pass.'css/index_nr.css">
<link rel="stylesheet" type="text/css" href="../../ricepot/css/kitchenitem.css">
'; //何か追加で読み込みたいCSSがあればlinkタグごと記述
$extra_js = '
<script src="'.$page_pass.'common/js/scripts.js"></script>
<script src="'.$page_pass.'common/js/lib.js"></script>
<script src="../../ricepot/js/kitchenitems.js"></script>
<script>
$(window).on("load", function() {
    if(divece!="pc"){
        $(".movie").attr("id", "bg");
    }else{
        if(!$("#movie-video")[0]) return false;
        setTimeout(function(){
            var VideoSource = "<source src=../assets/img/hk_and_ph/kv.webm type=video/webm><source src=../assets/img/hk_and_ph/kv.mp4 type=video/mp4>";
            $("#movie-video").append(VideoSource);
        },500);
    }
});
</script>'; //何か追加で読み込みたいJSがあればscriptタグごと記述
 ?>
<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ja" class="ie ie6" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 7]>   <html lang="ja" class="ie ie7" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 8]>   <html lang="ja" class="ie ie8" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 9]>   <html lang="ja" class="ie ie9" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="ja" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Style-Type" content="text/css">
    <meta http-equiv="Content-Script-Type" content="text/javascript">

    <?php
    if($page_pass==""){
        $layer = "";
    }else{
        $layer = $page_pass;
    }
    ?>

    <?php
    $ua=$_SERVER['HTTP_USER_AGENT'];
    $device = ((strpos($ua,'iPhone')!==false)||(strpos($ua,'iPod')!==false)||(strpos($ua,'Android')!==false)||(strpos($ua,'iPad')!==false));
    if ($device == true){
        $device = 'other';
    }
    ?>
    <?php
    if($device == "other"){
        echo '<meta name="viewport" content="width=1280">';
    }else{
        echo '<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1280">';
    }
    ?>

    <link rel="canonical" href="http://www.vermicular.jp/<?php echo $page_directry; ?>">

    <title><?php echo $page_title; ?></title>
    <meta name="author" content="愛知ドビー株式会社">

    <meta name="description" content="<?php echo $page_description; ?>">
    <meta name="keywords" content="<?php echo $page_keywords; ?>">


    <meta name="format-detection" content="telephone=no,address=no,email=no">

    <link rel="stylesheet" type="text/css" href="<?php echo $page_pass; ?>common/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $page_pass; ?>common/css/styles.css">
    <?php echo $extra_css; ?>

    <link href="<?php echo $page_pass; ?>common/img/apple-touch-icon.png" rel="apple-touch-icon">
    <meta name="apple-mobile-web-app-title" content="Vermicular">

    <script src="<?php echo $page_pass; ?>common/js/jquery-1.11.0.min.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/jquery.transit.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/pace.min.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/jquery.colorbox-min.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/var.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/function.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/scripts.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/jquery.bxslider.min.js"></script>
    <?php echo $extra_js; ?>

    <!--[if lt IE 9]>
    <script src="<?php echo $page_pass; ?>common/js/html5shiv.js"></script>
    <script type="text/javascript" src="<?php echo $page_pass; ?>common/js/jquery.belatedPNG.min.js"></script>
    <script>
        $(function() {
            $(".pngfix").fixPng();
        });
    </script>
    <script src="<?php echo $page_pass; ?>common/js/jquery.backgroundSize.js"></script>
    <script>
        $(function(){
            $('#header #bg').css('background-size', 'cover');
        });
    </script>
    <![endif]-->
</head>
<body<?php echo ($page_id)? ' id="'.$page_id.'"':''; ?>>
<div id="document"<?php echo ($device == "pc")?' class="device_pc"':''; ?>>
    <header id="rpr_header">
        <div id="rpr_header_inner" class="clearfix">
            <h1 id="rpr_site_logo"><a href="<?php echo $page_pass; ?>"><img src="<?php echo $page_pass; ?>/img/gm/site_logo.png" height="27" width="143" alt="VERMICULAR（バーミキュラ） MADE IN JAPAN"></a></h1>
            <nav id="rpr_gnavi">
                <ul class="clearfix">
                    <li class="list navi01">
                        <span><img src="<?php echo $page_pass; ?>/img/gm/gnavi01.png" height="17" width="109" alt="製品ラインナップ"></span>
                        <ul class="dd_list">
                            <li class="list01"><a href="<?php echo $page_pass; ?>products/">オープンポットラウンド</a></li>
                            <li class="list02"><a href="<?php echo $page_pass; ?>products/ricepot/">ライスポット</a></li>
                            <li class="list03"><a href="<?php echo $page_pass; ?>products/kitchenitems/index.html">キッチンアイテム</a></li>
                            <li class="list04"><a href="<?php echo $page_pass; ?>products/food/">食べ物</a></li>
                            <li class="list05"><a href="http://shop.vermicular.jp/jp/group.php?id=12" class="ex_link white" target="_blank">レシピ本</a></li>
                        </ul>
                    </li>
                    <li class="list navi02">
                        <span><img src="<?php echo $page_pass; ?>/img/gm/gnavi02.png" height="17" width="90" alt="選ばれる理由"></span>
                        <ul class="dd_list">
                            <li class="list01"><a href="<?php echo $page_pass; ?>about/whatis/">暮らしを変える鍋</a></li>
                            <li class="list02"><a href="<?php echo $page_pass; ?>about/teshigoto/">手仕事とテクノロジー</a></li>
                            <li class="list03"><a href="<?php echo $page_pass; ?>support/">一生サポート</a></li>
                            <li class="list04"><a href="<?php echo $page_pass; ?>faq/">よくある質問</a></li>
                        </ul>
                    </li>
                    <li class="list navi03"><a href="https://owners.vermicular.jp/"><img src="<?php echo $page_pass; ?>/img/gm/gnavi03.png" height="17" width="37" alt="レシピ"></a></li>
                    <li class="list navi04">
                        <span><img src="<?php echo $page_pass; ?>/img/products/common/gnavi04.png" height="17" width="54" alt="サポート"></span>
                        <ul class="dd_list">
                            <li class="list01"><a href="<?php echo $page_pass; ?>support/">オーナーズデスク</a></li>
                            <li class="list02"><a href="<?php echo $page_pass; ?>faq/">よくある質問</a></li>
                        </ul>
                    </li>
                    <li class="list navi05"><a href="http://shop.vermicular.jp/jp/" class="ex_link white" target="_blank"><img src="<?php echo $page_pass; ?>/img/gm/gnavi05.png" height="17" width="110" alt="オンラインショップ"></a></li>
                    <li class="list navi06"><a href="http://www.vermicular.com/"><img src="<?php echo $page_pass; ?>/img/gm/gnavi06.png" height="17" width="50" alt="English"></a></li>
                </ul>
            </nav>
            <ul class="sns_list clearfix">
                <li class="sns01"><a href="https://www.facebook.com/vermicular" target="_blank"><img src="<?php echo $page_pass; ?>/img/gm/head_sns_icon_white01.png" height="16" width="17" alt="Facebook"></a></li>


                <li class="sns02"><a href="https://twitter.com/_VERMICULAR_" target="_blank"><img src="<?php echo $page_pass; ?>/img/gm/head_sns_icon_white02.png" height="16" width="17" alt="Twitter"></a></li>
                <li class="sns03"><a href="https://instagram.com/vermicular_japan/" target="_blank"><img src="<?php echo $page_pass; ?>/img/gm/head_sns_icon_white03.png" height="16" width="17" alt="Instagram"></a></li>
                <li class="sns04"><a href="http://shop.vermicular.jp/mail/" target="_blank"><img src="<?php echo $page_pass; ?>/img/gm/head_sns_icon_white04.png" height="16" width="17" alt="Mail"></a></li>



            </ul>
        </div>
    </header><!-- /#header -->
    <header id="page_title_wrap">
        <div id="kv_inner">
            <h1 class="product_name motion"><img src="../assets/img/hk_and_ph/kv_text.png?2" height="92" width="515" alt="Organic Cotton Series HEAT KEEPER & POT HOLDER オーガニックコットン ヒートキーパー & ポットホルダー"></h1>
            <p class="text motion">バーミキュラオリジナルの「ヒートキーパー」と「ポットホルダー」も、化学肥料を使わずに栽培した<br>
            ナチュラルでオーガニックな素材にこだわり、熟練した日本の職人が手仕事でつくりあげています。</p>
        </div>
        <div id="scroll_down">
            <div class="text"><img src="../../ricepot/img/products/common/scroll_down_text.png?2" height="11" width="36" alt="Scroll"></div>
            <div class="arrow"><img src="../../ricepot/img/products/common/scroll_down_arrow.png?2" height="18" width="36" alt=""></div>
        </div>
        <div class="movie">
            <video id="movie-video" class="movie__video" preload="metadata" loop autoplay muted></video>
            <div class="movie__backdrop"></div>
        </div>
    </header><!-- /#kv_wrap -->

    <div id="section_wrap01" class="js__scr-target">
        <section>
            <div class="text_wrap inner">
                <h1 class="copy">
                    <img src="../assets/img/hk_and_ph/tit_section01-1.png?2" height="132" width="306" alt="Organic Cotton HEAT KEEPER オーガニックコットン ヒートキーパー（保温カバー）">
                </h1>
                <p class="text">オーガニックコットン100%のキャンバス生地と分厚い中綿で、<br>
                普通の料理なら調理後約3時間、ご飯なら炊き上がり後約2時<br>
                間、温かいと感じる60度以上をキープ。自宅洗いもOKです。</p>
                <div class="btn clear_black w198"><a href="http://shop.vermicular.jp/jp/group.php?id=24" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../assets/img/top/onlineshop_text.png?2" height="11" width="99" alt="オンラインショップ"></span></span></a></div>
            </div>
            <span class="bg js__fade right" data-delay="0"><img src="../assets/img/hk_and_ph/img_section01-1.png?2" height="803" width="1011"></span>
        </section>
        <section>
            <div class="text_wrap inner">
                <h2 class="copy">
                    <img src="../assets/img/hk_and_ph/tit_section01-2.png?2" height="72" width="317" alt="おいしい料理を、おいしい温度でキープする。">
                </h2>
                <p class="text">「アツアツの一番おいしい状態で食べてもらいたい」。料理を作る全ての人の思いに応えたくて、驚くほど保温と断熱にすぐれた「ヒートキーパー（保温カバー）」を開発しました。<br>
                100℃に沸かしたお湯をバーミキュラに入れて3時間おいた場合、保温性の高いバーミキュラでも48℃まで下がったのに対して、「ヒートキーパー」に入れた場合は78℃をキープします。ご飯を炊いても、温かいと感じる60℃以上を約２時間保つことができます。</p>
            </div>
            <span class="bg"><img src="../assets/img/hk_and_ph/img_section01-2.jpg?2" height="391" width="580"></span>
        </section>
        <section class="inner clearfix">
            <div class="text_wrap">
                <h2 class="copy">
                    <img src="../assets/img/hk_and_ph/tit_section01-3.png?2" height="72" width="380" alt="バーミキュラなら、保温カバーさえ新しい調理器になる。">
                </h2>
                <p class="text">バーミキュラで加熱した料理を鍋ごと「ヒートキーパー」に入れておくだけで、高温の状態を長時間にわたってキープできるから、直火ともオーブンとも違う特別な加熱調理ができるのです。<br>
                    例えば、豚の角煮などは「ヒートキーパー」に入れておくだけで、肉の内部まで徐々に柔らかくなります。出汁が染み込んだアツアツのおでんや、プロの調理法「低温ロースト」でつくるジューシーなローストビーフ、茶碗蒸しやなめらかプリンなどのスチーム料理など、家庭のキッチンで難しいと思われていた高度な調理もカンタンに楽しめて、料理のバリエーションが驚くほど広がります。</p>
            </div>
            <div id="graph" class="image_wrap">
                <div class="image">
                    <img src="../assets/img/hk_and_ph/img_section01-3.jpg?2" height="292" width="580">
                    <div class="line1">
                        <img src="../assets/img/hk_and_ph/line_only.png?2" height="170" width="380">
                    </div>
                    <div class="line2">
                        <img src="../assets/img/hk_and_ph/line_other.png?2" height="111" width="380">
                    </div>
                    <div class="line3">
                        <img src="../assets/img/hk_and_ph/line_heatkeeper.png?2" height="66" width="380">
                    </div>
                </div>
                <ul class="clearfix">
                    <li>
                        <img src="../assets/img/hk_and_ph/img_section01-3-a.jpg?2" width="193" height="180" alt="豚の角煮">
                        <p class="title">豚の角煮</p>
                        <p class="text">（加熱25分+保温2時間）</p>
                    </li>
                    <li>
                        <img src="../assets/img/hk_and_ph/img_section01-3-b.jpg?2" width="193" height="180" alt="煮込みハンバーグ">
                        <p class="title">煮込みハンバーグ</p>
                        <p class="text">（加熱4分+保温1時間）</p>
                    </li>
                    <li>
                        <img src="../assets/img/hk_and_ph/img_section01-3-c.jpg?2" width="193" height="180" alt="肉じゃが">
                        <p class="title">肉じゃが</p>
                        <p class="text">（加熱8分+保温1時間）</p>
                    </li>
                </ul>
            </div>
        </section>
        <section class="feature">
            <div class="inner clearfix">
                <img class="image" src="../assets/img/hk_and_ph/img_feature.jpg?2" height="181" width="541">
                <div class="feature_wrap">
                    <h2 class="title"><img src="../assets/img/hk_and_ph/tit_feature.png?2" height="10" width="56" alt="Feature"></h2>
                    <p class="contents">
                        「ヒートキーパー」の底にはフラップがついていて、このフラップをボタン留めすることで全体がスリムにまとまり、引き出しや棚にもすっきり収まります。
                    </p>
                </div>
            </div>
        </section>
    </div><!-- /#item_line_up -->

    <section id="section_wrap02" class="js__scr-target">
        <div class="inner clearfix">
            <div class="text_wrap">
                <h1 class="copy"><img src="../assets/img/hk_and_ph/tit_section02-1.png?2" height="101" width="357" alt="Organic Cotton POT HOLDER オーガニックコットン ポットホルダー （鍋つかみ）"></h1>
                <p class="text">バーミキュラのダブルハンドルのためにデザインされた鍋つかみです。<br>
                日本の職人が時間をかけて丁寧に織り上げたオーガニックコットン100%のキャンバス生地は、しなやかで柔らかく丈夫です。</p>
                <div class="btn clear_black w198"><a href="http://shop.vermicular.jp/jp/group.php?id=25" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../assets/img/top/onlineshop_text.png?2" height="11" width="99" alt="オンラインショップ"></span></span></a></div>
            </div>
            <img class="image js__fade left" data-delay="0" src="../assets/img/hk_and_ph/img_section02.png?2" height="683" width="864" alt="POT HOLDER">
        </div>
        <div class="inner clearfix">
            <div class="text_wrap">
                <h1 class="copy"><img src="../assets/img/hk_and_ph/tit_section02-2.png?2" height="73" width="270" alt="バーミキュラオーナーが推薦する必需品。"></h1>
                <p class="text">小柄な日本の女性でも重さを感じにくいように設計されたバーミキュラの「ダブルハンドル」。その専用鍋つかみが、ポットホルダーです。蓋のつまみなどを持つ簡易ミトンとしても使えます。手をすっぽり入れるタイプのミトンと違い、付け外しの手間がなく、料理中でも片手ですっと取って使えます。フックに吊るしたり、立てて置いたりとキッチンでも置き場所に困りません。</p>
            </div>
            <div class="slide_wrap">
                <div class="slider">
                    <img class="image" src="../assets/img/hk_and_ph/img_section02_2_a.jpg?2" height="390" width="580">
                    <img class="image" src="../assets/img/hk_and_ph/img_section02_2_b.jpg?2" height="390" width="580">
                    <img class="image" src="../assets/img/hk_and_ph/img_section02_2_c.jpg?2" height="390" width="580">
                </div>
            </div>
        </div>
    </section><!-- /#section_wrap02 -->

    <section id="section_wrap03">
        <div class="inner clearfix">
            <div class="text_wrap">
                <p class="name"><span><img src="../assets/img/hk_and_ph/tit_organic01.png?2" height="20" width="240" alt="Organic Cotton Series"></span></p>
                <h1 class="copy"><img src="../assets/img/hk_and_ph/tit_organic02.png?2" height="72" width="410" alt="オーガニックコットン100%の素材と、職人の手仕事。"></h1>
                <p class="text">「ヒートキーパー」と「ポットホルダー」の生地は、ともに化学肥料を使わずに栽培したオーガニックコットン100%の糸を用いて、熟練した日本の職人の手仕事で丁寧に織り上げられています。ミシンの使えないディテール部分は手で縫い合わせるなど、バーミキュラの鍋と同様にいっさいの手間を惜しまずにつくられています。</p>
            </div>
            <img class="image" src="../assets/img/hk_and_ph/img_organic.jpg?2" height="340" width="540" alt="">
        </div>
    </section><!-- /#section_wrap03 -->

    <section class="lineup_wrap">
        <ul>
            <li>
                <img src="../assets/img/hk_and_ph/img_lineup01.jpg?2" height="213" width="400" alt="オーガニックコットン ヒートキーパー">
                <p class="text">
                    オーガニックコットン ヒートキーパー<br>
                    ¥10,250（税抜）
                </p>
                <div class="btn clear_black w198"><a href="http://shop.vermicular.jp/jp/group.php?id=24" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../assets/img/top/onlineshop_text.png?2" height="11" width="99" alt="オンラインショップ"></span></span></a></div>
            </li>
            <li>
                <img src="../assets/img/hk_and_ph/img_lineup02.jpg?2" height="213" width="400" alt="オーガニックコットン ポットホルダー">
                <p class="text">
                    オーガニックコットン ポットホルダー<br>
                    ¥4,000（税抜）/2個セット
                </p>
                <div class="btn clear_black w198"><a href="http://shop.vermicular.jp/jp/group.php?id=25" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../assets/img/top/onlineshop_text.png?2" height="11" width="99" alt="オンラインショップ"></span></span></a></div>
            </li>
        </ul>
    </section>

    <div class="top_items_wrap">
        <div class="inner">
            <div class="btn clear_black w198">
                <a href="../">
                    <span class="btn_inner">
                         <img src="../assets/img/btn_kitchenitems.png?2" height="10" width="113" alt="Kitchen Items TOP">
                    </span>
                </a>
            </div>
        </div>
    </div>

    <a id="online_btn" href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank">
        <img src="../../ricepot/common/img/btn_online.png?2" width="130" height="130" alt="Online Shop">
    </a>

<?php include $page_pass.'_footer.php'; ?>