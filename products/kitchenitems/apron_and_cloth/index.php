<?php
$page_id = 'apron_and_cloth'; //ページ独自にスタイルを指定する場合は入力
$page_type = 'kitchenitem'; // [ オーブンポッドラウンドのページの場合 「oven_pot_round」 ]
$page_title = 'APRON & KITCHEN CLOTH オーガニックリネン エプロン & キッチンクロス | Vermicular（バーミキュラ）'; //ページ名
$page_description = 'ナチュラルでオーガニックにこだわったキッチンアイテムが登場';
$page_keywords = 'バーミキュラ,Vermicular,鋳物,ホーロー鍋,無水調理,レシピ,通販,無水カレー,愛知ドビー,無水,ホーロー,パールホーロー,トリプルサーモーテクノロジー';
$page_pass = '../../../'; //自分の階層を指定
$page_directry = 'products/ricepot'; //自分のディレクトリ名を指定
$extra_css = '
<link rel="stylesheet" type="text/css" href="'.$page_pass.'css/products.css">
<link rel="stylesheet" type="text/css" href="'.$page_pass.'css/index_nr.css">
<link rel="stylesheet" type="text/css" href="../../ricepot/css/kitchenitem.css">
'; //何か追加で読み込みたいCSSがあればlinkタグごと記述
$extra_js = '
<script src="'.$page_pass.'common/js/scripts.js"></script>
<script src="'.$page_pass.'common/js/jquery.cookie.js"></script>
<script src="'.$page_pass.'common/js/lib.js"></script>
<script src="../../ricepot/js/kitchenitems.js"></script>
<script>
$(window).on("load", function() {
    if(divece!="pc"){
        $(".movie").attr("id", "bg");
    }else{
        if(!$("#movie-video")[0]) return false;
        
        var apronvisitCount;  //訪問回数
        cookieArr = $.cookie();
        if (cookieArr["apronvisitCount"] == null) {  //初訪問の時
            apronvisitCount = 1;
            $.cookie("apronvisitCount", apronvisitCount,{expires:7});
        } else {  //2回目以降の訪問の時
            apronvisitCount = $.cookie("apronvisitCount");
            apronvisitCount ++;
            $.cookie("apronvisitCount", apronvisitCount,{expires:7});
        }
        if(apronvisitCount % 2 == 0){
            var videoType = "<source src=../assets/img/apron_and_cloth/kv2.mp4?2 type=video/mp4>";
        }else{
            var videoType = "<source src=../assets/img/apron_and_cloth/kv.mp4?2 type=video/mp4>";
        }
        setTimeout(function(){
            var VideoSource = videoType;
            $("#movie-video").append(VideoSource);
        },500);
    }
});
</script>
'; //何か追加で読み込みたいJSがあればscriptタグごと記述
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ja" class="ie ie6" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 7]>   <html lang="ja" class="ie ie7" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 8]>   <html lang="ja" class="ie ie8" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 9]>   <html lang="ja" class="ie ie9" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="ja" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Style-Type" content="text/css">
    <meta http-equiv="Content-Script-Type" content="text/javascript">

    <?php
    if($page_pass==""){
        $layer = "";
    }else{
        $layer = $page_pass;
    }
    ?>

    <?php
    $ua=$_SERVER['HTTP_USER_AGENT'];
    $device = ((strpos($ua,'iPhone')!==false)||(strpos($ua,'iPod')!==false)||(strpos($ua,'Android')!==false)||(strpos($ua,'iPad')!==false));
    if ($device == true){
        $device = 'other';
    }
    ?>
    <?php
    if($device == "other"){
        echo '<meta name="viewport" content="width=1280">';
    }else{
        echo '<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1280">';
    }
    ?>

    <link rel="canonical" href="http://www.vermicular.jp/<?php echo $page_directry; ?>">

    <title><?php echo $page_title; ?></title>
    <meta name="author" content="愛知ドビー株式会社">

    <meta name="description" content="<?php echo $page_description; ?>">
    <meta name="keywords" content="<?php echo $page_keywords; ?>">


    <meta name="format-detection" content="telephone=no,address=no,email=no">

    <link rel="stylesheet" type="text/css" href="<?php echo $page_pass; ?>common/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $page_pass; ?>common/css/styles.css">
    <?php echo $extra_css; ?>

    <link href="<?php echo $page_pass; ?>common/img/apple-touch-icon.png" rel="apple-touch-icon">
    <meta name="apple-mobile-web-app-title" content="Vermicular">

    <script src="<?php echo $page_pass; ?>common/js/jquery-1.11.0.min.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/jquery.transit.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/pace.min.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/jquery.colorbox-min.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/var.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/function.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/scripts.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/jquery.bxslider.min.js"></script>
    <?php echo $extra_js; ?>

    <!--[if lt IE 9]>
    <script src="<?php echo $page_pass; ?>common/js/html5shiv.js"></script>
    <script type="text/javascript" src="<?php echo $page_pass; ?>common/js/jquery.belatedPNG.min.js"></script>
    <script>
        $(function() {
            $(".pngfix").fixPng();
        });
    </script>
    <script src="<?php echo $page_pass; ?>common/js/jquery.backgroundSize.js"></script>
    <script>
        $(function(){
            $('#header #bg').css('background-size', 'cover');
        });
    </script>
    <![endif]-->
</head>
<body<?php echo ($page_id)? ' id="'.$page_id.'"':''; ?>>
<div id="document"<?php echo ($device == "pc")?' class="device_pc"':''; ?>>
    <header id="rpr_header">
        <div id="rpr_header_inner" class="clearfix">
            <h1 id="rpr_site_logo"><a href="<?php echo $page_pass; ?>"><img src="<?php echo $page_pass; ?>/img/gm/site_logo.png" height="27" width="143" alt="VERMICULAR（バーミキュラ） MADE IN JAPAN"></a></h1>
            <nav id="rpr_gnavi">
                <ul class="clearfix">
                    <li class="list navi01">
                        <span><img src="<?php echo $page_pass; ?>/img/gm/gnavi01.png" height="17" width="109" alt="製品ラインナップ"></span>
                        <ul class="dd_list">
                            <li class="list01"><a href="<?php echo $page_pass; ?>products/">オープンポットラウンド</a></li>
                            <li class="list02"><a href="<?php echo $page_pass; ?>products/ricepot/">ライスポット</a></li>
                            <li class="list03"><a href="<?php echo $page_pass; ?>products/kitchenitems/index.html">キッチンアイテム</a></li>
                            <li class="list04"><a href="<?php echo $page_pass; ?>products/food/">食べ物</a></li>
                            <li class="list05"><a href="http://shop.vermicular.jp/jp/group.php?id=12" class="ex_link white" target="_blank">レシピ本</a></li>
                        </ul>
                    </li>
                    <li class="list navi02">
                        <span><img src="<?php echo $page_pass; ?>/img/gm/gnavi02.png" height="17" width="90" alt="選ばれる理由"></span>
                        <ul class="dd_list">
                            <li class="list01"><a href="<?php echo $page_pass; ?>about/whatis/">暮らしを変える鍋</a></li>
                            <li class="list02"><a href="<?php echo $page_pass; ?>about/teshigoto/">手仕事とテクノロジー</a></li>
                            <li class="list03"><a href="<?php echo $page_pass; ?>support/">一生サポート</a></li>
                            <li class="list04"><a href="<?php echo $page_pass; ?>faq/">よくある質問</a></li>
                        </ul>
                    </li>
                    <li class="list navi03"><a href="https://owners.vermicular.jp/"><img src="<?php echo $page_pass; ?>/img/gm/gnavi03.png" height="17" width="37" alt="レシピ"></a></li>
                    <li class="list navi04">
                        <span><img src="<?php echo $page_pass; ?>/img/products/common/gnavi04.png" height="17" width="54" alt="サポート"></span>
                        <ul class="dd_list">
                            <li class="list01"><a href="<?php echo $page_pass; ?>support/">オーナーズデスク</a></li>
                            <li class="list02"><a href="<?php echo $page_pass; ?>faq/">よくある質問</a></li>
                        </ul>
                    </li>
                    <li class="list navi05"><a href="http://shop.vermicular.jp/jp/" class="ex_link white" target="_blank"><img src="<?php echo $page_pass; ?>/img/gm/gnavi05.png" height="17" width="110" alt="オンラインショップ"></a></li>
                    <li class="list navi06"><a href="http://www.vermicular.com/"><img src="<?php echo $page_pass; ?>/img/gm/gnavi06.png" height="17" width="50" alt="English"></a></li>
                </ul>
            </nav>
            <ul class="sns_list clearfix">
                <li class="sns01"><a href="https://www.facebook.com/vermicular" target="_blank"><img src="<?php echo $page_pass; ?>/img/gm/head_sns_icon_white01.png" height="16" width="17" alt="Facebook"></a></li>


                <li class="sns02"><a href="https://twitter.com/_VERMICULAR_" target="_blank"><img src="<?php echo $page_pass; ?>/img/gm/head_sns_icon_white02.png" height="16" width="17" alt="Twitter"></a></li>
                <li class="sns03"><a href="https://instagram.com/vermicular_japan/" target="_blank"><img src="<?php echo $page_pass; ?>/img/gm/head_sns_icon_white03.png" height="16" width="17" alt="Instagram"></a></li>
                <li class="sns04"><a href="http://shop.vermicular.jp/mail/" target="_blank"><img src="<?php echo $page_pass; ?>/img/gm/head_sns_icon_white04.png" height="16" width="17" alt="Mail"></a></li>



            </ul>
        </div>
    </header><!-- /#header -->
    <header id="page_title_wrap">
        <div id="kv_inner">
            <h1 class="product_name motion"><img src="../assets/img/apron_and_cloth/kv_text.png?2" height="92" width="464" alt="APRON & KITCHEN CLOTH オーガニックリネン エプロン & キッチンクロス"></h1>
            <p class="text motion">希少性の高い上質なオーガニックリネン100%の糸を使って、日本の職人が <br>
            手間ひまを惜しまずに織り上げたメイド・イン・ジャパンのエプロンとキッチンクロスです。</p>
        </div>
        <div id="scroll_down">
            <div class="text"><img src="../../ricepot/img/products/common/scroll_down_text.png?2" height="11" width="36" alt="Scroll"></div>
            <div class="arrow"><img src="../../ricepot/img/products/common/scroll_down_arrow.png?2" height="18" width="36" alt=""></div>
        </div>
        <div class="movie">
            <video id="movie-video" class="movie__video" preload="metadata" loop autoplay muted></video>
            <div class="movie__backdrop"></div>
        </div>
    </header><!-- /#kv_wrap -->

    <section>
        <div id="section_wrap01" class="js__scr-target">
            <div class="text_wrap inner">
                <h1 class="copy">
                    <img src="../assets/img/apron_and_cloth/tit_section01-1.png?2" height="100" width="207" alt="Organic Linen APRON オーガニックリネン エプロン">
                </h1>
                <p class="text">オーガニックリネン100%の上質な生地を使用した<br>
                柔らかな肌触りのエプロン。とても細かい糸の密度<br>
                を極限まで高めて職人が織っています。<br>
                桜の花弁、炭、亜麻、自然素材の持つ優しい発色を<br>
                活かした3色展開です。</p>
                <div class="btn clear_black w198"><a href="http://shop.vermicular.jp/jp/group.php?id=20" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../assets/img/top/onlineshop_text.png?2" height="11" width="99" alt="オンラインショップ"></span></span></a></div>
            </div>
            <span class="bg js__r-fade right top" data-delay="0"><img src="../assets/img/apron_and_cloth/img_section01-1.jpg?2" height="780" width="938"></span>
        </div>
        <div id="section_wrap02">
            <div class="text_wrap inner">
                <h2 class="copy">
                    <img src="../assets/img/apron_and_cloth/tit_section01-2.png?2" height="28" width="472" alt="日本の色を映したナチュラルな草木染め。">
                </h2>
                <p class="text">桜の花弁を使って染色した女性らしい「Sakura」、炭で染め上げたシックな「Sumi」、素材本来の亜麻色を活かした<br>
                「Natural」すべて天然素材ならではの優しい発色で、色落ちしにくいのが特徴です。さらに、生地が薄手で軽く、足さばきも良いのが魅力です。</p>
            </div>
            <ul>
                <li>
                    <img src="../assets/img/apron_and_cloth/img_color01.jpg?2" height="365" width="126" alt="Sakura［桜染］">
                    <p>Sakura［桜染］</p>
                </li>
                <li>
                    <img src="../assets/img/apron_and_cloth/img_color02.jpg?2" height="365" width="126" alt="Sumi［炭染］">
                    <p>Sumi［炭染］</p>
                </li>
                <li>
                    <img src="../assets/img/apron_and_cloth/img_color03.jpg?2" height="365" width="126" alt="Natural［無染色］">
                    <p>Natural［無染色］</p>
                </li>
            </ul>
        </div>
    </section>
    <section id="feature">
        <h2 class="title"><img src="../assets/img/apron_and_cloth/tit_feature.png?2" height="10" width="56" alt="Feature"></h2>
        <ul>
            <li>
                <img src="../assets/img/apron_and_cloth/img_feature01.jpg?2" alt="２つのポケットにポットホルダー" width="182" height="245">
                <p>飽きの来ないシンプルなデザインで、２つのポケットにはポットホルダーが入ります。</p>
            </li>
            <li>
                <img src="../assets/img/apron_and_cloth/img_feature02.jpg?2" alt="スタイリッシュな印象" width="182" height="245">
                <p>後ろで交差するデザインでヒップの上部まで隠れるので、スタイリッシュな印象です。</p>
            </li>
            <li>
                <img src="../assets/img/apron_and_cloth/img_feature03.jpg?2" alt="ギャルソンタイプとしても使えます。" width="182" height="245">
                <p>エプロンの上部を内側に折り返せば、ギャルソンタイプとしても使えます。</p>
            </li>
            <li>
                <img src="../assets/img/apron_and_cloth/img_feature04.jpg?2" alt="エプロンの長さを調節できます。" width="182" height="245">
                <p>首のストラップを適当な長さに結べば、エプロンの長さを調節できます。</p>
            </li>
            <li>
                <img src="../assets/img/apron_and_cloth/img_feature05.jpg?2" alt="キッチンクロスを引っ掛けれます。" width="182" height="245">
                <p>腰のループにキッチンクロスを引っ掛けて頂くこともできます。</p>
            </li>
        </ul>
    </section>

    <section id="section_wrap03" class="js__scr-target">
        <div class="inner clearfix">
            <div class="text_wrap">
                <h1 class="copy"><img src="../assets/img/apron_and_cloth/tit_section02-1.png?2" height="99" width="368" alt="Organic Linen KITCHEN CLOTH オーガニックリネン キッチンクロス"></h1>
                <p class="text">目指したのは、10年以上愛用いただけるキッチンクロス。100年以上続<br>
                く老舗機屋の職人がゆっくりと密度高く織る麻の生地は、使い込むほど<br>
                に柔らかく肌なじみのよいクロスに育ちます。</p>
                <div class="btn clear_black w198"><a href="http://shop.vermicular.jp/jp/group.php?id=27" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../assets/img/top/onlineshop_text.png?2" height="11" width="99" alt="オンラインショップ"></span></span></a></div>
            </div>
            <img class="image js__r-fade left top" data-delay="0" src="../assets/img/apron_and_cloth/img_section02-1.png?2" height="758" width="874">
        </div>
        <div class="inner clearfix">
            <div class="text_wrap">
                <h1 class="copy"><img src="../assets/img/apron_and_cloth/tit_section02-2.png?2" height="73" width="359" alt="目指したのは、10年以上使えるキッチンクロス。"></h1>
                <p class="text">毎日の使用に耐えうる丈夫なキッチンクロスをつくるため、２本の糸を１本にした「２本双糸」を経糸にも横糸にも使っています。色は、生成りの「ナチュラル」と真っ白な「ホワイト」の２種類の生地に、赤・青のラインやグリーンのストライプをあしらっています。</p>
            </div>
            <div class="slide_wrap">
                <div class="slider">
                    <img class="image" src="../assets/img/apron_and_cloth/img_section02_2_a.jpg?2" height="390" width="580">
                    <img class="image" src="../assets/img/apron_and_cloth/img_section02_2_b.jpg?2" height="390" width="580">
                    <img class="image" src="../assets/img/apron_and_cloth/img_section02_2_c.jpg?2" height="390" width="580">
                </div>
            </div>
        </div>
    </section><!-- /#section_wrap03 -->

    <section id="section_wrap04">
        <div class="inner clearfix">
            <div class="text_wrap">
                <p class="name"><span><img src="../assets/img/apron_and_cloth/tit_organic01.png?2" height="20" width="240" alt="Organic Linen Series"></span></p>
                <h1 class="copy"><img src="../assets/img/apron_and_cloth/tit_organic02.png?2" height="72" width="365" alt="オーガニックリネン100%の糸をさらに強く、美しく。"></h1>
                <p class="text">バーミキュラの「エプロン」と「キッチンクロス」に使われるのは、化学肥料を用いずに栽培された希少性の高い上質なオーガニックリネン糸で、吸水性・速乾性・耐久性に大変優れています。このオーガニック100%のリネン糸に、熟練した職人が空気を含ませながらゆっくりと密度高く織り上げるので、とても肌触りの良い質感の生地が出来上がります。</p>
            </div>
            <img class="image" src="../assets/img/apron_and_cloth/img_organic.jpg?2" height="340" width="540">
        </div>
    </section><!-- /#section_wrap03 -->

    <section class="lineup_wrap">
        <ul>
            <li>
                <img src="../assets/img/apron_and_cloth/img_lineup01.png?2" height="215" width="285" alt="オーガニックリネン エプロン">
                <p class="text">
                    オーガニックリネン エプロン<br>
                    ¥10,500（税抜）〜
                </p>
                <div class="btn clear_black w198"><a href="http://shop.vermicular.jp/jp/group.php?id=20" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../assets/img/top/onlineshop_text.png?2" height="11" width="99" alt="オンラインショップ"></span></span></a></div>
            </li>
            <li>
                <img src="../assets/img/apron_and_cloth/img_lineup02.png?2" height="215" width="285" alt="オーガニックリネン キッチンクロス">
                <p class="text">
                    オーガニックリネン キッチンクロス<br>
                    各 ¥3,000（税抜）
                </p>
                <div class="btn clear_black w198"><a href="http://shop.vermicular.jp/jp/group.php?id=27" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../assets/img/top/onlineshop_text.png?2" height="11" width="99" alt="オンラインショップ"></span></span></a></div>
            </li>
        </ul>
    </section>

    <div class="top_items_wrap">
        <div class="inner">
            <div class="btn clear_black w198">
                <a href="../">
                    <span class="btn_inner">
                         <img src="../assets/img/btn_kitchenitems.png?2" height="10" width="113" alt="Kitchen Items TOP">
                    </span>
                </a>
            </div>
        </div>
    </div>

    <a id="online_btn" href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank">
        <img src="../../ricepot/common/img/btn_online.png?2" width="130" height="130" alt="Online Shop">
    </a>

<?php include $page_pass.'_footer.php'; ?>