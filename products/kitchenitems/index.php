<?php
$page_id = 'kitchenitem_top'; //ページ独自にスタイルを指定する場合は入力
$page_type = 'kitchenitem'; // [ オーブンポッドラウンドのページの場合 「oven_pot_round」 ]
$page_title = 'Kitchen Items | Vermicular（バーミキュラ）'; //ページ名
$page_description = 'ナチュラルでオーガニックにこだわったキッチンアイテムが登場';
$page_keywords = 'バーミキュラ,Vermicular,鋳物,ホーロー鍋,無水調理,レシピ,通販,無水カレー,愛知ドビー,無水,ホーロー,パールホーロー,トリプルサーモーテクノロジー';
$page_pass = '../../'; //自分の階層を指定
$page_directry = 'products/ricepot'; //自分のディレクトリ名を指定
$extra_css = '
<link rel="stylesheet" type="text/css" href="'.$page_pass.'css/products.css">
<link rel="stylesheet" type="text/css" href="'.$page_pass.'css/index_nr.css">
<link rel="stylesheet" type="text/css" href="../ricepot/css/kitchenitem.css">
'; //何か追加で読み込みたいCSSがあればlinkタグごと記述
$extra_js = '
<script src="'.$page_pass.'common/js/scripts.js"></script>
<script src="'.$page_pass.'common/js/lib.js"></script>
<script src="../ricepot/js/kitchenitems.js"></script>
'; //何か追加で読み込みたいJSがあればscriptタグごと記述
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ja" class="ie ie6" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 7]>   <html lang="ja" class="ie ie7" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 8]>   <html lang="ja" class="ie ie8" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 9]>   <html lang="ja" class="ie ie9" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="ja" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Style-Type" content="text/css">
    <meta http-equiv="Content-Script-Type" content="text/javascript">

    <?php
    if($page_pass==""){
        $layer = "";
    }else{
        $layer = $page_pass;
    }
    ?>

    <?php
    $ua=$_SERVER['HTTP_USER_AGENT'];
    $device = ((strpos($ua,'iPhone')!==false)||(strpos($ua,'iPod')!==false)||(strpos($ua,'Android')!==false)||(strpos($ua,'iPad')!==false));
    if ($device == true){
        $device = 'other';
    }
    ?>
    <?php
    if($device == "other"){
        echo '<meta name="viewport" content="width=1280">';
    }else{
        echo '<meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=1280">';
    }
    ?>

    <link rel="canonical" href="http://www.vermicular.jp/<?php echo $page_directry; ?>">

    <title><?php echo $page_title; ?></title>
    <meta name="author" content="愛知ドビー株式会社">

    <meta name="description" content="<?php echo $page_description; ?>">
    <meta name="keywords" content="<?php echo $page_keywords; ?>">


    <meta name="format-detection" content="telephone=no,address=no,email=no">

    <link rel="stylesheet" type="text/css" href="<?php echo $page_pass; ?>common/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $page_pass; ?>common/css/styles.css">
    <?php echo $extra_css; ?>

    <link href="<?php echo $page_pass; ?>common/img/apple-touch-icon.png" rel="apple-touch-icon">
    <meta name="apple-mobile-web-app-title" content="Vermicular">

    <script src="<?php echo $page_pass; ?>common/js/jquery-1.11.0.min.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/jquery.transit.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/pace.min.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/jquery.colorbox-min.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/var.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/function.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/scripts.js"></script>
    <script src="<?php echo $page_pass; ?>common/js/jquery.bxslider.min.js"></script>
    <?php echo $extra_js; ?>

    <!--[if lt IE 9]>
    <script src="<?php echo $page_pass; ?>common/js/html5shiv.js"></script>
    <script type="text/javascript" src="<?php echo $page_pass; ?>common/js/jquery.belatedPNG.min.js"></script>
    <script>
        $(function() {
            $(".pngfix").fixPng();
        });
    </script>
    <script src="<?php echo $page_pass; ?>common/js/jquery.backgroundSize.js"></script>
    <script>
        $(function(){
            $('#header #bg').css('background-size', 'cover');
        });
    </script>
    <![endif]-->
</head>
<body<?php echo ($page_id)? ' id="'.$page_id.'"':''; ?>>
<div id="document"<?php echo ($device == "pc")?' class="device_pc"':''; ?>>
    <header id="rpr_header">
        <div id="rpr_header_inner" class="clearfix">
            <h1 id="rpr_site_logo"><a href="<?php echo $page_pass; ?>"><img src="<?php echo $page_pass; ?>/img/gm/site_logo.png" height="27" width="143" alt="VERMICULAR（バーミキュラ） MADE IN JAPAN"></a></h1>
            <nav id="rpr_gnavi">
                <ul class="clearfix">
                    <li class="list navi01">
                        <span><img src="<?php echo $page_pass; ?>/img/gm/gnavi01.png" height="17" width="109" alt="製品ラインナップ"></span>
                        <ul class="dd_list">
                            <li class="list01"><a href="<?php echo $page_pass; ?>products/">オープンポットラウンド</a></li>
                            <li class="list02"><a href="<?php echo $page_pass; ?>products/ricepot/">ライスポット</a></li>
                            <li class="list03"><a href="<?php echo $page_pass; ?>products/kitchenitems/index.html">キッチンアイテム</a></li>
                            <li class="list04"><a href="<?php echo $page_pass; ?>products/food/">食べ物</a></li>
                            <li class="list05"><a href="http://shop.vermicular.jp/jp/group.php?id=12" class="ex_link white" target="_blank">レシピ本</a></li>
                        </ul>
                    </li>
                    <li class="list navi02">
                        <span><img src="<?php echo $page_pass; ?>/img/gm/gnavi02.png" height="17" width="90" alt="選ばれる理由"></span>
                        <ul class="dd_list">
                            <li class="list01"><a href="<?php echo $page_pass; ?>about/whatis/">暮らしを変える鍋</a></li>
                            <li class="list02"><a href="<?php echo $page_pass; ?>about/teshigoto/">手仕事とテクノロジー</a></li>
                            <li class="list03"><a href="<?php echo $page_pass; ?>support/">一生サポート</a></li>
                            <li class="list04"><a href="<?php echo $page_pass; ?>faq/">よくある質問</a></li>
                        </ul>
                    </li>
                    <li class="list navi03"><a href="https://owners.vermicular.jp/"><img src="<?php echo $page_pass; ?>/img/gm/gnavi03.png" height="17" width="37" alt="レシピ"></a></li>
                    <li class="list navi04">
                        <span><img src="<?php echo $page_pass; ?>/img/products/common/gnavi04.png" height="17" width="54" alt="サポート"></span>
                        <ul class="dd_list">
                            <li class="list01"><a href="<?php echo $page_pass; ?>support/">オーナーズデスク</a></li>
                            <li class="list02"><a href="<?php echo $page_pass; ?>faq/">よくある質問</a></li>
                        </ul>
                    </li>
                    <li class="list navi05"><a href="http://shop.vermicular.jp/jp/" class="ex_link white" target="_blank"><img src="<?php echo $page_pass; ?>/img/gm/gnavi05.png" height="17" width="110" alt="オンラインショップ"></a></li>
                    <li class="list navi06"><a href="http://www.vermicular.com/"><img src="<?php echo $page_pass; ?>/img/gm/gnavi06.png" height="17" width="50" alt="English"></a></li>
                </ul>
            </nav>
            <ul class="sns_list clearfix">
                <li class="sns01"><a href="https://www.facebook.com/vermicular" target="_blank"><img src="<?php echo $page_pass; ?>/img/gm/head_sns_icon_white01.png" height="16" width="17" alt="Facebook"></a></li>


                <li class="sns02"><a href="https://twitter.com/_VERMICULAR_" target="_blank"><img src="<?php echo $page_pass; ?>/img/gm/head_sns_icon_white02.png" height="16" width="17" alt="Twitter"></a></li>
                <li class="sns03"><a href="https://instagram.com/vermicular_japan/" target="_blank"><img src="<?php echo $page_pass; ?>/img/gm/head_sns_icon_white03.png" height="16" width="17" alt="Instagram"></a></li>
                <li class="sns04"><a href="http://shop.vermicular.jp/mail/" target="_blank"><img src="<?php echo $page_pass; ?>/img/gm/head_sns_icon_white04.png" height="16" width="17" alt="Mail"></a></li>



            </ul>
        </div>
    </header><!-- /#header -->

    <header id="kv_wrap">
        <div id="kv_inner">
            <h1 class="product_name motion"><img src="assets/img/top/tit_main.svg" height="100%" width="100%" alt="VERMICULAR "></h1>
        </div>
        <span id="bg"></span>
        <div id="scroll_down">
            <div class="text"><img src="../ricepot/img/products/common/scroll_down_text.png" height="11" width="36" alt="Scroll"></div>
            <div class="arrow"><img src="../ricepot/img/products/common/scroll_down_arrow.png" height="18" width="36" alt=""></div>
        </div>
    </header><!-- /#kv_wrap -->

    <section id="item_line_up">
        <div class="inner clearfix">
            <h1 class="copy">
                <img src="assets/img/top/tit_section01.png" height="25" widt="324" alt="Kitchen Item Line up" width="260">
            </h1>
            <p class="text">オーガニックな素材を厳選して、熟練した日本の職人の手仕事でつくられたバーミキュラのキッチンアイテムをご紹介します。</p>
            <ul class="item_list clearfix">
                <li>
                    <a class="hover" href="http://shop.vermicular.jp/jp/group.php?id=25" target="_blank">
                        <span class="image"><img src="assets/img/top/img_lineup01.jpg" alt="オーガニックコットンポットホルダー" width="175" height="205"></span>
                        <p>オーガニックコットン<br>ポットホルダー</p>
                    </a>
                </li>
                <li>
                    <a class="hover" href="http://shop.vermicular.jp/jp/group.php?id=26" target="_blank">
                        <span class="image"><img src="assets/img/top/img_lineup02.jpg" alt="マグネット トリベット" width="175" height="205"></span>
                        <p>マグネット トリベット</p>
                    </a>
                </li>
                <li>
                    <a class="hover" href="http://shop.vermicular.jp/jp/group.php?id=24" target="_blank">
                        <span class="image"><img src="assets/img/top/img_lineup03.jpg" alt="オーガニックコットンヒートキーパー" width="205" height="205"></span>
                        <p>オーガニックコットン<br>
                            ヒートキーパー</p>
                    </a>
                </li>
                <li>
                    <a class="hover" href="http://shop.vermicular.jp/jp/group.php?id=20" target="_blank">
                        <span class="image"><img src="assets/img/top/img_lineup04.jpg" alt="オーガニックリネンエプロン" width="205" height="205"></span>
                        <p>オーガニックリネン<br>
                            エプロン</p>
                    </a>
                </li>
                <li>
                    <a class="hover" href="http://shop.vermicular.jp/jp/group.php?id=27" target="_blank">
                        <span class="image"><img src="assets/img/top/img_lineup05.jpg" alt="オーガニックリネンキッチンクロス" width="205" height="205"></span>
                        <p>オーガニックリネン<br>
                            キッチンクロス</p>
                    </a>
                </li>
            </ul>
            <div class="btn_wrap">
                <div class="btn clear_black w198 center left"><a href="#modal" class="modal_open"><span class="btn_inner"><img src="assets/img/top/btn_concept.png" height="11" width="54" alt="コンセプト"></span></a></div>
                <div class="btn clear_black w198 center right"><a href="http://shop.vermicular.jp/jp/" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="assets/img/top/onlineshop_text.png" height="11" width="99" alt="オンラインショップ"></span></span></a></div>
            </div>
        </div>
    </section><!-- /#item_line_up -->

    <section id="section_wrap01" class="bg_section hover">
        <a href="magnettrivet/">
        <div class="inner clearfix">
            <div class="text_wrap">
                <h1 class="copy"><img src="assets/img/top/section_copy01.png" height="70" width="186" alt="バーミキュラを、すばやく食卓へ。"></h1>
                <p class="text">バーミキュラの鍋にフィットするデザインで、<br>
                天然の無垢材を使い、熟練した職人の<br>
                手仕事でつくられた鍋敷きです。<br>
                マグネット内蔵で着脱が自由だから、<br>
                できたての料理を鍋に入れたまま<br>
                素早く食卓に出して食事を楽しめます。</p>
                <p class="link"><img src="../ricepot/img/products/common/read_more_link_text.png" height="10" width="78" alt="Read More"></p>
            </div>
        </div>
        </a>
    </section><!-- /#section_wrap01 -->

    <section id="section_wrap02" class="bg_section hover">
        <a href="hk_and_ph/">
            <div class="inner clearfix">
                <div class="text_wrap">
                    <h1 class="copy"><img src="assets/img/top/section_copy02.png" height="70" width="305" alt="おいしい料理を、おいしい温度でキープする。"></h1>
                    <p class="text">バーミキュラで素材の味が引き出された料理を、<br>
                    アツアツの一番おいしい状態で食べてもらいたい。<br>
                    保温性と断熱性にすぐれた「ヒートキーパー」を<br>
                    使用すれば、約2時間もの間、高い温度をキープします。</p>
                    <p class="link"><img src="../ricepot/img/products/common/read_more_link_text.png" height="10" width="78" alt="Read More"></p>
                </div>
            </div>
        </a>
    </section><!-- /#section_wrap02 -->

    <section id="section_wrap03" class="bg_section hover">
        <a href="apron_and_cloth/">
            <div class="inner clearfix">
                <div class="text_wrap">
                    <h1 class="copy"><img src="assets/img/top/section_copy03.png" height="70" width="320" alt="素材が良いから、長く使えて、ずっと愛せる。"></h1>
                    <p class="text">オーガニックリネン100%の上質な生地を使用した<br>
                    柔らかな肌触りのエプロンとキッチンクロス。<br>
                    素材そのままの良さを活かしたデザインで、とても<br>
                    細い糸の密度を極限まで高めて職人が織っています。</p>
                    <p class="link"><img src="../ricepot/img/products/common/read_more_link_text_black.png" height="10" width="78" alt="Read More"></p>
                </div>
            </div>
        </a>
    </section><!-- /#section_wrap03 -->

    <div class="banner_section">
        <ul class="banner_list clearfix">
            <li>
                <dl>
                    <dt>
                        <a href="http://www.vermicular.jp/products/food/">
                            <img src="assets/img/top/banner_img01.jpg" height="130" width="386" alt="Food">
                        </a>
                    </dt>
                    <dd>
                        バーミキュラに最適な厳選された食材をご紹介します。
                    </dd>
                </dl>
            </li>
            <li>
                <dl>
                    <dt>
                        <a href="http://shop.vermicular.jp/jp/group.php?id=12">
                            <img src="assets/img/top/banner_img02.jpg" height="130" width="386" alt="Books">
                        </a>
                    </dt>
                    <dd>
                        バーミキュラを楽しむための専用レシピブックです。
                    </dd>
                </dl>
            </li>
        </ul>
    </div>

    <a id="online_btn" href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank">
        <img src="../ricepot/common/img/btn_online.png" width="130" height="130" alt="Online Shop">
    </a>

    <div id="modal">
        <div id="modal_content">
            <p class="title">
                <img src="assets/img/top/modal_title.png" height="208" width="516" alt="VERMICULAR KITCHEN ITEMS Natural,Organic,Made In Japan">
            </p>
            <p class="text">
                料理がもっと楽しくなるように、バーミキュラは鍋づくり同様、<br>
                キッチンアイテムも妥協しません。<br>
                厳選したオーガニック素材、日本の職人たちの丁寧な手仕事、<br>
                バーミキュラのためのデザインが生む究極の使いやすさと美しさ。<br>
                どれも長く愛用いただける自信作です。
            </p>
            <div class="btn clear_black w198 center"><a class="modal_close"><span class="btn_inner"><img src="assets/img/top/btn_close.png" height="12" width="34" alt="閉じる"></span></a></div>
        </div>
    </div>
    <div id="modal_overlay"></div>

<?php include $page_pass.'_footer.php'; ?>