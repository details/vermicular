// require jQuery JavaScript Library v1.10.1

/* 即時関数でローカル化 */
$(function(){

/* ============================================================
 * defineFoundationVariable
 * ============================================================ */
	// Variables
	var $doc = $(document),
		$win = $(window),
		winW = $win.width(),
		winH = $win.height();

	var ua = navigator.userAgent;
	var isIE = ua.match(/msie/i),
		isIE6 = ua.match(/msie [6.]/i),
		isIE7 = ua.match(/msie [7.]/i),
		isIE8 = ua.match(/msie [8.]/i),
		isIE9 = ua.match(/msie [9.]/i),
		isIE10 = ua.match(/msie [10.]/i);

	var smpUa = {
		iPhone: navigator.userAgent.indexOf('iPhone') != -1,
		iPad: navigator.userAgent.indexOf('iPad') != -1,
		iPod: navigator.userAgent.indexOf('iPod') != -1,
		android: navigator.userAgent.indexOf('Android') != -1,
		windows: navigator.userAgent.indexOf('Windows Phone') != -1
	}

	var $silder = $("#slider");

/*===================================================================================
	[transitionMotion ★★★★★　任意の名称へ変更して下さい　★★★★★ ]
===================================================================================*/

function transitionMotion(){
	var easing = 'easeInOutQuad';
	//transition未対応ブラウザはanimateへ変更
	if (!$.support.transition){
		$.fn.transition = $.fn.animate;
	}

	$("★★★ target ★★★").transition({opacity:1,top:0}, 600, easing);
}


/* ============================================================
 * Execute JavaScript when the Window Object is fully loaded.
 * ============================================================ */

	$doc.on('ready', function() {
	});// ready End

	$win.on('load', function() {
		if($("#spec-slider")[0]){
			var kvSlider = $('#spec-slider').bxSlider();
			kvSlider.reloadSlider({
				mode: 'fade',
				pager: false,
				auto: true,
				pause:6000,
				speed:2000,
				touchEnabled: false
			});
		}
	});// load End

	//$win.on('resize', function(){
	$win.resize(function(){
		var winW = $win.width(),
			winH = $win.height();
	});// resize End

});//End