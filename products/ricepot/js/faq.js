// require jQuery JavaScript Library v1.10.1

/* 即時関数でローカル化 */
$(function(){

/* ============================================================
 * defineFoundationVariable
 * ============================================================ */
	// Variables
	var $doc = $(document),
		$win = $(window),
		winW = $win.width(),
		winH = $win.height();

	var ua = navigator.userAgent;
	var isIE = ua.match(/msie/i),
		isIE6 = ua.match(/msie [6.]/i),
		isIE7 = ua.match(/msie [7.]/i),
		isIE8 = ua.match(/msie [8.]/i),
		isIE9 = ua.match(/msie [9.]/i),
		isIE10 = ua.match(/msie [10.]/i);

	var smpUa = {
		iPhone: navigator.userAgent.indexOf('iPhone') != -1,
		iPad: navigator.userAgent.indexOf('iPad') != -1,
		iPod: navigator.userAgent.indexOf('iPod') != -1,
		android: navigator.userAgent.indexOf('Android') != -1,
		windows: navigator.userAgent.indexOf('Windows Phone') != -1
	}

	var $silder = $("#slider");

/*===================================================================================
	[transitionMotion ★★★★★　任意の名称へ変更して下さい　★★★★★ ]
===================================================================================*/

function transitionMotion(){
	var easing = 'easeInOutQuad';
	//transition未対応ブラウザはanimateへ変更
	if (!$.support.transition){
		$.fn.transition = $.fn.animate;
	}

	$("★★★ target ★★★").transition({opacity:1,top:0}, 600, easing);
}

/*===================================================================================
	[ faqAccodion ]
===================================================================================*/

function faqAccodion(){
	 $("#faq_list_wrap li h3").on("click", function() {
	 	$("#faq_list_wrap li h3").not(this).next().slideUp(400,"easeOutQuart");
        $(this).next().slideToggle(400,"easeOutQuart");
       	$("#faq_list_wrap li h3").not(this).removeClass("active");
        $(this).toggleClass("active");
    });
}


/* ============================================================
 * Execute JavaScript when the Window Object is fully loaded.
 * ============================================================ */

	$doc.on('ready', function() {
	});// ready End

	$win.on('load', function() {
		faqAccodion();
	});// load End

	//$win.on('resize', function(){
	$win.resize(function(){
		var winW = $win.width(),
			winH = $win.height();
	});// resize End


/*===================================================================================
	[ shoplistAccodion ]
===================================================================================*/

$('#section01 a').click(function() {
	console.log("click");
	var targetList = $(this).data('to');
	$('#section01 a.on').removeClass('on');
	$('.tab_cont.on').fadeOut(600, function() {
		$('.tab_cont.on').removeClass('on');
		$(targetList).fadeIn(600).addClass('on');	
	});
	$(this).addClass('on');
});


var easing = "easeInOutQuart",
		speed = 300;

$(document).on({
   'mouseenter' : function() {
     $(this).stop().animate({opacity:1},speed,easing);
   },
   'mouseleave' : function(){
     $(this).stop().animate({opacity:0.5},speed,easing);
   }}, '#section01 .hover:not(".on")');




});//End



