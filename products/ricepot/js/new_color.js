
$(function() {

    var scr = 0;
    var scrval = 0;
    var targetPosi = [];
    var paraPosi = [];
    var $scrTarget = '';

//--------------------------- color fade ---------------------------

    function Change(){

        var startTime = 0,
        // start_ex = -1900,
        // start_dx = 500,
        start_ex = 0,
        start_dx = 620,
        end_ex = 0,
        end_dx = 620,
        timer = 0,
        reverceFlg = false,
        currentParam = 0,
        rcurrentParam = 0;
        //index = 0,
        //printX = 0,
        //printY = 0,
        //positionX = 0,
        //positionY = 0,

        function Loop(){
            // 画面クリア
            context.clearRect(0, 0, 554, 382);
            //context.globalCompositeOperation = 'source-out';

            // 画像表示 context.drawImage(image, sx, sy, sw, sh, dx, dy, dw, dh)
            // 画像, 画像をどこから表示するか, 画像の大きさ, 画像の表示位置, 画像の大きさ
            context.drawImage(fade, Change.printX - (index * 1), Change.printY, 588, 382, Change.positionX, Change.positionY, 588, 382);
            context.drawImage(Change.image, 0, 0, 554, 382);
            // フレーム番号++
            index++;
            // フレーム番号が３ならば０に初期化する
            if (index === 550) index = 0;
        }

        var canvas = document.getElementById('color-white');
        var context = canvas.getContext('2d');
        context.globalCompositeOperation = 'source-out';
        var fade = new Image();
        var image = new Image();
        fade.src = '../img/new_color/fade2.png';
        image.src = '../img/new_color/img_main_white.png';

        startTime = new Date();
        timer = setInterval(Animation, 1000 / 100);

        function Animation() {
            context.clearRect(0, 0, 554, 382);
            var now = new Date();
            // if (reverceFlg) { //逆再生
            //     rcurrentParam = easeOutQubic(now - startTime, ex, dx - ex, 2000);
            //     context.drawImage(fade, currentParam - rcurrentParam, 0, 2000, 382);
            //     context.drawImage(image, 0, 0, 554, 382);
            //     if (now - startTime >= 2300) {
            //         reverceFlg = false;
            //         startTime = new Date();
            //         clearInterval(timer);
            //         setTimeout(function () {
            //             startTime = new Date();
            //             timer = setInterval(Animation, 1000 / 100);
            //         }, 1500);
            //     }
            // }
            //
            if (reverceFlg) { //逆再生
                //console.log(currentParam);
                rcurrentParam = easeInOutCirc(now - startTime, end_ex, end_dx - end_ex, 1200);
                context.drawImage(fade, (currentParam - 820) + rcurrentParam, 0, 2000, 382);
                context.drawImage(image, 0, 0, 554, 382);
                if (now - startTime >= 1200) {
                    reverceFlg = false;
                    startTime = new Date();
                    clearInterval(timer);
                    setTimeout(function () {
                        startTime = new Date();
                        timer = setInterval(Animation, 1000 / 100);
                    }, 1500);
                }
            }else { //通常
                currentParam = easeInOutCirc(now - startTime, start_ex, start_dx - start_ex, 1200);
                context.drawImage(fade, currentParam - 1880, 0, 2000, 382);
                context.drawImage(image, 0, 0, 554, 382);
                if (now - startTime >= 1200) {
                    clearInterval(timer);
                    reverceFlg = true;
                    startTime = new Date();
                    clearInterval(timer);
                    setTimeout(function () {
                        startTime = new Date();
                        timer = setInterval(Animation, 1000 / 100);
                    }, 1600);
                }
            }
        }

        function easeOutQubic(t, b, c, d) { //t=経過時間 b=開始時間 c=終了時間 d=所要時間
            var ts = (t /= d) * t;
            var tc = ts * t;
            return b + c * (tc + -2 * ts + 2 * t);
        }

        function easeInOutQuart(t, b, c, d) { //t=経過時間 b=開始時間 c=終了時間 d=所要時間
            t /= d/2;
            if(t < 1) return c/2*t*t*t*t + b;
            if(t -= 2) return -c/2 * (t*t*t*t - 2) + b;
        }

        function easeInOutQuad(t, b, c, d) { //t=経過時間 b=開始時間 c=終了時間 d=所要時間
            t /= d/2
            if(t < 1) return c/2*t*t*t*t + b;
            if(t -= 2) return -c/2 * (t*t*t*t - 2) + b;
        }

        function easeInOutQuint(t, b, c, d) { //t=経過時間 b=開始時間 c=終了時間 d=所要時間
            if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
            return c/2*((t-=2)*t*t*t*t + 2) + b;
        }

        function easeInOutExpo(t, b, c, d) { //t=経過時間 b=開始時間 c=終了時間 d=所要時間
            if (t == 0) return b;
            if (t == d) return b + c;
            if ((t /= d / 2) < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
            return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
        }

        function easeInOutCirc(t, b, c, d) { //t=経過時間 b=開始時間 c=終了時間 d=所要時間
            if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
            return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
        }


    }

//--------------------------- effect ---------------------------


    //kvFade
    function kvFadeInit() {

        TweenMax.set($("#rpr_header li"),{x: '-200px',opacity: 0});
        TweenMax.set($("#rpr_header #rpr_site_logo"),{y: '-90px',opacity: 0});
        TweenMax.set($("#rpr_header #rp_sns"),{y: '-90px',opacity: 0});

        TweenMax.set($("#kv-content .bg"), {scale: 1.05, opacity: 0});
        var text = new SplitText($("#kv-content .page_title .top"), {type: "words,chars"}),
            chars = text.chars;
        TweenMax.set(chars, {y: '100%'});
        TweenMax.set($("#kv-content .page_title img"), {y: '22px'});
    }
    function kvFadeAnimation() {
        if ($("#kv-content").hasClass('is-done')) return false;
        $("#kv-content").addClass('is-done');
        TweenMax.staggerTo($("#kv-content .page_title .top").children().children(), .88,
            {
                y: '0%', ease: Power2.easeOut,
            },
            .04
        );
        TweenMax.to($("#kv-content .page_title .bottom img"), 0.78,
            {
                delay: 0.88,
                y: '0px', ease: Power3.easeOut,
                onStart: function(){
                    naviShow();
                }
            }
        );
        TweenMax.to($("#kv-content .bg"), 1.28, {
            delay: 1.18, scale: 1, opacity: 1, ease: Power3.easeOut,
            onComplete: function () {
                TweenMax.to($("#scroll_down"), .38, {
                    opacity: 1, ease: Power1.easeOut,
                    onComplete: function () {
                        scrolldownAnimation();
                    }
                });
            }
        });
    }

    //textFade
    function textFadeInit() {
        var text = new SplitText($(".js__text-fade"), {type: "words,chars"}),
            chars = text.chars;
        TweenMax.set(chars, {y: '100%'});
    }
    function textFadeAnimation($target) {
        if ($target.hasClass('is-done')) return false;
        $target.addClass('is-done');
        TweenMax.staggerTo($target.children().children(), 1.28,
            {
                y: '0%', ease: Power3.easeOut
            },
            .03
        );
    }

    //planeText
    function planeTextInit() {
        TweenMax.set($(".js__plane-text>span"), {y: '50px', opacity: 0});
    }
    function planeTextAnimation($target) {
        if ($target.hasClass('is-done')) return false;
        $target.addClass('is-done');
        TweenMax.staggerTo($target.children('span'), 1.28,
            {
                y: '0px', opacity: 1, ease: Power3.easeOut,
            },
            0.042
        );
    }

    function useTextInit() {
        var text = new SplitText($(".js__use-text .text"), {type: "words,chars"}),
            chars = text.chars;
        TweenMax.set(chars, {y: '100%'});
        TweenMax.set($(".js__use-text .object01"), {height: 0});
        TweenMax.set($(".js__use-text .object02"), {height: 0});
    }
    function useTextAnimation($target) {
        if ($target.hasClass('is-done')) return false;
        $target.addClass('is-done');
        TweenMax.to($target.children('.object01'), .38, {
            height: 78, ease: Power1.easeOut
        });
        TweenMax.to($target.children('.object02'), .38, {
            delay: .38, height: 78, ease: Power1.easeOut
        });
        TweenMax.staggerTo($target.children('.text').children().children(), 1.0,
            {
                y: '0%', ease: Power3.easeOut
            },
            0.03
        );
    }

    //colorNumber
    function colorNumberInit() {
        TweenMax.set([$(".js__color-num .en"), $(".js__color-num .number")], {y: '100%'});
        TweenMax.set($(".js__color-num .line"), {width: '0px'});
    }
    function colorNumberAnimation($target) {
        if ($target.hasClass('is-done')) return false;
        $target.addClass('is-done');
        TweenMax.staggerTo([$target.children('.en'), $target.children('.number')], 0.88,
            {
                y: '0%', ease: Power3.easeOut
            },
            .58
        );
        TweenMax.to($target.children('.line'), 1.28, {
            width: '120px', ease: Power3.easeOut
        });
    }

    //colorTitle: {
    function colorTitleInit() {
        TweenMax.set($(".js__color-title .bg"), {opacity: 0, width: '0%'});
    }
    function colorTitleAnimation($target) {
        if ($target.hasClass('is-done')) return false;
        $target.addClass('is-done');
        TweenMax.to($target.children('.bg'), 1.18,
            {
                opacity: 1, width: '100%', ease: Power3.easeOut
            }
        );
    }

    //photoFade
    function photoFadeInit() {
        TweenMax.set($(".js__photo-fade.top").children().children(), {opacity: 0, y: '-30%'});
        TweenMax.set($(".js__photo-fade.bottom").children().children(), {opacity: 0, y: '30%'});
        TweenMax.set($(".js__photo-fade.left").children().children(), {opacity: 0, x: '-30%'});
        TweenMax.set($(".js__photo-fade.right").children().children(), {opacity: 0, x: '30%'});
    }
    function photoFadeAnimation($target) {
        if ($target.hasClass('is-done')) return false;
        $target.addClass('is-done');
        TweenMax.to($target.children().children(), 1.58,
            {
                opacity: 1, x: '0%', y: '0%', ease: Power2.easeOut
            }
        );
    }

    //objectFade
    function objectFadeInit() {
        TweenMax.set($(".js__object-fade.top"), {opacity: 0, y: '-50px'});
        TweenMax.set($(".js__object-fade.bottom"), {opacity: 0, y: '50px'});
        TweenMax.set($(".js__object-fade.right"), {opacity: 0, x: '50px'});
        TweenMax.set($(".js__object-fade.left"), {opacity: 0, x: '-50px'});
    }
    function objectFadeAnimation($target) {
        if ($target.hasClass('is-done')) return false;
        $target.addClass('is-done');
        var delay = $target.data('delay');
        TweenMax.to($target, 1.98,
            {
                delay: delay, opacity: 1, x: 0, y: 0, ease: Power2.easeOut,
                onStart: function () {
                    if ($target.attr('id') !== 'ricepot_image') return false;
                    Change();
                }
            }
        );
    }


/* ------------------------------------------------------------

 * [ naviShow ]

 * ------------------------------------------------------------ */

function naviShow(){

    TweenMax.to($("#rpr_header #rpr_site_logo"), .78, {
        delay: 1.1,
        y:0,
        opacity:1,
        ease:Power4.easeOut
    });
    TweenMax.staggerTo($("#rpr_header li"), 1.08, {
        delay: 1.2,
        x:0,
        opacity:1,
        ease:Power4.easeOut
    },0.04);
    TweenMax.to($("#rpr_header #rp_sns"), .78, {
        delay: 1.2,
        y:0,
        opacity:1,
        ease:Power4.easeOut
    });


}

//--------------------------- parallax ---------------------------


    var $pbox = '.js__para-content',
    $ptarget = '.js__scr-para',
    pshift = [];

    function parallaxMove(scrval){


        for (var i = 0; i < $($pbox).length; i++) {

            if (scrval > paraPosi[i]) {
                for (var j = 0; j < $($pbox).eq(i).find($ptarget).length; j++) {

                    pshift[j] = $($pbox).eq(i).find($ptarget).eq(j).data('shift');

                    TweenMax.to($($pbox).eq(i).find($ptarget).eq(j), 1.68,
                        {
                            y: -(scrval - paraPosi[i]) / pshift[j] / 2, ease: Power1.easeOut,
                        }
                    );
                }
            }
        }
    }


//--------------------------- smooth scroll ---------------------------

    var $smoothScBox = $("#scroll-container"),
        $smoothScBody = $("#scroll-body"),
        $smoothScPara = $(".js__para");

    var smoothFlg = false,
        smoothInc = 0,
        smoothPrevInc = 0,
        smoothNowInc = 0,
        smoothParaInc = 0,
        smoothParaPoint = [],
        smoothDir = "next",
        smoothEndPoint = 0;

    var smoothWinH = window.innerHeight;


    var requestAnimationFrame = window.requestAnimationFrame
        || window.mozRequestAnimationFrame
        || window.webkitRequestAnimationFrame
        || window.msRequestAnimationFrame;
    window.requestAnimationFrame = requestAnimationFrame;

    function smoothScroll() {

        setTimeout(function () {

            smoothScrollResize();

        },1e3)




        $(window).on("resize",function () {
            smoothScrollResize();
        });

        smoothScrollMove();

    }


    function smoothScrollMove() {


        if(!requestAnimationFrame){
            setTimeout(function () {
                smoothScrollMove();
            },1000/60);
        }else{
            requestAnimationFrame(smoothScrollMove);
        }


        smoothPrevInc = smoothNowInc;

        smoothNowInc += .1 * (smoothInc - smoothNowInc);
        smoothParaInc += .1 * (smoothInc - smoothParaInc)/100;


        smoothScrollTrans(smoothNowInc);
    }

    function smoothScrollTrans(inc) {

        // if($("html").hasClass("ie9")) return;

        if($("html").hasClass("ie9")){
            $smoothScBody.css({
                // "transform" : 'translate3d(0, -' + inc + 'px, 0)'
                "top" : -inc
            });
        }else{
            $smoothScBody.css({
                // "transform" : 'translate3d(0, -' + inc + 'px, 0)'
                "transform" : "translateY(" + -inc + "px) translateZ(0)"
            });
        }





        for( var i=0; i < smoothParaPoint.length; i++){

            var $p = $smoothScPara.eq(i);


            if(smoothParaPoint[i] - (smoothWinH*1.5) < smoothNowInc  ){

                var pInc = ( Math.floor(smoothNowInc + smoothWinH) - smoothParaPoint[i] ) / 12;

                if($p.hasClass("js__type-minus")) {
                    $p.css({
                        "transform": 'translate3d(0, ' + pInc + 'px, 0)'
                    });

                }else if($p.hasClass("js__type-x")){

                    if( $p.hasClass("js__dir-left") ){

                        var op = Math.floor(pInc/40*100)/100;

                        op = (op>1)?1:op;

                        $p.css({
                            "transform": 'translate3d(-' + pInc + 'px,0,0)',
                            "opacity":op
                        });

                    }else if( $p.hasClass("js__dir-right") ){
                        var op = Math.floor(pInc/40*100)/100;

                        op = (op>1)?1:op;

                        $p.css({
                            "transform": 'translate3d(' + pInc + 'px,0,0)',
                            "opacity":op
                        });

                    }else{
                        $p.css({
                            "transform": 'translate3d(-' + pInc/2 + 'px,0,0)'
                        });
                    }



                }else{
                    $p.css({
                        "transform" : 'translate3d(0, -' + pInc + 'px, 0)'
                    });
                }


            }


        }

    }

    function smoothScrollResize() {

        var h = $smoothScBody.height();
        smoothWinH = window.innerHeight;
        smoothEndPoint = h - smoothWinH;

        $("#document").css({"height": h });
        $("body").css({"height": h });
        // $("#scroll-container").css({"width":window.innerWidth,"height":window.innerHeight});

        for( var i=0; i<$smoothScPara.length; i++){

            smoothParaPoint[i] = $smoothScPara.eq(i).offset().top;

        }

    }





//--------------------------- scrolldown ---------------------------


    var $scrollIcon = '#scroll_down .arrow';

    function scrolldownAnimation() {
        TweenMax.to($($scrollIcon), .68,
            {
                delay: 0.2,
                y: '15px',
                opacity: 0,
                ease: Power1.easeOut,
                onComplete: function () {
                    TweenMax.set($($scrollIcon), {y: '-10px'});
                    TweenMax.to($($scrollIcon), .68,
                        {
                            delay: 0.3,
                            y: '0px',
                            opacity: 1,
                            ease: Power1.easeOut,
                            onComplete: function () {
                                scrolldownAnimation();
                            }
                        }
                    );
                }
            }
        );

    }


    /*------------------------------------------------------

     event

     ------------------------------------------------------*/


    $(window).on("load", function () {


        kvFadeInit();
        setTimeout(function () {
            kvFadeAnimation();
        }, 1e3);

        if(navigator.userAgent.match(/(iPhone|iPad|iPod|Android)/)){
            $("#kv-content").addClass("sp");
            Change();
        }else{
            textFadeInit();
            planeTextInit();
            useTextInit();
            colorNumberInit();
            colorTitleInit();
            photoFadeInit();
            objectFadeInit();
            $scrTarget = $('.js__scr-target');
            for (var i = 0; i < $scrTarget.length; i++) {
                targetPosi[i] = $scrTarget.eq(i).offset().top;
            }
            for (var i = 0; i < $('.js__para-content').length; i++) {
                paraPosi[i] = $('.js__para-content').eq(i).offset().top;
            }
            parallaxMove($(window).scrollTop() + $(window).height());
            smoothScroll();
        }

    });

    var precScInc = 0;

    $(window).on("scroll", function () {

        scr = $(this).scrollTop() + $(window).height() / 2;
        scrval = $(this).scrollTop() + ($(window).height() * 0.85);
        for (var i = 0; i < targetPosi.length; i++) {
            if (scr > targetPosi[i]) {
                for (var j = 0; j < $scrTarget.eq(i).find('.js__text-fade').length; j++) {
                    textFadeAnimation($scrTarget.eq(i).find('.js__text-fade').eq(j));
                }
                for (var j = 0; j < $scrTarget.eq(i).find('.js__use-text').length; j++) {
                    useTextAnimation($scrTarget.eq(i).find('.js__use-text').eq(j));
                }
                for (var j = 0; j < $scrTarget.eq(i).find('.js__plane-text').length; j++) {
                    planeTextAnimation($scrTarget.eq(i).find('.js__plane-text').eq(j));
                }
                for (var j = 0; j < $scrTarget.eq(i).find('.js__color-num').length; j++) {
                    colorNumberAnimation($scrTarget.eq(i).find('.js__color-num').eq(j));
                }
                for (var j = 0; j < $scrTarget.eq(i).find('.js__color-title').length; j++) {
                    colorTitleAnimation($scrTarget.eq(i).find('.js__color-title').eq(j));
                }
                for (var j = 0; j < $scrTarget.eq(i).find('.js__photo-fade').length; j++) {
                    photoFadeAnimation($scrTarget.eq(i).find('.js__photo-fade').eq(j));
                }
                for (var j = 0; j < $scrTarget.eq(i).find('.js__object-fade').length; j++) {
                    objectFadeAnimation($scrTarget.eq(i).find('.js__object-fade').eq(j));
                }
            }
        }

        // parallaxMove(scrval);


        smoothInc = window.pageYOffset || document.documentElement.scrollTop;

        smoothDir = (smoothInc>precScInc)?"next":"prev";

        precScInc = smoothInc;

    });

    // document.addEventListener("scroll", function (e) {
    //
    //
    // });
    /*------------------------------------------------------

     scroll control

     ------------------------------------------------------*/

// scrLength = 190;
//
// var mousewheelevent = 'onwheel' in document ? 'wheel' : 'onmousewheel' in document ? 'mousewheel' : 'DOMMouseScroll';
// $(document).on(mousewheelevent,function(e){
//     e.preventDefault();
//     var delta = e.originalEvent.deltaY ? -(e.originalEvent.deltaY) : e.originalEvent.wheelDelta ? e.originalEvent.wheelDelta : -(e.originalEvent.detail);
//     if (delta < 0){
//         scrSet =  $(document).scrollTop()+scrLength;
//     } else {
//         scrSet =  $(document).scrollTop()-scrLength;
//     }
//     TweenMax.to($('html,body'), .98,
//         {
//             scrollTop:scrSet, ease:Power2.easeOut,
//         }
//     );
//     return false;
// });

// if (!navigator.userAgent.match(/(iPhone|iPad|iPod|Android)/)) {
//
//     var MOUSE_WHEEL_EVENT = 'onwheel' in document ? 'onwheel' : 'onmousewheel' in document ? 'onmousewheel' : 'DOMMouseScroll';
//     var callback = function () {
//     };
//     var isFired = false;
//
//     var sleep = 50;// ここの値はお好み。小さくしすぎると一度のユーザースクロール中に複数回発火するし、大きすぎるとそもそもcallbackが発火しない。
//     var scrSet = 0;
//
//     var delta, uintDelta, timeStamp;
//
//     // キャッシュ
//     callback.delta = 0;
//     callback.timeStamp = Date.now();
//
//     document[MOUSE_WHEEL_EVENT] = function (e) {
//         e.preventDefault();
//
//         delta = e.deltaY ? -(e.deltaY) : e.wheelDelta ? e.wheelDelta : -(e.detail);
//
//         // 移動量が0の場合は無視。
//         // ユーザースクロールと慣性スクロールの切り替わりの瞬間に、0が発生することがあるので、それの予防も兼ねて。
//         if (!delta) return;
//
//         uintDelta = Math.abs(delta);
//
//         if (uintDelta - callback.delta > 0) {
//             // 増加
//             timeStamp = e.timeStamp;
//
//             if (!isFired && timeStamp - callback.timeStamp > sleep) {
//                 callback();
//                 isFired = true;
//             }
//
//             callback.timeStamp = timeStamp;
//
//         } else {
//             // 減退
//             isFired = false;
//         }
//         callback.delta = uintDelta;
//         if (e.deltaY > 0) {
//             scrSet = $(document).scrollTop() + callback.delta;
//         } else {
//             scrSet = $(document).scrollTop() - callback.delta;
//         }
//         TweenMax.to($('html,body'), .98,
//             {
//                 scrollTop: scrSet, ease: Power2.easeOut,
//             }
//         );
//
//     };
//
// }

function Scroll() {
    if (navigator.userAgent.match(/(iPhone|iPad|iPod|Android)/)) return false;

    var e = $(window);
    e.on("mousewheel DOMMouseScroll", function (t) {
        t.preventDefault();
        var n = t.originalEvent.wheelDelta / 140 || -t.originalEvent.detail / 3,
            i = e.scrollTop(),
            r = Math.floor(i - parseInt(170 * n));
        TweenMax.to(e, 1, {
            ease     : Power3.easeOut,
            overwrite: !0,
            scrollTo : {
                autoKill: !0,
                y       : r
            }
        })
    })
}

//Scroll();

});


