$(function(){

    var availableTags = [
        "yahoo.co.jp",
        "gmail.com",
        "icloud.com",
        "ezweb.ne.jp",
        "softbank.ne.jp",
        "i.softbank.jp",
        "d.vodafone.ne.jp",
        "h.vodafone.ne.jp",
        "t.vodafone.ne.jp",
        "c.vodafone.ne.jp",
        "k.vodafone.ne.jp",
        "r.vodafone.ne.jp",
        "n.vodafone.ne.jp",
        "s.vodafone.ne.jp",
        "q.vodafone.ne.jp",
        "docomo.ne.jp",
        "disney.ne.jp",
        "emnet.ne.jp",
        "wcm.ne.jp",
        "outlook.jp",
        "outlook.com",
        "hotmail.co.jp",
        "live.jp",
        "infoseek.jp",
        "excite.co.jp",
        "inter7.jp",
        "smoug.net",
        "aol.jp",
        "youngpostman.net",
        "tora.zzn.com",
        "zoho.com",
        "goo.jp",
        "mail.goo.ne.jp",
        "auone.jp",
        "livedoor.com"
    ];
    function extractLast( val ) {
        if (val.indexOf("@")!=-1){
            var tmp=val.split("@");
            //console.log(tmp[tmp.length-1]);
            return tmp[tmp.length-1];
        }
        //console.log("returning empty");
        return "";
    }

    $( ".domain-autocomplete" )
        // don't navigate away from the field on tab when selecting an item
        .bind( "keydown", function( event ) {
            if ( event.keyCode === $.ui.keyCode.TAB &&
                    $( this ).data( "autocomplete" ).menu.active ) {
                event.preventDefault();
            }
        })
        .autocomplete({
            minLength: 1,
            source: function( request, response ) {
                        var mail = extractLast(request.term);
                        if(mail.length<1){return;}
                        var matcher = new RegExp( "^" + mail, "i" );
                        response( $.grep( availableTags, function( item ){
                            return matcher.test( item );
                        }));
             },
            focus: function() {
                // prevent value inserted on focus
                return false;
            },

            select: function( event, ui ) {
    this.value = this.value.substring(0, this.value.indexOf('@') + 1) + ui.item.value;
    return false;
}
        });

});