// require jQuery JavaScript Library v1.10.1

/* 即時関数でローカル化 */
$(function(){

/* ============================================================
 * defineFoundationVariable
 * ============================================================ */
	// Variables
	var $doc = $(document),
		$win = $(window),
		winW = $win.width(),
		winH = $win.height();

	var ua = navigator.userAgent;
	var isIE = ua.match(/msie/i),
		isIE6 = ua.match(/msie [6.]/i),
		isIE7 = ua.match(/msie [7.]/i),
		isIE8 = ua.match(/msie [8.]/i),
		isIE9 = ua.match(/msie [9.]/i),
		isIE10 = ua.match(/msie [10.]/i);

	var smpUa = {
		iPhone: navigator.userAgent.indexOf('iPhone') != -1,
		iPad: navigator.userAgent.indexOf('iPad') != -1,
		iPod: navigator.userAgent.indexOf('iPod') != -1,
		android: navigator.userAgent.indexOf('Android') != -1,
		windows: navigator.userAgent.indexOf('Windows Phone') != -1
	}

	$win.on("resize load",function(){
		winW = $win.width(),
		winH = $win.height();

		var _resizeW = (winW>960)?winW:960;

		$("#section02 li").css({"width":_resizeW,"height":_resizeW*0.382});

	});

	$('.video_wrap img').on('click',function() {
		var video1Tween = new TweenMax.to($(this),0.48,
			{
				alpha: 0, display: 'none', opacity: 0,
				ease:Power2.easeOut,
			}
		);
		var video2Tween = new TweenMax.to($("#whats_video"),0.48,
			{
				opacity: 1,
				ease:Power2.easeOut,
			}
		);
        ytPlayer.playVideo();
    });

    var controller = new ScrollMagic.Controller();  //複数のアニメーションがあっても一つどこかに書いてあればOK

    if(smpUa.iPhone || smpUa.iPad || smpUa.iPod || smpUa.android || smpUa.windows) return false;

    var tween1 = TweenMax.from($('#section02>ul>li:nth-child(1)'), .48, {
		opacity:0,
		y:20,
		ease: Power0.easeOut
    });
    new ScrollMagic.Scene({
		triggerElement: "#section02>ul>li:nth-child(1)",
		offset:-winH/2+200
	}) 
	.setTween(tween1)
    .addTo(controller);
    var tween2 = TweenMax.from($('#section02>ul>li:nth-child(2)'), .48, {
		opacity:0,
		y:20,
		ease: Power0.easeOut
    });
    new ScrollMagic.Scene({
		triggerElement: "#section02>ul>li:nth-child(2)",
		offset:-winH/2+200
	}) 
	.setTween(tween2)
    .addTo(controller);
    var tween3 = TweenMax.from($('#section02>ul>li:nth-child(3)'), .48, {
		opacity:0,
		y:20,
		ease: Power0.easeOut
    });
    new ScrollMagic.Scene({
		triggerElement: "#section02>ul>li:nth-child(3)",
		offset:-winH/2+200
	}) 
	.setTween(tween3)
    .addTo(controller);
    var tween4 = TweenMax.from($('#section02>ul>li:nth-child(4)'), .48, {
		opacity:0,
		y:20,
		ease: Power0.easeOut
    });
    new ScrollMagic.Scene({
		triggerElement: "#section02>ul>li:nth-child(4)",
		offset:-winH/2+200
	}) 
	.setTween(tween4)
    .addTo(controller);
    var tween5 = TweenMax.from($('#section02>ul>li:nth-child(5)'), .48, {
		opacity:0,
		y:20,
		ease: Power0.easeOut
    });
    new ScrollMagic.Scene({
		triggerElement: "#section02>ul>li:nth-child(5)",
		offset:-winH/2+200
	}) 
	.setTween(tween5)
    .addTo(controller);
    var tween6 = TweenMax.from($('#section02>ul>li:nth-child(6)'), .48, {
		opacity:0,
		y:20,
		ease: Power0.easeOut
    });
    new ScrollMagic.Scene({
		triggerElement: "#section02>ul>li:nth-child(6)",
		offset:-winH/2+200
	}) 
	.setTween(tween6)
    .addTo(controller);
    var tween7 = TweenMax.from($('#section02>ul>li:nth-child(7)'), .48, {
		opacity:0,
		y:20,
		ease: Power0.easeOut
    });
    new ScrollMagic.Scene({
		triggerElement: "#section02>ul>li:nth-child(7)",
		offset:-winH/2+200
	}) 
	.setTween(tween7)
    .addTo(controller);
    var tween8 = TweenMax.from($('#section02>ul>li:nth-child(8)'), .48, {
		opacity:0,
		y:20,
		ease: Power0.easeOut
    });
    new ScrollMagic.Scene({
		triggerElement: "#section02>ul>li:nth-child(8)",
		offset:-winH/2+200
	}) 
	.setTween(tween8)
    .addTo(controller);
    var tweengraph = TweenMax.from($('#section04 .figure_block .ricepot'), .48, {
    	delay:.2,
		opacity:0,
		scale: 0.3,
		rotation: -25,
		ease: Power2.easeOut
    });
    new ScrollMagic.Scene({
		triggerElement: "#section04 .figure_block"
	}) 
	.setTween(tweengraph)
    .addTo(controller);

});

// IFrame Player API の読み込み
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// YouTubeの埋め込み
function onYouTubeIframeAPIReady() {
    ytPlayer = new YT.Player(
        'whats_video', // 埋め込む場所の指定
        {
            width: 800, // プレーヤーの幅
            height: 450, // プレーヤーの高さ
            videoId: 'BLUS84yaDSw', // YouTubeのID
            // パラメータの設定
            events: {
                'onReady': onPlayerReady, // プレーヤーの準備ができたときに実行
                'onStateChange': onPlayerStateChange // プレーヤーの状態が変更されたときに実行
            },
            playerVars: {
                rel: 0, // 再生終了後に関連動画を表示するかどうか設定
                autoplay: 0 // 自動再生するかどうか設定
            }
        }
    );
}
// プレーヤーの準備ができたとき
function onPlayerReady(event) {
}

// プレーヤーの状態が変更されたとき
function onPlayerStateChange(event) {
	// 現在のプレーヤーの状態を取得
    var ytStatus = event.data;
    // 再生終了したとき
    if (ytStatus == YT.PlayerState.ENDED) {
        var video1Tween = new TweenMax.to($(".video_wrap img"),0.48,
			{
				alpha: 1, display: 'block', opacity: 1,
				ease:Power2.easeOut,
			}
		);
		var video2Tween = new TweenMax.to($("#whats_video"),0.48,
			{
				opacity: 0,
				ease:Power2.easeOut,
			}
		);
    }
}