/* ============================================================
 * defineFoundationVariable
 * ============================================================ */
    // Variables
    var $doc = $(document),
        $win = $(window);

    var smpUa = {
        iPhone: navigator.userAgent.indexOf('iPhone') != -1,
        iPad: navigator.userAgent.indexOf('iPad') != -1,
        iPod: navigator.userAgent.indexOf('iPod') != -1,
        android: navigator.userAgent.indexOf('Android') != -1,
        windows: navigator.userAgent.indexOf('Windows Phone') != -1
    }

    if(smpUa.iPhone || smpUa.iPad || smpUa.android || smpUa.windows){
        var triggerEvent = "touchstart";
    }else{
        var triggerEvent = "click";
    }

    //[ バリデート除外クラス ]
    var valiExclude = ".vali_exclude";

/*===================================================================================
    [ バリデーション ]
===================================================================================*/

//除外ID 初期値
var excludeID = "#emailcheck";

//エラー表示処理
function errorShow(targetID,type){


    var $target = $("#"+targetID);

    //console.log($target.parents("li"));

    //console.log(targetID);

    //
    if(type == "emailcheck"){
        var excludeID = "#unfield";
    }else{
        var excludeID = "#emailcheck";
    }

    if(!$target.parents(".input_wrap").find("li")[0]){
        //1つの項目に複数項目が無い場合
        var $targetParent = $target.not(excludeID).parents(".input_wrap"),
            getThisText = $target.parents("tr").find("th").text(),
            multipleFlag = false;
    }else{
        //1つの項目に複数項目がある場合
        var $targetParent = $target.not(excludeID).parents("li"),
            thisIndex = $target.parents("li").index(),
            getThisText = $target.parents(".input_wrap").find("li").eq(thisIndex).find(".text").text(),
            multipleFlag = true;
        //console.log("項目複数あり");
    }
    //エラーテキストがあれば一旦消す
    if($targetParent.find(".err_text")[0]){
        $targetParent.find(".err_text").remove();
    }
    //console.log($target);
    switch(type){
        case "text":
            var thisErrorText = "メールアドレスを入力してください。";
        break;
        case "email":
            var thisErrorText = "メールアドレスの形式が違います。";
        break;
        case "emailcheck":
            var thisErrorText = "メールアドレスが一致しません。";
        break;
        case "passwordcheck":
            var thisErrorText = "パスワードが一致しません。";
        break;
        case "passwordlen":
            var thisErrorText = "半角英数字で8文字以上入力して下さい。";
        break;
        case "passHalf":
            var thisErrorText = "半角英字を1文字以上入力して下さい。";
        break;
        case "passEm":
            var thisErrorText = "半角数字を1文字以上入力して下さい。";
        break;
        case "number":
            var thisErrorText = "半角数字を入力してください。";
        break;
        case "kana":
            var thisErrorText = "ひらがなで入力してください。";
        break;
        case "pref":
            var thisErrorText = "都道府県を選択して下さい。";
        break;
        case "space":
            var thisErrorText = "苗字と名前の間にスペースを入れて下さい。";
        break;
        case "zip":
            var thisErrorText = "郵便番号を正しく入力して下さい。";
        break;
        case "tel":
            var thisErrorText = getThisText + "を正しく入力して下さい。";
        break;
        case "address":
            var thisErrorText = getThisText + "を正しく入力して下さい。";
        break;
        case "halfAngle":
            var thisErrorText = "半角文字で入力してください。";
        break;
    }
    $targetParent.append('<p class="err_text">'+ thisErrorText + '</p>');
    //エラー表示をした際に項目の高さを調整する
    var getThisHeight = $targetParent.height();
    //console.log($target.attr("class"));
    //console.log($target.attr("class"));
    if($target.attr("class").match(/err/g)==null){
/*
        if(!multipleFlag){
            //console.log(excludeID);
            $target.not(excludeID).parents("tr").find("th .title").css({"height":getThisHeight});
        }else{
            $target.not(excludeID).parents("tr").find("th .title li").eq(thisIndex).css({"height":getThisHeight});
        }
*/
        $target.addClass("err");
    }
}
//エラー非表示処理
function errorHide(targetID){
   //console.log("errorHide");
    //if(!targetID.match(/pref/g)){
        var $target = $("#"+targetID);
   /* }else{
        if($("#"+targetID).parents(".input_wrap").find("li")[0]){
            var $target = $("#"+targetID).parents("li").find(".customSelect");
        }else{
            var $target = $("#"+targetID).parents(".input_wrap").find(".customSelect");
        }
    }*/

   if(!$target.parents(".input_wrap").find("li")[0]){
        //1つの項目に複数項目が無い場合
        var parentTag = ".input_wrap",
            multipleFlag = false;
    }else{
        //1つの項目に複数項目がある場合
        var parentTag = "li",
            $targetParent = $target.parents("li"),
            thisIndex = $target.parents("li").index(),
            multipleFlag = true;
    }
    $target.removeClass("err");
    //同じ項目内にエラーが無かった場合テキストを消す
    if(!$target.parents(parentTag).find(".err")[0]){
        $target.parents(parentTag).find(".err_text").remove();
    }
}

//一括処理
//----------------------------------------------------------

var checkCompFlag = false,
    firstSubmitFlag = false,
    $form = $(".form_wrap");

function requiredCheckAll(){
    $("#submit_btn").on(triggerEvent,function(){
        var $targetWrap = $(this).parents(".form_wrap");
        //必須項目入力
       for(var i=0; i< $targetWrap.find("input,textarea").length; i++){
            var $targetRq = $targetWrap.find("input,textarea").eq(i);
            if($targetRq.hasClass("required") && !$targetRq.attr("disabled") && !$targetRq.attr("id").match(/pref/g)){
                if(!$targetRq.val()){
                    var getThisID = $targetRq.attr("id");
                    //console.log(getThisID);
                   errorShow(getThisID,"text","#emailcheck");
                    //エラー表示後トップへ移動
                }
            }
       }
        //エラー表示が無ければ送信する
        if($("body").find(".err_text")[0]){
            return false;
        }else{
            e.preventDefault();
            document.form1.submit();
        }
    });
    $win.keyup(function(e){
        if(e.keyCode == 13){
            var $targetWrap = $(".form_wrap");
            //必須項目入力
           for(var i=0; i< $targetWrap.find("input,textarea").length; i++){
                var $targetRq = $targetWrap.find("input,textarea").eq(i);
                if($targetRq.hasClass("required") && !$targetRq.attr("disabled") && !$targetRq.attr("id").match(/pref/g)){
                    if(!$targetRq.val()){
                        var getThisID = $targetRq.attr("id");
                        //console.log(getThisID);
                       errorShow(getThisID,"text","#emailcheck");
                        //エラー表示後トップへ移動
                    }
                }
           }
            //エラー表示が無ければ送信する
            if($("body").find(".err_text")[0]){
                return false;
        }else{
            e.preventDefault();
            document.form1.submit();
        }
        }
    });
}
//郵便番号検索ボタンクリック時にチェック
function addSearchBtnClick(){
    $(".add_search_btn, #zip_btn, #dif_zip_btn").on(triggerEvent,function(){
        var thisID = "#" + $(this).attr("id");
        //setTimeout(function(){
            for(var i=0; i<$(this).parents(".input_wrap").find('input, select').not(thisID).length; i++){
            //$(this).parents(".input_wrap").find('input, select').each(function(){
                var $target = $(this).parents(".input_wrap").find('input, select').not(thisID).eq(i),
                    trgetID = $target.attr("id");
                //console.log($target.attr("id") + ":::::" + $target.val());
                if(trgetID.match(/pref/g)){
                    //if($target.val() != ""){
                        /*for(var j = 0; j <= $target.children("option").length; j++){
                            //console.log($target.children("option").eq(i).attr("value"));
                            if($target.val() == $target.children("option").eq(i).attr("value")){
                                var selectPref = $target.children("option").eq(i).text();
                            }
                        }*/
                        /*setTimeout(function(){
                            $("#"+trgetID).parents(".input_wrap").find(".customSelectInner").text(selectPref);
                        },50);*/
                        errorHide($target.attr("id"));
                    //}
                }else{
                    //console.log($target.attr("class").match(/err/g));
                    //console.log($target.val());
                    if($target.attr("class").match(/err/g)){
                        //console.log($target.val());
                        //if($target.val()){
                            var getThisID = $target.attr("id");

                            errorHide(getThisID);
                        //}
                    }
                }
            }
            //});
        //},1000);
    });
}


//個別処理
//----------------------------------------------------------
//必須項目入力
function requiredCheck(){
    $(".form_wrap input,.form_wrap textarea").blur(function(){
        var thisID = $(this).attr("id");
        if($(this).hasClass("required")){
            if(!$(this).val()){
                errorShow(thisID,"text","#emailcheck");
            }else{
                errorHide(thisID);
            }
        }
    });
}

//email形式チェック
var emailCheckFlag = false;
function emailCheck(){
    $('.form_wrap input[type="email"]').blur(function(){
        var thisID = $(this).attr("id");
        if($(this).val()){
            if(!$(this).val().match(/.+@.+\..+/g)){
//                console.log("アドレス確認");
                emailCheckFlag = false;
                errorShow(thisID,"email","#emailcheck");
            }else{
                emailCheckFlag = true;
                errorHide(thisID);
            }
        }
        if($(this).attr("id") == "emailcheck"){
            if($("#email").val() != $("#emailcheck").val()){
                errorShow(thisID,"emailcheck");
            }else{
                errorHide(thisID);
            }
        }
    });
}

//パスワードチェック
function passwordCheck(){



    $('.form_wrap input[type="password"]').blur(function(){
    //$('#password').blur(function(){
        var thisID = $(this).attr("id"),
            errShowFlag = false;


        if($(this).val()){
            //console.log(thisID);

            //[ 文字数をチェック ]
            if($(this).val().length < 8){
                errorShow(thisID,"passwordlen");
                errShowFlag = true;
            }else{
                errorHide(thisID);
                errShowFlag = false;
            }

            //[ 形式をチェック ]
           if(errShowFlag) return;
            var halfAngleCount = 0,
                emCount = 0;
                //cosole.log($(this).val().length);
            for(var i=0; i<=$("#"+thisID).val().length; i++){
                //console.log($("#"+thisID).val().substr(i,1));
                 if($("#"+thisID).val().substr(i,1).match(/[A-Z a-z]/g)){
                    halfAngleCount++;
                 }
                 if($("#"+thisID).val().substr(i,1).match(/[0-9]/g)){
                    emCount++;
                 }
            }

             if(halfAngleCount == 0){
                errorShow(thisID,"passHalf");
                errShowFlag = true;
             }else{
                errShowFlag = false;
             }
             if(emCount == 0){
                errorShow(thisID,"passEm");
                errShowFlag = true;
             }else{
                errShowFlag = false;
             }


            //[ 確認と一致しているかチェック ]
            if(errShowFlag) return;
           if(thisID.match(/check/g)){
                //console.log(thisID);
                if($("#"+thisID.replace("_check","")).val() != $("#"+thisID).val()){
                    errorShow(thisID,"passwordcheck");
                }else{
                    errorHide(thisID);
                }
            }
        }else{
            //[ 何も入力がなければエラーを消す ]
           if(!$("#"+thisID).attr("class").match(/required/g)) errorHide(thisID);
        }

    });
}



//半角英数字入力確認
function halfAngleCheck(){
    $('.form_wrap input[type="email"]').blur(function(){
        if(!emailCheckFlag) return;
        var thisID = $(this).attr("id");
//        console.log($(this).val());
        if($(this).val()){
//        console.log("半角数字チェック");
             if(!$(this).val().match(/^[a-zA-Z0-9!-/:-@¥[-`{-~]+$/)){
                errorShow(thisID,"halfAngle");
            }else{
                errorHide(thisID);
            }

        }
    });
}



//パスワード入力確認
function checkAlphanumeric(){
    $('.alphanumeric').blur(function(){
        var thisID = $(this).attr("id");
        if($(this).val()){
             if(!$(this).val().match(/^[a-zA-Z0-9\-\_]+$/)){
                errorShow(thisID,"alphanumeric");
            }else{
                errorHide(thisID);
            }

        }
        //console.log("passwordcheck");
        if($(this).attr("id") == "passwordcheck"){
            //console.log("passwordcheck");
            if($("#password").val() != $("#passwordcheck").val()){
                errorShow(thisID,"passwordcheck");
            }else{
                errorHide(thisID);
            }
        }
    });
}


function prefCheck(){
    $('.pref_select,#form_pref,#dif_zip_btn').change(function(){
        //console.log("change");
        var thisID = $(this).attr("id");
         if($(this).val()==="" && !$(this).attr("disabled")){
            errorShow(thisID,"pref");
        }else{
            errorHide(thisID);
        }
    });
}

//[ 郵便番号チェック ]

function zipNumberCheck(){
    $('input[type="text"]').blur(function(){
        var thisID = $(this).attr("id"),
            thisVal = $(this).val(),
            thisCheck = false;
        if(thisID.match(/zip/g) && thisVal){
            if(thisVal.match(/-/g)){
                var thiErrIF = thisVal.length != 8;
            }else{
               // console.log("-なし");
                var thiErrIF = thisVal.length != 7;
            }

             if(thiErrIF){
                //console.log("文字数足りません");
                errorShow(thisID,"zip");
            }else{
                errorHide(thisID);
            }

        }
    });
}

//[ 電話番号チェック ]
function telNumberCheck(){
    $("input").blur(function(){
        var thisID = $(this).attr("id"),
            thisVal = $(this).val(),
            thisCheck = false;
        if(thisID.match(/tel|phone|mobile/g) && thisVal){
            if(thisVal.match(/-/g)){
                var thiErrIF = thisVal.length < 12;
            }else{
                var thiErrIF = thisVal.length < 10;
            }

             if(thiErrIF){
                errorShow(thisID,"tel");
            }else{
                errorHide(thisID);
            }

        }
    });
}

function textareaCount (){
    if($(".text_count")[0]){
        var textCountNum = $(".text_count").parents(".input_wrap").find("textarea").val().length;
        $('.text_count .num').text(textCountNum);
    }
    $("textarea.count_check").on("keyup",function(){
        var thisValueLength = $(this).val().length;
        $(this).parents(".input_wrap").find('.text_count .num').text(thisValueLength);
        if(thisValueLength > 500){
            //console.log($(this).parents(".input_wrap").find(".err_text"));
            if($(this).parents(".input_wrap").find(".err_text").length==0) $(this).parents(".input_wrap").append('<p class="err_text">500文字を超えています。</p>');
        }else{
            $(this).parents(".input_wrap").find(".err_text").remove();
        }
    });
}


function checkboxCheck (){
    $("ul.required_checkbox input").on("change",function(){
        if($(this).parents("ul").find("input:checked")){
            $("ul.required_checkbox").next().remove();
        }
    });
}


/* ============================================================
 * Execute JavaScript when the Window Object is fully loaded.
 * ============================================================ */

    $win.on('load', function() {
        requiredCheck();
        emailCheck();
        passwordCheck();
        requiredCheckAll();
        addSearchBtnClick();
        //checkAlphanumeric();
        prefCheck();
        //differentShow();
        //corporationFrom();
        //spaceCheck();
        halfAngleCheck();
        //checkAddress();
    });//End
    $win.on('resize', function() {
    });//End
