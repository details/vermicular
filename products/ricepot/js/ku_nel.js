// require jQuery JavaScript Library v1.10.1

/* 即時関数でローカル化 */
$(function(){

/* ============================================================
 * defineFoundationVariable
 * ============================================================ */
	// Variables
	var $doc = $(document),
		$win = $(window),
		winW = $win.width(),
		winH = $win.height();

	!function(e,o){function i(e){return n.indexOf(e)!=-1}function r(e){var o=e.split("."),i={};return i.str=e,i.float=parseFloat(e)||0,i.major=o.length>0?parseInt(o[0])||0:0,i.minor=o.length>1?parseInt(o[1])||0:0,i.build=o.length>2?parseInt(o[2])||0:0,i.revision=o.length>3?parseInt(o[3])||0:0,i}var a={};a._detects=["mobile","tablet","pc","windows","mac","linux","ios","android","edge","ie","safari","chrome","firefox","opera"];var n=a.userAgent=e.navigator.userAgent.toLowerCase();a.mobile=i("iphone")||i("ipod")||i("android")&&i("mobile")||i("windows")&&i("phone")||i("firefox")&&i("mobile")||i("blackberry"),a.tablet=i("ipad")||i("android")&&!i("mobile")||i("windows")&&i("touch")&&!i("tablet pc")||i("firefox")&&i("tablet")||i("kindle")||i("silk")||i("playbook"),a.pc=!i("iphone")&&!i("ipod")&&!i("ipad")&&!i("android")&&(!i("windows")||!i("phone")&&(!i("touch")||i("tablet pc")))&&(!i("firefox")||!i("mobile")&&!i("tablet"))&&!i("blackberry")&&!i("kindle")&&!i("silk")&&!i("playbook"),a.windows=i("windows"),a.mac=i("mac os x")&&!i("iphone")&&!i("ipad")&&!i("ipod"),a.linux=i("linux")&&!i("android"),a.ios=i("iphone")||i("ipad")||i("ipod"),a.ios&&(a.ios=new Boolean(!0),n.match(/ os ([\d_]+)/g),a.ios.version=r(RegExp.$1.replace("_","."))),a.android=i("android"),a.android&&(a.android=new Boolean(!0),n.match(/android ([\d\.]+)/g),a.android.version=r(RegExp.$1)),a.edge=i("edge"),a.ie=i("trident")||i("msie"),a.safari=i("safari")&&!i("android")&&!i("edge")&&!i("opera")&&!i("opr")&&!i("chrome"),a.chrome=i("chrome")&&!i("edge")&&!i("opera")&&!i("opr"),a.chrome&&(a.chrome=new Boolean(!0),n.match(/chrome\/([\d.]+)/g),a.chrome.version=r(RegExp.$1)),a.firefox=i("firefox")&&!i("edge"),a.opera=i("opera")||i("opr");var d,t,s,l=a._classPrefix="",p=o.documentElement,c=p.className;for(t=a._detects.length,d=0;d<t;d++)s=a._detects[d],c+=a[s]?" "+l+s:" "+l+"no-"+s;p.className=c,e.Useragnt=a}(window,document);

	var $silder = $("#slider");

	$(document).ready(function(){
		$('.bxslider').bxSlider({
			captions:true
		});
	});

	var rH = 0;

	function cboxre(){

		$(".modal_area").removeAttr('style');
		$("#cboxContent").find(".r_area").removeAttr('style');
		//変数化
		rH = $("#cboxContent").find(".r_area").height();
		if(winH < rH + 100){
			rH = winH - 100;
			$("#cboxContent").find(".r_area").height(rH);
			$('#cboxContent .modal_area').height(rH).css({"top":0,"bottom":0,"position":"absolute"});
		}else{
			rH = $("#cboxContent").find(".r_area").height();
			$("#cboxContent").find(".r_area").removeAttr('style');
			$('#cboxContent .modal_area').height(rH).css({"top":0,"bottom":0,"position":"absolute"});
		}

	}

    $(".modal_box.recipe01").colorbox({
	    rel:'recipe01',
	    inline: true,
	    transition: "fade",
	    speed: 400,
	    slideshowSpeed: 1300,
	    loop: true,
	    returnFocus: false,
	    previous: "",
	    next: "",
	    opacity: 1,
	    width: "960px",
	    //height: '100%',
	    maxHeight: '1080px',
	    closeButton: false,
	    onComplete:function(){
	    	$('body').height(winH).css("overflow","hidden");
	    	//resize
	    	cboxre();
	    	$win.on('touchmove.noScroll', function(e) {
			    e.preventDefault();
			});
	    },
	    onClosed:function(){
	    	//reset
			$("body,.modal_area").removeAttr('style');
			$win.off('.noScroll');
	    }
	});

	$(".modal_box.recipe02").colorbox({
	    rel:'recipe02',
	    inline: true,
	    transition: "fade",
	    speed: 400,
	    slideshowSpeed: 1300,
	    loop: true,
	    returnFocus: false,
	    previous: "",
	    next: "",
	    opacity: 1,
	    width: '960px',
	    //height: '100%',
	    maxHeight: '1080px',
	    closeButton: false,
	    onComplete:function(){
	    	$('body').height(winH).css("overflow","hidden");
	    	//resize
	    	cboxre();
	    	$win.on('touchmove.noScroll', function(e) {
			    e.preventDefault();
			});
	    },
	    onClosed:function(){
	    	//reset
			$("body,.modal_area").removeAttr('style');
			$win.off('.noScroll');
	    }
	});

    $win.on("load",function(){
	    $(".scrollbox").mCustomScrollbar({
			advanced:{
				updateOnContentResize: true
			}
		});
	});

    var orientationtimer = false;
    $win.on("resize",function(){
    	winH = $win.height();
    	cboxre();
    	$.colorbox.resize({
    		height: '100%'
    	});
    	if (orientationtimer !== false) {
			clearTimeout(orientationtimer);
		}
		orientationtimer = setTimeout(function() {
			$.colorbox.position(0);
		}, 600);
    });

    $win.on("orientationchange",function(){
	    	if(Useragnt.tablet || Useragnt.mobile){
			$.colorbox.close()
		}
    });

	  
	var time = 600;

	 $('.banner a[href^=#]').click(function() {
	  var target = $(this.hash);
	    if (!target.length) return ;
	    var targetY = target.offset().top;
	    $('html,body').animate({scrollTop: targetY}, time, 'swing');
	    window.history.pushState(null, null, this.hash);
	    return false;
	  });

});
