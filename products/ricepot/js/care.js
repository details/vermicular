// require jQuery JavaScript Library v1.10.1

$(function(){

$(document).ready(function() {
     //初期表示
     $(".tab_content").hide();//全ての.tab_contentを非表示
     $("ul.tabs li:first").addClass("active").show();//tabs内最初のliに.activeを追加
     $(".tab_content:first").addClass("active").show();//最初の.tab_contentを表示
     //タブクリック時
     $(document).on("click","ul.tabs li",function() {
        $('.btn_change').not(this).css("opacity",0.5);
          $("ul.tabs li").removeClass("active");//.activeを外す
          $(this).addClass("active");//クリックタブに.activeを追加
          $(".tab_content.active").fadeOut(600,function(){
            $(".tab_content.active").removeClass("active");
            var activeTab = $("ul.tabs .active").find("a").attr("href");//アクティブタブコンテンツ
          $(activeTab).fadeIn(600).addClass("active");//アクティブタブコンテンツをフェードイン
          });//全ての.tab_contentを非表示
          return false;
     });
});

var easing = "easeInOutQuart",
        speed = 300;

$(document).on({
   'mouseenter' : function() {
     $(this).stop().animate({opacity:1},speed,easing);
   },
   'mouseleave' : function(){
     $(this).stop().animate({opacity:0.5},speed,easing);
   }}, '.btn_change:not(".active")');

$(window).load(function(){
  if("#cook" == location.hash) {
    $(".btn_change").removeClass("active");
    $("#cook").addClass("active");
    $("#mode01").css({"display":"none"});
    $("#mode02").css({"display":"block"});
  }
});

});