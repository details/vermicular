// require jQuery JavaScript Library v1.10.1

/* 即時関数でローカル化 */
$(function(){

	$(document).ready(function(){
	    $('html,body').animate({ scrollTop: 0 }, '1');
	});
	$('html,body').animate({ scrollTop: 0 }, '1');

	var $kvWrap = $("#kv_wrap"),
		$kvInner = $("#kv_inner"),
		$scrollWrap = $("#scroll_down");

	/* ------------------------------------------------------------
	 * [ Resize ]
	 * ------------------------------------------------------------ */


	function resize(winW,winH){

		if(winW > minWidth){
			var resizeW = winW;
		}else{
			var resizeW = minWidth;
		}

		if(winH > minHeight){
			var resizeH = winH;
		}else{
			var resizeH = minHeight;
		}

		if(divece!="pc"){
			var resizeH = resizeW*0.65;
		}

		$kvWrap.css({"width":resizeW,"height":resizeH});
		$kvInner.css({"top":[resizeH-$kvInner.height()]/2});
		$scrollWrap.css({"left":[resizeW-$scrollWrap.width()]/2});
	}

	/* ------------------------------------------------------------
	 * [ kvView ]
	 * ------------------------------------------------------------ */

	function firstShow (){
		$kvWrap.addClass("show");
		TweenMax.set($("#rpr_header li"),{x: '-200px',opacity: 0});
		TweenMax.set($("#rpr_header #rpr_site_logo"),{y: '-90px',opacity: 0});
		TweenMax.set($("#rpr_header #rp_sns"),{y: '-90px',opacity: 0});
		TweenMax.set($("#kv_wrap .product_name img"),{y: 40});
		TweenMax.set($("#kv_wrap #bg"),{scale:1.1,opacity:0});
			kvView(winW,winH);

		setTimeout(function(){
			var titleTween = new TweenMax.to($("#kv_wrap .product_name img"),1.2,
				{
					y:0,
					ease:Power4.easeOut,
				}
			);
			var bgTween = new TweenMax.to($("#kv_wrap #bg"),0.78,
				{
					delay: 0.85,
					scale: 1,
					opacity: 1,
					ease:Power1.easeOut,
				}
			);
			var navilogoTween = TweenMax.to($("#rpr_header #rpr_site_logo"), .78, {
				delay: 1.1,
	        	y:0,
	            opacity:1,
	            ease:Power4.easeOut,
	        });
			var navilistTween = TweenMax.staggerTo($("#rpr_header li"), 1.08, {
				delay: 1.2,
	        	x:0,
	            opacity:1,
	            ease:Power4.easeOut,
	        },0.04);
	        var navisnsTween = TweenMax.to($("#rpr_header #rp_sns"), .78, {
				delay: 1.2,
	        	y:0,
	            opacity:1,
	            ease:Power4.easeOut,
	        });
	        setTimeout(function(){
	        	var scrollTween = TweenMax.to($("#scroll_down"), .78, {
		            opacity:1,
		            ease:Power4.easeOut,
		        });
		        scrollMove();
				
			},1500);

		},1800);

	}

	/* ------------------------------------------------------------
	 * [ kvView ]

	 * ------------------------------------------------------------ */

		var bgFlg = 1,
			bgDelay = 6000,
			bgTimer = [];
	function kvView(winW,winH){
		var $Head = $("#kv_wrap"),
		$headLogo = $("#kv_wrap h1"),
		$headText = $("#kv_wrap h2"),
		$headBg = $("#kv_wrap #bg"),
		$scrollIcon = $("#kv_wrap #scroll_down");

		$headText.css({"margin-top":winH/2-68});

	    			kvslider();

		function kvslider(){
			clearTimeout(bgTimer);
		    	bgFlg ++;
			bgDelay = (bgFlg==1)?8000:6000;

		    bgTimer = setTimeout(function(){
		    	$("#kv_wrap #bg").animate({"opacity":"hide"},1500,"linear",function(){
		    		$("#kv_wrap #bg").removeClass();
		    		$("#kv_wrap #bg").addClass("slide0"+bgFlg+"");
		    	});
		    	$("#kv_wrap #bg").animate({"opacity":"show"},1500,"easeInQuad",function(){
	    			if(bgFlg == 6){
	    				bgFlg = 0;
	    			}
	    			kvslider();
	    		});
		    },bgDelay);

		}

	    // setInterval(function(){
	    // 	bgFlg ++;
	    // 	$("#kv_wrap #bg").animate({"opacity":"hide"},1500,"linear",function(){
	    // 		$("#kv_wrap #bg").removeClass();
	    // 		$("#kv_wrap #bg").addClass("slide0"+bgFlg+"");
	    // 	});
	    // 	$("#kv_wrap #bg").animate({"opacity":"show"},1500,"easeInQuad",function(){
    	// 		if(bgFlg == 6){
    	// 			bgFlg = 0;
    	// 		}
    	// 	});
	    // },6000);
	}

	/* ------------------------------------------------------------
	 * [ scrollMove ]
	 * ------------------------------------------------------------ */
	var $scrollIcon = $("#header #scroll_icon .icon,#scroll_down .arrow");

	function scrollMove(){
		$scrollIcon.delay(200).animate({"top":15,"opacity":0},600,"easeOutSine",function(){
			$(this).css({"top":-10});
			$(this).delay(300).animate({"top":0,"opacity":1},300,"easeOutSine",function(){
				scrollMove();
			});
		});

		// setInterval(function(){
	 //        $scrollIcon.delay(200).animate({"top":15},500,"easeOutQuart");
	 //        $scrollIcon.animate({"opacity":0},500,"easeOutQuart");
	 //        $scrollIcon.animate({"top":8},300,"easeOutQuart");
	 //        $scrollIcon.animate({"opacity":1},500,"easeOutQuart");
	 //    },200);
	}

	function scrollLoop(){
	}


	/* ------------------------------------------------------------
	 * [ newsSort ]
	 * ------------------------------------------------------------ */

	function newsSort(){
		var $List = $("#news nav ul li"),
		$newsWrap = $(".news_wrap");

		$List.on("click",function(){
			var Index = $(this).index();
			$List.removeClass("active");
			$List.eq(Index).addClass("active");
			$newsWrap.stop().animate({"opacity":0},300,function(){
				$newsWrap.children('div').css({"display":"none"});
				$newsWrap.children('div').eq(Index).css({"display":"block"});
				$newsWrap.children('div').removeClass("active");
				$newsWrap.children('div').eq(Index).addClass("active");
				$newsWrap.animate({"opacity":1},500);
			});
		});
	}


/* ============================================================
 * Execute JavaScript when the Window Object is fully loaded.
 * ============================================================ */
	winW = $win.width(),
	winH = $win.height();

	resize(winW,winH);

	if (isIE8){
		// $("#header h1 img").attr("src",$("#header h1 img").attr("src").replace(".svg", ".png"));
		// $("#header p img").attr("src",$("#header p img").attr("src").replace(".svg", ".png"));
	}


	$win.on('load', function() {
		firstShow();
		//kvView(winW,winH);
		resize(winW,winH);
	});// load End

	$win.on('resize', function() {
		winW = $win.width(),
		winH = $win.height();
		resize(winW,winH);
	});// load End

	if(divece!="pc"){
		$win.on('orientationchange', function(){
			winW = $win.width(),
			winH = $win.height();
			resize(winW,winH);
		});
	}



	$win.on('scroll', function(){

	});// scroll End

});//End









// require jQuery JavaScript Library v1.10.1

/* 即時関数でローカル化 */
$(function(){

/* ============================================================
 * defineFoundationVariable
 * ============================================================ */
	// Variables
	var $doc = $(document),
		$win = $(window),
		winW = $win.width(),
		winH = $win.height();

	var ua = navigator.userAgent;
	var isIE = ua.match(/msie/i),
		isIE6 = ua.match(/msie [6.]/i),
		isIE7 = ua.match(/msie [7.]/i),
		isIE8 = ua.match(/msie [8.]/i),
		isIE9 = ua.match(/msie [9.]/i),
		isIE10 = ua.match(/msie [10.]/i);

	var smpUa = {
		iPhone: navigator.userAgent.indexOf('iPhone') != -1,
		iPad: navigator.userAgent.indexOf('iPad') != -1,
		iPod: navigator.userAgent.indexOf('iPod') != -1,
		android: navigator.userAgent.indexOf('Android') != -1,
		windows: navigator.userAgent.indexOf('Windows Phone') != -1
	}

	var $silder = $("#slider");


/*===================================================================================
	[ scroll Icon ]
===================================================================================*/

function scrollIcon(){
	if(isIE8){
	}else{
		setInterval(function(){
			$("#scroll_icon").animate({"bottom":"-="+10,"opacity":"show"},500,"easeInOutExpo",
				function(){
					$(this).animate({"bottom":"-="+10,"opacity":"hide"},500,"easeOutSine",
						function(){
							$("#scroll_icon").css({"bottom":"+="+20});
						}
					);
				}
			);
		},1500);
	}
}

/* ============================================================
 * Execute JavaScript when the Window Object is fully loaded.
 * ============================================================ */

	// $doc.on('ready', function() {
	// 	resize(winW,winH);
	// });// ready End

	$win.on('load', function() {
		scrollIcon();
		if($("#slider")[0]){
			var kvSlider = $('#slider').bxSlider();
			kvSlider.reloadSlider({
				mode: 'fade',
				pager: true,
				auto: true,
				pause:6000,
				speed:2000,
				touchEnabled: false,
				onSliderLoad:function(){
					if($("#mailmagazine_banner")[0]) $("#mailmagazine_banner").delay(1000).animate({"opacity":1,"right":10},600,"easeOutQuart");
					if($("#scroll_icon")[0]) $("#scroll_icon").delay(1000).animate({"opacity":1},600,"easeOutQuart");
				},
				onSlideBefore:function(){
					if(isIE8){
						$("#product_name").css({"display":"none"});
					}else{
						$("#product_name").fadeOut(400);
					}
				},
				onSlideAfter:function(){
					var currentSlider = kvSlider.getCurrentSlide(),
						getThisColor = $('#slider li').eq(currentSlider).attr("class");

					if(isIE8){
						$("#product_name").removeClass().addClass(getThisColor).css({"display":"block"});
						$("#kv_slider_wrap #slider .white").css({backgroundSize: "cover"});
	        			$("#kv_slider_wrap #slider .black").css({backgroundSize: "cover"});
					}else{
						$("#product_name").removeClass().addClass(getThisColor).fadeIn(400);
					}
				}
			});
		}
	});// load End


});//End