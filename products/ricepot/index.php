﻿<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ja" class="ie ie6" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 7]>   <html lang="ja" class="ie ie7" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 8]>   <html lang="ja" class="ie ie8" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 9]>   <html lang="ja" class="ie ie9" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="ja" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">


<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1280">
<link rel="canonical" href="http://www.vermicular.jp/products/ricepot">
<title>バーミキュラ ライスポット　|Vermicular（バーミキュラ）公式サイト</title>
<meta name="author" content="愛知ドビー株式会社">
<meta name="description" content="バーミキュラ ライスポット　|Vermicular（バーミキュラ）公式サイト。バーミキュラライスポット。炊飯器を超えたのは、炊飯鍋でした。目指したのは、世界一、おいしいご飯が炊ける炊飯器。">
<meta name="keywords" content="ライスポット,炊飯鍋,炊飯器,バーミキュラ,Vermicular,ricepot,鋳物,愛知ドビー,バーミキュラ公式サイト">

<meta property="og:title" content="Vermicular（バーミキュラ） RICEPOT" />
<meta property="og:type" content="website">
<meta property="og:url" content="http://vermicular.jp/products/ricepot/index.php " />
<meta property="og:image" content="http://www.vermicular.jp/products/ricepot/common/img/ogp.jpg" />
<meta property="og:site_name" content="Vermicular（バーミキュラ） RICEPOT" />
<meta property="og:description" content="バーミキュラ ライスポット　|Vermicular（バーミキュラ）公式サイト。バーミキュラライスポット。炊飯器を超えたのは、炊飯鍋でした。目指したのは、世界一、おいしいご飯が炊ける炊飯器。" />
<meta property="fb:app_id" content="171078026685763" />

<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="Vermicular（バーミキュラ） RICEPOT">
<meta name="twitter:title" content="Vermicular（バーミキュラ） RICEPOT">
<meta name="twitter:url" content="/vermicular/products/ricepot/index.php ">
<meta name="twitter:description" content="バーミキュラ ライスポット　|Vermicular（バーミキュラ）公式サイト。バーミキュラライスポット。炊飯器を超えたのは、炊飯鍋でした。目指したのは、世界一、おいしいご飯が炊ける炊飯器。">


<meta name="format-detection" content="telephone=no,address=no,email=no">

<link rel="stylesheet" type="text/css" href="common/css/normalize.css?20161101">
<link rel="stylesheet" type="text/css" href="common/css/base.css?20161101">
<link rel="stylesheet" type="text/css" href="common/css/styles_nw.css?20161101">


<link rel="stylesheet" type="text/css" href="css/products.css">
<link rel="stylesheet" type="text/css" href="common/css/styles_nw.css">
<link rel="stylesheet" type="text/css" href="css/index.css">

<link href="common/img/apple-touch-icon.png" rel="apple-touch-icon">
<meta name="apple-mobile-web-app-title" content="Vermicular">

<script src="common/js/jquery-1.11.0.min.js?20161101"></script>
<script src="common/js/jquery.transit.js?20161101"></script>
<script src="common/js/pace.min.js?20161101"></script>
<script src="common/js/jquery.colorbox-min.js?20161101"></script>
<script src="common/js/var.js?20161101"></script>
<script src="common/js/function.js?20161101"></script>
<script src="common/js/base.js?20161101"></script>
<script src="common/js/jquery.bxslider.min.js?20161101"></script>

<script src="common/js/scripts_nw.js"></script>
<script src="common/js/lib.js"></script>
<script src="js/index.js"></script>




<!--[if lt IE 9]>
<script src="common/js/html5shiv.js"></script>
<script type="text/javascript" src="common/js/jquery.belatedPNG.min.js"></script>
<script>
$(function() {
	$(".pngfix").fixPng();
});
</script>
<script src="common/js/jquery.backgroundSize.js"></script>
<script>
$(function(){
 $('#header #bg').css('background-size', 'cover');
});
</script>
<![endif]-->


</head>
<body id="rp_top">
<div id="document">
	
	<header id="rpr_header">
		<div id="rpr_header_inner" class="clearfix">
			<h1 id="rpr_site_logo"><a href="http://www.vermicular.jp/" class="hover"><img src="common/img/site_logo.png" height="27" width="143" alt="VERMICULAR（バーミキュラ） MADE IN JAPAN"></a></h1>
			<nav id="rpr_gnavi">
				<ul class="clearfix">
					

					

					
					<li class="list"><a href="<?php echo $page_pass; ?>concept/" class="hover"><img src="common/img/n_gm/gnavi01.png" width="65" height="17" alt="コンセプト"></a></li>
					
					

                                 
					
					<li class="list">
					<a class="hover"><img src="common/img/n_gm/gnavi03.png" width="75" height="17" alt="選ばれる理由"></a>
					<ul class="dd_list">
						<li class="list01"><a href="<?php echo $page_pass; ?>technology/">テクノロジー</a></li>
						<li class="list02"><a href="<?php echo $page_pass; ?>philosophy/">デザイン思想</a></li>
						<li class="list03"><a href="<?php echo $page_pass; ?>cookingmode/">調理機能</a></li>
						<li class="list04"><a href="<?php echo $page_pass; ?>safe/">安心・安全</a></li>
						<li class="list05"><a href="<?php echo $page_pass; ?>accessories/">付属品</a></li>
					</ul>
					</li>


					<li class="list"><a href="<?php echo $page_pass; ?>care/" class="hover"><img src="common/img/n_gm/gnavi04.png" width="90" height="17" alt="使い方・お手入れ"></a></li>


					<li class="list">
					<span><a class="hover"><img src="common/img/n_gm/gnavi05.png" width="90" height="17" alt="スペック・FAQ"></a></span>
					<ul class="dd_list">
						<li class="list01"><a href="<?php echo $page_pass; ?>spec/">スペック</a></li>
						<li class="list02"><a href="<?php echo $page_pass; ?>faq/">FAQ</a></li>
					</ul>
					</li>



					<li class="list"><a href="http://shop.vermicular.jp/jp/group.php?id=36" class="hover"><img src="common/img/n_gm/gnavi06.png" width="105" height="17" alt="オンラインショップ"></a></li> 	




				</ul>
				</nav>




			<nav id="rp_sns">
			<div class="btn"  class="hover"><img src="common/img/head_sns_icon_btn.png" height="14" width="10" alt="SNS"></div>
			<ul class="sns_list clearfix">
				<li class="sns01"><a href="https://www.facebook.com/vermicular" target="_blank"  class="hover"><img src="common/img/head_sns_icon_01.png" height="17" width="7" alt="Facebook"></a></li>
				<li class="sns02"><a href="https://twitter.com/_VERMICULAR_" target="_blank"  class="hover"><img src="common/img/head_sns_icon_02.png" height="11" width="14" alt="Twitter"></a></li>
				<li class="sns03"><a href="https://instagram.com/vermicular_japan/" target="_blank"  class="hover"><img src="common/img/head_sns_icon_03.png" height="13" width="13" alt="Instagram"></a></li>
				<li class="sns04"><a href="http://shop.vermicular.jp/mail/" target="_blank"  class="hover"><img src="common/img/head_sns_icon_04.png" height="10" width="15" alt="Mail"></a></li>
			</ul>
			</nav>



		</div>
	</header><!-- /#header -->
	
	<main id="main_wrap">	<header id="kv_wrap">
		<div id="kv_inner">
		<h1 class="product_name motion"><img src="img/top/tit_main.svg" height="100%" width="100%" alt="VERMICULAR RICE POT"></h1>
		</div>
		<span id="bg"></span>
		<div id="scroll_down">
				<div class="text"><img src="img/products/common/scroll_down_text.png" height="11" width="36" alt="Scroll"></div>
				<div class="arrow"><img src="img/products/common/scroll_down_arrow.png" height="18" width="36" alt=""></div>
		</div>
	</header>

		<div id="slider_wrap">
			<div id="kv_slider_wrap">
				<ul id="slider">
					<li id="slide01" class="white">
						<h2 class="copy center"><img src="img/kv/kv_img01_copy.png" height="25" width="547" alt="鍋炊きご飯のおいしさと、指先ひとつのカンタンさ。 "></h2>
					</li>
					<li id="slide02" class="white">
						<h2 class="copy center"><img src="img/kv/kv_img01_copy.png" height="25" width="547" alt="鍋炊きご飯のおいしさと、指先ひとつのカンタンさ。 "></h2>
					</li>
					<li id="slide03" class="black">
						<h2 class="copy center"><img src="img/kv/kv_img01_copy.png" height="25" width="547" alt="鍋炊きご飯のおいしさと、指先ひとつのカンタンさ。 "></h2>
					</li>
				</ul>
			</div>
			<div class="btn_wrap">
<div class="btn clear_black w198 center left"><a href="spec/" ><span class="btn_inner"><img src="img/top/btn_spec.png" height="10" width="46" alt="スペック"></span></a></div>
<div class="btn clear_black w198 center right"><a href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="img/products/top/onlineshop_text.png" height="13" width="101" alt="オンラインショップ"></span></span></a></div>
			</div>
		</div>

		<section id="section_wrap01" class="bg_section hover">
			<a href="concept/">
			<div class="inner clearfix">
				<div class="text_wrap">
					<h1 class="copy"><img src="img/top/section_copy01.png" height="120" width="443" alt="目指したのは目指したのは、世界一、お米本来の味を引き出す炊飯器。"></h1>
					<p class="text">あのバーミキュラの鋳物ホーロー鍋と、<br/>
						理想の火加減を指先ひとつで可能にするポットヒーターが出会い、<br/>
						最高の炊飯器が誕生しました。</p>
					<p class="link"><img src="img/products/common/read_more_link_text.png" height="10" width="78" alt="Read More"></p>
				</div>
			</div>
			</a>
		</section><!-- /#section_wrap01 -->



		<section id="section_wrap02" class="bg_section hover">
			<a href="technology/">
			<div class="inner clearfix">
				<div class="text_wrap">
					<h1 class="copy"><img src="img/top/section_copy02.png" height="120" width="329" alt="おいしさの秘密は、革新のテクノロジー。"></h1>
					<p class="text">熱の伝わり方をコントロールするトリプルサーモテクノロジーと、<br/>
						かまどのように立体的に加熱するラップアップヒートテクノロジー。</p>
					<p class="link"><img src="img/products/common/read_more_link_text.png" height="10" width="78" alt="Read More"></p>
				</div>
			</div>
			</a>
		</section><!-- /#section_wrap02 -->

		<section id="section_wrap03" class="bg_section hover">
			<a href="philosophy/">
			<div class="inner clearfix">
				<div class="text_wrap">
					<h1 class="copy"><img src="img/top/section_copy03.png" height="118" width="253" alt="ライスポットに、保温機能がない理由。"></h1>
					<p class="text">それは、究極のおいしいご飯を炊くためです。<br/>
						理想の炊飯には、一般的な炊飯器にある保温用</br>
						のフタを無くす必要がありました。</p>
					<p class="link"><img src="img/top/read_more_link_text_black.png" height="10" width="78" alt="Read More"></p>
				</div>
			</div>
			</a>
		</section><!-- /#section_wrap03 -->

		<section id="section_wrap04" class="bg_section hover">
			<a href="cookingmode/">
			<div class="inner clearfix">
				<div class="text_wrap">
					<h1 class="copy"><img src="img/top/section_copy04.png" height="118" width="302" alt="この火加減、炊飯だけではもったいない。"></h1>
					<p class="text">素材本来の味を引き出す鍋と、理想の熱源がひとつになったラ<br/>
						イスポット。炊飯だけでなく、バーミキュラの鍋で楽しめる全て<br/>
						の調理が、誰にでも理想的な火加減でできるのです。</p>
					<p class="link"><img src="img/products/common/read_more_link_text.png" height="10" width="78" alt="Read More"></p>
				</div>
			</div>
			</a>
		</section><!-- /#section_wrap04 -->

		<section id="section_wrap05" class="bg_section hover">
			<a href="care/">
			<div class="inner clearfix">
				<div class="text_wrap">
					<h1 class="copy"><img src="img/top/section_copy05.png" height="78" width="468" alt="操作はシンプルに、お手入れは簡単に。"></h1>
					<p class="text">白米・玄米・おこげ・おかゆが自由自在。<br/>
						温度と時間の管理はおまかせ。<br/>
						メンテナンスもカンタンだから、いつも衛生的です。</p>
					<p class="link"><img src="img/products/common/read_more_link_text.png" height="10" width="78" alt="Read More"></p>
				</div>
			</div>
			</a>
		</section><!-- /#section_wrap05 -->

		<div class="banner_section">
			<h3 class="title"><img src="img/top/tit_section06.png" height="48" width="209" alt="More Information"></h3>
			<ul class="banner_list clearfix inner">
				<li><dl><dt><a href="safe/"><img src="img/top/banner_img01.jpg" height="230" width="540" alt="安心・安全"></a></dt></dl></li>
				<li><dl><dt><a href="accessories/"><img src="img/top/banner_img02.jpg" height="230" width="540" alt="付属品/アクセサリー"></a></dt></dl></li>
				<li><dl><dt><a href="faq/"><img src="img/top/banner_img03.jpg" height="230" width="540" alt="よくある質問"></a></dt></dl></li>
				<li><dl><dt><a href="../../shoplist/?ricepot"><img src="img/top/banner_img04.jpg" height="230" width="540" alt="取り扱いショップリスト"></a></dt></dl></li>
			</ul>
		</div>
		
		
		
		
<a id="online_btn" href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank">
	<img src="common/img/btn_online.png" width="130" height="130" alt="Online Shop">
</a>


			


<div class="btn clear_black w198 center"><a href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="img/products/top/onlineshop_text.png" height="13" width="101" alt="オンラインショップ"></span></span></a></div>
		</br>
			</div>
		
	
		


<nav class="gnav_bottom">
			<p class="ttl"><a href="" class="hover"><img src="common/img/gnav_bottom_logo.svg" height="12" width="191" alt="VERMICULAR RICE POT" /></a></p>
		</nav>	</main><!-- /#main_wrap -->

	<footer id="footer">
		<div class="inner_min clearfix">
			<p class="title"><img src="common/img/tit_footer.png" width="76" height="18" alt="Sitemap"></p>
			<div class="footer_sitemap fll">
				<div class="list">
					<dl>
						<dt>製品ラインナップ</dt>
						<dd>
							<ul>
								<li>
									<dl>
										<dt>鋳物ホーロー鍋</dt>
										<dd>
											<ul>
												<li><a href="../../products/">オーブンポットラウンド</a></li>
											</ul>
										</dd>
									</dl>
								</li>
								<li>
									<dl>
										<dt>キッチン家電</dt>
										<dd>
											<ul>
												<li><a href="../../products/ricepot/">バーミキュラ ライスポット</a></li>
											</ul>
										</dd>
									</dl>
								</li>
								<li>
									<dl>
										<dt>キッチンアイテム</dt>
										<dd>
											<ul>
												<li><a href="../../products/kitchenitems/magnettrivet.html">ナチュラルウッドシリーズ</a></li>
												<li><a href="../../products/kitchenitems/hk_and_ph.html">オーガニックコットンシリーズ</a></li>
												<li><a href="../../products/kitchenitems/apron_and_cloth.html">オーガニックリネンシリーズ</a></li>
											</ul>
										</dd>
									</dl>
								</li>
								<li>
									<dl>
										<dt>食べ物</dt>
										<dd>
											<ul>
												<li><a href="../../products/food/rice.html">有機栽培こしひかり</a></li>
												<li><a href="../../products/food/curry.html">バーミキュラ カレールーセット</a></li>
											</ul>
										</dd>
									</dl>
								</li>
								<li>
									<dl>
										<dt>レシピ本</dt>
										<dd>
											<ul>
												<li><a href="http://shop.vermicular.jp/jp/item.php?id=I00000201" class="ex_link black" target="_blank">Vermicular Recipe Book 00号</a></li>
												<li><a href="http://shop.vermicular.jp/jp/item.php?id=I00000206" class="ex_link black" target="_blank">Vermicular Recipe Book 01号</a></li>
											</ul>
										</dd>
									</dl>
								</li>
							</ul>
						</dd>
					</dl>
				</div><!-- /.list -->
				<div class="list">
					<dl>
						<dt>バーミキュラについて</dt>
						<dd>
							<ul>
								<li><a href="../../about/us/">5つの約束</a></li>
								<li><a href="../../about/us/story/">開発ストーリー</a></li>
								<li><a href="../../voices/">お客様の声</a></li>
								<li><a href="../../taste/howto/">ご使用方法</a></li>
							</ul>
						</dd>
					</dl>
					<dl>
						<dt>レシピ</dt>
						<dd>
							<ul>
								<li><a href="https://owners.vermicular.jp/">オーナーズマイページ</a></li>
							</ul>
						</dd>
					</dl>
					<dl>
						<dt>ご購入</dt>
						<dd>
							<ul>
								<li><a href="http://shop.vermicular.jp/jp/" class="ex_link black" target="_blank">オンラインショップ</a></li>
								<li><a href="../../shoplist/" class="ex_link black" target="_blank">取扱店舗</a></li>
							</ul>
						</dd>
					</dl>
				</div><!-- /.list -->
				<div class="list last">
					<dl>
						<dt>サポート</dt>
						<dd>
							<ul>
								<li><a href="../../support/">オーナーズデスク</a></li>
								<li><a href="../../faq/">よくある質問</a></li>
							</ul>
						</dd>
					</dl>
					<dl>
							<dt>私たちについて</dt>
						<dd>
							<ul>
								<li><a href="../../company/">私たちについて</a></li>
								<li><a href="../../recruit/">採用情報</a></li>
								<li><a href="../../pr/">取材に関する問い合わせ先</a></li>
							</ul>
						</dd>
					</dl>
					<dl>
						<dt>その他</dt>
						<dd>
							<ul>
								<li><a href="../../privacy/">プライバシーポリシー</a></li>
								<li><a href="http://www.vermicular.com/?lang=en">English</a></li>
							</ul>
						</dd>
					</dl>
				</div><!-- /.list -->
			</div>
			<div class="footer_other_cnt flr">
				<div class="list">
					<dl>
						<dt>オンラインショップ</dt>
						<dd class="banner">
							<a href="http://shop.vermicular.jp/jp/" target="_blank"><img src="common/img/bnr_footer01.jpg" width="280" height="100" alt="Online Shop"></a>
						</dd>
					</dl>
					<dl>
						<dt>バーミキュラの最新情報はこちら</dt>
						<dd>
							<div class="footer_facebook_wrap">
								<div id="fb-root"></div>
								<script>(function(d, s, id) {
								var js, fjs = d.getElementsByTagName(s)[0];
								if (d.getElementById(id)) return;
								js = d.createElement(s); js.id = id;
								js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.5";
								fjs.parentNode.insertBefore(js, fjs);
								}(document, 'script', 'facebook-jssdk'));</script>
								<div class="fb-page" data-href="https://www.facebook.com/vermicular" data-width="280" data-height="205" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/vermicular"><a href="https://www.facebook.com/vermicular">バーミキュラ（Vermicular）</a></blockquote></div></div>
							</div><!-- /.footer_facebook_wrap -->

							<ul class="sns_list">
								<li class="btn clear_gray w133 fll"><a href="https://twitter.com/_VERMICULAR_" target="_blank"><span class="btn_inner"><img src="common/img/footer_btn_text01.png" alt=""></span></a></li>
								<li class="btn clear_gray w133 flr"><a href="https://instagram.com/vermicular_japan/" target="_blank"><span class="btn_inner"><img src="common/img/footer_btn_text02.png" alt=""></span></a></li>
								<li class="btn clear_gray w278 clear mail"><a href="http://shop.vermicular.jp/mail/" target="_blank"><span class="btn_inner"><img src="common/img/footer_btn_text03.png" alt=""></span></a></li>
							</ul>

						</dd>
					</dl>
				</div>
			</div>
		</div>
		<div><!-- /.inner_min -->
		<p id="copy"><img src="common/img/txt_copy.png" width="252" height="12" alt="© Aichi Dobby. LTD. All Rights Reserved."></p>
		</div>
	</footer><!-- /#footer -->
</div><!-- /#document -->
<a id="page_top" href="#document"><img src="common/img/btn_pagetop.png" width="45" height="45" alt="↑"></a>
<script type="text/javascript"> 

var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www."); 

document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E")); 

</script> 

<script type="text/javascript"> 

try { 

var pageTracker = _gat._getTracker("UA-12538240-1"); 

pageTracker._trackPageview(); 

} catch(err) {}
</script>
</body>
</html>