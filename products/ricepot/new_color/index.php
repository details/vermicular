<?php
$page_id = 'rp_new_color'; //ページ独自にスタイルを指定する場合は入力
$page_type = 'ricepot'; // [ オーブンポッドラウンドのページの場合 「oven_pot_round」 ]
$page_title = 'NEW COLORS DEBUT | Vermicular（バーミキュラ）ライスポット'; //ページ名
$page_description = '';
$page_keywords = '';
$page_pass = '../'; //自分の階層を指定
$page_directry = 'products/ricepot/new_color'; //自分のディレクトリ名を指定 ?>
<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ja" class="ie ie6" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 7]>   <html lang="ja" class="ie ie7" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 8]>   <html lang="ja" class="ie ie8" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 9]>   <html lang="ja" class="ie ie9" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="ja" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="Content-Style-Type" content="text/css">
    <meta http-equiv="Content-Script-Type" content="text/javascript">


    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=1280">
    <link rel="canonical" href="http://www.vermicular.jp/products/ricepot/spec">
    <title>NEW COLORS DEBUT | Vermicular（バーミキュラ）ライスポット</title>
    <meta name="author" content="愛知ドビー株式会社">
    <meta name="description" content="ライスポットに新しい２つのカラーが登場しました。食のシーンを彩るカラーで、幅広いライフスタイルを演出します。">
    <meta name="keywords" content="新色,ライスポット,炊飯鍋,炊飯器,バーミキュラ,Vermicular,ricepot,鋳物,愛知ドビー">

    <meta property="og:title" content="NEW COLORS DEBUT | Vermicular（バーミキュラ）ライスポット" />
    <meta property="og:type" content="website">
    <meta property="og:url" content="http://www.vermicular.jp/products/ricepot/spec/index.php " />
    <meta property="og:image" content="http://www.vermicular.jp/products/ricepot/common/img/ogp.jpg" />
    <meta property="og:site_name" content="NEW COLORS DEBUT | Vermicular（バーミキュラ）ライスポット" />
    <meta property="og:description" content="ライスポットに新しい２つのカラーが登場しました。食のシーンを彩るカラーで、幅広いライフスタイルを演出します。" />
    <meta property="fb:app_id" content="171078026685763" />

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="NEW COLORS DEBUT | Vermicular（バーミキュラ）ライスポット">
    <meta name="twitter:title" content="NEW COLORS DEBUT | Vermicular（バーミキュラ）ライスポット">
    <meta name="twitter:url" content="/vermicular/products/ricepot/spec/index.php ">
    <meta name="twitter:description" content="ライスポットに新しい２つのカラーが登場しました。食のシーンを彩るカラーで、幅広いライフスタイルを演出します。">

    <meta name="format-detection" content="telephone=no,address=no,email=no">

    <link rel="stylesheet" type="text/css" href="../common/css/normalize.css?20161101">
    <link rel="stylesheet" type="text/css" href="../common/css/base_nw.css?20161101">
    <link rel="stylesheet" type="text/css" href="../common/css/styles_nw.css?20161101">
    <link rel="stylesheet" type="text/css" href="../common/css/lowlayer.css?20161101">
    <link rel="stylesheet" type="text/css" href="../css/new_color.css">

    <link href="../common/img/apple-touch-icon.png" rel="apple-touch-icon">
    <meta name="apple-mobile-web-app-title" content="Vermicular">

    <script src="../common/js/jquery-1.11.0.min.js?20161101"></script>
    <script src="../common/js/jquery.transit.js?20161101"></script>
    <script src="../common/js/pace.min.js?20161101"></script>
    <script src="../common/js/jquery.colorbox-min.js?20161101"></script>
    <script src="../common/js/var.js?20161101"></script>
    <script src="../common/js/function.js?20161101"></script>
    <script src="../common/js/base.js?20161101"></script>
    <script src="../common/js/jquery.bxslider.min.js?20161101"></script>

    <script src="../common/js/scripts_nw.js"></script>
    <script src="../js/lib.js"></script>
    <script src="../js/new_color.js"></script>

    <!--[if lt IE 9]>
    <script src="../common/js/html5shiv.js"></script>
    <script type="text/javascript" src="../common/js/jquery.belatedPNG.min.js"></script>
    <script>
        $(function() {
            $(".pngfix").fixPng();
        });
    </script>
    <script src="../common/js/jquery.backgroundSize.js"></script>
    <script>
        $(function(){
            $('#header #bg').css('background-size', 'cover');
        });
    </script>
    <![endif]-->
</head>
<body id="rp_new_color">
<div id="document">

    <header id="rpr_header">
        <div id="rpr_header_inner" class="clearfix">
            <h1 id="rpr_site_logo"><a href="http://www.vermicular.jp/" class="hover"><img src="../common/img/site_logo.png" height="27" width="143" alt="VERMICULAR（バーミキュラ） MADE IN JAPAN"></a></h1>
            <nav id="rpr_gnavi">
                <ul class="clearfix">





                    <li class="list"><a href="../concept/" class="hover"><img src="../common/img/n_gm/gnavi01.png" width="65" height="17" alt="コンセプト"></a></li>




                    <li class="list">
                        <a class="hover"><img src="../common/img/n_gm/gnavi03.png" width="75" height="17" alt="選ばれる理由"></a>
                        <ul class="dd_list">
                            <li class="list01"><a href="../technology/">テクノロジー</a></li>
                            <li class="list02"><a href="../philosophy/">デザイン思想</a></li>
                            <li class="list03"><a href="../cookingmode/">調理機能</a></li>
                            <li class="list04"><a href="../safe/">安心・安全</a></li>
                            <li class="list05"><a href="../accessories/">付属品</a></li>
                        </ul>
                    </li>


                    <li class="list"><a href="../care/" class="hover"><img src="../common/img/n_gm/gnavi04.png" width="90" height="17" alt="使い方・お手入れ"></a></li>


                    <li class="list">
                        <span><a class="hover"><img src="../common/img/n_gm/gnavi05.png" width="90" height="17" alt="スペック・FAQ"></a></span>
                        <ul class="dd_list">
                            <li class="list01"><a href="../spec/">スペック</a></li>
                            <li class="list02"><a href="../faq/">FAQ</a></li>
                        </ul>
                    </li>



                    <li class="list"><a href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank"  class="hover"><img src="../common/img/n_gm/gnavi06.png" width="105" height="17" alt="オンラインショップ"></a></li>




                </ul>
            </nav>



            <nav id="rp_sns">
                <div class="btn"  class="hover"><img src="../common/img/head_sns_icon_btn.png" height="14" width="10" alt="SNS"></div>
                <ul class="sns_list clearfix">
                    <li class="sns01"><a href="https://www.facebook.com/vermicular" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_01.png" height="17" width="7" alt="Facebook"></a></li>
                    <li class="sns02"><a href="https://twitter.com/_VERMICULAR_" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_02.png" height="11" width="14" alt="Twitter"></a></li>
                    <li class="sns03"><a href="https://instagram.com/vermicular_japan/" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_03.png" height="13" width="13" alt="Instagram"></a></li>
                    <li class="sns04"><a href="http://shop.vermicular.jp/mail/" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_04.png" height="10" width="15" alt="Mail"></a></li>
                </ul>
            </nav>
        </div>
    </header><!-- /#header -->

    <div id="scroll-container">
        <div id="scroll-body">

    <main id="main_wrap">

	<header id="kv-content">
		<div id="kv-content__inner">
			<h1 class="page_title">
				<span class="top">
                    NEW COLOR DEBUT.
                </span>
                <span class="bottom">
                    <img src="../img/new_color/tit_kv02.svg" height="13" width="200" alt="VERMICULAR RICEPOT">
                </span>
			</h1>
            <div class="bg"></div>
            <div id="scroll_down">
                <div class="text"><img src="../img/products/common/scroll_down_text.png?2" height="11" width="36" alt="Scroll"></div>
                <div class="arrow"><img src="../img/products/common/scroll_down_arrow.png?2" height="18" width="36" alt=""></div>
            </div>
		</div>
	</header>

    <div id="main-content" class="js__scr-target">
        <div id="main-content__inner">
            <h2 class="title js__text-fade">NEW COLOR<br>DEBUT.</h2>
            <p class="text js__plane-text">
                <span>ライスポットに新しい２つのカラーが登場しました。</span>
                <span>食のシーンを彩るカラーで、幅広いライフスタイルを演出します。</span></p>
            <div id="ricepot_image" class="js__object-fade bottom">
                <img src="../img/new_color/img_main_gray.png?2" height="432" width="960">
                <canvas id="color-white" width="554" height="382"></canvas>
            </div>
        </div>
    </div><!-- #new_color_main -->

    <section id="new_color_gray">

        <div class="scene-content">
            <div class="scene-content__inner js__scr-target">
                <p class="scene-content__title js__use-text">
                    <span class="text">use of scene</span>
                    <span class="object01"></span>
                    <span class="object02"></span>
                </p>
                <div class="scene-content__image js__para-content">
                    <!-- js__photo-fade left -->
                    <div class="image01 " data-delay="0">
                        <div>
                            <span class="bg js__para js__type-x" data-rate="0.01"></span>
                        </div>
                    </div>
                    <div class="image02 js__photo-fade bottom js__scr-para js__para js__type-minus" data-rate="0.2" data-shift="-10.0" data-delay="0.8">
                        <div>
                            <span class="bg"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .scene-content -->

        <div class="color-content js__para-content">
            <div class="color-content__inner js__scr-target">
                <h2 class="color-content__title">
                    <span class="color-content__title-color js__color-num js__scr-para" data-shift="7.2">
                        <span class="en">New Color</span>
                        <span class="line"></span>
                        <span class="number">01</span>
                    </span>
                    <span class="js__scr-para" data-shift="5.8">
                        <span class="color-content__title-name js__color-title">
                            <img src="../img/new_color/tit_gray_mask.svg" height="338" width="523" alt="Truffle Gray">
                            <span class="bg"></span>
                        </span>
                    </span>
                </h2>
                <div class="color-content__text js__object-fade bottom js__scr-para" data-shift="6.8" data-delay="0">
                    <h3 class="title">
                        <span class="concept">Color Concept</span>
                        <span class="copy">芳醇で静謐な佇まいの<br>
                        トリュフグレー。</span>
                    </h3>
                    <p class="text">まるで、森の中で長い時間、誰かに見つけられるそのときを、芳醇な香りを保ちながら待ち続けるトリュフのように静かにキッチンに佇むトリュフグレー。高級感のある、落ち着いたシーンを演出します。</p>
                </div>
                <div class="color-content__image right js__scr-para js__para js__type-x js__dir-left" data-rate="0.2" data-shift="12.0" data-delay="0">
                    <img src="../img/new_color/img_gray01.png?2" height="695" width="1437">
                </div>
            </div>
        </div><!-- .color-content -->

        <div class="detail-content js__para-content js__scr-target">
            <div class="detail-content__inner">
                <p class="detail-content__caption js__photo-fade right" data-delay="0">
                    <span><span>Color Detail</span></span>
                </p>
                <div class="detail-content__text">
                    <div class="detail-content__text-inner">
                        <h4 class="title js__text-fade">現代的な空間に合う<br>
                        重厚な存在感。</h4>
                        <p class="text js__plane-text">
                            <span>質感溢れる深味のある木材や、黒いスチールやコンクリートなどをベースとした現代的な空間によく馴染みます。また、照明を落とした薄明りの中でも重厚な存在感を醸し出します。</span>
                        </p>
                    </div>
                </div>
                <div class="detail-content__image">
                    <div class="image01 left js__scr-para js__para js__type-minus" data-rate="0.2" data-shift="10.8">
                        <div class="js__photo-fade top" data-delay="0">
                            <div>
                                <img src="../img/new_color/img_gray02.jpg?2" height="440" width="440">
                            </div>
                        </div>
                    </div>
                    <div class="image02 right js__scr-para js__para" data-rate="0.1" data-shift="3.2">
                        <div class="js__photo-fade bottom" data-delay="0.1">
                            <div>
                                <img src="../img/new_color/img_gray03.jpg?2" height="440" width="440">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .detail-content -->

    </section><!-- #new_color_gray -->

    <section id="new_color_white">

        <div class="scene-content">
            <div class="scene-content__inner js__scr-target">
<!--                <p class="scene-content__title js__use-text">-->
<!--                    <span class="text">use of scene</span>-->
<!--                    <span class="object01"></span>-->
<!--                    <span class="object02"></span>-->
<!--                </p>-->
                <div class="scene-content__image js__para-content">
                    <div class="image01 right" data-delay="0">
                        <div>
                            <span class="bg js__para js__type-x" data-rate="0.01"></span>
                        </div>
                    </div>
                    <div class="image02 js__photo-fade bottom js__scr-para" data-shift="-10.0" data-delay="0.8">
                        <div>
                            <span class="bg"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .scene-content -->

        <div class="color-content js__para-content">
            <div class="color-content__inner js__scr-target">
                <h2 class="color-content__title">
                    <span class="color-content__title-color js__color-num js__scr-para" data-shift="7.2">
                        <span class="en">New Color</span>
                        <span class="line"></span>
                        <span class="number">02</span>
                    </span>
                    <span class="js__scr-para" data-shift="5.8">
                        <span class="color-content__title-name js__color-title">
                            <img src="../img/new_color/tit_white_mask.svg" height="303" width="528" alt="Seasolt White">
                            <span class="bg"></span>
                        </span>
                    </span>
                </h2>
                <div class="color-content__text js__object-fade bottom js__scr-para" data-shift="6.8" data-delay="0">
                    <h3 class="title">
                        <span class="concept">Color Concept</span>
                        <span class="copy">天然塩のようなやさしい質感、<br>
                            シーソルトホワイト。</span>
                    </h3>
                    <p class="text">海水を乾燥させて、混じり気のない自然の成分だけでつくった天然塩のようなやさしい質感のシーソルトホワイト。そこに置かれるだけで、モダンで清潔感のあるキッチンシーンが生まれます。</p>
                </div>
                <div class="color-content__image left js__scr-para js__para js__type-x js__dir-right" data-rate="0.2" data-shift="12.0" data-delay="0">
                    <img src="../img/new_color/img_white01.png?2" height="747" width="1240">
                </div>
            </div>
        </div><!-- .color-content -->

        <div class="detail-content js__scr-target">
            <div class="detail-content__inner">
                <p class="detail-content__caption js__photo-fade right" data-delay="0">
                    <span><span>Color Detail</span></span>
                </p>
                <div class="detail-content__text js__scr-para" data-shift="6.8">
                    <div class="detail-content__text-inner">
                        <h4 class="title js__text-fade">アートのようなオーラと、<br>
                            自然な風合いのあたたかさ。</h4>
                        <p class="text js__plane-text">
                            <span>白を基調としたキッチンやインテリア、シンプルでアーティスティックな空間によく合います。調理で使うと、自然で暖かい、家庭的な風合いも感じられます。</span>
                        </p>
                    </div>
                </div>
                <div class="detail-content__image">
                    <div class="image01 right js__scr-para js__para js__type-minus" data-rate="0.3" data-shift="10.8">
                        <div class="js__photo-fade top" data-delay="0">
                            <div>
                                <img src="../img/new_color/img_white02.jpg?2" height="440" width="440">
                            </div>
                        </div>
                    </div>
                    <div class="image02 left js__scr-para js__para" data-rate="0.1" data-shift="3.2">
                        <div class="js__photo-fade bottom" data-delay="0.1">
                            <div>
                                <img src="../img/new_color/img_white03.jpg?2" height="440" width="440">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .detail-content -->

        <div class="spec_order-content">
            <div class="spec_order-content__inner js__scr-target">
                <p class="spec_order-content__title js__use-text">
                    <span class="text">Spec / Order</span>
                    <span class="object01"></span>
                    <span class="object02"></span>
                </p>
                <div class="spec_order-content__btn js__object-fade bottom">
                    <div class="spec_order-content__btn-bg">
                        <div class="btn_wrap">
                            <div class="btn clear w198 center left"><a href="../spec/" ><span class="btn_inner"><img src="../img/new_color/btn_spec.svg" height="10" width="46" alt="スペック"></span></a></div>
                            <div class="btn clear w198 center right"><a href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank"><span class="btn_inner"><span class="ex_link white"><img src="../img/new_color/btn_reservation.svg" height="12" width="58" alt="先行予約"></span></span></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .spec_order-content -->

    </section><!-- #new_color_white -->


<?php
include '../_footer_rp.php';
?>