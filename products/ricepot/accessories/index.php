﻿<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ja" class="ie ie6" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 7]>   <html lang="ja" class="ie ie7" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 8]>   <html lang="ja" class="ie ie8" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 9]>   <html lang="ja" class="ie ie9" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="ja" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">


<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1280">
<link rel="canonical" href="http://www.vermicular.jp/products/ricepot/accessories">
<title>付属品 | Vermicular（バーミキュラ） RICEPOT</title>
<meta name="author" content="愛知ドビー株式会社">
<meta name="description" content="付属品と呼びたくないくらいの力作ぞろい。">
<meta name="keywords" content="付属品,料理がさらに楽しくなる,もっと料理をしたくなる,レシピブック,計量カップ,リッドスタンド,ヒートキーパー,トリベット,ポットホルダー,ライスポット,炊飯鍋,炊飯器,バーミキュラ,Vermicular,ricepot,鋳物,愛知ドビー">

<meta property="og:title" content="付属品 | Vermicular（バーミキュラ） RICEPOT" />
<meta property="og:type" content="website">
<meta property="og:url" content="http://www.vermicular.jp/products/ricepot/accessories/index.php " />
<meta property="og:image" content="http://www.vermicular.jp/products/ricepot/common/img/ogp.jpg" />
<meta property="og:site_name" content="付属品 | Vermicular（バーミキュラ） RICEPOT" />
<meta property="og:description" content="付属品と呼びたくないくらいの力作ぞろい。" />
<meta property="fb:app_id" content="171078026685763" />

<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="付属品 | Vermicular（バーミキュラ） RICEPOT">
<meta name="twitter:title" content="付属品 | Vermicular（バーミキュラ） RICEPOT">
<meta name="twitter:url" content="/vermicular/products/ricepot/accessories/index.php ">
<meta name="twitter:description" content="付属品と呼びたくないくらいの力作ぞろい。">

<meta name="format-detection" content="telephone=no,address=no,email=no">

<link rel="stylesheet" type="text/css" href="../common/css/normalize.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/base_nw.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/styles_nw.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/lowlayer.css?20161101">


<link rel="stylesheet" type="text/css" href="../css/accessories.css">

<link href="../common/img/apple-touch-icon.png" rel="apple-touch-icon">
<meta name="apple-mobile-web-app-title" content="Vermicular">

<script src="../common/js/jquery-1.11.0.min.js?20161101"></script>
<script src="../common/js/jquery.transit.js?20161101"></script>
<script src="../common/js/pace.min.js?20161101"></script>
<script src="../common/js/jquery.colorbox-min.js?20161101"></script>
<script src="../common/js/var.js?20161101"></script>
<script src="../common/js/function.js?20161101"></script>
<script src="../common/js/base.js?20161101"></script>
<script src="../common/js/jquery.bxslider.min.js?20161101"></script>

<script src="../common/js/scripts_nw.js"></script>

<!--[if lt IE 9]>
<script src="../common/js/html5shiv.js"></script>
<script type="text/javascript" src="../common/js/jquery.belatedPNG.min.js"></script>
<script>
$(function() {
	$(".pngfix").fixPng();
});
</script>
<script src="../common/js/jquery.backgroundSize.js"></script>
<script>
$(function(){
 $('#header #bg').css('background-size', 'cover');
});
</script>
<![endif]-->
</head>
<body id="rp_accessories">
<div id="document">
	
	<header id="rpr_header">
		<div id="rpr_header_inner" class="clearfix">
			<h1 id="rpr_site_logo"><a href="http://www.vermicular.jp/" class="hover"><img src="../common/img/site_logo.png" height="27" width="143" alt="VERMICULAR（バーミキュラ） MADE IN JAPAN"></a></h1>
			<nav id="rpr_gnavi">
				<ul class="clearfix">
					
					<li class="list"><a href="../concept/" class="hover"><img src="../common/img/n_gm/gnavi01.png" width="65" height="17" alt="コンセプト"></a></li>

					<li class="list">
					<a class="hover"><img src="../common/img/n_gm/gnavi03.png" width="75" height="17" alt="選ばれる理由"></a>
					<ul class="dd_list">
						<li class="list01"><a href="../technology/">テクノロジー</a></li>
						<li class="list02"><a href="../philosophy/">デザイン思想</a></li>
						<li class="list03"><a href="../cookingmode/">調理機能</a></li>
						<li class="list04"><a href="../safe/">安心・安全</a></li>
						<li class="list05"><a href="../accessories/">付属品</a></li>
					</ul>
					</li>

					<li class="list"><a href="../care/" class="hover"><img src="../common/img/n_gm/gnavi04.png" width="90" height="17" alt="使い方・お手入れ"></a></li>

					<li class="list">
					<span><a class="hover"><img src="../common/img/n_gm/gnavi05.png" width="90" height="17" alt="スペック・FAQ"></a></span>
					<ul class="dd_list">
						<li class="list01"><a href="../spec/">スペック</a></li>
						<li class="list02"><a href="../faq/">FAQ</a></li>
					</ul>
					</li>

					<li class="list"><a href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank"  class="hover"><img src="../common/img/n_gm/gnavi06.png" width="105" height="17" alt="オンラインショップ"></a></li> 	

				</ul>
				</nav>

			<nav id="rp_sns">
			<div class="btn hover"><img src="../common/img/head_sns_icon_btn.png" height="14" width="10" alt="SNS"></div>
			<ul class="sns_list clearfix">
				<li class="sns01"><a href="https://www.facebook.com/vermicular" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_01.png" height="17" width="7" alt="Facebook"></a></li>
				<li class="sns02"><a href="https://twitter.com/_VERMICULAR_" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_02.png" height="11" width="14" alt="Twitter"></a></li>
				<li class="sns03"><a href="https://instagram.com/vermicular_japan/" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_03.png" height="13" width="13" alt="Instagram"></a></li>
				<li class="sns04"><a href="http://shop.vermicular.jp/mail/" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_04.png" height="10" width="15" alt="Mail"></a></li>
			</ul>
			</nav>
		</div>
	</header><!-- /#header -->

	<main id="main_wrap">
		<header id="rpr_page_title_wrap">
			<div class="inner">
				<h1 id="page_title"><img src="../img/accessories/page_title.png" height="58" width="391" alt="付属品・別売アクセサリー" /></h1>
			</div>
		</header><!-- /#kv_wrap -->
		<div id="catch_copy">
			<h2 class="catch"><img src="../img/accessories/section_title01.png" height="35" width="666" alt="付属品と呼びたくないくらいの力作ぞろい。" /></h2>
			<p class="text">
				バーミキュラは鍋以外の付属品も全てメイド・イン・ジャパン。料理がさらに楽しくなる。もっと料理をしたくなる。<br />
				手料理のある暮らしに、豊かさを添えるアイデアが揃っています。
			</p>
		</div><!-- /#catch_copy -->

		<section id="section01" class="clearfix">
			<ul>
				<li class="clearfix">
					<div class="copy">
						<h3><img src="../img/accessories/section01_copy01.png" height="73" width="287" alt="ライスポットを楽しむ為の、レシピブックさしあげます。" /></h2>
						<p class="text">
							ライスポットを購入すると、もれなくついてくるオリジナルのレシピブック。ライスポットでどんどん料理がつくりたくなる、絶品レシピが詰まっています。（ハードカバーP144／レシピ全100品掲載）
						</p>
						<div class="btn clear_black btn_left">
							<a href="../img/pdf/ricepot_recipebook.pdf" target=brank><span class="btn_inner"><img src="../img/accessories/btn_link_recipe.png" width="154" height="13" alt="レシピブックを見る"></span></a>
						</div>
					</div>
					<div class="image"><img src="../img/accessories/section01_img01.jpg" height="390" width="580" alt="ごはん生活を楽しくする、レシピブックさしあげます。" /></div>
				</li>
				<li class="clearfix">
					<div class="image"><img src="../img/accessories/section01_img02.jpg" height="390" width="580" alt="使いやすい、計量カップが付いてきます。" /></div>
					<div class="copy">
						<h3><img src="../img/accessories/section01_copy02.png" height="74" width="302" alt="使いやすい、計量カップが付いてきます。" /></h2>
						<p class="text">
							おいしく炊くには、お米と水の量を正確に計ることが大切です。<br/>
							使いやすいと評判のオリジナル計量カップをお付けしています。
						</p>
					</div>
				</li>
				<li class="clearfix">
					<div class="copy">
						<h3><img src="../img/accessories/section01_copy03.png" height="70" width="263" alt="フタを置ける、専用リッドスタンド付き。" /></h2>
						<p class="text">
							調理中、意外と困るのがフタの置き場。ライスポットは専用の<br />
							リッドスタンドがついてくるので便利です。
						</p>
					</div>
					<div class="image"><img src="../img/accessories/section01_img03.jpg" height="390" width="580" alt="フタを置ける、専用リッドスタンド付き。" /></div>
				</li>
			</ul>
		</section>

		<section id="section02" class="clearfix">
			<h1><img src="../img/accessories/section_title02.png" height="33" width="257" alt="別売りアクセサリー" /></h1>
			<ul class="clearfix">
				<li>
					<div class="image"><img src="../img/accessories/section02_img01.jpg" height="230" width="330" alt="オーガニックコットンヒートキーパー" /></div>
					<h2><img src="../img/accessories/section02_copy01.png" height="16" width="267" alt="オーガニックコットンヒートキーパー" /></h2>
					<p>オーガニックコットン100％のキャンバスを使用した保温カバー。炊いてから約2時間、温かいと感じる60度以上を保つ性能があります。</p>
					<div class="btn clear_black w220"><a href="http://shop.vermicular.jp/jp/group.php?id=24" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../img/accessories/btn_online.png" height="14" width="73" alt="オンラインショップ" /></span></span></a></div>
				</li>
				<li>
					<div class="image"><img src="../img/accessories/section02_img02.jpg" height="230" width="330" alt="マグネット トリベット" /></div>
					<h2><img src="../img/accessories/section02_copy02.png" height="16" width="148" alt="マグネット トリベット" /></h2>
					<p>天然素材を使用した、画期的な着脱機能を持つバーミキュラ専用鍋敷きです。</p>
					<div class="btn clear_black w220"><a href="http://shop.vermicular.jp/jp/group.php?id=22" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../img/accessories/btn_online.png" height="14" width="73" alt="オンラインショップ" /></span></span></a></div>
				</li>
				<li>
					<div class="image"><img src="../img/accessories/section02_img03.jpg" height="230" width="330" alt="オーガニックコットン ポットホルダー" /></div>
					<h2><img src="../img/accessories/section02_copy03.png" height="16" width="272" alt="オーガニックコットン ポットホルダー" /></h2>
					<p>バーミキュラのダブルハンドルにすっぽりと収まる鍋つかみです。</p>
					<div class="btn clear_black w220"><a href="http://shop.vermicular.jp/jp/group.php?id=25" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../img/accessories/btn_online.png" height="14" width="73" alt="オンラインショップ" /></span></span></a></div>
				</li>
			</ul>
		</section>
		
<a id="online_btn" href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank">
<img src="../common/img/btn_online.png" width="130" height="130" alt="Online Shop">
</a>	
<div class="btn clear_black w198 center "><a href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../img/products/top/onlineshop_text.png" height="13" width="101" alt="オンラインショップ"></span></span></a></div></br>
<?php
	include '../_footer_rp.php';
	?>