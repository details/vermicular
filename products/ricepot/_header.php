<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ja" class="ie ie6" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 7]>   <html lang="ja" class="ie ie7" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 8]>   <html lang="ja" class="ie ie8" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 9]>   <html lang="ja" class="ie ie9" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="ja" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">

<?php
	if($page_pass==""){
		$layer = "";
	}else{
		$layer = $page_pass;
	}
?>

<?php
	$ua=$_SERVER['HTTP_USER_AGENT'];
	$device = ((strpos($ua,'iPhone')!==false)||(strpos($ua,'iPod')!==false)||(strpos($ua,'Android')!==false)||(strpos($ua,'iPad')!==false));
	if ($device == true){
		$device = 'other';
	}
?>
<?php
	if($device == "other"){
		echo '<meta name="viewport" content="width=1280">';
	}else{
		echo '<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1280">';
	}
?>

<link rel="canonical" href="http://www.vermicular.jp/<?php echo $page_directry; ?>">
<?php $URL = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>
<title><?php echo $page_title; ?></title>
<meta name="author" content="愛知ドビー株式会社">
<meta name="description" content="<?php echo $page_description; ?>">
<meta name="keywords" content="<?php echo $page_keywords; ?>">

<meta property="og:title" content="<?php echo $page_title; ?>" />
<meta property="og:type" content="website">
<meta property="og:url" content="<?php echo $URL; ?> " />
<meta property="og:image" content="http://www.vermicular.jp/products/ricepot/common/img/ogp.jpg" />
<meta property="og:site_name" content="<?php echo $page_title; ?>" />
<meta property="og:description" content="<?php echo $page_description; ?>" />
<meta property="fb:app_id" content="171078026685763" />

<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="<?php echo $page_title; ?>">
<meta name="twitter:title" content="<?php echo $page_title; ?>">
<meta name="twitter:url" content="<?php echo $_SERVER["REQUEST_URI"]; ?> ">
<meta name="twitter:description" content="<?php echo $page_description; ?>">

<meta name="format-detection" content="telephone=no,address=no,email=no">

<link rel="stylesheet" type="text/css" href="<?php echo $page_pass; ?>common/css/normalize.css?20161101">
<link rel="stylesheet" type="text/css" href="<?php echo $page_pass; ?>common/css/base.css?20161101">
<link rel="stylesheet" type="text/css" href="<?php echo $page_pass; ?>common/css/styles.css?20161101">
<?php if($page_id!="rp_top"): ?>
<link rel="stylesheet" type="text/css" href="<?php echo $page_pass; ?>common/css/lowlayer.css?20161101">
<?php endif; ?>

<?php echo $extra_css; ?>

<link href="<?php echo $page_pass; ?>common/img/apple-touch-icon.png" rel="apple-touch-icon">
<meta name="apple-mobile-web-app-title" content="Vermicular">

<script src="<?php echo $page_pass; ?>common/js/jquery-1.11.0.min.js?20161101"></script>
<script src="<?php echo $page_pass; ?>common/js/jquery.transit.js?20161101"></script>
<script src="<?php echo $page_pass; ?>common/js/pace.min.js?20161101"></script>
<script src="<?php echo $page_pass; ?>common/js/jquery.colorbox-min.js?20161101"></script>
<script src="<?php echo $page_pass; ?>common/js/var.js?20161101"></script>
<script src="<?php echo $page_pass; ?>common/js/function.js?20161101"></script>
<script src="<?php echo $page_pass; ?>common/js/base.js?20161101"></script>
<script src="<?php echo $page_pass; ?>common/js/jquery.bxslider.min.js?20161101"></script>
<?php echo $extra_js; ?>

<!--[if lt IE 9]>
<script src="<?php echo $page_pass; ?>common/js/html5shiv.js"></script>
<script type="text/javascript" src="<?php echo $page_pass; ?>common/js/jquery.belatedPNG.min.js"></script>
<script>
$(function() {
	$(".pngfix").fixPng();
});
</script>
<script src="<?php echo $page_pass; ?>common/js/jquery.backgroundSize.js"></script>
<script>
$(function(){
 $('#header #bg').css('background-size', 'cover');
});
</script>
<![endif]-->
</head>
<body<?php echo ($page_id)? ' id="'.$page_id.'"':''; ?>>
<div id="document"<?php echo ($device == "pc")?' class="device_pc"':''; ?>>
	<?php if($page_type == 'oven_pot_round' || $page_type == 'ricepot'): // [ オーブンポットラウンド用 ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ ] ?>

	<header id="rpr_header">
		<div id="rpr_header_inner" class="clearfix">
			<h1 id="rpr_site_logo"><a href="<?php echo $page_pass; ?>../../" class="hover"><img src="<?php echo $page_pass; ?>common/img/site_logo.png" height="27" width="143" alt="VERMICULAR（バーミキュラ） MADE IN JAPAN"></a></h1>
			<nav id="rpr_gnavi">
				<ul class="clearfix">
					<li class="list navi01 <?php if ($page_id == "rp_concept") {echo "on";} ?>"><a href="<?php echo $page_pass; ?>concept/" class="hover"><img src="<?php echo $page_pass; ?>common/img/gnavi01.png" height="13" width="44" alt="コンセプト"></a></li>
	
					<li class="list navi03 <?php if ($page_id == "rp_technology") {echo "on";} ?>"><a href="<?php echo $page_pass; ?>technology/" class="hover"><img src="<?php echo $page_pass; ?>common/img/gnavi03.png" height="12" width="54" alt="テクノロジー"></a></li>
					<li class="list navi04 <?php if ($page_id == "rp_philosophy") {echo "on";} ?>"><a href="<?php echo $page_pass; ?>philosophy/" class="hover"><img src="<?php echo $page_pass; ?>common/img/gnavi04.png" height="12" width="66" alt="デザイン思想"></a></li>
					<li class="list navi05 <?php if ($page_id == "rp_cookingmode") {echo "on";} ?>"><a href="<?php echo $page_pass; ?>cookingmode/" class="hover"><img src="<?php echo $page_pass; ?>common/img/gnavi05.png" height="13" width="50" alt="調理機能"></a></li>

					<li class="list navi06 <?php if ($page_id == "rp_care") {echo "on";} ?>"><a href="<?php echo $page_pass; ?>care/" class="hover"><img src="<?php echo $page_pass; ?>common/img/gnavi06.png" height="13" width="88" alt="使い方・お手入れ"></a></li>
					<li class="list navi07 <?php if ($page_id == "rp_safe") {echo "on";} ?>"><a href="<?php echo $page_pass; ?>safe/" class="hover"><img src="<?php echo $page_pass; ?>common/img/gnavi07.png" height="12" width="54" alt="安心・安全"></a></li>
					<li class="list navi08 <?php if ($page_id == "rp_accessories") {echo "on";} ?>"><a href="<?php echo $page_pass; ?>accessories/" class="hover"><img src="<?php echo $page_pass; ?>common/img/gnavi08.png" height="13" width="37" alt="付属品"></a></li>
					<li class="list navi09 <?php if ($page_id == "rp_spec") {echo "on";} ?>"><a href="<?php echo $page_pass; ?>spec/" class="hover"><img src="<?php echo $page_pass; ?>common/img/gnavi09.png" height="13" width="39" alt="スペック"></a></li>
					<li class="list navi10 <?php if ($page_id == "rp_faq") {echo "on";} ?>"><a href="<?php echo $page_pass; ?>faq/" class="hover"><img src="<?php echo $page_pass; ?>common/img/gnavi10.png" height="13" width="28" alt="FAQ"></a></li>

					<li class="list navi11 <?php if ($page_id == "rp_shoplist") {echo "on";} ?>"><a href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank" class="hover"><img src="<?php echo $page_pass; ?>common/img/gnavi11.png" height="12" width="86" alt="ショップ"></a></li>
				</ul>
			</nav>
			<nav id="rp_sns">
			<div class="btn"  class="hover"><img src="<?php echo $page_pass; ?>common/img/head_sns_icon_btn.png" height="14" width="10" alt="SNS"></div>
			<ul class="sns_list clearfix">
				<li class="sns01"><a href="https://www.facebook.com/vermicular" target="_blank"  class="hover"><img src="<?php echo $page_pass; ?>common/img/head_sns_icon_01.png" height="17" width="7" alt="Facebook"></a></li>
				<li class="sns02"><a href="https://twitter.com/_VERMICULAR_" target="_blank"  class="hover"><img src="<?php echo $page_pass; ?>common/img/head_sns_icon_02.png" height="11" width="14" alt="Twitter"></a></li>
				<li class="sns03"><a href="https://instagram.com/vermicular_japan/" target="_blank"  class="hover"><img src="<?php echo $page_pass; ?>common/img/head_sns_icon_03.png" height="13" width="13" alt="Instagram"></a></li>
				<li class="sns04"><a href="http://shop.vermicular.jp/mail/" target="_blank"  class="hover"><img src="<?php echo $page_pass; ?>common/img/head_sns_icon_04.png" height="10" width="15" alt="Mail"></a></li>
			</ul>
			</nav>
		</div>
	</header><!-- /#header -->

	<?php endif;  // [ オーブンポットラウンド用 ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ ] ?>

	<main id="main_wrap">