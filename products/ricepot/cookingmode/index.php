﻿<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ja" class="ie ie6" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 7]>   <html lang="ja" class="ie ie7" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 8]>   <html lang="ja" class="ie ie8" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 9]>   <html lang="ja" class="ie ie9" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="ja" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">


<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1280">
<link rel="canonical" href="http://www.vermicular.jp/products/ricepot/cookingmode">
<title>調理モード | Vermicular（バーミキュラ）ライスポット</title>
<meta name="author" content="愛知ドビー株式会社">
<meta name="description" content="この火加減、炊飯だけではもったいない。">
<meta name="keywords" content="調理モード,火加減,理想,熱源,究極,焼き付け,炒め,低温調理,無水調理,本格調理,ライスポット,炊飯鍋,炊飯器,バーミキュラ,Vermicular,ricepot,鋳物,愛知ドビー">

<meta property="og:title" content="調理モード | Vermicular（バーミキュラ）ライスポット" />
<meta property="og:type" content="website">
<meta property="og:url" content="http://www.vermicular.jp/products/ricepot/cookingmode/index.php " />
<meta property="og:image" content="http://www.vermicular.jp/products/ricepot/common/img/ogp.jpg" />
<meta property="og:site_name" content="調理モード | Vermicular（バーミキュラ）ライスポット" />
<meta property="og:description" content="この火加減、炊飯だけではもったいない。" />
<meta property="fb:app_id" content="171078026685763" />

<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="調理モード | Vermicular（バーミキュラ）ライスポット">
<meta name="twitter:title" content="調理モード | Vermicular（バーミキュラ）ライスポット">
<meta name="twitter:url" content="/vermicular/products/ricepot/cookingmode/index.php ">
<meta name="twitter:description" content="この火加減、炊飯だけではもったいない。">

<meta name="format-detection" content="telephone=no,address=no,email=no">

<link rel="stylesheet" type="text/css" href="../common/css/normalize.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/base_nw.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/styles_nw.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/lowlayer.css?20161101">


<link rel="stylesheet" type="text/css" href="../css/cookingmode_nw.css">

<link href="../common/img/apple-touch-icon.png" rel="apple-touch-icon">
<meta name="apple-mobile-web-app-title" content="Vermicular">

<script src="../common/js/jquery-1.11.0.min.js?20161101"></script>
<script src="../common/js/jquery.transit.js?20161101"></script>
<script src="../common/js/pace.min.js?20161101"></script>
<script src="../common/js/jquery.colorbox-min.js?20161101"></script>
<script src="../common/js/var.js?20161101"></script>
<script src="../common/js/function.js?20161101"></script>
<script src="../common/js/base.js?20161101"></script>
<script src="../common/js/jquery.bxslider.min.js?20161101"></script>

<script src="../common/js/scripts_nw.js"></script>

<!--[if lt IE 9]>
<script src="../common/js/html5shiv.js"></script>
<script type="text/javascript" src="../common/js/jquery.belatedPNG.min.js"></script>
<script>
$(function() {
	$(".pngfix").fixPng();
});
</script>
<script src="../common/js/jquery.backgroundSize.js"></script>
<script>
$(function(){
 $('#header #bg').css('background-size', 'cover');
});
</script>
<![endif]-->
</head>
<body id="rp_cookingmode">
<div id="document">
	
	<header id="rpr_header">
		<div id="rpr_header_inner" class="clearfix">
			<h1 id="rpr_site_logo"><a href="http://www.vermicular.jp/" class="hover"><img src="../common/img/site_logo.png" height="27" width="143" alt="VERMICULAR（バーミキュラ） MADE IN JAPAN"></a></h1>
			<nav id="rpr_gnavi">
				<ul class="clearfix">
					

					

					
					<li class="list"><a href="../concept/" class="hover"><img src="../common/img/n_gm/gnavi01.png" width="65" height="17" alt="コンセプト"></a></li>
					
					
					
					
					<li class="list">
					<a class="hover"><img src="../common/img/n_gm/gnavi03.png" width="75" height="17" alt="選ばれる理由"></a>
					<ul class="dd_list">
						<li class="list01"><a href="../technology/">テクノロジー</a></li>
						<li class="list02"><a href="../philosophy/">デザイン思想</a></li>
						<li class="list03"><a href="../cookingmode/">調理機能</a></li>
						<li class="list04"><a href="../safe/">安心・安全</a></li>
						<li class="list05"><a href="../accessories/">付属品</a></li>
					</ul>
					</li>


					<li class="list"><a href="../care/" class="hover"><img src="../common/img/n_gm/gnavi04.png" width="90" height="17" alt="使い方・お手入れ"></a></li>


					<li class="list">
					<span><a class="hover"><img src="../common/img/n_gm/gnavi05.png" width="90" height="17" alt="スペック・FAQ"></a></span>
					<ul class="dd_list">
						<li class="list01"><a href="../spec/">スペック</a></li>
						<li class="list02"><a href="../faq/">FAQ</a></li>
					</ul>
					</li>



					<li class="list"><a href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank"  class="hover"><img src="../common/img/n_gm/gnavi06.png" width="105" height="17" alt="オンラインショップ"></a></li> 	




				</ul>
				</nav>



			<nav id="rp_sns">
			<div class="btn"  class="hover"><img src="../common/img/head_sns_icon_btn.png" height="14" width="10" alt="SNS"></div>
			<ul class="sns_list clearfix">
				<li class="sns01"><a href="https://www.facebook.com/vermicular" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_01.png" height="17" width="7" alt="Facebook"></a></li>
				<li class="sns02"><a href="https://twitter.com/_VERMICULAR_" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_02.png" height="11" width="14" alt="Twitter"></a></li>
				<li class="sns03"><a href="https://instagram.com/vermicular_japan/" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_03.png" height="13" width="13" alt="Instagram"></a></li>
				<li class="sns04"><a href="http://shop.vermicular.jp/mail/" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_04.png" height="10" width="15" alt="Mail"></a></li>
			</ul>
			</nav>
		</div>
	</header><!-- /#header -->

	
	<main id="main_wrap">		<header id="rpr_page_title_wrap">
			<div class="inner">
				<h1 id="page_title"><img src="../img/cookingmode/page_title.png" height="61" width="141" alt="調理機能" /></h1>
			</div>
		</header><!-- /#kv_wrap -->

		<div id="catch_copy">
			
			<h2 class="catch"><img src="../img/cookingmode/copy01.png" height="35" width="640" alt="この火加減、炊飯だけではもったいない。" /></h2>
			<p class="text">
				素材本来の味を引き出す鍋と、理想の熱源がひとつになったライスポット。<br/>
				炊飯だけでなく、バーミキュラの鍋で楽しめる全ての調理が、誰にでも理想的な火加減でできるのです。
			</p>


		</div><!-- /#catch_copy -->


		<secttion id="section01">
			<ul class="clearfix">
				<li class="clearfix">
					<div class="copy">
						<h3><img src="../img/cookingmode/section_copy01.png" height="72" width="433" alt="バーミキュラの「究極の無水調理」を、いつも完璧な火加減で。" /></h2>
						<p class="text">
							フタと本体の密閉性が非常に高いバーミキュラの鍋だからできる「無水調理」が、簡単に楽しめます。
						</p>
					</div>
					<div class="image"><img src="../img/cookingmode/section_img01.jpg" height="390" width="580" alt="バーミキュラの「究極の無水調理」を、いつも完璧な火加減で。" /></div>
				</li>
				<li class="clearfix">
					<div class="image"><img src="../img/cookingmode/section_img02.jpg" height="390" width="580" alt="焼き付けも、思い通り。" /></div>
					<div class="copy">
						<h3><img src="../img/cookingmode/section_copy02.png" height="26" width="248" alt="焼き付けも、思い通り。" /></h2>
						<p class="text">
							ハイパワーIHコイルが鍋底を均一に加熱するから、<br/>
							肉の焼き付けや炒め調理もムラなく理想的に仕上がります。
						</p>
					</div>
				</li>
				<li class="clearfix">
					<div class="copy">
						<h3><img src="../img/cookingmode/section_copy03.png" height="27" width="452" alt="プロの「低温調理」が、あなたにもできる。" /></h2>
						<p class="text">
							ポットヒーターは、30℃から95℃まで1度刻みの温度設定が可能。<br/>
							ローストビーフやコンフィのように絶妙な火加減が必要な「低温調理」が、誰にでも楽しめます。
						</p>
					</div>
					<div class="image"><img src="../img/cookingmode/section_img03.jpg" height="390" width="580" alt="プロの「低温調理」が、あなたにもできる。" /></div>
				</li>
				<li class="btn_area">
					<div class="btn clear_black w218">
							<a href="../care/#cook"><span class="btn_inner"><img src="../img/cookingmode/btn_link_cook.png" width="151" height="12" alt="調理機能の使い方を見る"></span></a>
					</div>
				</li>
			</ul>
		</section>

		<section id="section02" class="clearfix">
			<h1><img src="../img/cookingmode/section_title02.png" height="32" width="459" alt="ライスポットで広がる調理の世界。" /></h1>
			<p>究極の無水調理に、絶妙な火入れのロースト、ふわふわのパンから色とりどりのデザートまで。
				<span class="sub_text">バーミキュラなら、本格調理がいつも最高の仕上がりに。</span>
			</p>
			<div class="image"><img src="../img/cookingmode/section_img04.jpg" height="auto" width="100%" alt="ライスポットで広がる調理の世界。"/></div>

		</section>

		<div id="book" class="clearfix">
<!-- 			<div class="inner"> -->
				<h3><img src="../img/cookingmode/section_copy05.png" height="21" width="498" alt="ライスポット生活を楽しくする、レシピブックさしあげます。" /></h3>
				<p>購入すると、もれなくついてくるオリジナルのレシピブック。ライスポットで炊いたご飯を楽しむ<br/>
					レシピから、本格的な調理レシピまで、全100品の充実した一冊です。（ハードカバー144P ）</p>
				<div class="image"><img src="../img/cookingmode/section_img05.png" height="183" width="130" alt="ライスポット生活を楽しくする、レシピブックさしあげます。" /></div>
<!-- 			</div> -->
		
		<div id="recipe_link" class="btn_area last_area">
			<div class="btn clear_black w218">
					<a href="../img/pdf/ricepot_recipebook.pdf" target=brank><span class="btn_inner"><img src="../img/cookingmode/btn_link_recipe.png" width="154" height="13" alt="レシピブックを見る"></span></a>
			</div>
		</div>

				</div>
				
				
				
		
							<a id="online_btn" href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank">
	<img src="../common/img/btn_online.png" width="130" height="130" alt="Online Shop">
</a>

									
													
								
			
<div class="btn clear_black w198 center "><a href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../img/products/top/onlineshop_text.png" height="13" width="101" alt="オンラインショップ"></span></span></a></div></br>
<?php
	include '../_footer_rp.php';
	?>