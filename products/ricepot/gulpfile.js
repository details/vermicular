var gulp = require('gulp');

var sass = require("gulp-sass"); //[ sassコンパイル ]

var autoprefixer = require("gulp-autoprefixer"); //[ ベンダープレフィックス自動付与 ]

var browser = require("browser-sync");
var plumber = require('gulp-plumber');
var through2 = require('through2');


// require('es6-promise').polyfill();


gulp.task("server", function() {
    browser({
        server: {
            baseDir: "vermicular/"
        }
    });
});

gulp.task("sass", function() {
    gulp.src("_sass/**/*.scss")
        .pipe(plumber())
        .pipe(sass()) //[ sassコンパイル ]
        .pipe(autoprefixer()) //[ ベンダープレフィックス自動付与 ]
        .pipe(gulp.dest("./css"))
        .pipe(browser.reload({stream:true}));
    gulp.src("_sass_common/**/*.scss")
        .pipe(plumber())
        .pipe(sass()) //[ sassコンパイル ]
        .pipe(autoprefixer()) //[ ベンダープレフィックス自動付与 ]
        .pipe(gulp.dest("./common/css"))
        .pipe(browser.reload({stream:true}));
});


/* [ JSの圧縮 ] */
var uglify = require("gulp-uglify");

var webserver = require('gulp-webserver');

gulp.task("js", function() {
    gulp.src(["_js/*.js","!js/*.js"])
        .pipe(plumber())
        .pipe(uglify())
        .pipe(gulp.dest("./js/"))
        .pipe(browser.reload({stream:true}));
});


/* [ ファイルの監視 ] */
gulp.task("default",['server'], function() {
    gulp.watch(["_js/**/*.js","!js/_min/**/*.js","!js/common/_lib/*.js"],["js"]);
    // gulp.watch(["js/*.js","!js/_min/*.js"],["js"]);
    gulp.watch("_sass/**/*.scss",["sass"]);
    gulp.watch("_sass_common/**/*.scss",["sass"]);
});
