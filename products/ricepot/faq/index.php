﻿<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ja" class="ie ie6" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 7]>   <html lang="ja" class="ie ie7" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 8]>   <html lang="ja" class="ie ie8" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 9]>   <html lang="ja" class="ie ie9" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="ja" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">


<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1280">
<link rel="canonical" href="http://www.vermicular.jp/products/ricepot/faq">
<title>FAQ | Vermicular（バーミキュラ） RICEPOT</title>
<meta name="author" content="愛知ドビー株式会社">
<meta name="description" content="よくあるご質問">
<meta name="keywords" content="FAQ,機能,スペック,使用,お手入れ,安全,アフターサービス,ライスポット,炊飯鍋,炊飯器,バーミキュラ,Vermicular,ricepot,鋳物,愛知ドビー">

<meta property="og:title" content="FAQ | Vermicular（バーミキュラ） RICEPOT" />
<meta property="og:type" content="website">
<meta property="og:url" content="http://www.vermicular.jp/products/ricepot/faq/index.php " />
<meta property="og:image" content="http://www.vermicular.jp/products/ricepot/common/img/ogp.jpg" />
<meta property="og:site_name" content="FAQ | Vermicular（バーミキュラ） RICEPOT" />
<meta property="og:description" content="よくあるご質問" />
<meta property="fb:app_id" content="171078026685763" />

<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="FAQ | Vermicular（バーミキュラ） RICEPOT">
<meta name="twitter:title" content="FAQ | Vermicular（バーミキュラ） RICEPOT">
<meta name="twitter:url" content="/vermicular/products/ricepot/faq/index.php ">
<meta name="twitter:description" content="よくあるご質問">

<meta name="format-detection" content="telephone=no,address=no,email=no">

<link rel="stylesheet" type="text/css" href="../common/css/normalize.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/base_nw.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/styles_nw.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/lowlayer.css?20161101">


<link rel="stylesheet" type="text/css" href="../css/faq.css">

<link href="../common/img/apple-touch-icon.png" rel="apple-touch-icon">
<meta name="apple-mobile-web-app-title" content="Vermicular">

<script src="../common/js/jquery-1.11.0.min.js?20161101"></script>
<script src="../common/js/jquery.transit.js?20161101"></script>
<script src="../common/js/pace.min.js?20161101"></script>
<script src="../common/js/jquery.colorbox-min.js?20161101"></script>
<script src="../common/js/var.js?20161101"></script>
<script src="../common/js/function.js?20161101"></script>
<script src="../common/js/base.js?20161101"></script>
<script src="../common/js/jquery.bxslider.min.js?20161101"></script>

<script src="../common/js/scripts_nw.js"></script>
<script src="../js/faq.js"></script>

<!--[if lt IE 9]>
<script src="../common/js/html5shiv.js"></script>
<script type="text/javascript" src="../common/js/jquery.belatedPNG.min.js"></script>
<script>
$(function() {
	$(".pngfix").fixPng();
});
</script>
<script src="../common/js/jquery.backgroundSize.js"></script>
<script>
$(function(){
 $('#header #bg').css('background-size', 'cover');
});
</script>
<![endif]-->
</head>
<body id="rp_faq">
<div id="document">
	
	<header id="rpr_header">
		<div id="rpr_header_inner" class="clearfix">
			<h1 id="rpr_site_logo"><a href="http://www.vermicular.jp/" class="hover"><img src="../common/img/site_logo.png" height="27" width="143" alt="VERMICULAR（バーミキュラ） MADE IN JAPAN"></a></h1>
			<nav id="rpr_gnavi">
				<ul class="clearfix">
					

					

					
					<li class="list"><a href="../concept/" class="hover"><img src="../common/img/n_gm/gnavi01.png" width="65" height="17" alt="コンセプト"></a></li>
					
					
					
				
					
					<li class="list">
					<a class="hover"><img src="../common/img/n_gm/gnavi03.png" width="75" height="17" alt="選ばれる理由"></a>
					<ul class="dd_list">
						<li class="list01"><a href="../technology/">テクノロジー</a></li>
						<li class="list02"><a href="../philosophy/">デザイン思想</a></li>
						<li class="list03"><a href="../cookingmode/">調理機能</a></li>
						<li class="list04"><a href="../safe/">安心・安全</a></li>
						<li class="list05"><a href="../accessories/">付属品</a></li>
					</ul>
					</li>


					<li class="list"><a href="../care/" class="hover"><img src="../common/img/n_gm/gnavi04.png" width="90" height="17" alt="使い方・お手入れ"></a></li>


					<li class="list">
					<span><a class="hover"><img src="../common/img/n_gm/gnavi05.png" width="90" height="17" alt="スペック・FAQ"></a></span>
					<ul class="dd_list">
						<li class="list01"><a href="../spec/">スペック</a></li>
						<li class="list02"><a href="../faq/">FAQ</a></li>
					</ul>
					</li>



					<li class="list"><a href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank"  class="hover"><img src="../common/img/n_gm/gnavi06.png" width="105" height="17" alt="オンラインショップ"></a></li> 	




				</ul>
				</nav>



			<nav id="rp_sns">
			<div class="btn"  class="hover"><img src="../common/img/head_sns_icon_btn.png" height="14" width="10" alt="SNS"></div>
			<ul class="sns_list clearfix">
				<li class="sns01"><a href="https://www.facebook.com/vermicular" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_01.png" height="17" width="7" alt="Facebook"></a></li>
				<li class="sns02"><a href="https://twitter.com/_VERMICULAR_" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_02.png" height="11" width="14" alt="Twitter"></a></li>
				<li class="sns03"><a href="https://instagram.com/vermicular_japan/" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_03.png" height="13" width="13" alt="Instagram"></a></li>
				<li class="sns04"><a href="http://shop.vermicular.jp/mail/" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_04.png" height="10" width="15" alt="Mail"></a></li>
			</ul>
			</nav>
		</div>
	</header><!-- /#header -->

	
	<main id="main_wrap">		<header id="rpr_page_title_wrap">
			<div class="inner">
				<h1 id="page_title"><img src="../img/faq/page_title.png" height="58" width="195" alt="よくあるご質問"></h1>
			</div>
		</header><!-- /#kv_wrap -->

		<section id="section01" class="clearfix">
			<nav>
				<a data-to="#section02" class="hover on"><img src="../img/faq/section_text01.png" height="16" width="158" alt="機能・スペックについて"/></a>
				<a data-to="#section03" class="hover"><img src="../img/faq/section_text02.png" height="16" width="172" alt="使用・お手入れについて"/></a>
				<a data-to="#section04" class="hover"><img src="../img/faq/section_text03.png" height="16" width="92" alt="安全について"/></a>
				<a data-to="#section05" class="hover"><img src="../img/faq/section_text04.png" height="16" width="170" alt="アフタ－サービスについて"/></a>
			</nav>
		</section>


		<section id="section02" class="tab_cont on clearfix">
			<div class="block">
			<h2>炊飯モードについて</h2>
			<div class="inner">
				<ul id="faq_list_wrap">
					<li>
						<h3 class="hover">炊き上がりまでの時間は？</h3>
						<p>
							3合の「白米・ふつう」モードで約60分（浸水30分+炊飯30分）。「浸水時間短縮」モードで約30分です。<br/>
							炊き上がりまでの時間は、炊飯量、室温、水温などによって前後いたします。
						</p>
					</li>
					<li>
						<h3 class="hover">玄米は炊けますか？</h3>
						<p>
							玄米は1～4合まで炊飯可能です。
						</p>
					</li>

					<li>
						<h3 class="hover">無洗米は炊けますか？</h3>
						<p>
							白米と同様においしく炊くことができます。<br/>
							一度軽く洗米し、通常のお米と同量の水（白米：水＝1：1）で炊飯することを、お勧めしています。
						</p>
					</li>


					<li>
						<h3 class="hover">分づき米は炊けますか？</h3>
						<p>
							分づき米は下記の炊き方で炊くことで、おいしく炊くことができます。<br/>
<br/>
							・3分づき米は、玄米と同じように洗米・浸水し、玄米モードで炊いてください。<br/>
							　ただし、通常の玄米よりも吸水が早いので、通常の玄米の6時間の浸水を、2時間に短縮してもおいしく炊くことができます。<br/>
							<br/>
							・5分づき米・7分づき米は、白米と同じように洗米し、白米モードで炊いてください。<br/>
							　ただし、通常の白米よりも吹きこぼれやすいので、炊飯量を最大4カップまでとしてください。
						</p>
					</li>

					<li>
						<h3 class="hover">どんな炊飯ができますか？</h3>
						<p>
							白米は「ふつう・おこげ・おかゆ」、玄米は「ふつう・おこげ」のモードが選択できます。
						</p>
					</li>
					<li>
						<h3 class="hover">タイマー予約はできますか？</h3>
						<p>
							炊飯モードでは炊き上がり時刻を設定するタイマー予約が可能です。
						</p>
					</li>
					<li>
						<h3 class="hover">ご飯の保温はできますか？</h3>
						<p>
							ご飯の保温はできません。おいしいご飯を追求した結果、保温よりも温め直しを推奨しています。また、別売りのヒートキーパーを使用すれば、2時間おいしく温かく保つことができます。調理モードでは保温機能や「調理後自動保温」機能も使用できます。
						</p>
					</li>
					<li>
						<h3 class="hover">炊飯1回当たりの電気料金は？</h3>
						<p>
							　「白米・ふつう」モードで、5合＝約7.9円　4合＝約6.9円　3合＝約5.8円　2合＝約4.9円　1合＝約4.0円　です。<br/>
							　 ※1kWhあたり27円として計算しています。「平成26年4月 公益社団法人 全国家庭電気製品公正取引協議会 新電力料金目安単価(税込)」
						</p>
					</li>
				</ul>
			</div>
			</div>

			<div class="block">
			<h2>調理モードについて</h2>
			<div class="inner">
				<ul id="faq_list_wrap">
					<li>
						<h3 class="hover">調理モードの火加減調整はできますか？</h3>
						<p>
							「中火・弱火・極弱火・保温」の４つの火加減が選択可能です。<br/>また、保温は30℃～95℃までの温度設定が可能です。
						</p>
					</li>
					<li>
						<h3 class="hover">どんな調理ができますか？</h3>
						<p>
							「無水調理・ロースト・炒める・煮こむ・蒸す」といった調理が可能です。
						</p>
					</li>
					<li>
						<h3 class="hover">タイマー予約はできますか？</h3>
						<p>
							調理モードでは指定時間後に電源OFFするタイマー切予約が可能です。<br/>タイマー切予約は360分まで設定可能です。
						</p>
					</li>
					<li>
						<h3 class="hover">「調理後自動保温」とはどんな機能ですか？</h3>
						<p>
							調理モードでタイマー切設定した調理（中火・弱火・極弱火）後に、指定した温度で保温する機能です。
						</p>
					</li>
					<li>
						<h3 class="hover">天ぷらなどの揚げ物には、使用できるのですか？</h3>
						<p>
							高温の油の飛散りなどで機器を傷める恐れがあるため、使用できません。
						</p>
					</li>
				</ul>
			</div>
			</div>

			<div class="block">
			<h2>その他</h2>
			<div class="inner">
				<ul id="faq_list_wrap">
					<li>
						<h3 class="hover">消費電力は？</h3>
						<p>炊飯時は最大1350W・調理時は最大1000Wです。</p>
					</li>
					<li>
						<h3 class="hover">安全装置はついていますか？</h3>
						<p>ポットヒーターには異常過熱時や鍋なし運転、長時間操作されない場合などに、異常検知して安全停止する機能が搭載されています。</p>
					</li>
					<li>
						<h3 class="hover">付属品は何かありますか？</h3>
						<p>ライスポットには「ポット（専用炊飯鍋）」「ポットヒーター」の他に<br/>「水計量カップ」「米計量カップ」「リッドスタンド」「レシピブック」が同梱されます。</p>
					</li>
					<li>
						<h3 class="hover">ポット又はポットヒーターのみ、購入したいのですが？</h3>
						<p>単品のみでの販売もございます。単品でのご購入希望の方はバーミキュラオーナーズデスク（0120-766-787）までご連絡ください。</p>
					</li>
					<li>
						<h3 class="hover">ポットの重さはどのくらいですか？</h3>
						<p>フタと本体で約4㎏です。</p>
					</li>
					<li>
						<h3 class="hover">生産国はどこでしょうか？</h3>
						<p>ポット・ポットヒーター・計量カップ等の付属品まですべて日本製です。</p>
					</li>
					<li>
						<h3 class="hover">メロディを変えたり、音量を変えたりできますか？</h3>
						<p>変更できません。</p>
					</li>
				</ul>
			</div>
			</div>
		</section>

		<section id="section03" class="tab_cont clearfix">
			<div class="block">
			<h2>使用・お手入れについて</h2>
			<div class="inner">
				<ul id="faq_list_wrap">
					<li>
						<h3 class="hover">ポットヒーターで既存のバーミキュラは使用可能ですか？</h3>
						<p>調理モードでは使用できます（22㎝のみ）が、炊飯モードでは使用できません。<br/>
「ポット（専用炊飯鍋）」は炊飯モードにおける温度センサー対応の特殊仕様となっており、また吹きこぼれを最小限に抑える機構を備えております。その他の鍋につきましては炊飯モードでは使用できません。
</p>
					</li>
					<li>
						<h3 class="hover">ポットは別の加熱機器で使えますか？</h3>
						<p>使用可能です。ガスコンロやIH調理器、300℃までのオーブンでの使用が可能です。</p>
					</li>
					<li>
						<h3 class="hover">ご飯を炊く前に浸水する必要はありますか？</h3>
						<p>白米では必要ありません。<br/>
自動で浸水も行いますので、洗米してすぐに炊飯開始することができます。<br/>
玄米では浸水しておく必要があります。
</p>
					</li>
					<li>
						<h3 class="hover">ポットで洗米できますか？</h3>
						<p>ホーローを痛める可能性がありますので、できません。ボウルとザルを使用した洗米方法を推奨しています。</p>
					</li>
					<li>
						<h3 class="hover">ポットヒーターのシリコンリングは洗えますか？</h3>
						<p>汚れたら、取外しして水洗い可能です。</p>
					</li>
					<li>
						<h3 class="hover">海外で使いたいのですが</h3>
						<p>ポットヒーターは交流100V50/60Hzの日本国内向け仕様となっておりますので、その他地域での使用はできません。</p>
					</li>
				</ul>
			</div>
			</div>
		</section>


		<section id="section04" class="tab_cont clearfix">
			<div class="block">
			<h2>安全について</h2>
			<div class="inner">
				<ul id="faq_list_wrap">
					<li>
						<h3 class="hover">使用時にポット（専用炊飯鍋）は熱くなりますか？</h3>
						<p>炊飯時及び調理時にポットは高温になります。ライスポットを使用する際は、必ずお子様の手の届かない場所に設置してください。</p>
					</li>
					<li>
						<h3 class="hover">使用時にポットの取手は高温になりますか？</h3>
						<p>ポットの取手は高温になります。鍋を掴むときは必ず鍋つかみを使用してください。</p>
					</li>
					<li>
						<h3 class="hover">使用時にポットヒーター部分は熱くなりますか？</h3>
						<p>ポットヒーター内側のガラストップ・アルミヒーターは高温になりますが、外側部分は高温にはなりません。</p>
					</li>
					<li>
						<h3 class="hover">炊飯時の吹きこぼれが心配なのですが</h3>
						<p>ポット（専用炊飯鍋）には新技術フローティングリッドを採用しており、吹きこぼれを最小限に抑えていますので、安心して自動で炊飯することができます。（状況により少量の吹きこぼれがある場合があります）</p>
					</li>
					<li>
						<h3 class="hover">加熱時に蒸気はどこから出ますか？</h3>
						<p>ポットのフタのマークのある部分から蒸気が出る構造となっていますので、フタのマークがある方向に物を置かないようにしてください。</p>
					</li>
					<li>
						<h3 class="hover">ライスポットのホーローにカドミウムを使用していますか？</h3>
						<p>ライスポットのホーローにも、バーミキュラ同様カドミウムは使用しておりません。</p>
					</li>
					<li>
						<h3 class="hover">IHの電磁波について心配はありませんか？</h3>
						<p>IHから発生する電磁波は、人体に有害とも言われ、欧州では厳格な国際規格の遵守が求められています。ライスポットはIHコイルからの電磁波のモレを防ぐ「アルミ電磁波シールド」を採用し、前後左右上方向にて国際規格の約10分の１のレベルを実現しています。（IEC62233に準拠したEMF測定による）</p>
					</li>
					<li>
						<h3 class="hover">心臓ペースメーカーに影響ありますか？</h3>
						<p>IHコイルの動作が心臓用ペースメーカーに影響を与える可能性があります。<br/>
心臓用ペースメーカーをお使いの方は、本製品のご使用に当たっては担当医師とよくご相談ください。</p>
					</li>
				</ul>
			</div>
			</div>
		</section>

		<section id="section05" class="tab_cont clearfix">
			<div class="block">
			<h2>アフタ－サービスについて</h2>
			<div class="inner">
				<ul id="faq_list_wrap">
					<li>
						<h3 class="hover">修理やメンテナンスはおこなっていますか？</h3>
						<p>鍋のホーローが割れたり・汚れが取れなくなったりした場合のリペアサービス（有料）などの 修理を行っております。ご希望の場合はバーミキュラオーナーズデスク（0120-766-787）までご連絡ください。</p>
					</li>
					<li>
						<h3 class="hover">製品保証はありますでしょうか？</h3>
						<p>製品の製造上の保証はしております。<br/>
ポットヒーターは3年間の保証となります。<br/>
詳しくは、バーミキュラ・コールセンター（052-353-5333）までお問い合わせください。<br/>
但し、誤使用等により破損した商品の保証はいたしかねますので、ご了承ください。
</p>
					</li>
				</ul>
			</div>
			</div>
		</section>

				
				<a id="online_btn" href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank">
	<img src="../common/img/btn_online.png" width="130" height="130" alt="Online Shop">
</a>

							
							
								
			
<div class="btn clear_black w198 center "><a href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../img/products/top/onlineshop_text.png" height="13" width="101" alt="オンラインショップ"></span></span></a></div></br>
<?php
	include '../_footer_rp.php';
	?>