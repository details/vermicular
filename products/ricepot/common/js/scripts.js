// require jQuery JavaScript Library v1.10.1

/* 即時関数でローカル化 */
$(function(){

/* ------------------------------------------------------------
 * [ gnavi fixed ]
 * ------------------------------------------------------------ */

 var fixedFlg = false;

function globalfixed(){

	var winSdr = $win.scrollTop();
	if($("#kv_wrap")[0]){
		minSdr = $("#kv_wrap").height(); //[ 最低限スクロールさせる量 ]
	}else{
		minSdr = $("#rpr_page_title_wrap").height(); //[ 最低限スクロールさせる量 ]
	}

	if(!$("#rpr_header")[0] ) return false;

	var $globalNaviWrap = $("#rpr_header"),
		globalNaviWrapPosi = $globalNaviWrap.offset().top,
		$globalNavi = $("#rpr_header_inner");

	if(winSdr < minSdr) globalNaviWrapPosi = minSdr;

	if(winSdr > globalNaviWrapPosi && fixedFlg == false){
		fixedFlg = true;
		$globalNavi.css({"top":-90});
		$globalNavi.addClass("fixed").delay(200).animate({"top":0},600,"easeOutQuart");
	}
	if(winSdr < globalNaviWrapPosi && fixedFlg == true){
		fixedFlg = false;
		$globalNavi.animate({"top":-90},450,"easeOutQuart",function(){
			$globalNavi.removeClass("fixed").css("top",0);
		});
	}
	
}

// ライスポットメニューSNS
$("#rp_sns").hover(
  function () {
    $(this).find(".sns_list").slideDown(250,"easeOutSine");
    $(this).find(".btn").addClass("open");
  },
  function () {
    $(this).find(".sns_list").slideUp(250,"easeOutSine");
    $(this).find(".btn").removeClass("open");
  }
);
/*$('#rp_sns .btn').on('click',function(){
    $(this).next(".sns_list").slideToggle(200);
    $(this).toggleClass("open");
});*/

$win.on('load', function() {
	globalfixed();
});

$doc.ready(function(){
	var width = $("#rpr_gnavi").width();
	var padding = (width - 593)/12/2;
	$("#rpr_gnavi li").css("margin-left",padding);
	$("#rpr_gnavi li").css("margin-right",padding);
});

$win.on('scroll', function(winW,winH) {
	globalfixed();
});//End

$win.resize(function(){
	var width = $("#rpr_gnavi").width();
	var padding = (width - 593)/12/2;
	$("#rpr_gnavi li").css("margin-left",padding);
	$("#rpr_gnavi li").css("margin-right",padding);
});

});//End