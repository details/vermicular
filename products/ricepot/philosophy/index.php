﻿<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ja" class="ie ie6" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 7]>   <html lang="ja" class="ie ie7" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 8]>   <html lang="ja" class="ie ie8" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 9]>   <html lang="ja" class="ie ie9" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="ja" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">


<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1280">
<link rel="canonical" href="http://www.vermicular.jp/products/ricepot/philosophy">
<title>デザイン思想 | Vermicular（バーミキュラ）ライスポット</title>
<meta name="author" content="愛知ドビー株式会社">
<meta name="description" content="ライスポットに、保温機能がない理由。">
<meta name="keywords" content="デザイン思想,保温機能がない,ライスポット,温度差,熱対流,蒸らし,かまど,炊飯鍋,炊飯器,バーミキュラ,Vermicular,ricepot,鋳物,愛知ドビー">

<meta property="og:title" content="デザイン思想 | Vermicular（バーミキュラ）ライスポット" />
<meta property="og:type" content="website">
<meta property="og:url" content="http://www.vermicular.jp/products/ricepot/philosophy/index.php " />
<meta property="og:image" content="http://www.vermicular.jp/products/ricepot/common/img/ogp.jpg" />
<meta property="og:site_name" content="デザイン思想 | Vermicular（バーミキュラ）ライスポット" />
<meta property="og:description" content="ライスポットに、保温機能がない理由。" />
<meta property="fb:app_id" content="171078026685763" />

<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="デザイン思想 | Vermicular（バーミキュラ）ライスポット">
<meta name="twitter:title" content="デザイン思想 | Vermicular（バーミキュラ）ライスポット">
<meta name="twitter:url" content="/vermicular/products/ricepot/philosophy/index.php ">
<meta name="twitter:description" content="ライスポットに、保温機能がない理由。">

<meta name="format-detection" content="telephone=no,address=no,email=no">

<link rel="stylesheet" type="text/css" href="../common/css/normalize.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/base_nw.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/styles_nw.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/lowlayer.css?20161101">


<link rel="stylesheet" type="text/css" href="../css/philosophy.css">

<link href="../common/img/apple-touch-icon.png" rel="apple-touch-icon">
<meta name="apple-mobile-web-app-title" content="Vermicular">

<script src="../common/js/jquery-1.11.0.min.js?20161101"></script>
<script src="../common/js/jquery.transit.js?20161101"></script>
<script src="../common/js/pace.min.js?20161101"></script>
<script src="../common/js/jquery.colorbox-min.js?20161101"></script>
<script src="../common/js/var.js?20161101"></script>
<script src="../common/js/function.js?20161101"></script>
<script src="../common/js/base.js?20161101"></script>
<script src="../common/js/jquery.bxslider.min.js?20161101"></script>

<script src="../common/js/scripts_nw.js"></script>

<!--[if lt IE 9]>
<script src="../common/js/html5shiv.js"></script>
<script type="text/javascript" src="../common/js/jquery.belatedPNG.min.js"></script>
<script>
$(function() {
	$(".pngfix").fixPng();
});
</script>
<script src="../common/js/jquery.backgroundSize.js"></script>
<script>
$(function(){
 $('#header #bg').css('background-size', 'cover');
});
</script>
<![endif]-->
</head>
<body id="rp_philosophy">
<div id="document">
	
	<header id="rpr_header">
		<div id="rpr_header_inner" class="clearfix">
			<h1 id="rpr_site_logo"><a href="http://www.vermicular.jp/" class="hover"><img src="../common/img/site_logo.png" height="27" width="143" alt="VERMICULAR（バーミキュラ） MADE IN JAPAN"></a></h1>
			<nav id="rpr_gnavi">
				<ul class="clearfix">
					

					

					
					<li class="list"><a href="../concept/" class="hover"><img src="../common/img/n_gm/gnavi01.png" width="65" height="17" alt="コンセプト"></a></li>
					
					
					
					<li class="list">
					<a class="hover"><img src="../common/img/n_gm/gnavi03.png" width="75" height="17" alt="選ばれる理由"></a>
					<ul class="dd_list">
						<li class="list01"><a href="../technology/">テクノロジー</a></li>
						<li class="list02"><a href="../philosophy/">デザイン思想</a></li>
						<li class="list03"><a href="../cookingmode/">調理機能</a></li>
						<li class="list04"><a href="../safe/">安心・安全</a></li>
						<li class="list05"><a href="../accessories/">付属品</a></li>
					</ul>
					</li>


					<li class="list"><a href="../care/" class="hover"><img src="../common/img/n_gm/gnavi04.png" width="90" height="17" alt="使い方・お手入れ"></a></li>


					<li class="list">
					<span><a class="hover"><img src="../common/img/n_gm/gnavi05.png" width="90" height="17" alt="スペック・FAQ"></a></span>
					<ul class="dd_list">
						<li class="list01"><a href="../spec/">スペック</a></li>
						<li class="list02"><a href="../faq/">FAQ</a></li>
					</ul>
					</li>



					<li class="list"><a href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank"  class="hover"><img src="../common/img/n_gm/gnavi06.png" width="105" height="17" alt="オンラインショップ"></a></li> 	




				</ul>
				</nav>



			<nav id="rp_sns">
			<div class="btn"  class="hover"><img src="../common/img/head_sns_icon_btn.png" height="14" width="10" alt="SNS"></div>
			<ul class="sns_list clearfix">
				<li class="sns01"><a href="https://www.facebook.com/vermicular" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_01.png" height="17" width="7" alt="Facebook"></a></li>
				<li class="sns02"><a href="https://twitter.com/_VERMICULAR_" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_02.png" height="11" width="14" alt="Twitter"></a></li>
				<li class="sns03"><a href="https://instagram.com/vermicular_japan/" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_03.png" height="13" width="13" alt="Instagram"></a></li>
				<li class="sns04"><a href="http://shop.vermicular.jp/mail/" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_04.png" height="10" width="15" alt="Mail"></a></li>
			</ul>
			</nav>
		</div>
	</header><!-- /#header -->

	
	<main id="main_wrap">		<header id="rpr_page_title_wrap">
			<div class="inner">
				<h1 id="page_title"><img src="../img/philosophy/page_title.png" height="58" width="185" alt="デザイン思想" /></h1>
			</div>
		</header><!-- /#kv_wrap -->

		<div class="main_content">
			<div id="catch_copy">
				<h2 class="catch"><img src="../img/philosophy/copy_01.png" height="37" width="686" alt="ライスポットに、保温機能がない3つの理由。" /></h2>
				<p class="text">
					それは、究極のおいしいご飯を炊くためです。理想の炊飯には、一般的な炊飯器にある保温用の<br />
					フタを無くす必要がありました。保温機能はなくなりましたが、一度味わえば、きっと納得していただけるでしょう。<br />
					ライスポットで炊いた、おいしいご飯だからこそ、保温より温め直して召し上がることをおすすめします。
				</p>
			</div><!-- /#catch_copy -->
			<section id="section01" class="clearfix">
				<div class="section_copy">
					<div class="reason">
						<img src="../img/philosophy/reason_01.png" height="22" width="127" alt="reason1">
					</div>
					<div class="reason_title">
						<img src="../img/philosophy/copy_02.png" height="34" width="796" alt="炊飯器の保温用のフタを無くすと、ご飯がおいしく炊ける。">
					</div>
				</div>
				<ul>
					<li class="clearfix">
						<div class="copy">
							<h3><img src="../img/philosophy/section_title01.png" height="71" width="365" alt="保温用のフタをなくしたら、ご飯が本当においしくなりました。" /></h2>
							<p class="text">
								保温用のフタをなくすと鍋上部がむき出しになり、加熱される鍋の下部と外気で冷やされる上部に大きな温度差が生まれて激しい熱対流が起こります。すると、お米一粒一粒までむらなく炊き上がるのです。
							</p>
						</div>
						<div class="image">
							<img src="../img/philosophy/section_img01.jpg" height="390" width="580" alt="上下で温度差が生じ激しい熱対流が起こる" />
						</div>
					</li>
				</ul>
				<div class="sub_title">
					<h3>
						<img src="../img/philosophy/section_title02.png" height="27" width="431" alt="ポットヒーターは、「蒸らし」も得意です。" />
					</h3>
					<p class="title_text">
						保温用のフタがないから、ポットヒーター内蔵のファンが加熱を終えた鍋を一気に冷まします。<br>
						すると、お米の外側の水分を内部にしっかり吸収させる「蒸らし」が可能になり、格別なツヤと食感を実現できたのです。<br>
						この究極の炊飯器は、かまどの未来形です。
					</p>
				</div>
				<div class="image_container clearfix">
					<div class="image left">
						<img src="../img/philosophy/section_img02.jpg" height="332" width="498" alt="ポットヒーター内蔵のファン" />
					</div>
					<div class="image right">
						<img src="../img/philosophy/section_img03.png" height="335" width="472" alt="お米の外側の水分を内部にしっかり吸収させる「蒸らし」が可能" />
					</div>
				</div>
			</section>

			<section id="section02" class="clearfix">
				<div class="section_copy">
					<div class="reason">
						<img src="../img/philosophy/reason_02.png" height="22" width="128" alt="reason2">
					</div>
					<div class="reason_title">
						<img src="../img/philosophy/copy_04.png" height="35" width="798" alt="保温用のフタを無くすと、お手入れがカンタンで、衛生的。">
					</div>
				</div>
				<ul>
					<li class="clearfix">
						<div class="copy">
							<h3><img src="../img/philosophy/section02_title.png" height="73" width="436" alt="一般的な炊飯器の保温用のフタにはいろいろな問題がありました。" /></h2>
							<p class="text">
								一般的な炊飯器には、保温用のフタについている「ゴムのパッキン」や<br>
								「細かい金属部品」が多いため、お手入れが大変で、衛生面でも<br>
								問題がありました。さらに、臭いを吸収しやすい性質のあるお米は、<br>
								ゴムや金属の臭いがとても移りやすいのです。
							</p>
						</div>
						<div class="image">
							<img src="../img/philosophy/section02_img_01.png" height="285" width="540" alt="一般的な炊飯器の保温用のフタにはいろいろな問題がありました。" />
						</div>
					</li>
				</ul>
				<div class="sub_title">
					<h3>
						<img src="../img/philosophy/copy_05.png" height="73" width="498" alt="シンプルな構造だから、衛生的で、お米本来の持つ香りの良さを引き出します。" />
					</h3>
					<p class="title_text">
						ライスポットの鍋自体は、継ぎ目のない完全一体構造。ポットヒーターもとてもシンプルな構造です。<br>
						「ゴムのパッキン」や、アルミやステンレスの「細かい金属部品」も食材と直には接しません。<br>
						だから、鍋を洗うだけで、常に衛生的にご使用いただけます。さらには、お米本来の持つ香りの良さを損ないません。
					</p>
				</div>
				<div class="image_container clearfix">
					<div class="image left">
						<img src="../img/philosophy/section02_img_02.jpg" height="300" width="519" alt="ライスポット" />
					</div>
					<div class="image right">
						<img src="../img/philosophy/section02_img_03.jpg" height="300" width="519" alt="ポットヒーター" />
					</div>
				</div>
			</section>

			<section id="section03" class="clearfix">
				<div class="section_copy">
					<div class="reason">
						<img src="../img/philosophy/reason_03.png" height="22" width="129" alt="reason3">
					</div>
					<div class="reason_title">
						<img src="../img/philosophy/copy_06.png" height="34" width="422" alt="保温したご飯は、おいしくない。">
					</div>
					<div class="reason_text">
						<p class="title_text">
							バーミキュラは、おいしく炊けたご飯こそ、保温より温めなおして召し上がることをおすすめします。<br>
							事実、愛知ドビーが実施した調査では、「炊飯器で保温」している人は、20%程度で少数派でした。<br>
							また、3人に2人は「保温が必要ない」か「保温がなくても困らない」と回答しています。
						</p>
					</div>
				</div>
				<div class="image_container clearfix">
					<div class="image left graph_rod">
						<img src="../img/philosophy/section03_title_01.png" height="57" width="445" alt="Q あなたのご家庭では、炊いたご飯が余った場合、残りのご飯をどのように保存していますか？" class="image_text">
						<img src="../img/philosophy/section03_img_01.png" height="354" width="455" alt="ご飯を保存するためによく使う方法・ご飯の保存で最も好むもの" class="rod" />
						<p>※調査対象者：女性20〜69歳 500ss：週1回以上自分でご飯を炊く人（2016.9月：愛知ドビー調べ）</p>
					</div>
					<div class="image right graph_circle">
						<img src="../img/philosophy/section03_title_02.png" height="23" width="492" alt="Q あなたは、炊飯器の保温機能が必要だと思いますか？" class="image_text">
						<img src="../img/philosophy/section03_img_02.png" height="346" width="346" alt="保温は必要無い・22.2% 保温がなくても困らない・42.4%　保温は絶対必要・35.4%" class="circle" />
						<p>※調査対象者：女性20〜69歳 500ss：週1回以上自分でご飯を炊く人（2016.9月：愛知ドビー調べ）</p>
					</div>
				</div>
			</section>
		</div>


<a id="online_btn" href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank">
	<img src="../common/img/btn_online.png" width="130" height="130" alt="Online Shop">
</a>



				
			
<div class="btn clear_black w198 center "><a href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../img/products/top/onlineshop_text.png" height="13" width="101" alt="オンラインショップ"></span></span></a></div></br>
<?php
	include '../_footer_rp.php';
	?>