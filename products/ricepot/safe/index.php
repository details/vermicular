﻿<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ja" class="ie ie6" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 7]>   <html lang="ja" class="ie ie7" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 8]>   <html lang="ja" class="ie ie8" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 9]>   <html lang="ja" class="ie ie9" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="ja" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">


<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1280">
<link rel="canonical" href="http://www.vermicular.jp/products/ricepot/safe">
<title>安心・安全 | Vermicular（バーミキュラ）ライスポット</title>
<meta name="author" content="愛知ドビー株式会社">
<meta name="description" content="お手入れのしやすさと、メイド・イン・ジャパンの安心を。">
<meta name="keywords" content="安心・安全,電磁波,アルミ電磁波シールド, IEC62233,EMP測定,3年保証,メイド・イン・ジャパン,ライスポット,炊飯鍋,炊飯器,バーミキュラ,Vermicular,ricepot,鋳物,愛知ドビー">

<meta property="og:title" content="安心・安全 | Vermicular（バーミキュラ）ライスポット" />
<meta property="og:type" content="website">
<meta property="og:url" content="http://www.vermicular.jp/products/ricepot/safe/index.php " />
<meta property="og:image" content="http://www.vermicular.jp/products/ricepot/common/img/ogp.jpg" />
<meta property="og:site_name" content="安心・安全 | Vermicular（バーミキュラ）ライスポット" />
<meta property="og:description" content="お手入れのしやすさと、メイド・イン・ジャパンの安心を。" />
<meta property="fb:app_id" content="171078026685763" />

<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="安心・安全 | Vermicular（バーミキュラ）ライスポット">
<meta name="twitter:title" content="安心・安全 | Vermicular（バーミキュラ）ライスポット">
<meta name="twitter:url" content="/vermicular/products/ricepot/safe/index.php ">
<meta name="twitter:description" content="お手入れのしやすさと、メイド・イン・ジャパンの安心を。">

<meta name="format-detection" content="telephone=no,address=no,email=no">

<link rel="stylesheet" type="text/css" href="../common/css/normalize.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/base_nw.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/styles_nw.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/lowlayer.css?20161101">


<link rel="stylesheet" type="text/css" href="../css/safe.css">

<link href="../common/img/apple-touch-icon.png" rel="apple-touch-icon">
<meta name="apple-mobile-web-app-title" content="Vermicular">

<script src="../common/js/jquery-1.11.0.min.js?20161101"></script>
<script src="../common/js/jquery.transit.js?20161101"></script>
<script src="../common/js/pace.min.js?20161101"></script>
<script src="../common/js/jquery.colorbox-min.js?20161101"></script>
<script src="../common/js/var.js?20161101"></script>
<script src="../common/js/function.js?20161101"></script>
<script src="../common/js/base.js?20161101"></script>
<script src="../common/js/jquery.bxslider.min.js?20161101"></script>

<script src="../common/js/scripts_nw.js"></script>

<!--[if lt IE 9]>
<script src="../common/js/html5shiv.js"></script>
<script type="text/javascript" src="../common/js/jquery.belatedPNG.min.js"></script>
<script>
$(function() {
	$(".pngfix").fixPng();
});
</script>
<script src="../common/js/jquery.backgroundSize.js"></script>
<script>
$(function(){
 $('#header #bg').css('background-size', 'cover');
});
</script>
<![endif]-->
</head>
<body id="rp_safe">
<div id="document">
	
	<header id="rpr_header">
		<div id="rpr_header_inner" class="clearfix">
			<h1 id="rpr_site_logo"><a href="http://www.vermicular.jp/" class="hover"><img src="../common/img/site_logo.png" height="27" width="143" alt="VERMICULAR（バーミキュラ） MADE IN JAPAN"></a></h1>
			<nav id="rpr_gnavi">
				<ul class="clearfix">
					

					

					
					<li class="list"><a href="../concept/" class="hover"><img src="../common/img/n_gm/gnavi01.png" width="65" height="17" alt="コンセプト"></a></li>
					

					<li class="list">
					<a class="hover"><img src="../common/img/n_gm/gnavi03.png" width="75" height="17" alt="選ばれる理由"></a>
					<ul class="dd_list">
						<li class="list01"><a href="../technology/">テクノロジー</a></li>
						<li class="list02"><a href="../philosophy/">デザイン思想</a></li>
						<li class="list03"><a href="../cookingmode/">調理機能</a></li>
						<li class="list04"><a href="../safe/">安心・安全</a></li>
						<li class="list05"><a href="../accessories/">付属品</a></li>
					</ul>
					</li>


					<li class="list"><a href="../care/" class="hover"><img src="../common/img/n_gm/gnavi04.png" width="90" height="17" alt="使い方・お手入れ"></a></li>


					<li class="list">
					<span><a class="hover"><img src="../common/img/n_gm/gnavi05.png" width="90" height="17" alt="スペック・FAQ"></a></span>
					<ul class="dd_list">
						<li class="list01"><a href="../spec/">スペック</a></li>
						<li class="list02"><a href="../faq/">FAQ</a></li>
					</ul>
					</li>



					<li class="list"><a href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank"  class="hover"><img src="../common/img/n_gm/gnavi06.png" width="105" height="17" alt="オンラインショップ"></a></li> 	




				</ul>
				</nav>



			<nav id="rp_sns">
			<div class="btn"  class="hover"><img src="../common/img/head_sns_icon_btn.png" height="14" width="10" alt="SNS"></div>
			<ul class="sns_list clearfix">
				<li class="sns01"><a href="https://www.facebook.com/vermicular" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_01.png" height="17" width="7" alt="Facebook"></a></li>
				<li class="sns02"><a href="https://twitter.com/_VERMICULAR_" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_02.png" height="11" width="14" alt="Twitter"></a></li>
				<li class="sns03"><a href="https://instagram.com/vermicular_japan/" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_03.png" height="13" width="13" alt="Instagram"></a></li>
				<li class="sns04"><a href="http://shop.vermicular.jp/mail/" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_04.png" height="10" width="15" alt="Mail"></a></li>
			</ul>
			</nav>
		</div>
	</header><!-- /#header -->

	
	<main id="main_wrap">		<header id="rpr_page_title_wrap">
			<div class="inner">
				<h1 id="page_title"><img src="../img/safe/page_title.png" height="58" width="158" alt="安心・安全" /></h1>
			</div>
		</header><!-- /#kv_wrap -->
		<div id="catch_copy">
			<h2 class="catch"><img src="../img/safe/copy_01.png" height="37" width="478" alt="メイド・イン・ジャパンの安心を。" /></h2>
			<p class="text">
				食にかかわる者として、本当に安全な製品をお届けしたい。<br/>
				バーミキュラが一番大切にしているこだわりです。
			</p>
		</div><!-- /#catch_copy -->


		<section id="section01" class="clearfix">
			<ul class="outer" id="desk">
				<li class="clearfix rpr_section_wrap inner_min clearfix" id="support">
					<div class="cont_wrap flr">
					<h1 class="rpr_section_title"><img src="../img/support/section_title01-1.png" height="15" width="129" alt="オーナーズデスク"></h1>
					<p class="title"><img src="../img/support/section_title01-2.png" alt="コンシェルジュの仕事に、マニュアルはありません。" width="282" height="70"></p>
					<p class="text">購入前のご相談から、注文、使い方、調理方法の疑問、修理など、どんなことでもお気軽にご連絡ください。バーミキュラのコンシェルジェがサポートいたします。</p>
					<div class="policy_wrap">
						<h2><img src="../img/support/section_title01-3.png" width="321" height="20" alt="バーミキュラオーナーズデスクの方針"></h2>
						<ul>
							<li><span>常にお客様の側に立ち、自分・会社の利益を超えて、お客様の満足を最優先させます。</span></li>
							<li><span>常にバーミキュラの一番の使い手となり、ご注文からご使用の一生にわたってサポートし続けます。</span></li>
							<li><span>常にお客様の状況を想像し、心のこもったおもてなしを目指します。</span></li>
							<li><span>バーミキュラブランドで世界の家庭にひとつでも笑顔を増やすことを目指します。</span></li>
						</ul>
					</div>
					<dl class="contact_wrap bg_gray">
						<dt>オーナーズデスク</dt>
						<dd>
							<span><img src="../img/safe/icon_freedial.png" width="39" height="22" alt="freedial"><span class="tel_text">0120-766-787</span></span><br>
							<span class="month">（月～金 9:00-12:00、13:00-17:00）</span>
						</dd>
					</dl>
					</div>
				<p class="photo_wrap fll"><img src="../img/support/section_img01.jpg" height="628" width="580" alt="オーナーズデスク イメージ写真"></p>
				</li>
				<li class="clearfix normal">
					<div class="copy">
						<h3><img src="../img/safe/section_copy02.png" height="72" width="265" alt="一生使える鍋だから、一生お直しいたします。" /></h2>
						<p class="text">
							覚えておいてください。使い込んで傷んだ鍋や、ホーローの剥がれも、バーミキュラなら何度でも修理できます。親子で使い続けられる、一生ものですから。（有料）
						</p>
					<div class="btn clear_black w218">
						<a href="http://shop.vermicular.jp/jp/free.php?id=13" target="_blank"><span class="btn_inner"><img src="../img/safe/btn_link_more.png" width="31" height="10" alt="more"></span></a>
					</div>
					</div>
					<div class="image"><img src="../img/safe/section_img02.jpg" height="390" width="580" alt="一生使える鍋だから、一生お直しいたします。" /></div>
				</li>
				<li class="clearfix normal">
					<div class="image"><img src="../img/safe/section_img03.jpg" height="390" width="580" alt="安心の電磁波対策。" /></div>
					<div class="copy">
						<h3><img src="../img/safe/section_copy03.png" height="27" width="241" alt="安心の電磁波対策。" /></h2>
						<p class="text">
							IHから発生する電磁波は、人体に有害とも言われ、欧州等では厳格な国際規格の遵守が求められています。ライスポットはコイルからの電磁波のモレを防ぐ「アルミ電磁波シールド」を採用し、前後左右上方向にて国際規格の約10分の１のレベルを実現しています。<br/>（IEC62233に準拠したEMF測定による）
						</p>
					</div>
				</li>
				<li class="clearfix normal">
					<div class="copy">
						<h3><img src="../img/safe/section_copy04.png" height="27" width="293" alt="ポットヒーターは３年保証。" /></h2>
						<p class="text">
							一生ものの鍋とセットのポットヒーターを長く使っていただくために、通常１年とされる保証を３年に。メイド・イン・ジャパンだからできる安心です。
						</p>
					</div>
					<div class="image"><img src="../img/safe/section_img04.jpg" height="390" width="579" alt="ポットヒーターは３年保証。" /></div>
				</li>
				<li class="clearfix normal">
					<div class="image"><img src="../img/safe/section_img05.jpg" height="390" width="579" alt="付属品も、全てメイド・イン・ジャパン。" /></div>
					<div class="copy">
						<h3><img src="../img/safe/section_copy05.png" height="28" width="414" alt="付属品も、全てメイド・イン・ジャパン。" /></h2>
						<p class="text">
							鍋だけでなく、ポットヒーター・専用計量カップなど付属品も全てメイド・イン・ジャパン。安心・安全もハイレベルです。
						</p>
						<div class="btn clear_black w218">
							<a href="../accessories/">
								<span class="btn_inner">
									<img src="../img/safe/btn_link_more.png" width="31" height="10" alt="more">
								</span>
							</a>
						</div>
					</div>
				</li>
			</ul>
		</section>
				
				
				<a id="online_btn" href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank">
	<img src="../common/img/btn_online.png" width="130" height="130" alt="Online Shop">
</a>


				
								
			
<div class="btn clear_black w198 center "><a href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../img/products/top/onlineshop_text.png" height="13" width="101" alt="オンラインショップ"></span></span></a></div></br>
<?php
	include '../_footer_rp.php';
	?>