﻿<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ja" class="ie ie6" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 7]>   <html lang="ja" class="ie ie7" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 8]>   <html lang="ja" class="ie ie8" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 9]>   <html lang="ja" class="ie ie9" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="ja" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">


<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1280">
<link rel="canonical" href="http://www.vermicular.jp/products/ricepot/technology">
<title>テクノロジー | Vermicular（バーミキュラ）ライスポット</title>
<meta name="author" content="愛知ドビー株式会社">
<meta name="description" content="お米本来のうまみを引き出す、ふたつのテクノロジー。">
<meta name="keywords" content="テクノロジー,トリプルサーモテクノロジー,ラップアップヒートテクノロジー,対流を促す丸み,一体構造のフタ,飯返ししやすい鍋底のリブ,フローティングリッド,ハイパワーIHコイル,アルミヒーター,ヒートセンサー,ライスポット,炊飯鍋,炊飯器,バーミキュラ,Vermicular,ricepot,鋳物,愛知ドビー">

<meta property="og:title" content="テクノロジー | Vermicular（バーミキュラ）ライスポット" />
<meta property="og:type" content="website">
<meta property="og:url" content="http://www.vermicular.jp/products/ricepot/technology/index.php " />
<meta property="og:image" content="http://www.vermicular.jp/products/ricepot/common/img/ogp.jpg" />
<meta property="og:site_name" content="テクノロジー | Vermicular（バーミキュラ）ライスポット" />
<meta property="og:description" content="お米本来のうまみを引き出す、ふたつのテクノロジー。" />
<meta property="fb:app_id" content="171078026685763" />

<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="テクノロジー | Vermicular（バーミキュラ）ライスポット">
<meta name="twitter:title" content="テクノロジー | Vermicular（バーミキュラ）ライスポット">
<meta name="twitter:url" content="/vermicular/products/ricepot/technology/index.php ">
<meta name="twitter:description" content="お米本来のうまみを引き出す、ふたつのテクノロジー。">

<meta name="format-detection" content="telephone=no,address=no,email=no">

<link rel="stylesheet" type="text/css" href="../common/css/normalize.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/base_nw.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/styles_nw.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/lowlayer.css?20161101">


<link rel="stylesheet" type="text/css" href="../css/technology.css">

<link href="../common/img/apple-touch-icon.png" rel="apple-touch-icon">
<meta name="apple-mobile-web-app-title" content="Vermicular">

<script src="../common/js/jquery-1.11.0.min.js?20161101"></script>
<script src="../common/js/jquery.transit.js?20161101"></script>
<script src="../common/js/pace.min.js?20161101"></script>
<script src="../common/js/jquery.colorbox-min.js?20161101"></script>
<script src="../common/js/var.js?20161101"></script>
<script src="../common/js/function.js?20161101"></script>
<script src="../common/js/base.js?20161101"></script>
<script src="../common/js/jquery.bxslider.min.js?20161101"></script>

<script src="../common/js/scripts_nw.js"></script>
<script src="../common/js/lib.js"></script>

<!--[if lt IE 9]>
<script src="../common/js/html5shiv.js"></script>
<script type="text/javascript" src="../common/js/jquery.belatedPNG.min.js"></script>
<script>
$(function() {
	$(".pngfix").fixPng();
});
</script>
<script src="../common/js/jquery.backgroundSize.js"></script>
<script>
$(function(){
 $('#header #bg').css('background-size', 'cover');
});
</script>
<![endif]-->
</head>
<body id="rp_technology">
<div id="document">
	
	<header id="rpr_header">
		<div id="rpr_header_inner" class="clearfix">
			<h1 id="rpr_site_logo"><a href="http://www.vermicular.jp/" class="hover"><img src="../common/img/site_logo.png" height="27" width="143" alt="VERMICULAR（バーミキュラ） MADE IN JAPAN"></a></h1>
			<nav id="rpr_gnavi">
				<ul class="clearfix">
					

					

					
					<li class="list"><a href="../concept/" class="hover"><img src="../common/img/n_gm/gnavi01.png" width="65" height="17" alt="コンセプト"></a></li>
					
					
					
					<li class="list">
					<a class="hover"><img src="../common/img/n_gm/gnavi03.png" width="75" height="17" alt="選ばれる理由"></a>
					<ul class="dd_list">
						<li class="list01"><a href="../technology/">テクノロジー</a></li>
						<li class="list02"><a href="../philosophy/">デザイン思想</a></li>
						<li class="list03"><a href="../cookingmode/">調理機能</a></li>
						<li class="list04"><a href="../safe/">安心・安全</a></li>
						<li class="list05"><a href="../accessories/">付属品</a></li>
					</ul>
					</li>


					<li class="list"><a href="../care/" class="hover"><img src="../common/img/n_gm/gnavi04.png" width="90" height="17" alt="使い方・お手入れ"></a></li>


					<li class="list">
					<span><a class="hover"><img src="../common/img/n_gm/gnavi05.png" width="90" height="17" alt="スペック・FAQ"></a></span>
					<ul class="dd_list">
						<li class="list01"><a href="../spec/">スペック</a></li>
						<li class="list02"><a href="../faq/">FAQ</a></li>
					</ul>
					</li>



					<li class="list"><a href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank"  class="hover"><img src="../common/img/n_gm/gnavi06.png" width="105" height="17" alt="オンラインショップ"></a></li> 	




				</ul>
				</nav>



			<nav id="rp_sns">
			<div class="btn"  class="hover"><img src="../common/img/head_sns_icon_btn.png" height="14" width="10" alt="SNS"></div>
			<ul class="sns_list clearfix">
				<li class="sns01"><a href="https://www.facebook.com/vermicular" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_01.png" height="17" width="7" alt="Facebook"></a></li>
				<li class="sns02"><a href="https://twitter.com/_VERMICULAR_" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_02.png" height="11" width="14" alt="Twitter"></a></li>
				<li class="sns03"><a href="https://instagram.com/vermicular_japan/" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_03.png" height="13" width="13" alt="Instagram"></a></li>
				<li class="sns04"><a href="http://shop.vermicular.jp/mail/" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_04.png" height="10" width="15" alt="Mail"></a></li>
			</ul>
			</nav>
		</div>
	</header><!-- /#header -->

	
	<main id="main_wrap">		<header id="rpr_page_title_wrap">
			<div class="inner">
				<h1 id="page_title"><img src="../img/technology/page_title.png" height="57" width="161" alt="テクノロジー"></h1>
			</div>
		</header><!-- /#kv_wrap -->
		<div id="catch_copy">
			<h2 class="catch"><img src="../img/technology/section_title01.png" height="97" width="474" alt="お米本来のうまみを引き出す、ふたつのテクノロジー。"></h2>
			<p class="text">
				熱の伝わり方をコントロールするトリプルサーモテクノロジーと、<br/>
				かまどのように立体的に加熱するラップアップヒートテクノロジー。
			</p>
		</div><!-- /#catch_copy -->


		<section id="section01" class="clearfix">
			<div class="cont_wrap">
				<h3 class="rpr_section_title"><img src="../img/technology/section_title02.png" height="71" width="503" alt="感動する炊き上がり。バーミキュラは、鍋をここまで進化させました。 "></h3>
				<p class="text">
					素材本来の味を引き出すトリプルサーモテクノロジーはそのままに、炊飯のためにすべてのデザインを刷新。<br/>
					対流を促す丸みを帯びた鍋のカーブ、継ぎ目の無い一体構造のフタ、<br/>
					飯返ししやすい鍋底のリブ、高火力を受け止める新技術も搭載しました。
				</p>
			</div>

		</section>
		<div class="section01_list">
			<div class="title_block">
				<h2><img src="../img/technology/section01_titile03.png" height="22" width="302" alt="炊飯用に鍋のデザインを刷新。"></h2>
			</div>
			<div class="type_list">
				<ul class="clearfix">
					<li>
						<img src="../img/technology/section01_thumb01.jpg" height="191" width="290" alt="">
						<dl>
							<dt><img src="../img/technology/thumb01_titile01.png" height="16" width="102" alt="鍋全体の丸み"></dt>
							<dd>スムーズな対流を生み出します。</dd>
						</dl>
					</li>
					<li>
						<img src="../img/technology/section01_thumb02.jpg" height="191" width="290" alt="">
						<dl>
							<dt><img src="../img/technology/thumb01_titile02.png" height="16" width="132" alt="水紋状のリブ形状"></dt>
							<dd>飯返しがしやすく、洗いやすくなりました。</dd>
						</dl>
					</li>
					<li>
						<img src="../img/technology/section01_thumb03.jpg" height="191" width="290" alt="">
						<dl>
							<dt><img src="../img/technology/thumb01_titile03.png" height="15" width="74" alt="つまみレス"></dt>
							<dd>継ぎ目のない一体構造で衛生的です。</dd>
						</dl>
					</li>
				</ul>
			</div>
		</div>

		<section id="section02" class="clearfix">
			<div class="cont_wrap wrap1">
				<div class="wrap_inner">
					<h1 class="rpr_section_title"><img src="../img/technology/section_title03.png" height="57" width="539" alt="トリプルサーモテクノロジー"></h1>
					<div class="detail">
						<h2><img src="../img/technology/section01_copy.png" height="56" width="275" alt="トリプルサーモテクノロジーが、お米の旨味を引き出す。"></h2>
						<p>熱伝導・遠赤外線加熱・蒸気対流という３つの熱の伝わり方をコントロールして、素材本来の味を引き出す。この画期的な熱伝達システムを、ライスポットにも応用しています。</p>
						<div class="image"><img src="../img/technology/section03_img02.jpg" height="99" width="439" alt="トリプルサーモテクノロジーが、お米の旨味を引き出す。"></div>
					</div>
					<p class="btn clear w220 center"><a href="../../../about/teshigoto/"><span class="btn_inner"><img src="../img/technology/link_btn.png" height="13" width="153" alt="テクノロジーの詳細はこちら"></span></a></p>
				</div>
			</div>
			<div class="cont_wrap">
				<ul class="outer clearfix">
					<li class="clearfix normal">
						<div class="image"><img src="../img/technology/section04_img01.jpg" height="250" width="521" alt="高火力を受け止める、新開発フローティンググリッド" /></div>
						<div class="copy">
							<h3><img src="../img/technology/section04_copy01.png" height="57" width="244" alt="高火力を受け止める、新開発フローティンググリッド" /></h2>
							<p class="text">
								鋳物ホーロー鍋の弱点は、高火力時の吹きこぼれでした。新開発のフローティングリッドが、密閉性を保ちながら蒸気の吹き出し箇所をコントロールすることで、吹きこぼれを最小限に抑えて、高火力でご飯をおいしく炊き上げます。
							</p>
						</div>
					</li>
					<li class="clearfix normal">
						<div class="copy">
							<h3><img src="../img/technology/section04_copy02.png" height="23" width="231" alt="新技術ダブルリッドリング" /></h2>
							<p class="text">
								フタの裏にある二つのリング状の突起が、炊き上がった後のご飯のべちゃつきを抑えるので、鍋をそのまま食卓に運び、「おひつ」のようにご使用できます。
							</p>
						</div>
						<div class="image"><img src="../img/technology/section04_img02.jpg" height="250" width="520" alt="新技術ダブルリッドリング" /></div>
					</li>
					<li class="clearfix normal">
						<div class="video_wrap clearfix">
						    <p class="thum">
						        <a href="https://www.youtube.com/embed/TTqnrWaiumg?autoplay=1&amp;rel=0&amp;wmode=transparent" class="clbox_youtube cboxElement">
						        	<img src="../img/technology/section04_img03.jpg" height="250" width="520" alt="鍋に、100分の1ミリの精度。">
						        </a>
						    </p>
						</div>
						<div class="copy">
							<h3><img src="../img/technology/section04_copy03.png" height="22" width="249" alt="鍋に、100分の1ミリの精度" /></h2>
							<p class="text">
								日本職人の技術力が生み出す、鋳物ホーロー鍋として例を見ないフタと本体の高い密閉性が、トリプルサーモテクノロジーを支えています。
							</p>
						</div>
					</li>
				</ul>
			</div>
			<div class="cont_wrap wrap3">
				
			</div>
		</section>

		<section id="section03" class="clearfix">
			<div class="cont_wrap wrap1">
				<div class="wrap_inner">
				<p class="text_1"><img src="../img/technology/section_title05.png" height="72" width="588" alt="あなたは指先で触れるだけ。ポットヒーターが、直火を超える究極の加熱を実現。"></p>
				<p class="text_2">
					炎が鍋を包み込むかまどのような加熱を、指先ひとつで自在にコントロール。
				</p>
				</div>
			</div>

			<div class="cont_wrap wrap2">
				<div class="wrap_inner">
					<h2 class="rpr_section_title"><img src="../img/technology/section_title04.png" height="58" width="505" alt="ラップアップテクノロジー"></h2>
					<div class="detail">
						<h3><img src="../img/technology/section03_copy.png" height="57" width="273" alt="かまどに学び、かまどを超えた、ラップアップヒートテクノロジー。"></h3>
						<p>かまどの炎を再現したら、このデザインになりました。底面のハイパワーIHコイルと側面のアルミヒーター、断熱カバーを組み合わせて、鍋を包みこむように加熱することで、かまどの炎のような立体的な加熱を実現しました。</p>
					</div>
				</div>
			</div>
			<div class="last_area">
				<ul class="outer clearfix">
					<li class="clearfix normal">
						<div class="image"><img src="../img/technology/section06_img02.jpg" height="251" width="452" alt="直火のような熱分布を実現" /></div>
						<div class="copy">
							<h3><img src="../img/technology/section06_copy02.png" height="22" width="259" alt="直火のような熱分布を実現" /></h2>
							<p class="text">
								熱分布を比較すると、「ポットヒーター」は、「直火」のような熱分布を実現。断熱カバーの効果で、外気の影響を受けないため、直火よりも均一に加熱が可能です。
							</p>
						</div>
					</li>
					<li class="clearfix normal">
						<div class="copy">
							<h3><img src="../img/technology/section06_copy03.png" height="55" width="263" alt="あなたの代わりに火を見張る、ヒートセンサー。" /></h2>
							<p class="text">
								鍋の性能を生かすのは、火加減です。ヒートセンサー（温度センサー）が鍋底の状態を見守って、家族の誰よりも繊細な温度調整をしてくれます。
							</p>
						</div>
						<div class="image"><img src="../img/technology/section06_img03.jpg" height="250" width="520" alt="あなたの代わりに火を見張る、ヒートセンサー。" /></div>
					</li>
				</ul>
			</div>
		</section>


				<a id="online_btn" href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank">
	<img src="../common/img/btn_online.png" width="130" height="130" alt="Online Shop">
</a>

							
								
										
			
<div class="btn clear_black w198 center "><a href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../img/products/top/onlineshop_text.png" height="13" width="101" alt="オンラインショップ"></span></span></a></div></br>
<?php
	include '../_footer_rp.php';
	?>