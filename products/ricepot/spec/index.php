﻿<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ja" class="ie ie6" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 7]>   <html lang="ja" class="ie ie7" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 8]>   <html lang="ja" class="ie ie8" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 9]>   <html lang="ja" class="ie ie9" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="ja" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">


<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1280">
<link rel="canonical" href="http://www.vermicular.jp/products/ricepot/spec">
<title>スペック | Vermicular（バーミキュラ）ライスポット</title>
<meta name="author" content="愛知ドビー株式会社">
<meta name="description" content="バーミキュラライスポットのスペック詳細について。">
<meta name="keywords" content="スペック,ライスポット,炊飯鍋,炊飯器,バーミキュラ,Vermicular,ricepot,鋳物,愛知ドビー">

<meta property="og:title" content="スペック | Vermicular（バーミキュラ）ライスポット" />
<meta property="og:type" content="website">
<meta property="og:url" content="http://www.vermicular.jp/products/ricepot/spec/index.php " />
<meta property="og:image" content="http://www.vermicular.jp/products/ricepot/common/img/ogp.jpg" />
<meta property="og:site_name" content="スペック | Vermicular（バーミキュラ）ライスポット" />
<meta property="og:description" content="バーミキュラライスポットのスペック詳細について。" />
<meta property="fb:app_id" content="171078026685763" />

<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="スペック | Vermicular（バーミキュラ）ライスポット">
<meta name="twitter:title" content="スペック | Vermicular（バーミキュラ）ライスポット">
<meta name="twitter:url" content="/vermicular/products/ricepot/spec/index.php ">
<meta name="twitter:description" content="バーミキュラライスポットのスペック詳細について。">

<meta name="format-detection" content="telephone=no,address=no,email=no">

<link rel="stylesheet" type="text/css" href="../common/css/normalize.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/base_nw.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/styles_nw.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/lowlayer.css?20161101">


<link rel="stylesheet" type="text/css" href="../css/spec.css">

<link href="../common/img/apple-touch-icon.png" rel="apple-touch-icon">
<meta name="apple-mobile-web-app-title" content="Vermicular">

<script src="../common/js/jquery-1.11.0.min.js?20161101"></script>
<script src="../common/js/jquery.transit.js?20161101"></script>
<script src="../common/js/pace.min.js?20161101"></script>
<script src="../common/js/jquery.colorbox-min.js?20161101"></script>
<script src="../common/js/var.js?20161101"></script>
<script src="../common/js/function.js?20161101"></script>
<script src="../common/js/base.js?20161101"></script>
<script src="../common/js/jquery.bxslider.min.js?20161101"></script>

<script src="../common/js/scripts_nw.js"></script>

<!--[if lt IE 9]>
<script src="../common/js/html5shiv.js"></script>
<script type="text/javascript" src="../common/js/jquery.belatedPNG.min.js"></script>
<script>
$(function() {
	$(".pngfix").fixPng();
});
</script>
<script src="../common/js/jquery.backgroundSize.js"></script>
<script>
$(function(){
 $('#header #bg').css('background-size', 'cover');
});
</script>
<![endif]-->
</head>
<body id="rp_spec">
<div id="document">
	
	<header id="rpr_header">
		<div id="rpr_header_inner" class="clearfix">
			<h1 id="rpr_site_logo"><a href="http://www.vermicular.jp/" class="hover"><img src="../common/img/site_logo.png" height="27" width="143" alt="VERMICULAR（バーミキュラ） MADE IN JAPAN"></a></h1>
			<nav id="rpr_gnavi">
				<ul class="clearfix">
					

					

					
					<li class="list"><a href="../concept/" class="hover"><img src="../common/img/n_gm/gnavi01.png" width="65" height="17" alt="コンセプト"></a></li>
					
					
					

					<li class="list">
					<a class="hover"><img src="../common/img/n_gm/gnavi03.png" width="75" height="17" alt="選ばれる理由"></a>
					<ul class="dd_list">
						<li class="list01"><a href="../technology/">テクノロジー</a></li>
						<li class="list02"><a href="../philosophy/">デザイン思想</a></li>
						<li class="list03"><a href="../cookingmode/">調理機能</a></li>
						<li class="list04"><a href="../safe/">安心・安全</a></li>
						<li class="list05"><a href="../accessories/">付属品</a></li>
					</ul>
					</li>


					<li class="list"><a href="../care/" class="hover"><img src="../common/img/n_gm/gnavi04.png" width="90" height="17" alt="使い方・お手入れ"></a></li>


					<li class="list">
					<span><a class="hover"><img src="../common/img/n_gm/gnavi05.png" width="90" height="17" alt="スペック・FAQ"></a></span>
					<ul class="dd_list">
						<li class="list01"><a href="../spec/">スペック</a></li>
						<li class="list02"><a href="../faq/">FAQ</a></li>
					</ul>
					</li>



					<li class="list"><a href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank"  class="hover"><img src="../common/img/n_gm/gnavi06.png" width="105" height="17" alt="オンラインショップ"></a></li> 	




				</ul>
				</nav>



			<nav id="rp_sns">
			<div class="btn"  class="hover"><img src="../common/img/head_sns_icon_btn.png" height="14" width="10" alt="SNS"></div>
			<ul class="sns_list clearfix">
				<li class="sns01"><a href="https://www.facebook.com/vermicular" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_01.png" height="17" width="7" alt="Facebook"></a></li>
				<li class="sns02"><a href="https://twitter.com/_VERMICULAR_" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_02.png" height="11" width="14" alt="Twitter"></a></li>
				<li class="sns03"><a href="https://instagram.com/vermicular_japan/" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_03.png" height="13" width="13" alt="Instagram"></a></li>
				<li class="sns04"><a href="http://shop.vermicular.jp/mail/" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_04.png" height="10" width="15" alt="Mail"></a></li>
			</ul>
			</nav>
		</div>
	</header><!-- /#header -->

	
	<main id="main_wrap">		<header id="rpr_page_title_wrap">
			<div class="inner">
				<h1 id="page_title"><img src="../img/spec/page_title.png" height="54" width="107" alt="スペック" /></h1>
			</div>
		</header><!-- /#kv_wrap -->

		<section id="section01" class="clearfix">
			<div class="image left">
				<img src="../img/spec/section01_img01.png" height="318" width="397" alt="寸法図（正面）" />
			</div>
			<div class="image right">
				<img src="../img/spec/section01_img02.png" height="302" width="259" alt="寸法図（側面）" />
			</div>
		</section>

		<section id="section02" class="clearfix">
			<table>
				<thead>
					<tr>
						<th colspan="2">バーミキュラ ライスポット（セット） / RP23A-SV</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th>寸法［mm］</th>
						<td>W約259（A） / W約311（B）×D約296×H約208</td>
					</tr>
					<tr>
						<th>重量</th>
						<td>約6.9kg</td>
					</tr>
					<tr>
						<th>セット価格</th>
						<td>79,800円（税抜）</td>
					</tr>
				</tbody>
			</table>
		</section>

		<section id="section03" class="clearfix">
			<div class="figureBlock">
				<img src="../img/spec/table_img01.jpg" height="134" width="240" alt="">
			</div>
			<table>
				<thead>
					<tr>
						<th colspan="2">ポット（鋳物ホーロー鍋） / PT23Ａ-BK</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th>満水容量</th>
						<td>3.7L</td>
					</tr>
					<tr>
						<th>寸法［mm］</th>
						<td>W約311×D約243×H約127</td>
					</tr>
					<tr>
						<th>重量</th>
						<td>約4.0kg</td>
					</tr>
					<tr>
						<th>材質</th>
						<td>鋳物ホーロー</td>
					</tr>
					<tr>
						<th>付属品</th>
						<td>
							<ul>
								<li>・米用計量カップ/水用計量カップ（材質：トライタン、耐熱105℃）</li>
								<li>・リッドスタンド（材質：鋳鉄ホーロー）</li>
								<li>・バーミキュラ ライスポット レシピブック（B5変形、ハードカバー144P）</li>
							</ul>
						</td>
					</tr>
				</tbody>
			</table>
		</section>
		<section id="section04" class="clearfix">
			<div class="figureBlock">
				<img src="../img/spec/table_img02.jpg" height="151" width="205" alt="ポットヒーター（IH調理器） / ＰＨ23A-SV">
			</div>
			<table>
				<thead>
					<tr>
						<th colspan="4">ポットヒーター（IH調理器） / PH23A-SV</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th>寸法［mm］</th>
						<td colspan="4">W約259×D約296×H約154</td>
					</tr>
					<tr>
						<th>電源コードの長さ</th>
						<td colspan="4">1m</td>
					</tr>
					<tr>
						<th>重量</th>
						<td colspan="4">約2.9kg</td>
					</tr>
					<tr>
						<th>材質</th>
						<td colspan="4">外枠：ポリカーボネート/内枠：アルミニウム/トッププレート：結晶化ガラス</td>
					</tr>
					<tr>
						<th>定格電圧</th>
						<td colspan="4">100V 50/60Hz</td>
					</tr>
					<tr>
						<th>定格電力</th>
						<td colspan="4">1350W</td>
					</tr>
					<tr>
						<th>加熱方式</th>
						<td colspan="4">IH：最大1300W～360W/ヒーター：50W</td>
					</tr>
					<tr>
						<th rowspan="2">基本性能</th>
						<td colspan="2" class="text_center">炊飯モード<br />（ライスポット専用鍋を使用に限る）</td>
						<td colspan="2" class="rice_mode">
							<ul>
								<li>・白米（ふつう・おこげ）1〜5合</li>
								<li>・玄米（ふつう・おこげ）1〜4合</li>
								<li>・白米（おかゆ） 1〜1.5合</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="text_center">調理モード</td>
						<td colspan="2">
							中火・弱火・極弱火・保温<br />
							※保温温度設定：30℃～95℃（1℃毎に設定可）
						</td>
					</tr>
					<tr>
						<th>操作方式</th>
						<td colspan="4">スマートタッチキー（バックライト付静電式タッチパネル）</td>
					</tr>
					<tr>
						<th>表示方式</th>
						<td colspan="4">LED方式</td>
					</tr>
					<tr>
						<th>タイマー</th>
						<td colspan="4">炊飯モード：炊上り時刻セット方式/調理モード：オフタイマーセット方式</td>
					</tr>
					<tr>
						<th>付属品</th>
						<td colspan="4">ポットヒーター取扱説明書</td>
					</tr>
				</tbody>
			</table>
			<div class="text_area">
				<p>*原産国：日本　*ポット/ポットヒーターの単品販売もございます。詳しくは、<a href="../safe/#desk">バーミキュラオーナーズデスク</a>までお問い合わせください。</p>
			</div>
		</section>
			<!-- <table>
				<thead>
					<tr>
						<th></th>
						<th>ポット（鋳物ホーロー鍋） / PT23Ａ-BK</th>
						<th>ポットヒーター（IH調理器） / ＰＨ23A-SV</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th>満水容量</th>
						<td>3.7L</td>
						<td>-</td>
					</tr>
					<tr>
						<th>寸法［mm］</th>
						<td>W311×D243×H127</td>
						<td>W259×D296×H154約4.0kg</td>
					</tr>
					<tr>
						<th>重量</th>
						<td>約4.0kg</td>
						<td>約2.9kg</td>
					</tr>
					<tr>
						<th>材質</th>
						<td>鋳物ホーロー</td>
						<td class="no_space">外枠：ポリカーボネート/内枠：アルミニウム、トッププレート：結晶化ガラス</td>
					</tr>
					<tr>
						<th>定格電圧</th>
						<td>-</td>
						<td>100V 50/60Hz</td>
					</tr>
					<tr>
						<th>定格電力</th>
						<td>-</td>
						<td>1350W</td>
					</tr>
					<tr>
						<th>加熱方式</th>
						<td>-</td>
						<td>IH：最大1300W～360W/ヒーター：50W</td>
					</tr>
					<tr>
						<th rowspan="2">基本性能</th>
						<td rowspan="2">-</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td class="separate clearfix">
							<dl>
								<dt>炊飯モード<br />（ライスポット専用鍋を使用に限る）</dt>
								<dd>
									<ul>
										<li>・白米（ふつう・おこげ）1〜5合</li>
										<li>・玄米（ふつう・おこげ）1〜4合</li>
										<li>・白米（おかゆ） 1〜1.5合</li>
									</ul>
								</dd>
							</dl>
							<dl>
								<dt>調理モード</dt>
								<dd>
									中火・弱火・極弱火・保温<br />
									※保温温度設定：30℃～95℃<br />
									（1℃毎に設定可）
								</dd>
							</dl>
						</td>
					</tr>
					<tr>
						<th>操作方式</th>
						<td>-</td>
						<td>バックライト付静電式タッチパネル</td>
					</tr>
					<tr>
						<th>表示方式</th>
						<td>-</td>
						<td>LED方式</td>
					</tr>
					<tr>
						<th>タイマー</th>
						<td>-</td>
						<td>炊飯モード：炊上り時刻セット方式/調理モード：オフタイマーセット方式</td>
					</tr>
					<tr>
						<th>付属品</th>
						<td>
							<ul>
								<li>・米用計量カップ/水用計量カップ<br />（材質：トライタン、耐熱105℃）</li>
								<li>・リッドスタンド（材質：鋳鉄ホーロー）</li>
								<li>・バーミキュラ ライスポット レシピブック<br />（B5変形、ハードカバー144P）</li>
							</ul>
						</td>
						<td>ポットヒーター取扱説明書</td>
					</tr>
				</tbody>
			</table> -->
		</section>
		
		
		<a id="online_btn" href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank">
	<img src="../common/img/btn_online.png" width="130" height="130" alt="Online Shop">
</a>

						
			
<div class="btn clear_black w198 center "><a href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../img/products/top/onlineshop_text.png" height="13" width="101" alt="オンラインショップ"></span></span></a></div></br>
<?php
	include '../_footer_rp.php';
	?>