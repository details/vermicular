﻿<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ja" class="ie ie6" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 7]>   <html lang="ja" class="ie ie7" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 8]>   <html lang="ja" class="ie ie8" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 9]>   <html lang="ja" class="ie ie9" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="ja" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">


<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1280">
<link rel="canonical" href="http://www.vermicular.jp/products/ricepot/concept">
<title>コンセプト | Vermicular（バーミキュラ）ライスポット</title>
<meta name="author" content="愛知ドビー株式会社">
<meta name="description" content="目指したのは、世界一、おいしいご飯が炊ける炊飯器。">
<meta name="keywords" content="コンセプト,専用ポットヒーター,誰も見たことのない炊飯器,ライスポット,炊飯鍋,炊飯器,バーミキュラ,Vermicular,ricepot,鋳物,愛知ドビー">

<meta property="og:title" content="コンセプト | Vermicular（バーミキュラ）ライスポット" />
<meta property="og:type" content="website">
<meta property="og:url" content="http://www.vermicular.jp/products/ricepot/concept/index.php " />
<meta property="og:image" content="http://www.vermicular.jp/products/ricepot/common/img/ogp.jpg" />
<meta property="og:site_name" content="コンセプト | Vermicular（バーミキュラ）ライスポット" />
<meta property="og:description" content="目指したのは、世界一、おいしいご飯が炊ける炊飯器。" />
<meta property="fb:app_id" content="171078026685763" />

<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="コンセプト | Vermicular（バーミキュラ）ライスポット">
<meta name="twitter:title" content="コンセプト | Vermicular（バーミキュラ）ライスポット">
<meta name="twitter:url" content="/vermicular/products/ricepot/concept/index.php ">
<meta name="twitter:description" content="目指したのは、世界一、おいしいご飯が炊ける炊飯器。">

<meta name="format-detection" content="telephone=no,address=no,email=no">

<link rel="stylesheet" type="text/css" href="../common/css/normalize.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/base_nw.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/styles_nw.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/lowlayer.css?20161101">


<link rel="stylesheet" type="text/css" href="../css/concept2.css?20161031">

<link href="../common/img/apple-touch-icon.png" rel="apple-touch-icon">
<meta name="apple-mobile-web-app-title" content="Vermicular">

<script src="../common/js/jquery-1.11.0.min.js?20161101"></script>
<script src="../common/js/jquery.transit.js?20161101"></script>
<script src="../common/js/pace.min.js?20161101"></script>
<script src="../common/js/jquery.colorbox-min.js?20161101"></script>
<script src="../common/js/var.js?20161101"></script>
<script src="../common/js/function.js?20161101"></script>
<script src="../common/js/base.js?20161101"></script>
<script src="../common/js/jquery.bxslider.min.js?20161101"></script>

<script src="../common/js/scripts_nw.js"></script>
<script src="../common/js/lib.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.3/ScrollMagic.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.3/plugins/animation.gsap.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.3/plugins/debug.addIndicators.js"></script>
<script src="../js/concept.js"></script>

<!--[if lt IE 9]>
<script src="../common/js/html5shiv.js"></script>
<script type="text/javascript" src="../common/js/jquery.belatedPNG.min.js"></script>
<script>
$(function() {
	$(".pngfix").fixPng();
});
</script>
<script src="../common/js/jquery.backgroundSize.js"></script>
<script>
$(function(){
 $('#header #bg').css('background-size', 'cover');
});
</script>
<![endif]-->
</head>
<body id="rp_concept">
<div id="document">
	
	<header id="rpr_header">
		<div id="rpr_header_inner" class="clearfix">
			<h1 id="rpr_site_logo"><a href="http://www.vermicular.jp/" class="hover"><img src="../common/img/site_logo.png" height="27" width="143" alt="VERMICULAR（バーミキュラ） MADE IN JAPAN"></a></h1>
			<nav id="rpr_gnavi">
				<ul class="clearfix">
					

					

					
					<li class="list"><a href="../concept/" class="hover"><img src="../common/img/n_gm/gnavi01.png" width="65" height="17" alt="コンセプト"></a></li>
					
					
                                 
					
					<li class="list">
					<a class="hover"><img src="../common/img/n_gm/gnavi03.png" width="75" height="17" alt="選ばれる理由"></a>
					<ul class="dd_list">
						<li class="list01"><a href="../technology/">テクノロジー</a></li>
						<li class="list02"><a href="../philosophy/">デザイン思想</a></li>
						<li class="list03"><a href="../cookingmode/">調理機能</a></li>
						<li class="list04"><a href="../safe/">安心・安全</a></li>
						<li class="list05"><a href="../accessories/">付属品</a></li>
					</ul>
					</li>


					<li class="list"><a href="../care/" class="hover"><img src="../common/img/n_gm/gnavi04.png" width="90" height="17" alt="使い方・お手入れ"></a></li>


					<li class="list">
					<span><a class="hover"><img src="../common/img/n_gm/gnavi05.png" width="90" height="17" alt="スペック・FAQ"></a></span>
					<ul class="dd_list">
						<li class="list01"><a href="../spec/">スペック</a></li>
						<li class="list02"><a href="../faq/">FAQ</a></li>
					</ul>
					</li>



					<li class="list"><a href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank"  class="hover"><img src="../common/img/n_gm/gnavi06.png" width="105" height="17" alt="オンラインショップ"></a></li> 	




				</ul>
				</nav>



			<nav id="rp_sns">
			<div class="btn"  class="hover"><img src="../common/img/head_sns_icon_btn.png" height="14" width="10" alt="SNS"></div>
			<ul class="sns_list clearfix">
				<li class="sns01"><a href="https://www.facebook.com/vermicular" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_01.png" height="17" width="7" alt="Facebook"></a></li>
				<li class="sns02"><a href="https://twitter.com/_VERMICULAR_" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_02.png" height="11" width="14" alt="Twitter"></a></li>
				<li class="sns03"><a href="https://instagram.com/vermicular_japan/" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_03.png" height="13" width="13" alt="Instagram"></a></li>
				<li class="sns04"><a href="http://shop.vermicular.jp/mail/" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_04.png" height="10" width="15" alt="Mail"></a></li>
			</ul>
			</nav>
		</div>
	</header><!-- /#header -->

	
	<main id="main_wrap">		<header id="rpr_page_title_wrap">
			<div class="inner">
				<h1 id="page_title"><img src="../img/concept/page_title.png" height="56" width="129" alt="コンセプト" /></h1>
			</div>
		</header><!-- /#kv_wrap -->
		
		<div id="catch_copy">
			
			<h2 class="catch"><img src="../img/concept/section01_copy01.png" height="45" width="744" alt="炊飯器を超えたのは、炊飯鍋でした。" /></h2>
			<h3 class="title"><img src="../img/concept/section01_copy02.png" height="22" width="541" alt="目指したのは、世界一、おいしいお米が炊ける炊飯器" /></h3>
			<p class="text font_l">
				「バーミキュラで炊いたご飯は、どんな炊飯器よりおいしい」<br />
				オーナーの方の声が、このプロジェクトのはじまりでした。 メイド・イン・ジャパンの<br />
				精度が可能にした高気密の鋳物ホーロー鍋が、 実は最高の炊飯鍋だったのです。<br />
				鍋炊きで悩ましいのが、火加減。 そこでバーミキュラは、専用ポットヒーターを開発しました。<br />
				炎が鍋を包み込むかまどのような加熱を、 指先ひとつでまかせられるテクノロジーが、<br />
				ついに完成したのです。 食卓のまん中に、白いご飯を。 誰も見たことのない炊飯器<br />
				「バーミキュラ ライスポット」が、あなたの暮らしを変えていきます。
			</p>


		</div><!-- /#catch_copy -->


		<section id="section01" class="clearfix">
			<div class="video_wrap">
				<div id="whats_video"></div>
				<img class="hover" src="../img/concept/section01_img01.jpg" height="353" width="629" alt="Vermicular　RICEPOT" />
			</div>
		</section>

		<section id="section02" class="clearfix">
			<h1><img src="../img/concept/section02_title01.png" height="32" width="636" alt="もっと、ご飯を味わおう。もっと、ご飯を楽しもう。" /></h1>
			<p class="text center">お米の味をしっかり引き出すから、ご飯を使った料理が味わったことのないおいしさに。<br />
				しかも、調理が本当に簡単。ご飯のある暮らしが、きっと変わります。
			</p>
			<ul>
				<li>
					<div class="inner">
						<div class="cont_wrap">
							<h2><img src="../img/concept/section02_copy01.png" height="25" width="190" alt="お米本来の香り。" /></h2>
							<p>
								ライスポットで炊いたご飯は、<br />
								お米本来の持つ良い香りが引き出されます。<br />
								まずはシンプルに、ご飯のお供と一緒に<br />
								香りごとご飯をいただきましょう。
							</p>
						</div>
					</div>
				</li>
				<li>
					<div class="inner">
						<div class="cont_wrap">
							<h2><img src="../img/concept/section02_copy02.png" height="25" width="219" alt="いつまでも続く甘味。" /></h2>
							<p>
								「冷めた方が、おいしいかもしれない。」 ライスポットで<br />
								炊いたご飯は、冷めるとさらに甘味が感じられ、<br />
								炊き立てとは違うおいしさがはじまります。<br />
								明日から、毎日のお弁当が楽しくなります。
							</p>
						</div>
					</div>
				</li>
				<li>
					<div class="inner">
						<div class="cont_wrap">
							<h2><img src="../img/concept/section02_copy03.png" height="26" width="245" alt="素材の味が引き立つ。" /></h2>
							<p>
								お寿司にすれば、ネタの素材の味を<br />
								最大限に引き立てながら、<br />
								ご飯のおいしさも味わえます。<br />
								親しい人たちを呼んで、<br />
								寿司パーティーだってカンタンです。
							</p>
						</div>
					</div>
				</li>
				<li>
					<div class="inner">
						<div class="cont_wrap">
							<h2><img src="../img/concept/section02_copy04.png" height="25" width="239" alt="お店のようなおこげも。" /></h2>
							<p>
								ご家庭では難しかった、カリッと香ばしい<br />
								お店のようなおこげだって<br />
								ライスポットだったら簡単。<br />
								いつものビビンバが、<br />
								お店では味わえないおいしさに。
							</p>
						</div>
					</div>
				</li>
				<li>
					<div class="inner">
						<div class="cont_wrap">
							<h2><img src="../img/concept/section02_copy05.png" height="25" width="352" alt="あらゆる具材の炊き込みご飯も。" /></h2>
							<p>
								素材の味を引き出す
								バーミキュラの鍋だから、季節の旬の素材と<br />
								一緒にご飯を炊けば、旨味が凝縮された<br />
								あらゆる具材の炊き込みご飯が楽しめます。
							</p>
						</div>
					</div>
				</li>
				<li>
					<div class="inner">
						<div class="cont_wrap">
							<h2><img src="../img/concept/section02_copy06.png" height="25" width="287" alt="一粒一粒が際立つ食感。" /></h2>
							<p>
								ライスポットで炊いたご飯は、<br />
								どんな料理でも一粒一粒の食感が際立ちます。<br />
								だから、洋風料理だって、まるで新しい<br />
								メニューのようなおいしさです。
							</p>
						</div>
					</div>
				</li>
				<li>
					<div class="inner">
						<div class="cont_wrap">
							<h2><img src="../img/concept/section02_copy07.png" height="24" width="330" alt="世界のお米料理もカンタンに。" /></h2>
							<p>
								世界には、お米を使った料理がたくさんあります。<br />
								今まで挑戦できなかった料理だって<br />
								ライスポットならカンタンでおいしくできます。<br />
								好きなスタイルでお米をもっと楽しみましょう。
							</p>
						</div>
					</div>
				</li>
				<li>
					<div class="inner">
						<div class="cont_wrap">
							<h2><img src="../img/concept/section02_copy08.png" height="25" width="159" alt="優しいおかゆ。" /></h2>
							<p>
								何かおなかに優しいものが食べたいとき。<br />
								そんなときだってライスポットで<br />
								おかゆをつくれば、カラダに優しく、<br />
								しかもおいしく過ごせます。
							</p>
						</div>
					</div>
				</li>
			</ul>
		</section>

		<div id="section03" class="clearfix">
			<h2>
				<img src="../img/concept/section03_title01.png" height="65" width="609" alt="鍋炊きのご飯ならではの、香ばしいおこげも。旨味たっぷりの玄米も。あらゆる具材の炊き込みご飯も。ライスポットにおまかせです。">
			</h2>
			<ul class="clearfix">
				<li><img src="../img/concept/section03_img01.jpg" height="230" width="340" alt=""></li>
				<li><img src="../img/concept/section03_img02.jpg" height="230" width="340" alt=""></li>
				<li><img src="../img/concept/section03_img03.jpg" height="230" width="340" alt=""></li>
			</ul>
			<div class="recipe_area">
				<div id="book" class="clearfix">
					<h3><img src="../img/cookingmode/section_copy05.png" height="21" width="498" alt="ライスポット生活を楽しくする、レシピブックさしあげます。" /></h3>
					<p>購入すると、もれなくついてくるオリジナルのレシピブック。ライスポットで炊いたご飯を楽しむ<br/>
						レシピから、本格的な調理レシピまで、全100品の充実した一冊です。（ハードカバー144P ）</p>
					<div class="image"><img src="../img/cookingmode/section_img05.png" height="183" width="130" alt="ライスポット生活を楽しくする、レシピブックさしあげます。" /></div>
				</div>
				<div class="btn_area">
					<div class="btn clear_black">
							<a href="../img/pdf/ricepot_recipebook.pdf" target=brank><span class="btn_inner"><img src="../img/cookingmode/btn_link_recipe.png" width="154" height="13" alt="レシピブックを見る"></span></a>
					</div>
				</div>
			</div>
		</div>

		
		
		<div id="section05">
			<section id="pare_copy01" class="clearfix">
				<div class="cont_wrap">
					<h3><img src="../img/concept/section01_copy03.png" height="73" width="267" alt="あなたの指を、光で導くスマートタッチキー" /></h3>
					<p>必要なボタンだけが光ることで次の操作を導くスマートタッチキーは、とにかく簡単。よく使う「炊き上がりタイマー」の設定も迷いません。</p>
				</div>
				<div class="image"><img src="../img/concept/section01_img02.jpg" height="390" width="580" alt="あなたの指を、光で導くスマートタッチキー" /></div>
			</section>
			<section id="pare_copy02" class="clearfix">
				<div class="image"><img src="../img/concept/section01_img03.jpg" height="390" width="580" alt="誰にも似てない。だから、どんな食卓にも、よく似合う。" /></div>
				<div class="cont_wrap">
					<h3><img src="../img/concept/section01_copy04.png" height="72" width="390" alt="誰にも似てない。だから、どんな食卓にも、よく似合う。" /></h3>
					<p>
						ごはんが炊けた後は、鍋をおひつのように食卓のまん中へ。<br />
						落ち着いた鋳物の存在感が、どんな料理にもよく馴染みます。
					</p>
				</div>
			</section>
		</div>

				
				<a id="online_btn" href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank">
	<img src="../common/img/btn_online.png" width="130" height="130" alt="Online Shop">
</a>

				
				
				
			
<div class="btn clear_black w198 center "><a href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../img/products/top/onlineshop_text.png" height="13" width="101" alt="オンラインショップ"></span></span></a></div></br>
<?php
	include '../_footer_rp.php';
	?>