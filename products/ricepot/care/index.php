﻿<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ja" class="ie ie6" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 7]>   <html lang="ja" class="ie ie7" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 8]>   <html lang="ja" class="ie ie8" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if IE 9]>   <html lang="ja" class="ie ie9" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="ja" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">


<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1280">
<link rel="canonical" href="http://www.vermicular.jp/products/ricepot/care">
<title>使い方・手入れ | Vermicular（バーミキュラ）ライスポット</title>
<meta name="author" content="愛知ドビー株式会社">
<meta name="description" content="操作はシンプルに。お手入れはカンタンに。">
<meta name="keywords" content="使い方,手入れ,操作はシンプル,お手入れはカンタン,メンテナンスもカンタン,衛生的,オリジナル計量カップ,スマートタッチキー,シンプルな構造,一体構造,炊飯モード,調理モード,火加減モード,ライスポット,炊飯鍋,炊飯器,バーミキュラ,Vermicular,ricepot,鋳物,愛知ドビー">

<meta property="og:title" content="使い方・手入れ | Vermicular（バーミキュラ）ライスポット" />
<meta property="og:type" content="website">
<meta property="og:url" content="http://www.vermicular.jp/products/ricepot/care/index.php " />
<meta property="og:image" content="http://www.vermicular.jp/products/ricepot/common/img/ogp.jpg" />
<meta property="og:site_name" content="使い方・手入れ | Vermicular（バーミキュラ）ライスポット" />
<meta property="og:description" content="操作はシンプルに。お手入れはカンタンに。" />
<meta property="fb:app_id" content="171078026685763" />

<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="使い方・手入れ | Vermicular（バーミキュラ）ライスポット">
<meta name="twitter:title" content="使い方・手入れ | Vermicular（バーミキュラ）ライスポット">
<meta name="twitter:url" content="/vermicular/products/ricepot/care/index.php ">
<meta name="twitter:description" content="操作はシンプルに。お手入れはカンタンに。">

<meta name="format-detection" content="telephone=no,address=no,email=no">

<link rel="stylesheet" type="text/css" href="../common/css/normalize.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/base_nw.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/styles_nw.css?20161101">
<link rel="stylesheet" type="text/css" href="../common/css/lowlayer.css?20161101">


<link rel="stylesheet" type="text/css" href="../css/care_nw.css">

<link href="../common/img/apple-touch-icon.png" rel="apple-touch-icon">
<meta name="apple-mobile-web-app-title" content="Vermicular">

<script src="../common/js/jquery-1.11.0.min.js?20161101"></script>
<script src="../common/js/jquery.transit.js?20161101"></script>
<script src="../common/js/pace.min.js?20161101"></script>
<script src="../common/js/jquery.colorbox-min.js?20161101"></script>
<script src="../common/js/var.js?20161101"></script>
<script src="../common/js/function.js?20161101"></script>
<script src="../common/js/base.js?20161101"></script>
<script src="../common/js/jquery.bxslider.min.js?20161101"></script>

<script src="../common/js/scripts_nw.js"></script>
<script src="..//js/care.js"></script>

<!--[if lt IE 9]>
<script src="../common/js/html5shiv.js"></script>
<script type="text/javascript" src="../common/js/jquery.belatedPNG.min.js"></script>
<script>
$(function() {
	$(".pngfix").fixPng();
});
</script>
<script src="../common/js/jquery.backgroundSize.js"></script>
<script>
$(function(){
 $('#header #bg').css('background-size', 'cover');
});
</script>
<![endif]-->
</head>
<body id="rp_care">
<div id="document">
	
	<header id="rpr_header">
		<div id="rpr_header_inner" class="clearfix">
			<h1 id="rpr_site_logo"><a href="http://www.vermicular.jp/" class="hover"><img src="../common/img/site_logo.png" height="27" width="143" alt="VERMICULAR（バーミキュラ） MADE IN JAPAN"></a></h1>
			<nav id="rpr_gnavi">
				<ul class="clearfix">
					

					

					
					<li class="list"><a href="../concept/" class="hover"><img src="../common/img/n_gm/gnavi01.png" width="65" height="17" alt="コンセプト"></a></li>
					
					

					
					<li class="list">
					<a class="hover"><img src="../common/img/n_gm/gnavi03.png" width="75" height="17" alt="選ばれる理由"></a>
					<ul class="dd_list">
						<li class="list01"><a href="../technology/">テクノロジー</a></li>
						<li class="list02"><a href="../philosophy/">デザイン思想</a></li>
						<li class="list03"><a href="../cookingmode/">調理機能</a></li>
						<li class="list04"><a href="../safe/">安心・安全</a></li>
						<li class="list05"><a href="../accessories/">付属品</a></li>
					</ul>
					</li>


					<li class="list"><a href="../care/" class="hover"><img src="../common/img/n_gm/gnavi04.png" width="90" height="17" alt="使い方・お手入れ"></a></li>


					<li class="list">
					<span><a class="hover"><img src="../common/img/n_gm/gnavi05.png" width="90" height="17" alt="スペック・FAQ"></a></span>
					<ul class="dd_list">
						<li class="list01"><a href="../spec/">スペック</a></li>
						<li class="list02"><a href="../faq/">FAQ</a></li>
					</ul>
					</li>



					<li class="list"><a href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank"  class="hover"><img src="../common/img/n_gm/gnavi06.png" width="105" height="17" alt="オンラインショップ"></a></li> 	




				</ul>
				</nav>



			<nav id="rp_sns">
			<div class="btn"  class="hover"><img src="../common/img/head_sns_icon_btn.png" height="14" width="10" alt="SNS"></div>
			<ul class="sns_list clearfix">
				<li class="sns01"><a href="https://www.facebook.com/vermicular" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_01.png" height="17" width="7" alt="Facebook"></a></li>
				<li class="sns02"><a href="https://twitter.com/_VERMICULAR_" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_02.png" height="11" width="14" alt="Twitter"></a></li>
				<li class="sns03"><a href="https://instagram.com/vermicular_japan/" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_03.png" height="13" width="13" alt="Instagram"></a></li>
				<li class="sns04"><a href="http://shop.vermicular.jp/mail/" target="_blank"  class="hover"><img src="../common/img/head_sns_icon_04.png" height="10" width="15" alt="Mail"></a></li>
			</ul>
			</nav>
			
			
		</div>
	</header><!-- /#header -->

	
	<main id="main_wrap">		<header id="rpr_page_title_wrap">
			<div class="inner">
				<h1 id="page_title"><img src="../img/care/page_title.png" height="58" width="253" alt="使い方・お手入れ" /></h1>
			</div>
		</header><!-- /#kv_wrap -->
		<div id="catch_copy">
			<h2 class="catch"><img src="../img/care/page_copy.png" height="36" width="668" alt="操作はシンプルに、お手入れは簡単に。" /></h2>
			<p class="text">
				白米・玄米・おこげ・おかゆが自由自在。温度と時間の管理はおまかせ。<br />
				メンテナンスもカンタンだから、いつも衛生的です。
			</p>
		</div><!-- /#catch_copy -->


		<section id="section01" class="clearfix">
			<header>
				<h1><img src="../img/care/section01_copy01.png" height="32" width="502" alt="炊飯はいつもの炊飯器と変わりません。" /></h1>
				<p>
					お米用・水用のオリジナル計量カップを使うだけ。<br />
					特別なことをしなくても、おいしく炊けます。
				</p>
			</header>
			<div class="inner">
				<ul class="clearfix">
					<li>
						<h2><img src="../img/care/section01_img01.png" height="246" width="192" alt="計りやすい専用計量カップ" /></h2>
						<p>付属の米計量カップを使用し、すりきりで正確に米の量を測る。</p>
					</li>
					<li>
						<h2><img src="../img/care/section01_img02.png" height="246" width="192" alt="手早く米を研ぐ" /></h2>
						<p>米をつぶさないように<br />3本指で優しく洗う。</p>
					</li>
					<li>
						<h2><img src="../img/care/section01_img03.png" height="246" width="192" alt="ゆっくり内釜に米を入れる" /></h2>
						<p>ザルをあげ、しっかりと水気を切り、米をポットに入れる。</p>
					</li>
					<li>
						<h2><img src="../img/care/section01_img04.png" height="246" width="192" alt="ワンタッチで炊飯ステート" /></h2>
						<p>水計量カップを使用し、水量を正確に測ってポットに入れる。</p>
					</li>
					<li>
						<h2><img src="../img/care/section01_img05.png" height="246" width="192" alt="ワンタッチで炊飯ステート" /></h2>
						<p>炊き上がり時間を設定したら、「START」を押す。</p>
					</li>
				</ul>
			</div>
		</section>

		<section id="section-care" class="clearfix">
			<header>
				<h1><img src="../img/care/section01_copy02.png" height="34" width="603" alt="シンプルな構造だからお手入れがカンタン。" /></h1>
				<p>
					バーミキュラ ライスポットは構造をシンプルにすることで、<br />
					毎日のお手入れをカンタンにするデザインを目指しました。
				</p>
			</header>
			<div class="inner">
				<div class="cont_wrap clearfix">
				<div class="left">
					<h2><img src="../img/care/section-care_copy02.png" height="351" width="520" alt="継ぎ目のない一体構造" /></h2>
					<p>すみずみまで洗えるから、衛生的。いつでも安心して料理ができます。</p>
				</div>
				<div class="right">
					<h2><img src="../img/care/section-care_copy03.png" height="351" width="520" alt="お手入れカンタン、衛生的。" /></h2>
					<p class="text_wide">一般的な炊飯器のフタにある「ゴムパッキン」や<br />「細かい金属部品」が無いため、メンテナンスが簡単です。</p>
				</div>
			</div>
			</div>
		</section>
		<section id="section02" class="clearfix">
			<header>
				<div class="inner">
					<div class="cont_wrap">
						<h1><img src="../img/care/section02_copy01.png" height="83" width="353" alt="もう、操作で迷わない。スマートタッチキー採用。" /></h1>
						<p>
							ボタンが光って指先を案内してくれるから、<br />
							炊き上がりタイマーの設定も迷いません。
						</p>
					</div>
				</div>
			</header>
			<div class="mode_control">
				<h2><img src="../img/care/section02_copy02.png" height="32" width="166" alt="炊飯モードの機能" /></h2>
				<div class="cont_wrap clearfix">
					<ul class="tabs clearfix">
						<li class="btn_change left active" id="rice"><a href="#mode01"><img src="../img/care/section02_copy03.png" height="45" width="182" alt=""></a></li>
						<li class="btn_change right" id="cook"><a href="#mode02"><img src="../img/care/section02_copy04.png" height="45" width="180" alt=""></a></li>
					</ul>
				</div>
			</div>
			<section id="mode01" class="tab_content">
				<div class="img_caption01">
					<img src="../img/care/section02_subcopy01.png" alt="3つの機能">
				</div>
				<div class="rice_mode01 fade_img">
					<ul class="rice_list clearfix">
						<li>
							<dt>
								<dl><img src="../img/care/rice_mode_title01.png" height="44" width="238" alt="1.炊き分けモード"></dl>
								<dd>白米 / 玄米</dd>
							</dt>
						</li>
						<li>
							<dt>
								<dl><img src="../img/care/rice_mode_title02.png" height="44" width="238" alt="1.炊き分けモード"></dl>
								<dd>ふつう / おこげ / おかゆ</dd>
							</dt>
						</li>
						<li>
							<dt>
								<dl><img src="../img/care/rice_mode_title03.png" height="44" width="238" alt="1.炊き分けモード"></dl>
								<dd>指定した時刻に炊けるように</br>
									タイマーをセットできます
								</dd>
							</dt>
						</li>
					</ul>
				</div>
				<div class="img_caption02">
					<img src="../img/care/section02_subcopy02.png" alt="3ステップのカンタン操作">
				</div>
				<div class="rice_mode02 fade_img">
					<ul class="rice_list02 clearfix">
						<li>
							<dt>
								<dl><img src="../img/care/rice_mode_image01.png" height="236" width="212" alt="1.炊飯コース・量を設定"></dl>
								<dd>炊飯コース・炊飯量を設定</br>
									お米の種類と炊き方を選択</dd>
							</dt>
						</li>
						<li>
							<dt>
								<dl><img src="../img/care/rice_mode_image02.png" height="236" width="212" alt="2.炊き上がり時刻を設定"></dl>
								<dd>炊き上がり時刻を矢印、あるいは</br>
									プリセットタイマーボタンで設定</dd>
							</dt>
						</li>
						<li>
							<dt>
								<dl><img src="../img/care/rice_mode_image03.png" height="236" width="212" alt="1.炊き分けモード"></dl>
								<dd>炊き上がり時刻を設定したら</br>
									『START』を押す
								</dd>
							</dt>
						</li>
					</ul>
					
					<div class="center"><img src="../img/care/section02_txt.png" height="16" width="378" alt="詳しい炊飯・調理方法は取扱説明書をご覧ください。"></div>
			<div class="dl_btn btn clear_black">
				<a href="../img/pdf/ricepot_manual.pdf" target=brank>
					<span class="btn_inner">
						<img src="../img/care/dl_btn.png" height="15" width="157" alt="取扱説明書を見る">
					</span>
				</a>
			</div>
					
					
					
					
				</div>
			</section><!-- rice-mode -->
			<section id="mode02" class="tab_content">
				<div class="img_caption01">
					<img src="../img/care/section02_subcopy01.png" alt="3つの機能">
				</div>
				<div class="cook_mode01 fade_img">
					<ul class="cook_list clearfix">
						<li>
							<dt>
								<dl><img src="../img/care/cook_mode_title01.png" height="44" width="238" alt="1.火加減モード"></dl>
								<dd>中火 / 弱火 / 極弱火 / 保温</dd>
							</dt>
						</li>
						<li>
							<dt>
								<dl><img src="../img/care/cook_mode_title02.png" height="44" width="238" alt="2.保温機能"></dl>
								<dd>30〜95℃まで設定可能</dd>
							</dt>
						</li>
						<li>
							<dt>
								<dl><img src="../img/care/cook_mode_title03.png" height="44" width="238" alt="3.タイマー機能"></dl>
								<dd>指定した時間に加熱・保温を停止する</br>
									ようにタイマーをセットできます
								</dd>
							</dt>
						</li>
					</ul>
				</div>
				<div class="img_caption02">
					<img src="../img/care/section02_subcopy02.png" alt="3ステップのカンタン操作">
				</div>
				<div class="cook_mode02 fade_img">
					<ul class="cook_list02 clearfix">
						<li>
							<dt>
								<dl><img src="../img/care/cook_mode_image01.png" height="236" width="212" alt="火力を設定"></dl>
								<dd>バーミキュラが必要とする「3つ</br>の火加減」または「保温」を選択</dd>
							</dt>
						</li>
						<li>
							<dt>
								<dl><img src="../img/care/cook_mode_image02.png" height="236" width="212" alt="タイマー切を選択"></dl>
								<dd>加熱・保温の停止までの時間、</br>
									あるいは保温の温度を矢印で設定</dd>
							</dt>
						</li>
						<li>
							<dt>
								<dl><img src="../img/care/cook_mode_image03.png" height="236" width="212" alt="調理または保温"></dl>
								<dd>お料理にあった火加減で調理、</br>
									お好みの温度で保温開始
								</dd>
							</dt>
						</li>
					</ul>
					
					
									<div class="center"><img src="../img/care/section02_txt.png" height="16" width="378" alt="詳しい炊飯・調理方法は取扱説明書をご覧ください。"></div>
			<div class="dl_btn btn clear_black">
				<a href="../img/pdf/ricepot_manual.pdf" target=brank>
					<span class="btn_inner">
						<img src="../img/care/dl_btn.png" height="15" width="157" alt="取扱説明書を見る">
					</span>
				</a>
			</div>
					
					
					
					
				</div>
				

				
				
				
			</section>
			
		</section>
		
		
		<a id="online_btn" href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank">
	<img src="../common/img/btn_online.png" width="130" height="130" alt="Online Shop">
</a>

		
						
			
<div class="btn clear_black w198 center "><a href="http://shop.vermicular.jp/jp/group.php?id=36" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../img/products/top/onlineshop_text.png" height="13" width="101" alt="オンラインショップ"></span></span></a></div></br>
<?php
	include '../_footer_rp.php';
	?>