<?php
$page_id = 'rpr_top'; //ページ独自にスタイルを指定する場合は入力
$page_type = 'oven_pot_round'; // [ オーブンポッドラウンドのページの場合 「oven_pot_round」 ]
$page_title = '目指したのは、世界一、素材本来の味を引き出す鍋 | Vermicular（バーミキュラ）OvenPotRound'; //ページ名
$page_description = '日本の手仕事とテクノロジーで、あなたの暮らしを変えていく鍋、Vermicular（バーミキュラ）OvenPotRound。バーミキュラが選ばれるその理由をご紹介します。';
$page_keywords = 'バーミキュラ,Vermicular,ホーロー鍋,愛知ドビー,オーブンポットラウンド';
$page_pass = '../'; //自分の階層を指定
$page_directry = 'products/'; //自分のディレクトリ名を指定
$extra_css = '
<link rel="stylesheet" type="text/css" href="'.$page_pass.'css/products.css">
'; //何か追加で読み込みたいCSSがあればlinkタグごと記述
$extra_js = '
<script src="'.$page_pass.'js/products.js"></script>
'; //何か追加で読み込みたいJSがあればscriptタグごと記述
include $page_pass.'_header.php'; ?>
		<header id="kv_wrap">
			<div id="kv_inner">
				<h1 class="product_name motion">
					<span class="en"><img src="../img/products/top/kv_product_name_en.png" height="31" width="311" alt="Oven Pot Round"></span>
					<span class="jp"><img src="../img/products/top/kv_product_name_jp.png" height="12" width="122" alt="オーブンポットラウンド"></span>
				</h1>
				<h2 class="copy">
					<span class="first_motion"><img src="../img/products/top/kv_copy01.png" height="37" width="40" alt="世"></span>
					<span class="first_motion"><img src="../img/products/top/kv_copy02.png" height="37" width="38" alt="界"></span>
					<span class="first_motion"><img src="../img/products/top/kv_copy03.png" height="37" width="38" alt="一"></span>
					<span class="first_motion"><img src="../img/products/top/kv_copy04.png" height="37" width="27" alt="、"></span>
					<span class="second_motion"><img src="../img/products/top/kv_copy05.png" height="37" width="36" alt="素"></span>
					<span class="second_motion"><img src="../img/products/top/kv_copy06.png" height="37" width="39" alt="材"></span>
					<span class="second_motion"><img src="../img/products/top/kv_copy07.png" height="37" width="42" alt="本"></span>
					<span class="second_motion"><img src="../img/products/top/kv_copy08.png" height="37" width="39" alt="来"></span>
					<span class="second_motion"><img src="../img/products/top/kv_copy09.png" height="37" width="35" alt="の"></span>
					<span class="second_motion"><img src="../img/products/top/kv_copy10.png" height="37" width="41" alt="味"></span>
					<span class="second_motion"><img src="../img/products/top/kv_copy11.png" height="37" width="31" alt="を"></span>
					<span class="second_motion"><img src="../img/products/top/kv_copy12.png" height="37" width="38" alt="引"></span>
					<span class="second_motion"><img src="../img/products/top/kv_copy13.png" height="37" width="29" alt="き"></span>
					<span class="second_motion"><img src="../img/products/top/kv_copy14.png" height="37" width="41" alt="出"></span>
					<span class="second_motion"><img src="../img/products/top/kv_copy15.png" height="37" width="36" alt="す"></span>
					<span class="second_motion"><img src="../img/products/top/kv_copy16.png" height="37" width="38" alt="鍋"></span>
				</h2>
			</div>
			<div id="scroll_down">
				<div class="text"><img src="../img/products/common/scroll_down_text.png" height="11" width="36" alt="Scroll"></div>
				<div class="arrow"><img src="../img/products/common/scroll_down_arrow.png" height="18" width="36" alt=""></div>
			</div>
		</header><!-- /#kv_wrap -->
		<section id="product_lineup">
			<div class="inner">
				<h1 class="title"><img src="../img/products/top/lineup_title.png" height="26" width="284" alt=""></h1>
				<p class="text">究極の無水調理で素材本来の旨味を引き出す、鋳物ホーロー鍋のラインナップをご紹介します。</p>

				<div class="lineup_body">
					<ul class="variation_wrap clearfix">
						<li>
							<p class="photo"><img src="../img/products/top/product_img01.png" height="320" width="229" alt=""></p>
							<p class="name"><span><img src="../img/products/top/product_name01_comingsoon.png" height="41" width="229" alt=""></span></p>
						</li>
						<li>
							<p class="photo"><img src="../img/products/top/product_img02.png" height="320" width="154" alt=""></p>
							<p class="name"><span><img src="../img/products/top/product_name02_comingsoon.png" height="41" width="154" alt=""></span></p>
						</li>
						<li class="hover">
							<p class="photo"><img src="../img/products/top/product_img03.png" height="320" width="190" alt=""></p>
							<p class="name"><a href="http://shop.vermicular.jp/jp/group.php?id=9" target="_blank"><img src="../img/products/top/product_name03.png" height="15" width="190" alt=""></a></p>
						</li>
						<li class="hover">
							<p class="photo"><img src="../img/products/top/product_img04.png" height="320" width="214" alt=""></p>
							<p class="name"><a href="http://shop.vermicular.jp/jp/group.php?id=3" target="_blank"><img src="../img/products/top/product_name04.png" height="15" width="214" alt=""></a></p>
						</li>
						<li>
							<p class="photo"><img src="../img/products/top/product_img05.png" height="320" width="241" alt=""></p>
							<p class="name"><span><img src="../img/products/top/product_name05_comingsoon.png" height="41" width="241" alt=""></span></p>
						</li>
					</ul>
				</div><!-- /.lineup_body -->

				<div class="btn clear_black w198 center"><a href="http://shop.vermicular.jp/jp/group.php?id=19" target="_blank"><span class="btn_inner"><span class="ex_link black"><img src="../img/products/top/onlineshop_text.png" height="13" width="99" alt="オンラインショップ"></span></span></a></div>

			</div>
		</section><!-- /#product_lineup -->

		<section id="section_wrap01" class="bg_section hover box_link">
			<div class="inner clearfix">
				<div class="text_wrap">
					<h1 class="copy"><img src="../img/products/top/section_copy01.png" height="30" width="222" alt="暮らしを変える鍋。"></h1>
					<p class="text">「息子がブロッコリーを食べた」「調味料を使わなくなった」<br>
						「外食しなくなりました」「誰かといっしょに食べたくなる」<br>
						そう。バーミキュラは、その機能性の高さで、<br>
						あなたの暮らしを変えていく鍋なのです。</p>
					<p class="link"><a href="<?php echo $page_pass; ?>about/whatis/"><img src="../img/products/common/read_more_link_text.png" height="10" width="78" alt="Read More"></a></p>
				</div>
			</div>
		</section><!-- /#section_wrap01 -->

		<section id="section_wrap02" class="bg_section">
			<div class="inner clearfix">
				<div class="text_wrap">
					<h1 class="copy"><img src="../img/products/top/section_copy02.png" height="30" width="270" alt="手仕事とテクノロジー。"></h1>
					<p class="text">フタと本体の接合部分を日本の職人が手作業で0.01mmの精度で<br>
						削り出し、考え抜かれた熱伝達のテクノロジーを重ねることで、<br>
						究極の無水調理が実現しました。<br>
						すべては、素材のうま味を引き出すためです。</p>
					<p class="link"><a href="<?php echo $page_pass; ?>about/teshigoto/"><img src="../img/products/common/read_more_link_text.png" height="10" width="78" alt="Read More"></a></p>
				</div>
			</div>
		</section><!-- /#section_wrap02 -->

		<section id="section_wrap03" class="bg_section">
			<div class="inner clearfix">
				<div class="text_wrap">
					<h1 class="copy"><img src="../img/products/top/section_copy03.png" height="70" width="251" alt="レシピも妥協しない。それが、バーミキュラ。"></h1>
					<p class="text">オリジナルレシピブックやバーミキュラホームページの<br>
						レシピコーナーを開くと、きっとびっくりすると思います。<br>
						ひとつの鍋で？鍋に任せっぱなし？あの調味料は使わないの？<br>
						はい、開発した私たちも驚いたくらいです。<br>
						心を込めるって大変、という手料理のイメージを変えるのも、<br>
						バーミキュラの仕事です。</p>
					<p class="link"><a href="https://owners.vermicular.jp/"><img src="../img/products/common/read_more_link_text.png" height="10" width="78" alt="Read More"></a></p>
				</div>
			</div>
		</section><!-- /#section_wrap03 -->

		<section id="section_wrap04" class="bg_section">
			<div class="inner clearfix">
				<div class="text_wrap">
					<h1 class="copy"><img src="../img/products/top/section_copy04.png" height="70" width="334" alt="注文から、レシピ、修理まで、一生サポート。"></h1>
					<p class="text">バーミキュラを、毎日使ってほしいから。もっと楽しんでほしいから。<br>
						もっと安全に使ってほしいから。お客様の、あらゆる悩みにお答えします。<br>
						コンシェルジュの仕事に、マニュアルはありません。<br>
						購入前のご相談から、注文、調理、修理まで、バーミキュラにまつわるすべてを、<br>
						一生にわたってサポートすることを約束します。</p>
					<p class="link"><a href="<?php echo $page_pass; ?>support/"><img src="../img/products/common/read_more_link_text.png" height="10" width="78" alt="Read More"></a></p>
				</div>
			</div>
		</section><!-- /#section_wrap04 -->

		<section id="section_wrap05" class="bg_section">
			<div class="inner clearfix">
				<div class="text_wrap">
					<h1 class="copy"><img src="../img/products/top/section_copy05.png" height="30" width="272" alt="もっと美味しくする道具。"></h1>
					<p class="text">素材からこだわったキッチンツールが、<br>
						バーミキュラの料理を、もっとおいしく、<br>
						もっと楽しくしてくれます。</p>
					<p class="link"><a href="<?php echo $page_pass; ?>products/kitchenitems/index.html"><img src="../img/products/common/read_more_link_text.png" height="10" width="78" alt="Read More"></a></p>
				</div>
			</div>
		</section><!-- /#section_wrap05 -->

		<?php include $page_pass.'_lowlayer_banner.php'; ?>

<?php include $page_pass.'_footer.php'; ?>