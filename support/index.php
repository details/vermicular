<?php
$page_id = 'rpr_support'; //ページ独自にスタイルを指定する場合は入力
$page_type = 'oven_pot_round'; // [ オーブンポッドラウンドのページの場合 「oven_pot_round」 ]
$page_title = 'サポート | Vermicular（バーミキュラ）OvenPotRound'; //ページ名
$page_description = 'バーミキュラを、毎日使ってほしいから。もっと楽しんでほしいから。お客様のあらゆる悩みにお答えします。コンシェルジェの仕事に、マニュアルはありません。購入前のご相談から、注文、調理、修理まで、バーミキュラにまつわるすべてを、一生にわたってサポートすることを約束します。';
$page_keywords = 'バーミキュラ,Vermicular,ホーロー鍋,愛知ドビー,サポート';
$page_pass = '../'; //自分の階層を指定
$page_directry = 'support/'; //自分のディレクトリ名を指定
$extra_css = '
<link rel="stylesheet" type="text/css" href="'.$page_pass.'css/products.css">
'; //何か追加で読み込みたいCSSがあればlinkタグごと記述
$extra_js = '
<script src="'.$page_pass.'common/js/lib.js"></script>
'; //何か追加で読み込みたいJSがあればscriptタグごと記述
include $page_pass.'_header.php'; ?>
		<header id="rpr_page_title_wrap">
			<div class="inner">
				<h1 id="page_title"><img src="../img/support/page_title.png" height="38" width="690" alt="注文から、レシピ、修理まで、一生サポート。"></h1>
				<p class="text">バーミキュラを、毎日使ってほしいから。もっと楽しんでほしいから。もっと安全に使ってほしいから。お客様の、あらゆる悩みにお答えします。<br>
				コンシェルジュの仕事に、マニュアルはありません。購入前のご相談から、注文、調理、修理まで、<br>
				バーミキュラにまつわるすべてを、一生にわたってサポートすることを約束します。</p>
			</div>
		</header><!-- /#kv_wrap -->

		<section id="section01" class="rpr_section_wrap inner_min clearfix">
			<div class="cont_wrap flr">
				<h1 class="rpr_section_title"><img src="../img/support/section_title01-1.png" height="15" width="129" alt="オーナーズデスク"></h1>
				<p class="title"><img src="../img/support/section_title01-2.png" alt="コンシェルジュの仕事に、マニュアルはありません。" width="282" height="70"></p>
				<p class="text">購入前のご相談から、注文、使い方、調理方法の疑問、修理など、どんなことでもお気軽にご連絡ください。バーミキュラのコンシェルジェがサポートいたします。</p>
				<div class="policy_wrap">
					<h2><img src="../img/support/section_title01-3.png" width="321" height="20" alt="バーミキュラオーナーズデスクの方針"></h2>
					<ul>
						<li><span>常にお客様の側に立ち、自分・会社の利益を超えて、お客様の満足を最優先させます。</span></li>
						<li><span>常にバーミキュラの一番の使い手となり、ご注文からご使用の一生にわたってサポートし続けます。</span></li>
						<li><span>常にお客様の状況を想像し、心のこもったおもてなしを目指します。</span></li>
						<li><span>バーミキュラブランドで世界の家庭にひとつでも笑顔を増やすことを目指します。</span></li>
					</ul>
				</div>
				<dl class="contact_wrap bg_gray">
					<dt>オーナーズデスク</dt>
					<dd>
						<span><img src="../common/img/icon_freedial.png" width="32" height="18" alt="freedial"><span class="tel_text">0120-766-787</span></span><br>
						<span class="month">（月～金 9:00-12:00、13:00-17:00）</span>
					</dd>
				</dl>
			</div>
			<p class="photo_wrap fll"><img src="../img/support/section_img01.jpg" height="628" width="580" alt="オーナーズデスク イメージ写真"></p>
		</section>

		<section id="section02" class="rpr_section_wrap inner_min clearfix">
			<div class="cont_wrap fll">
				<h1 class="rpr_section_title"><img src="../img/support/section_title02-1.png" height="18" width="308" alt="リペア（再ホーローコーティング）サービス"></h1>
				<p class="title"><img src="../img/support/section_title02-2.png" alt="一生使える鍋だから、一生お直しいたします。" width="265" height="75"></p>
				<p class="text">どんな調理もこなすホーロー鍋は、毎日使えます。しかし、使い込めば傷みます。ホーローも少しずつ剥げてきます。覚えておいてください。バーミキュラは、何度でも修理できることを。古いホーローを全て剥がして再ホーローコーティングして、新品同様にしてお返しします。新しい色に変えて気分も変えられるお客様も、いらっしゃいます。</p>
				<div class="btn clear_black w218">
					<a href="http://shop.vermicular.jp/jp/free.php?id=13"><span class="btn_inner"><img src="../img/support/btn_section01.png" width="171" height="14" alt="リペアサービスの詳細はこちら"></span></a>
				</div>
			</div>
			<p class="photo_wrap flr"><img src="../img/support/section_img02.jpg" height="390" width="580" alt="リペア（再ホーローコーティング）サービス イメージ写真"></p>
		</section>

		<section id="section03" class="rpr_section_wrap inner_min clearfix">
			<div class="cont_wrap flr">
				<h1 class="rpr_section_title"><img src="../img/support/section_title03-1.png" height="18" width="289" alt="スペシャルオーダーネーミングサービス"></h1>
				<p class="title"><img src="../img/support/section_title03-2.png" alt="世界にひとつ、あなただけのバーミキュラ。" width="311" height="75"></p>
				<p class="text">メッセージや名前を型から削り出して、鋳物の文字を作る。世界にひとつしかないバーミキュラをオーダーできるサービスです。大切なひとへのプレゼントや、あなただけのオリジナルのバーミキュラをお持ちになりたい方へ。是非一度、デザインシミュレーションを体験してください。</p>
				<div class="btn clear_black w218">
					<a href="http://www.vermicular.jp/products/name/"><span class="btn_inner"><img src="../img/support/btn_section02.png" width="187" height="14" alt="ネーミングサービスの詳細はこちら"></span></a>
				</div>
			</div>
			<p class="photo_wrap fll"><img src="../img/support/section_img03.jpg" height="390" width="580" alt="スペシャルオーダーネーミングサービス イメージ写真"></p>
		</section>

		<div class="rpr_back_to_top">
			<div class="center btn w218 clear_black"><a href="<?php echo $page_pass; ?>products/"><span class="btn_inner"><img src="../img/products/common/back_top_top_btn_text.png" height="10" width="126" alt="Oven Pot Round TOP"></span></a></div>
		</div>

		<?php include $page_pass.'_lowlayer_banner.php'; ?>

<?php include $page_pass.'_footer.php'; ?>