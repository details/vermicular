﻿<?
require_once 'configure.php';
require_once 'util/DetermineLanguageSetting.class.php';

//ブラウザ言語設定判別 [@]2015.08
$determineLangSetting = new DetermineLanguageSetting();
$determineLangSetting->lang = DetermineLanguageSetting::$LANG_JP;

//cookie or request 'jp' 指定がある場合はスルー
if (!$determineLangSetting->isDesignated()
	&& !$determineLangSetting->isExclusion()	//除外でない場合
){
	if($determineLangSetting->isEn()){
		//redirect log
		$logmsg = ''.date('Y/m/d H:i:s')."\t". $_SERVER['HTTP_USER_AGENT'].' ('.$_SERVER["HTTP_X_FORWARDED_FOR"].') : '.$_SERVER['HTTP_ACCEPT_LANGUAGE']."\n";
		$logfile = dirname($_SERVER["DOCUMENT_ROOT"]).'/logs/debug_lang_redirect.log';
		error_log($logmsg, 3, $logfile);

		header("Location: http://".BRAND_DOMAIN_EN);
		exit;
	}
}
?>
<?php
$page_id = 'top'; //ページ独自にスタイルを指定する場合は入力
$page_type = ''; // [ オーブンポッドラウンドのページの場合 「oven_pot_round」 ]
$page_title = '手料理と生きよう。 | Vermicular（バーミキュラ）公式サイト';
$page_description = '世界一、素材本来の味を引き出す鍋を目指して、メイドインジャパンの圧倒的な技術力でつくられた鋳物ホーロー鍋、Vermicular（バーミキュラ）。使えば料理が楽しくなる。手料理をしたくなる。これは、あなたの暮らしや生き方さえをも変えていく画期的な鍋なのです。';
$page_keywords = 'バーミキュラ,Vermicular,ホーロー鍋,愛知ドビー,手料理';
$page_pass = ''; //自分の階層を指定
$page_directry = ''; //自分のディレクトリ名を指定
$extra_css = '
<link rel="stylesheet" type="text/css" href="'.ST_WWW.'/css/index_nr.css">
<link rel="stylesheet" type="text/css" href="'.ST_WWW.'/common/css/styles_bh.css">
'; //何か追加で読み込みたいCSSがあればlinkタグごと記述
$extra_js = '
<script src="'.ST_WWW.'/js/index.js"></script>
'; //何か追加で読み込みたいJSがあればscriptタグごと記述
include '_header.php'; ?>

<link rel="shortcut icon" href="http://static.vermicular.jp/www/img/favicon.ico">



		<section id="pickup" class="section_wrap inner">
			<h1><img src="<?= ST_WWW ?>/img/top/tit_section01.png" width="124" height="48" alt="ピックアップ Pickup"></h1>
			<div class="slider_wrap">
            
<ul id="slider" class="clearfix">


<li class="new"><a href="<?php echo $page_pass; ?>products/ricepot/new_color"><img class="pngfix" src="<?= ST_WWW ?>/img/top/pickup/bnr_pickup05.jpg" width="386" height="180" alt="NEW COLOR DEBUT. VERMICULAR RICEPOT"></a></li>

<li class="new"><a href="<?php echo $page_pass; ?>products/kitchenitems/"><img class="pngfix" src="<?= ST_WWW ?>/img/top/pickup/bnr_pickup06.jpg" width="386" height="180" alt="VERMICULAR KITCHEN ITEMS"></a></li>

<li><a href="<?php echo $page_pass; ?>products/ricepot/" target="_blank"><img src="<?= ST_WWW ?>/img/top/pickup/bnr_ricepot.jpg" width="386" height="180" alt="ricepot"></a></li>

<li class="new"><a href="https://veryweb.jp/trend/goods/13382/" target="_blank"><img src="<?= ST_WWW ?>/img/top/pickup/bnr_very_B.jpg" width="386" height="180" alt="VERY"></a></li>

<li class="new"><a href="http://shop.vermicular.jp/jp/item.php?id=I00000572" target="_blank"><img src="http://static.vermicular.jp/www/img/top/pickup/386_180_oobabook.jpg" width="386" height="180" alt="ooba_recipe_book"></a></li>

<li><a href="<?php echo $page_pass; ?>about/teshigoto/"><img src="<?= ST_WWW ?>/img/top/pickup/bnr_pickup03.jpg" width="386" height="180" alt="究極の無水調理を実現する　Triple ThermoTM Technology"></a></li>
                
<li><a href="<?php echo $page_pass; ?>about/us/story/"><img src="<?= ST_WWW ?>/img/top/pickup/bnr_pickup02.jpg" width="386" height="180" alt="バーミキュラ開発ストーリー"></a></li>                    
               
<li><a href="http://news.kodansha.co.jp/20170915_b01" target="_blank"><img src="<?= ST_WWW ?>/img/top/pickup/bnr_side-2.jpg" width="386" height="180" alt="バーミキュラ開発ストーリー"></a></li>     

                    
				</ul>
			</div>
		</section><!-- /#pick_up -->

		<section id="lineup" class="section_wrap inner">
			<h1><img src="<?= ST_WWW ?>/img/top/tit_section02.png" width="182" height="48" alt="製品ラインナップ Products Lineup"></h1>
			<p class="text">バーミキュラの製品は、素材本来の味を引き出すことを大切にして、手料理を簡単に楽しくします。<br>安心・安全な素材にこわだり、手間暇を惜しまず丁寧に一つひとつ職人が手づくりしています。</p>
			<ul class="clearfix">
				<li id="lineup01">
					<a href="<?php echo $page_pass; ?>products/"><img src="<?= ST_WWW ?>/img/top/img_lineup01.jpg" width="590" height="340" alt="オーブンポットラウンド　イメージ"></a>
					<dl>
						<dt><img src="<?= ST_WWW ?>/img/top/tit_lineup01.png" width="200" height="43" alt="Oven Pot Round　オーブンポットラウンド"></dt>
						<dd>
							「世界一、素材本来の味を引き出す鍋」<br>
							究極の無水調理で素材本来の味を引き出す、メイドインジャパンの鋳物ホーロー鍋です。
						</dd>
					</dl>
				</li>
				<li id="lineup02">
					<a href="<?php echo $page_pass; ?>products/ricepot/"><img src="<?= ST_WWW ?>/img/top/img_lineup02.jpg" width="590" height="340" alt="ライスポット　イメージ"></a>
					<dl>
						<dt><img src="<?= ST_WWW ?>/img/top/tit_lineup02.png" width="106" height="43" alt="Rice Pot　ライスポット"></dt>
						<dd>
							「炊飯器を超えたのは、炊飯鍋でした。」 <br>
							　目指したのは、世界一、おいしいご飯が炊ける炊飯器。バーミキュラ ライスポットです。
						</dd>
					</dl>
				</li>
				<li id="lineup03">
					<a href="<?php echo $page_pass; ?>products/kitchenitems/index.html"><img src="<?= ST_WWW ?>/img/top/img_lineup03.jpg" width="386" height="130" alt="Kitchen Items　イメージ"></a>
					<dl>
						<dt><img src="<?= ST_WWW ?>/img/top/tit_lineup03.png" width="120" height="15" alt="Kitchen Items"></dt>
						<dd>
							ナチュラル＆オーガニックのこだわりキッチンアイテムです。
						</dd>
					</dl>
				</li>
				<li id="lineup04">
					<a href="<?php echo $page_pass; ?>products/food/"><img src="<?= ST_WWW ?>/img/top/img_lineup04.jpg" width="386" height="130" alt="Food　イメージ"></a>
					<dl>
						<dt><img src="<?= ST_WWW ?>/img/top/tit_lineup04.png" width="47" height="15" alt="Kitchen Items"></dt>
						<dd>
							バーミキュラに最適な厳選された食材をご紹介します。
						</dd>
					</dl>
				</li>
				<li id="lineup05">
					<a href="http://shop.vermicular.jp/jp/group.php?id=12"><img src="<?= ST_WWW ?>/img/top/img_lineup05.jpg" width="386" height="130" alt="Books　イメージ"></a>
					<dl>
						<dt><img src="<?= ST_WWW ?>/img/top/tit_lineup05.png" width="55" height="15" alt="Books"></dt>
						<dd>
							バーミキュラを楽しむための専用レシピブックです。
						</dd>
					</dl>
				</li>
			</ul>
		</section><!-- /#line_up -->

		<section id="reason" class="section_wrap inner">
			<h1><img src="<?= ST_WWW ?>/img/top/tit_section03.png" width="152" height="48" alt="選ばれる理由 Reason"></h1>
			<p class="text">バーミキュラは機能性を追求することで、使う人の暮らしをも変えていきます。<br>革新的なテクノロジーと、一生のサポートで毎日の手料理をささえます。</p>
			<ul class="clearfix">
				<li>
					<h2><a href="<?php echo $page_pass; ?>about/whatis/"><img src="<?= ST_WWW ?>/img/top/bnr_reason01.jpg" width="386" height="180" alt="暮らしを変える鍋"></a></h2>
					<ul>
						<li><a href="<?php echo $page_pass; ?>about/whatis/#section01">世界一、素材本来の味を引き出す鍋</a></li>
						<li><a href="<?php echo $page_pass; ?>about/whatis/#section02">料理の楽しさを、この鍋が教えてくれた</a></li>
						<li><a href="<?php echo $page_pass; ?>about/whatis/#section03">うま味も、栄養も、逃がさない鍋</a></li>
					</ul>
				</li>
				<li>
					<h2><a href="<?php echo $page_pass; ?>about/teshigoto/"><img src="<?= ST_WWW ?>/img/top/bnr_reason02.jpg" width="386" height="180" alt="手仕事とテクノロジー"></a></h2>
					<ul>
						<li><a href="<?php echo $page_pass; ?>about/teshigoto/#technology_detail_wrap">トリプルサーモテクノロジー</a></li>
						<li><a href="<?php echo $page_pass; ?>about/teshigoto/#made_in_jp">メイド・イン・ジャパン品質</a></li>
						<li><a href="<?php echo $page_pass; ?>about/teshigoto/#biutiful_wrap">女性でも持ちやすいダブルハンドル</a></li>
						<li><a href="<?php echo $page_pass; ?>about/teshigoto/#section03">カドミウム不使用だから安心・安全</a></li>
					</ul>
				</li>
				<li>
					<h2><a href="<?php echo $page_pass; ?>support/"><img src="<?= ST_WWW ?>/img/top/bnr_reason03.jpg" width="386" height="180" alt="一生サポート"></a></h2>
					<ul>
						<li><a href="<?php echo $page_pass; ?>support/#section01">バーミキュラ オーナーズデスク</a></li>
						<li><a href="<?php echo $page_pass; ?>support/#section02">リペアサービス</a></li>
						<li><a href="<?php echo $page_pass; ?>support/#section03">スペシャルネーミングサービス</a></li>
					</ul>
				</li>
			</ul>
		</section><!-- /#reason -->

		<section id="recipe" class="section_wrap">
			<div id="recipe_wrap">
				<h1><img src="<?= ST_WWW ?>/img/top/txt_recipes.png" width="171" height="45" alt="Recipes"></h1>
                
                <p><br><br>オーナーズマイページは、みんなでバーミキュラのレシピを共有したり、オーナー様専用ショップでお買い物をしたり、<br>全てのバーミキュラオーナー様にバーミキュラをもっと楽しんでいただくためのスペシャルサイトです。<br><br></p>
                
                     <div class="btn clear w198 center">
                
                
                <a href="https://owners.vermicular.jp/"><span class="btn_inner"><img src="<?= ST_WWW ?>/img/top/btn_more_recipe_w.png" width="150" height="25" alt="more"></span></a>
                
				</div>
			</div>
		
		</section><!-- /#recipe -->



<!--
		<section id="support" class="section_wrap inner">
			<h1><img src="<?= ST_WWW ?>/img/top/tit_section04.png" width="89" height="48" alt="サポート Support"></h1>
			<p class="text">ご購入前のご相談・お問い合わせはもちろん、ご購入後の使い方や調理方法など、<br>私たちバーミキュラ コンシェルジュがサポートいたします。</p>
			<img src="<?= ST_WWW ?>/img/top/img_support.jpg" width="1200" height="400" alt="サポート イメージ">
			<ul class="clearfix inner_min">
				<li>
					<dl>
						<dt>コールセンター</dt>
						<dd>
							ご購入前のご相談などのお問い合わせはこちらへどうぞ。
							<div class="contact_text">
								<span class="tel_text">TEL：052-353-5333</span><br>
								<span class="month">（月～金 9:00-12:00、13:00-17:00）</span>
							</div>
						</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>オーナーズデスク</dt>
						<dd>
							使い方のトラブル、調理方法についてなどはこちらへどうぞ。 <a href="<?php echo $page_pass; ?>support/">詳細 ＞</a>
							<div class="contact_text">
								<span><img src="common/img/icon_freedial.png" width="32" height="18" alt="freedial"><span class="tel_text">0120-766-787</span></span><br>
								<span class="month">（月～金 9:00-12:00、13:00-17:00）</span>
							</div>
						</dd>
					</dl>
				</li>
			</ul>
			<div class="btn clear_black w198 center">
				<a href="mailto:infomail@vermicular.jp"><span class="btn_inner"><img src="<?= ST_WWW ?>/img/top/btn_mail_contact.png" width="138" height="14" alt="メールでのお問い合わせ"></span></a>
			</div>
            
 -->           
         
        

                 

		</section>

<!-- /#support -->

		<section id="news" class="section_wrap">
			<h1><img src="<?= ST_WWW ?>/img/top/tit_section05.png" width="95" height="48" alt="ニュース News Release"></h1>
			<nav>
				<ul class="clearfix">
					<li class="active">Media</li>
					<li>News</li>
				</ul>
			</nav>
			<div class="news_wrap inner_min">
				
<div class="active">

<article class="box_link new">
<time>2017.09.20</time>
<p><a href="https://magazineworld.jp/kunel/kunel-87/" target="_blank">2017年09月20日（水）『ku:nel vol.11』（マガジンハウス）にてバーミキュラが紹介されました。</a></p>
</article>

<article class="box_link new">
<time>2017.09.20</time>
<p><a href="https://www.ei-publishing.co.jp/magazines/detail/kurashijouzu-b-445550/" target="_blank">2017年09月20日（水）『Kurashi Vol.1』（枻出版社）にてバーミキュラ ライスポットが紹介されました。</a></p>
</article>

<article class="box_link new">
<time>2017.09.10</time>
<p><a href="https://serai.jp/news/244649" target="_blank">2017年09月10日（日）『サライ 2017年10月号』（小学館）にてバーミキュラ ライスポットが紹介されました。</a></p>
</article>

<article class="box_link new">
<time>2017.09.07</time>
<p><a href="http://www.elle.co.jp/magazine/magazine_mariage/170907?intcmp=kv&src=wedding&list=1" target="_blank">2017年09月07日（木）『エル・ マリアージュ no.31』（ハースト婦人画報社）にてバーミキュラ ライスポットが紹介されました。</a></p>
</article>

<article class="box_link new">
<time>2017.09.07</time>
<p><a href="http://www.ntv.co.jp/every/" target="_blank">2017年09月07日（木）『news every』（日本テレビ）にてバーミキュラ ライスポットが紹介されました。</a></p>
</article>

</div>
                
                
<div>


			<article class="box_link new">
					<time>2016.09.15</time>
					<p><a href="/info/sgp2016/" target="_blank">2016年09月15日（木）
                    「SNS写真投稿キャンペーン」の 当選者を発表しました！</a></p>
					</article> 



					<article class="box_link">
					<time>2016.04.29</time>
					<p><a href="/info/campaign_andpr.html" target="_blank">2016年04月29日（金）
                    「＆ Cooking わたしたちと、手料理」 発売記念 「14cmバーミキュラ プレゼントキャンペーン」の 当選者を発表しました！</a></p>
					</article> 


					<article class="box_link">
					<time>2015.12.01</time>
					<p><a href="http://www.vermicular.jp/info/info_20151201.html" target="_blank">2015年12月01日（火）【お詫び】ご案内メールの複数回送信について</a></p>
					</article>
                    
					<article class="box_link">
						<time>2015.11.19</time>
						<p><a href="http://www.vermicular.jp/">2015年11月19日（木）バーミキュラ ブランドサイトをリニューアルしました。</a></p>
					</article>
					
                   <article class="box_link">
						<time></time>
						<p><a href=""></a></p>
					</article>
                    
 </div></div>
<div class="btn clear_black w198 center">
<a href="<?php echo $page_pass; ?>info/"><span class="btn_inner"><img src="common/img/btn_more_black.png" width="35" height="10" alt="more"></span></a>
</div>
</section><!-- /#news -->

<!--
<div class="btn_bh">
<a href="info/info_bh.html" target="_blank">コールセンター、オーナーズデスク 休業日のお知らせ</a>
 </div>
-->


 		<ul id="footer_banner_wrap" class="inner clearfix">
			<li>
				<a href="<?php echo $page_pass; ?>about/us/story/"><img src="<?= ST_WWW ?>/img/top/bnr_footer01.jpg" width="386" height="130" alt="バーミキュラ開発ストーリー"></a>
			</li>
			<li>
				<a href="<?php echo $page_pass; ?>about/us/"><img src="<?= ST_WWW ?>/img/top/bnr_footer02.jpg" width="386" height="130" alt="バーミキュラ「5つの約束」"></a>
			</li>
			<li>
				<a href="<?php echo $page_pass; ?>company/"><img src="<?= ST_WWW ?>/img/top/bnr_footer03.jpg" width="386" height="130" alt="愛知ドビーの歴史"></a>
			</li>
		</ul>

		<a id="online_btn" href="http://shop.vermicular.jp/jp/" target="_blank"><img src="common/img/btn_online.png" width="130" height="130" alt="メーカー直販 Online Shop"></a>
<?php include '_footer.php'; ?>